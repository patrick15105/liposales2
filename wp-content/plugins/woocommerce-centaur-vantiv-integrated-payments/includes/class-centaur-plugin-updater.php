<?php
/**
 * Plugin Update Checker
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Patient_Management_System
 * @subpackage Patient_Management_System/includes
 * @author     Centaur Development <dev@centaurmarketing.co>
 */

class Centaur_Plugin_Updater {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	public $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Plugin Slug
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string		$slug
	 */
	private $slug;

	/**
	 * Plugin Information
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string		$plugin_data
	 */
	private $plugin_data;

	/**
	 * Plugin File
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string		$plugin_file
	 */
	private $plugin_file;

	/**
	 * Latest Plugin Release
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object		$latest_release
	 */
	private $latest_release;

	/**
	 * Constructor
	 *
	 * @since	1.0.0
	 */
	public function __construct( $plugin_name, $version, $plugin_file ) {
		add_filter( "pre_set_site_transient_update_plugins", array( $this, "set_transient") );
		add_filter( "plugins_api", array( $this, "set_plugininfo"), 10, 3 );
		add_filter( "upgrader_post_install", array( $this, "post_install"), 10, 3 );

		$this->plugin_file 	= $plugin_file;
		$this->plugin_name 	= $plugin_name;
		$this->version 		= $version;

	}

	/**
	 * Get information regarding our plugin from WordPress
	 *
	 * @since 	1.0.0
	 * @access  private
	 */
    private function init_plugindata() {
		$this->slug 		= plugin_basename( $this->plugin_file );
		$this->plugin_data 	= get_plugin_data( $this->plugin_file );
    }

	/**
	 * Get information regarding our plugin from Site
	 *
	 * @since 	1.0.0
	 * @access  private
	 * @return  object 	optional.
	 */
    private function get_releaseinfo() {

    	$url = "https://www.centaurmarketing.co/plugins/wordpress/{$this->plugin_name}/releases.json";

    	if ( ! empty( $this->latest_release ) ) {
    		return;
    	}

    	$releases = wp_remote_retrieve_body( wp_remote_get( $url ) );

   		$releases = json_decode( $releases );

   		if ( ! empty( $releases ) && is_array( $releases) ) {
   			$this->latest_release = $releases[0];
   		}

    }
 
 	/**
	 * Push in plugin version information to get the update notification
	 *
	 * @since 	1.0.0
	 * @access  private
	 * @return  string
	 */
    public function set_transient( $transient ) {

		if ( ! empty( $transient->checked ) ) {

			$this->init_plugindata();
			$this->get_releaseinfo();

			$trigger = version_compare( $this->latest_release->version, $transient->checked[$this->slug] );

			if ( $trigger == 1 ) {

				$package = $this->latest_release->url;

				$obj = new stdClass();
				$obj->slug = $this->slug;
				$obj->new_version = $this->latest_release->version;
				$obj->url = $this->plugin_data["PluginURI"];
				$obj->package = $package;
				$transient->response[$this->slug] = $obj;				

			}

		}

        return $transient;

    }
 
 	/**
	 * Push in plugin version information to display in the details lightbox
	 *
	 * @since 	1.0.0
	 * @access  private
	 * @return  string
	 */
    public function set_plugininfo( $false, $action, $response ) {

		$this->init_plugindata();
		$this->get_releaseinfo();

		if ( empty( $response->slug ) || $response->slug != $this->slug ) {
			return false;
		}

		$response->last_updated = $this->latest_release->published_at;
		$response->slug = $this->slug;
		$response->plugin_name  = $this->plugin_data["Name"];
		$response->version = $this->latest_release->version;
		$response->author = $this->plugin_data["AuthorName"];
		$response->homepage = $this->plugin_data["PluginURI"];
 
		// This is our release download zip file
		$response->download_link = $this->latest_release->url;

        return $response;

    }
 
 	/**
	 * Perform additional actions to successfully install our plugin
	 *
	 * @since 	1.0.0
	 * @access  private
	 * @return  string
	 */
    public function post_install( $true, $hook_extra, $result ) {
		
		global $wp_filesystem;

    	$this->init_plugindata();

    	$was_activated = is_plugin_active( $this->slug );

		$plugin_folder = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . dirname( $this->slug );

		$wp_filesystem->move( $result['destination'], $plugin_folder );

		$result['destination'] = $plugin_folder;

		if ( $was_activated ) {
		    $activate = activate_plugin( $this->slug );
		}

        return $result;

	}    

}