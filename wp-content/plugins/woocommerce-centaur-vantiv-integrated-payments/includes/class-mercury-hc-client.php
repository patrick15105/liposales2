<?php

/**

 * Mercury Payment Systems HostedCheckout PHP Client

 *

 * ©2013 Mercury Payment Systems, LLC - all rights reserved.

 *

 * Disclaimer:

 * This software and all specifications and documentation contained

 * herein or provided to you hereunder (the "Software") are provided

 * free of charge strictly on an "AS IS" basis. No representations or

 * warranties are expressed or implied, including, but not limited to,

 * warranties of suitability, quality, merchantability, or fitness for a

 * particular purpose (irrespective of any course of dealing, custom or

 * usage of trade), and all such warranties are expressly and

 * specifically disclaimed. Mercury Payment Systems shall have no

 * liability or responsibility to you nor any other person or entity

 * with respect to any liability, loss, or damage, including lost

 * profits whether foreseeable or not, or other obligation for any cause

 * whatsoever, caused or alleged to be caused directly or indirectly by

 * the Software. Use of the Software signifies agreement with this

 * disclaimer notice.

 */



class Mercury_HC_Client {



	/**

	 * WorldPay Client

	 *

	 * @since    1.0.0

	 * @access   protected

	 * @var      object 	$wsClient

	 */	

    protected $wsClient;



	/**

	 * WorldPay Client For Transaction

	 *

	 * @since    1.0.0

	 * @access   protected

	 * @var      object 	$wsTxnClient

	 */	

    protected $wsTxnClient;

    

	/**

	 * Merchant ID

	 *

	 * @since    1.0.0

	 * @access   protected

	 * @var      string 	$merchantId

	 */	

    protected $merchantId;



	/**

	 * Merchant Password

	 *

	 * @since    1.0.0

	 * @access   protected

	 * @var      string 	$password

	 */	

    protected $password;



	/**

	 * MercuryPay Link

	 *

	 * @since    1.0.0

	 * @access   protected

	 * @var      string 	$url

	 */	

    protected $url;



	/**

	 * Initialize the class and set its properties.

	 *

	 * @since    1.0.0

	 * @param    string    $merchantID

	 * @param    string    $password

	 * @param    string    $url

	 */



    public function __construct( $merchantId, $password, $url ) {



        $this->merchantId = $merchantId;

        $this->password = $password;



        // Hosted Checkout API WSDL URL

        $this->wsClient 	= new SoapClient( $url . '/hcws/HCService.asmx?WSDL' );

        $this->wsTxnClient 	= new SoapClient( $url . '/tws/transactionservice.asmx?WSDL' );

    }



	/**

	 * Initialize the class and set its properties.

	 *

	 * @since	1.0.0

	 * @param	string    $name

	 * @param	string    $args

	 * @return  object

	 */

    public function __call( $name, $args ) {



        $methodsAvaliable = [

            'InitializeCardInfo',

            'InitializePayment',

            'VerifyCardInfo',

            'VerifyPayment',

            'CreditReturnToken',

            'CreditVoidSaleToken'

        ];



        $pre = substr($name, 0, 4);

        $rest = substr($name, 4);



        if ($pre == 'send' && in_array($rest, $methodsAvaliable)) {



        	$userData = ( $rest == 'CreditReturnToken' || $rest == 'CreditVoidSaleToken' ) ? [ 'MerchantID' => $this->merchantId ]:                 

				[

                    'MerchantID' => $this->merchantId,

                    'Password' => $this->password,

                ];



            $requestData = array_merge(

                $userData,

                $args[0]

            );

            $resp = ( $rest == 'CreditReturnToken' || $rest == 'CreditVoidSaleToken' ) ? $this->wsTxnClient->$rest(['request' => $requestData, 'password' => $this->password]) : $this->wsClient->$rest(['request' => $requestData ]);



//            if ( get_option('debug_level') == "1" ) {

	            error_log($name);

    	        error_log("REQUEST");

				error_log( print_r(['request' => $requestData ], true) );
				error_log( 'passport: ' . $this->password );

            	error_log("RESPONSE");

				error_log( print_r($resp->{$rest.'Result'}, true) );

//            }



            return $resp->{$rest.'Result'};



        }



        throw new Exception('Error, the method ' . $rest . ' does not exist');



    }



	/**

	 * Get Types

	 *

	 * @since	1.0.0

	 * @return  object

	 */

    public function getTypes() {

        return $this->wsClient->__getTypes();

    }



}