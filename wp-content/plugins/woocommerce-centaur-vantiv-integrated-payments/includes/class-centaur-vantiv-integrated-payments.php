<?php

/**

 * Vantiv Integrated Payments Class

 *

 *

 * @package    Centaur

 * @subpackage Centaur/includes

 * @author     Centaur Marketing <dev@centaurmarketing.co>

 */

class Centaur_Vantiv_Integrated_Payments extends WC_Payment_Gateway {



	/**

	 * The ID of this plugin.

	 *

	 * @since    1.0.0

	 * @access   private

	 * @var      string    $plugin_name    The ID of this plugin.

	 */

	private $plugin_name;



	/**

	 * The version of this plugin.

	 *

	 * @since    1.0.0

	 * @access   private

	 * @var      string    $version    The current version of this plugin.

	 */

	private $version;



	/**

	 * MercuryPay Client

	 *

	 * @since    1.0.0

	 * @access   private

	 * @var      object    $client

	 */

	private $client;



	private $local_variables = array();



	/**

	 * Initialize the class and set its properties.

	 *

	 * @since    1.0.0

	 * @param      string    $plugin_name       The name of this plugin.

	 * @param      string    $version    The version of this plugin.

	 */

	public function __construct( $plugin_name = "woocommerce-centaur-vantiv-integrated-payments", $version = "1.0.0" ) {



		global $woocommerce;



		$this->id 					= __( 'woocommerce_centaur_vantiv_integrated_payments', $plugin_name );

		$this->plugin_name 			= $plugin_name;

		$this->version 				= $version;

		$this->method_title			= __( 'Vantiv for Credit Cards', $plugin_name );

		$this->method_description 	= __( 'Process WooCommerce Payments with the Vantiv Gateway for Credit Cards.', $plugin_name );



		$this->init_form_fields();

		$this->init_settings();



		// Form Fields

		foreach( $this->form_fields as $key => $field ) {

			$this->$key = $this->get_option( $key );

		}



		// Define Non-Form Variables

		$this->domain				= ( $this->test_mode == 'yes' ) ? "https://hc.mercurycert.net" : "https://hc.mercurypay.com";

		$this->has_fields 			= false;

		$this->mid 					= ( $this->test_mode == 'yes' ) ? $this->get_option( 'test_merchant_id' ) : $this->get_option( 'merchant_id' );

		$this->mpass 				= ( $this->test_mode == 'yes' ) ? $this->get_option( 'test_merchant_pass' ) : $this->get_option( 'merchant_pass' );

		$this->mode 				= ( $this->mode == "mobile_alt" ) ? ( ( wp_is_mobile() ) ? "hosted" : "iframe" ) : $this->mode;

		$this->operator				= ( $this->test_mode == 'yes' ) ? "CENTTEST" : substr($this->get_domain(), 0 , 9);

		$this->return_url			= add_query_arg( 'wc-api', $this->id."_api", home_url( '/index.php', ( ( is_ssl() ) ? "https" : "http"	 ) ) );

		$this->return_url_hosted	= add_query_arg( 'wc-api', $this->id."_hosted", home_url( '/index.php', ( ( is_ssl() ) ? "https" : "http" ) ) );

		$this->return_url_iframe	= add_query_arg( 'wc-api', $this->id."_iframe", home_url( '/index.php', ( ( is_ssl() ) ? "https" : "http" ) ) );

		$this->supports				= array(

										'products',

										'refunds'

										);



		$this->local_variables['wc_api_url'] = $this->return_url;



		// Setup MercuryPay Client

		$this->client 				= ( $this->mid != '' && $this->mpass != '' ) ? new Mercury_HC_Client( $this->mid, $this->mpass, $this->domain ) : "" ;



		add_action( 'woocommerce_update_options_payment_gateways_'. $this->id, array( $this, 'process_admin_options' ) );

		add_action( 'woocommerce_receipt_'. $this->id, array( $this, 'payment_receipt_page' ) );

		add_action( 'initialize_payment_'.$this->id, array( $this, 'initialize_payment' ) );

		add_action( 'woocommerce_api_'. $this->id.'_iframe', array( $this, 'payment_wc_api_router' ) );

		add_action( 'woocommerce_api_'. $this->id.'_hosted', array( $this, 'payment_wc_api_router' ) );

		add_action( 'woocommerce_api_'. $this->id.'_api', array( $this, 'payment_wc_api_router' ) );

		add_action( 'woocommerce_thankyou', array($this, 'custom_order_completed') );



	}



	/**

	 * Initialize Form Fields

	 *

	 * @since    1.0.0

	 */

	public function init_form_fields() {



		$this->form_fields = apply_filters( 'wc_vip_form_fields', array(

				'enabled' => array(

					'title'       => __( 'Enable/Disable', $this->plugin_name ),

					'label'       => __( 'Enable Vantiv Integrated Payments Gateway', $this->plugin_name ),

					'type'        => 'checkbox',

					'default'     => 'no'

				),

				'title' => array(

					'title'       => __( 'Title', $this->plugin_name ),

					'type'        => 'text',

					'description' => __( 'This controls the title which the user sees during checkout.', $this->plugin_name ),

					'default'     => __( 'Vantiv Integrated Payment', $this->plugin_name ),

					'desc_tip'    => true,

				),

				'description' => array(

					'title'       => __( 'Description', $this->plugin_name ),

					'type'        => 'textarea',

					'description' => __( 'This controls the description which the user sees during checkout.', $this->plugin_name ),

					'default'     => 'Pay with your credit card via Vantiv Payment Gateway.',

				),

				'test_mode' => array(

					'title'       => __( 'Test mode', $this->plugin_name ),

					'label'       => __( 'Enable Test Mode', $this->plugin_name ),

					'type'        => 'checkbox',

					'description' => __( 'Place the payment gateway in test mode using test API keys.', $this->plugin_name ),

					'default'     => 'yes',

					'desc_tip'    => true,

				),

				'debug_level' => array(

					'title' => __( 'Debug Level', $this->plugin_name ),

					'type' => 'select',

					'description' => __('Enables and sets the Debug Level', $this->plugin_name ),

					'options' => array(

						'0'=>'0: No information logged',

						'1'=>'1: Information logged'

					),

					'default' => __('0',  $this->plugin_name ),

				),

				'merchant_id' => array(

					'title'       => __( 'Merchant ID', $this->plugin_name ),

					'description' => __( "The Merchant's Account ID that payments will be processed under.", $this->plugin_name ),

					'type'        => 'text'

				),

				'merchant_pass' => array(

					'title'       => __( 'Password', $this->plugin_name ),

					'description' => __( 'The web services password generated by merchant or dealer/developer on the MercuryView portal to allow access to the web service.', $this->plugin_name ),

					'type'        => 'text'

				),

				'test_merchant_id' => array(

					'title'       => __( 'Test Merchant ID', $this->plugin_name ),

					'description' => __( "The Merchant's Account ID that payments will be processed under.", $this->plugin_name ),

					'type'        => 'text'

				),

				'test_merchant_pass' => array(

					'title'       => __( 'Test Password', $this->plugin_name ),

					'description' => __( 'The web services password generated by merchant or dealer/developer on the MercuryView portal to allow access to the web service.', $this->plugin_name ),

					'type'        => 'text'

				),

				'mode' => array(

					'title' => __('Mode', $this->plugin_name ),

					'type' => 'select',

					'description' => __('Decide where your customers enter their payment information. 

						<ul>

							<li>

								If you choose "Hosted on Vantiv" all customer traffic will be directed to the secure Hosted Checkout payment page hosted by Vantiv. After submitting 	credit card data at Vantiv, customers are returned to your site where the order results are displayed. 

							</li>

							<li>

								If you choose "iframe on your site" the plugin will securely load the Vantiv page seamlessly into your site. Users never leave your website.  We strongly suggest running this mode with an SSL certificate in place on your site so that the whole browser is running on a secure connection.  Your customers data is secure if you choose not to install an SSL certificate on your site because the iframe loads securely, but your customers won\'t see the 	indicator by their web browser that the page is loaded securely. This can be confusing or worrying for customers. 

							</li>

							<li>	

								The third option, "*iframe first, mobile users fallback to hosted mode" provides a solution for responsive websites that ensures the best user experience for mobile and desktop users.  Vantiv cannot at present scale their payments screen for mobile devices on a responsive website, although they have agreed to strongly consider this feature in their next product release. In the interim, this open prevents mobile users from seeing cut off credit 	card entry screens.  If your website is responsive and / or your customer base includes smartphone or other users, you should strongly consider this 	mode instead of the "iframe on your site" to ensure maximum compatibility. 

							</li>

						</ul>', $this->plugin_name ),

					'options' => array(

						'hosted'=>'1: Hosted on Vantiv',

						'iframe'=>'2: iframe on your site',

						'mobile_alt'=>'3: * iframe first, mobile users fallback to hosted mode'

					),

					'default' => __('mobile_alt',  $this->plugin_name ),

				),

				'iframe_description' => array(

					'title' => __('Additional Description (iframe)', $this->plugin_name ),

					'type' => 'textarea',

					'description' => __('Additional description for iframe mode.', $this->plugin_name ),

					'default' => __('You will enter your payment information securely on the next page.',  $this->plugin_name ),

					'class' => 'iframe_only',

				),

				'hosted_description' => array(

					'title' => __('Additional Description (Hosted)', $this->plugin_name ),

					'type' => 'textarea',

					'description' => __('Additional description for hosted mode.', $this->plugin_name ),

					'default' => __('You will be redirected to Vantiv\'s secure page to enter your payment information.',  $this->plugin_name ),

					'class' => '',

				),

				'AVSFields' => array(

					'title' => __('AVS Fields (Address Data Verification)', $this->plugin_name ),

					'type' => 'select',

					'description' => __('Allow for AVS Fields on payment page.', $this->plugin_name ),

					'options' => array(

						'Off'=>'1: None',

						'Zip'=>'2: Postal Code',

						'Both'=>'3: Street Address & Postal Code'

					),

					'default' => __('Off',  $this->plugin_name ),

				),

				'AVS_fail' => array(

					'title' => __('If AVS data verification fails', $this->plugin_name ),

					'type' => 'select',

					'description' => __('What should happen when the AVS data fails.', $this->plugin_name ),

					'options' => array(

						'warn'=>'1: Warn: Post a note in the order.',

						'hold'=>'2: On Hold: Post a note, and place the order on-hold.',

						'reject'=>'3: Reject the payment, void the transaction and direct the user to try again.',

					),

					'default' => __('warn',  $this->plugin_name ),

				),

				'cvv' => array(

					'title' => __( 'CVV', 'vantiv_woocommerce_gateway' ),

					'label' => __( 'Require customer to enter credit card CVV code', 'vantiv_woocommerce_gateway' ),

					'type' => 'checkbox',

					'label' => __('Enable CVV code security'),

					'default' => 'no',

				),

				'cvv_fail' => array(

					'title' => __('If CVV data verification fails', $this->plugin_name ),

					'type' => 'select',

					'description' => __('What should happen when the CVV data fails.', $this->plugin_name ),

					'options' => array(

						'warn'=>'1: Warn: Post a note in the order.',

						'hold'=>'2: On Hold: Post a note, and place the order on-hold.',

						'reject'=>'3: Reject the payment, void the transaction and direct the user to try again.',

					),

					'default' => __('warn',  $this->plugin_name ),

				),

				'notify_recipients' => array(
					'title'       => __( 'Recipient/s', $this->plugin_name ),
					'description' => __( 'Enter Email Addresses for Payment Status Notifications. Use comma to separate recipients.', $this->plugin_name ),
					'type'        => 'text'
				),

				'notify_subject' => array(
					'title'       => __( 'Subject', $this->plugin_name ),
					'description' => __( 'Available placeholders: {site_title}, {order_status}, {order_date}, {order_number}', $this->plugin_name ),
					'type'        => 'text',
					'default'	  => '[{site_title}]: {order_status} order#{order_number}'
				),

				'notify_heading' => array(
					'title'       => __( 'Email heading', $this->plugin_name ),
					'description' => __( 'Available placeholders: {site_title}, {order_status}, {order_date}, {order_number}', $this->plugin_name ),
					'type'        => 'text',
					'default'	  => '{order_status} Order: #{order_number}'
				),

			)

		);



	}



	/**

	 * Override Payment Fields

	 *

	 * @since	1.0.0

	 */

	public function payment_fields() {



		$output_html = ( $this->test_mode == 'yes' ) ? "<h4>TEST MODE</h4>" : "" ;

		$output_html .= $this->description . "<br/>";



		if ($this->mode == "iframe" ) {

			$output_html .= $this->iframe_description;

		} else {

			$output_html .= $this->hosted_description;

		}



		echo $output_html;



	}



	/**

	 * Process the payment and return the results

	 *

	 * @since 	1.0.0

	 * @param	int		$order_id

	 * @return	array

	 */

	public function process_payment( $order_id ) {



		global $woocommerce;



		// Setup variables

		$order 		= new WC_Order($order_id);

		$response	= $this->initialize_payment($order_id);



		// The text for the note

		// if ( get_option('debug_level') == "1" ) {

			$note = __( "Vantiv Initialize Payment Response " . json_encode( $response ), $this->plugin_name );

			$order->add_order_note( $note );

		// }



		// Add the note

		$order->add_order_note( __("VANTIV PAYMENT: " . $response->message, $this->plugin_name ) );



 		// Save the data

		$order->save();



		if ( $response->success ) {

			return array(

				"result" 	=> "success",

				"redirect"	=> $order->get_checkout_payment_url(true)

				);

		} else {



			wc_add_notice( __( "WC Init Payment Error: " . $response->message, $this->plugin_name ) );

			return;



		}



	}



	/**

	 * Payment Receipt Page

	 *

	 * @since 	1.0.0

	 * @param   int		$order_id

	 */

	public function payment_receipt_page( $order_id ) {



		global $woocommerce;



		$this->enqueue_scripts_styles();



		$order 				= new WC_Order( $order_id );

		$with_payment_data 	= get_post_meta( $order_id, "payment_data" );



		if ( !$with_payment_data ) {



			wp_redirect( wc_get_endpoint_url("checkout") );

			exit;



		} else {



			$payment_data 	= json_decode( end($with_payment_data) );

			$payment_id 	= $payment_data->pid;



			$this->local_variables['PID'] = $payment_id;



			wp_localize_script('vantiv_woocommerce_gateway_js', $this->id.'_addon_vars', $this->local_variables);



			if ( $this->mode == "iframe" ) {



				echo "

					<div style= 'position:relative;padding-top:35px;width:100%;padding-bottom:100%;height:0px;overflow:hidden;' id='vantiv_iframe_wrap'>

						<iframe id='vantiv_iframe' src='" . $this->domain . "/CheckoutiFrame.aspx?ReturnMethod=get&pid={$payment_id}' scrolling='none' frameborder='0' style='zoom:.64!important;position:absolute;left:0px;top:0px;width:100%;height:100%;overflow:hidden;padding:0px;' onload='this.style.visibility=\"visible\";'> Your browser does not support iFrames. To view this content, please download and use the latest version of one of the following browsers: Internet Explorer, Firefox, Google Chrome or Safari.

						</iframe>

					</div>



					<div id='please_wait' style='display:none;'><h1>Please Wait</h1> <h2>Your order is processing. The page will refresh when your payment is completed. Closing this window or hitting the back button will not cancel your order.</h2><h3>Order Number: {$order_id}</h3></div>			

				";



			} else {



				echo "

					<form name='frmCheckout' id='frmCheckout' method='Post' action='" . $this->domain . "/Checkout.aspx' >

						<input type='hidden' name='PaymentID' id='PaymentID' value='{$payment_id}' />

						<input type='hidden' name='ReturnMethod' id='ReturnMethod' value='GET' />

						<h3>Redirecting to Vantiv</h3>

						<p><a href='#' onclick='document.getElementById(\"frmCheckout\").submit();'>Click Here</a> if you are not redirected automatically.</p>

					</form>

				

					<script>document.getElementById(\"frmCheckout\").submit();</script>			

				";



			}



		}



	}



	/**

	 * Enqueue Scripts and Styles

	 *

	 * @since	1.0.0

	 * @param	boolean		$is_admin

	 */

	private function enqueue_scripts_styles( $is_admin = false ) {



		wp_enqueue_script( 'jquery' );



		if ( $is_admin ) {



			// wp_register_script('jscolor', plugins_url('',__FILE__).'/../jscolor/jscolor.js');

			// wp_enqueue_script('jscolor');



			wp_register_script(

				'vantiv_woocommerce_gateway_admin_js', 

				plugins_url() . '/' . $this->plugin_name .'/assets/js/vantiv_woocommerce_gateway_admin.js'

			);



			wp_enqueue_script('vantiv_woocommerce_gateway_admin_js');



		} else {



			wp_register_script(

				'vantiv_woocommerce_gateway_js', 

				plugins_url() . '/' . $this->plugin_name .'/assets/js/vantiv_woocommerce_gateway.js'

			);



			wp_enqueue_script('vantiv_woocommerce_gateway_js');



		}

	

	}



	/**

	 * Initialize Payment

	 *

	 * @since 	1.0.0

	 * @param	int		$order_id

	 * @return 	object

	 */

	public function initialize_payment( $order_id ) {



		global $woocommerce;



		$order 	= new WC_Order( $order_id );



		$amount = round( $order->get_total(), 2 );

		$tax 	= round( $order->get_total_tax(), 2 );



		$with_payment_data = get_post_meta( $order_id, "payment_data");



		if ( !$with_payment_data ) {



			// Setup Arguments

			$args = array(

				"OperatorID"			=> $this->operator,

				'LaneID' 				=> "WEB",

				'TranType'				=> "Sale",

				'TotalAmount'			=> $amount,

				'Frequency'				=> "OneTime",

				'Invoice' 				=> $order->get_id(),

				'Memo'					=> "WCVIP " . $this->version,

				'TaxAmount'				=> $tax,

				'CardHolderName'		=> $order->get_billing_first_name() . " " . $order->get_billing_last_name(),



				'AVSAddress' 			=> ( $this->AVSFields == "Both" ) ? $order->get_billing_address_1() : "" ,

				'AVSZip' 				=> ( $this->AVSFields == "Zip" ) ? $order->get_billing_postcode() : "" ,

				'CVV'					=> ( $this->cvv == "yes" ) ? "on" : "off",

				'ProcessCompleteUrl'	=> ( $this->mode == "iframe" ) ? $this->return_url_iframe : $this->return_url_hosted,

				'ReturnUrl'				=> ( $this->mode == "iframe" ) ? $this->return_url_iframe : $this->return_url_hosted,

				'PageTimeoutDuration'	=> 5,

				'CancelButton'			=> 'on'

			);



			$other_options = array(

				"AVSFields"

				// "DisplayStyle",

				// "FontColor",

				// "FontFamily",

				// "FontSize",

				// "LogoUrl",

				// "PageTitle",

				// "TotalAmountBackgroundColor",

				// "SubmitButtonText",

				// "CancelButtonText",

				// "ButtonTextColor",

				// "ButtonBackgroundColor",

				// "JCB",

				// "Diners"

			);

		

			foreach( $other_options as $option ) {

				$args[$option] = $this->{$option};

			}		

		

			$response 		= $this->client->sendInitializePayment( $args );



			$payment_data 	= array(

				"pid"		=> $response->PaymentID,

				"order_id"	=> $order_id,

				"success"	=> ( ( $response->ResponseCode != "0" ) ? false : true),

				"message"	=> $response->Message

				);



			if ( $response->ResponseCode == "0" ) {

				add_post_meta( $order_id, 'payment_data', json_encode( $payment_data ) );

			}



			return ( (object) $payment_data );



		} else {



			// Get Last Payment Data

			$payment_data = json_decode( end($with_payment_data) );

			return ( (object) $payment_data );



		}



	}



	/**

	 * Verify Payment

	 *

	 * @since 	1.0.0

	 * @access 	private

	 * @param	int		$order_id

	 * @return 	object

	 */

	private function verify_payment( $order_id, $payment_id ) {



		global $woocommerce;



		$orders = get_posts(

			array(

				'post_type'		=> 'shop_order',

				'post_status'	=> array_keys( wc_get_order_statuses() ),

				'meta_query'	=> array(

					array(

						'key'		=> 'payment_data',

						'value'		=> $payment_id,

						'compare'	=> 'LIKE'

					)

				)

			)

		);



		$message = ( count($orders) == 0 || $orders[0]->ID != $order_id ) ? "Order not found." : "";



		if ( count($orders) > 0 && $orders[0]->ID == $order_id ) {



			$order = new WC_Order($order_id);



			$args = array( "PaymentID" => $payment_id );



			$on_hold 	= false;

			$user_id 	= get_current_user_id();



			$response 	= $this->client->sendVerifyPayment( $args );



			// The text for the note

//			if ( get_option('debug_level') == "1" ) {

				$note = __( "Vantiv Verify Payment Response " . json_encode( $response ), $this->plugin_name );

				$order->add_order_note( $note ); // Add the note

//			}



			if ( $response->Status == "Approved" ) {



				// Check if CVV is enabled and if there is a matched error

		        if( $this->cvv == "yes" && $response->CvvResult != "M" ) {

					$order->add_order_note( __('Vantiv Payment Error: CVV mismatch', $this->plugin_name ) );



					if( $this->cvv_fail == "hold" ) {

						$on_hold = true;

					} elseif ( $this->cvv_fail == "reject" ) {

						return $this->process_payment_failure( $order_id, $payment_id, $response, "security" );

					}

				}



				// Check if AVS is enabled and if there is a matched error

				if( $this->AVSFields != "Off" && $response->AvsResult != "Y" ) {

					$order->add_order_note( __('!!! Vantiv Responded with AVS Mismatch !!!', 'vantiv_woocommerce_gateway') );



					if ( $this->AVS_fail == "hold" ) {

						$on_hold = true;

					} elseif ( $this->AVS_fail == "reject" ) {

						return $this->process_payment_failure( $order_id, $payment_id, $response, "security" );

					}

				}			



				foreach ( $response as $key => $value ) {

					if ( !empty( $value ) ) {

						update_post_meta( $order_id, $key, $value );

					}

				}

				

				$token = $response->Token;



				$woocommerce->cart->empty_cart();



				if ( $on_hold ) {

					$order->update_status('on-hold', __( 'Order placed on hold due to AVS or CVV mismatch.', $this->plugin_name ) );

				} else {

					$order->payment_complete($token);

				}



		    	update_post_meta( $order_id, 'vip_token', $token );

		    	add_user_meta( $user_id, 'vip_token', $token );



				$result = array(

					"success" 	=> true,

					"redirect"	=> $order->get_checkout_order_received_url($order_id)

				);

				

				



			} else {

				$result = array(

					"success" 	=> false,

					"redirect"	=> wc_get_checkout_url(),

					"message"	=> $response->DisplayMessage

				);



				wc_add_notice( __('Vantiv Payment Notice : ', 'woothemes') . $response->DisplayMessage, 'error' );



			}





		} else {



			wc_add_notice( __('WC Payment error: ', 'woothemes') . $response->DisplayMessage, 'error' );





			return ( (object) array( 'success' => false, 'message' => $response->DisplayMessage) );



		}



		return ( (object) $result );



	}



	/**

	 * Process Order Refund

	 *

	 * @since	1.0.0

	 */

	public function process_refund( $order_id, $amount = null, $reason = "" ) {



		global $woocommerce;



		$order 	= new WC_Order( $order_id );

		$token 	= get_post_meta( $order_id, "vip_token", true );

		$args 	= array(

			"Frequency"			=> "OneTime",

			"Invoice"			=> $order_id,

			"Memo"				=> "WCVIP " . $this->version,

			"PurchaseAmount"	=> round( $amount, 2 ),

			"OperatorID"		=> $this->operator,

			"Token"				=> $token

		);



		$response = $this->client->sendCreditReturnToken( $args );



		if ( $response->Status == "Approved" ) {

			$this->send_notification( $order_id, 'Refunded' );

			return true;

		}



		return false;



	}



	/**

	 * Process Payment Verification Failure

	 *

	 * @since	1.0.0

	 * 

	 * @access 	private

	 * @return	object

	 */

	private function process_payment_failure( $order_id, $payment_id, $response, $type = "" ) {



		global $woocommerce;

		$order = new WC_Order($order_id);



		// Security Failure

		if ( $type == "security" ) {



			$args 	= array(

				"AuthCode"			=> $response->AuthCode,

				"Frequency"			=> $this->frequency,

				"Invoice"			=> $order_id,

				"Memo"				=> $response->Memo,

				"PurchaseAmount"	=> $response->Amount,

				"RefNo"				=> $response->RefNo,

				"Token"				=> $response->Token

			);



			$response 	= $this->client->sendCreditVoidSaleToken( $args );

			$message 	= $response->StatusMessage.": ".$response->DisplayMessage;



			$results 	= array(

				"success" 	=> false,

				"redirect"	=> wc_get_checkout_url()

			);



		} elseif ( $response->Status == "Declined" || $response->PaymentIDExpired ) {



			$message = $response->StatusMessage.": ".$response->DisplayMessage;

			$order->add_order_note( __( ( $response->Status == "Declined" ) ? 'Vantiv declined the payment.' : ( ( $response->PaymentIDExpired ) ? 'Vantiv responded PaymentID has expired.' : "" ) ), $this->plugin_name );



		} else {



			switch($_REQUEST['ReturnCode']){

				case '102'://UserCancelled

					$message = "Payment Cancelled, please try again.";

					break;

				case '103'://SessionTimeout	

					$message = "Session Timeout, please try again.";

					break;

				case '105'://PageTimeout

					$message = "Page Timeout, please try again.";

					break;

				default:

					//if the api callback was triggered before the payment successfully processed.

					$message = "Payment Error, please try again.";

					break;

			}



			$order->add_order_note( __($message, $this->plugin_name ) );

		}



		delete_post_meta( $order_id, 'payment_data' );

		wc_add_notice( __( ( $type == "security ") ? 'AVS or CVV rejection: ' :  'Payment error: ', $this->plugin_name ) . $message, 'error' );



		if ( $type == "security" ) {

			return ( (object) $results );

		}



	}





	/**

	 * Callback Function

	 *

	 * @since 	1.0.0

	 * @access 	private

	 */

	public function vantiv_payment_callback() {



		global $woocommerce;



		$payment_id = $_REQUEST["PaymentID"];

		$orders = get_posts(

			array(

				'post_type'		=> 'shop_order',

				'post_status'	=> array_keys( wc_get_order_statuses() ),

				'meta_query'	=> array(

					array(

						'key'		=> 'payment_data',

						'value'		=> $payment_id,

						'compare'	=> 'LIKE'

					)

				)

			)

		);



		$order 		= new WC_Order($orders[0]->ID);

		$order_id 	= $orders[0]->ID;



		if ( $this->mode == "iframe" ) {

			echo "

					<h1>Thank you for your order!</h1>

					<p>Please wait while we process your order. Once your payment is complete, the page will refresh.</p>

					<p><small>Order Number: {$order_id}</small></p>

					<script data-cfasync='false' type='text/javascript'>

						function interval (func, wait, times) {

							var interv = function (w, t) {

						        return function() {

						            if(typeof t === 'undefined' || t-- > 0) {

    						            setTimeout(interv, w);

					    	            try {

					        	            func.call(null);

					            	    } catch(e){

						                    t = 0;

						                    throw e.toString();

						                }

						            }

						        };

						    } (wait, times);

					    	setTimeout(interv, wait);

						};

						interval ( function() {

							window.parent.ajax_vantiv_check_payment_status('{$payment_id}','{$order_id}');

						}, 10000, 1 );

			    	</script>";



			die();



		} else {



			$response 	= $this->verify_payment( $order_id, $payment_id );

			wp_redirect($response->redirect);



			exit;

			die();



		}





	}



	/**

	 * WooCommerce API Router

	 *

	 * @since	1.0.0

	 */

	public function payment_wc_api_router() {



		error_log("Payment WC API Router");



		if ( isset($_REQUEST["wc-api"]) && ( $_REQUEST['wc-api'] == "woocommerce_centaur_vantiv_integrated_payments_hosted" || $_REQUEST['wc-api'] == "woocommerce_centaur_vantiv_integrated_payments_iframe" ) ) {

			$this->vantiv_payment_callback();

			return;



		} else {



			$result = array();

			$request = $_REQUEST;



			error_log("Action: " . $request['action']);

		

			if ( !isset($request['action']) ) {

				die();

			}



			$action 	= $request['action'];

			$order_id 	= ( isset($request['order_id']) ) ? $request['order_id'] : "";

			$payment_id	= ( isset($request['PaymentID']) ) ? $request['PaymentID'] : "";

		

			//this is called for iframe ajax verify

			if( $action=='verify_payment' ) {

				$return = $this->verify_payment( $order_id, $payment_id );

			}

		

			echo json_encode($return,true);

		

			die();			



		}



	}



	/**

	 * Retrieve Domain name of the site

	 *

	 * @since	1.0.0

	 * @access 	private

	 * @return	string

	 */

	

	private function get_domain() {



		$domain = explode( ".", $_SERVER["SERVER_NAME"] );



		return ( count($domain) == 1 ) ? $domain[0] : $domain[1];



	}





	/**

	 * For Order Completion

	 *

	 * @since	1.0.0

	 * @access 	public

	 * @return	string

	 */

	public function custom_order_completed( $order_id ) {



	    if ( ! $order_id ) {

    	    return;

    	}



	    $order = wc_get_order( $order_id );



	    if ( $order->has_status('processing') ) {



	    	$order->update_status( 'completed' );



	    }



	}

	/**
	 * Send Email Notification 
	 *
	 * @since	1.2.0
	 * @param	integer		$order_id
	 * @param	string		$status
	 *
	 * @return	boolean
	 */
	private function send_notification( $order_id, $status ) {

	    $order = new WC_Order( $order_id );

		remove_filter( "wp_mail_content_type", array( $this, "set_html_content_type") );
		add_filter( "wp_mail_content_type", array( $this, "set_html_content_type") );

		// Process email recipients
		$recipients = explode( ' ', $this->notify_recipients );

		// Process Subject
		$subject = $this->process_placeholders( $this->notify_subject, '{site_title}', $order_id );

		$subject = $this->process_placeholders( (( $subject != '' ) ? $subject : $this->notify_subject ), '{order_status}', $order_id, $status );
		$subject = $this->process_placeholders( (( $subject != '' ) ? $subject : $this->notify_subject ), '{order_date}', $order_id );
		$subject = $this->process_placeholders( (( $subject != '' ) ? $subject : $this->notify_subject ), '{order_number}', $order_id );


		// Process Message
		$message = 
			'<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
				<tbody>
					<tr>
						<td align="center" valign="top">
							<div id="m_552754212364383419template_header_image">
								<p style="margin-top:0"><img src="https://ci5.googleusercontent.com/proxy/UasqbwRe2POFZ1mguhTQk74h5XZymyMF7JH9l_injTUy42hUbpxxFDfo-q6TcLvWGR_SzABjSAkdsA-1PqY8hDhYHxrSfpE-mjVAEkFjdkCq=s0-d-e1-ft#https://www.liposales.com/wp-content/uploads/2018/08/logo2.png" alt="Liposales" style="border:none;display:inline-block;font-size:14px;font-weight:bold;height:auto;outline:none;text-decoration:none;text-transform:capitalize;vertical-align:middle;margin-right:10px" class="CToWUd"></p>
							</div>
							<span class="HOEnZb"><font color="#888888"></font></span>
							<table border="0" cellpadding="0" cellspacing="0" width="600" id="m_552754212364383419template_container" style="background-color:#ffffff;border:1px solid #dedede;border-radius:3px">
								<tbody>
									<tr>
										<td align="center" valign="top">
											<table border="0" cellpadding="0" cellspacing="0" width="600" id="m_552754212364383419template_header" style="background-color:#056cb3;color:#ffffff;border-bottom:0;font-weight:bold;line-height:100%;vertical-align:middle;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;border-radius:3px 3px 0 0">
												<tbody>
													<tr>
														<td id="m_552754212364383419header_wrapper" style="padding:36px 48px;display:block">
															<h1 style="color:#ffffff;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:30px;font-weight:300;line-height:150%;margin:0;text-align:left">Thanks for shopping with us</h1>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td align="center" valign="top">
											<span class="HOEnZb"><font color="#888888"></font></span>
											<table border="0" cellpadding="0" cellspacing="0" width="600" id="m_552754212364383419template_body">
												<tbody>
													<tr>
														<td valign="top" id="m_552754212364383419body_content" style="background-color:#ffffff">
															<span class="HOEnZb"><font color="#888888"></font></span>
															<table border="0" cellpadding="20" cellspacing="0" width="100%">
																<tbody>
																	<tr>
																		<td valign="top" style="padding:48px 48px 0">
																			<div id="m_552754212364383419body_content_inner" style="color:#636363;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:14px;line-height:150%;text-align:left">

																				<p style="margin:0 0 16px">Hi,</p>
																				<p style="margin:0 0 16px">The status of the Order # ' . $order_id . ' has been changed to "' . $status . '"</p>
																			</div>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>';

		// Set Header
		$headers = "From: " . strip_tags( get_bloginfo('admin_email') ) . "\r\n";

		// Send Email
		$result = wp_mail( $recipients, $subject, $message, $headers );

		return $result;

	}
	
	/**
	 * Process Placeholders
	 *
	 * @since 	1.2.0
	 * @param 	string 		$haystack
	 * @param 	string 		$needle
	 * @param 	integer 	$order_id
	 * @return  string
	 */
	private function process_placeholders( $haystack, $needle, $order_id, $status = '' ) {

		if ( !$order_id ) {
			return;
		}

	    $order = new WC_Order( $order_id );

		if ( strpos( $haystack, $needle ) !== false ) {

			switch ( $needle ) {
				case '{site_title}': 
					return str_replace( '{site_title}', get_bloginfo( 'name' ), $haystack );
				break;
				case '{order_status}': 
					return str_replace( '{order_status}', ucwords( $status ), $haystack );
				break;
				case '{order_date}': 
					return str_replace( '{order_date}', $order->order_date, $haystack );
				break;
				case '{order_number}': 
					return str_replace( '{order_number}', $order_id, $haystack );
				break;
			}
			
		}

		return;

	}

	/**
	 * Set HTML Content type for mail 
	 *
	 * @since	1.2.0
	 * @return	string
	 */
	public function set_html_content_type() {
		return "text/html";
	}

}