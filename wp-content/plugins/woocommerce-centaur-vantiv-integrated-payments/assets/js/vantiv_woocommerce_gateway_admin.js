var $j = jQuery;
var page_style = ''
var hosted_mode = '';
$j(document).ready(function(){
	$j(document).off("change","#woocommerce_woocommerce_centaur_vantiv_integrated_payments_hosted_mode").on("change","#woocommerce_woocommerce_centaur_vantiv_integrated_payments_hosted_mode",restrict_admin_fields);	
	$j(document).off("change","#woocommerce_woocommerce_centaur_vantiv_integrated_payments_DisplayStyle").on("change","#woocommerce_woocommerce_centaur_vantiv_integrated_payments_DisplayStyle",restrict_admin_fields);	
	//$j(document).off("change","#woocommerce_vantiv_woocommerce_gateway_test_live").on("change","#woocommerce_vantiv_woocommerce_gateway_test_live",restrict_admin_fields);	
	restrict_admin_fields();
});

function restrict_admin_fields(){
	hosted_mode = $j("#woocommerce_woocommerce_centaur_vantiv_integrated_payments_hosted_mode").val();
	
	if(hosted_mode=='mobile_alt'){hosted_mode='iframe';}//treat mobile_alt just like iframe mode
	page_style = $j("#woocommerce_vantiv_woocommerce_gateway_DisplayStyle").val();
	//test_live = $j("#woocommerce_vantiv_woocommerce_gateway_test_live").val();

	//$j(".live_only, .test_only").prop('disabled', true);
	//$j("."+test_live+"_only").prop('disabled', false);
	
	$j(".hosted_only, .iframe_only").prop('disabled', true);
	$j("."+hosted_mode+"_only").prop('disabled', false);
	
	if(page_style=='mercury'){
		$j(".custom_style_only").prop('disabled', true);
	}else{
		if(hosted_mode=='iframe'){var opp_mode='hosted';}
		$j(".custom_style_only").prop('disabled', false);
		$j(".custom_style_only."+opp_mode+"_only").prop('disabled', true);
	}
	

}