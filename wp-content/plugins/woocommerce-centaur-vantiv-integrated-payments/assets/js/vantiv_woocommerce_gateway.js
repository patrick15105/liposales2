var $j;
var vload = 0;
jQuery(document).ready(function(){
	$j = jQuery;
	console.log("Vantiv JS initiated");
	
	
	$j('#vantiv_iframe').load(function(){
		log_vantiv_reload(woocommerce_centaur_vantiv_integrated_payments_addon_vars.PID);
	});
	
});
 
function ajax_vantiv_check_payment_status(PID,order_id){
	console.log("Doing ajax call to check payment status...");
	$j("#vantiv_iframe_wrap").hide();
	$j("#please_wait").show();
	var wc_api_url = woocommerce_centaur_vantiv_integrated_payments_addon_vars.wc_api_url;
	var action = "verify_payment";
	$j.ajax({
		'url':wc_api_url,
		'dataType':"json",
		'data':{
			'action':action,
			'PaymentID':PID,
			'order_id':order_id,
		},
		'type':'POST',

		success: function(new_data) {
			console.log(new_data);
			if(new_data['success']){
				window.location.href = new_data['redirect'];
			}else{
				window.location.href = new_data['redirect'];
			}
		},
		error: function(e){},
		complete:function(){

		}
	});
}

function log_vantiv_reload(PID){
	vload++;
	if(vload>1){
		console.log("Doing ajax log for iframe reloading");
		var wc_api_url = vantiv_woocommerce_gateway_addon_vars.wc_api_url;
		var action = "log_iframe_reload";

		alert("a")
		$j.ajax({
			'url':wc_api_url,
			'dataType':"json",
			'data':{
				'action':action,
				'PaymentID':PID,
			},
			'type':'POST',
	
			success: function(new_data) {

			},
			error: function(e){},
			complete:function(){
	
			}
		});

	}
}