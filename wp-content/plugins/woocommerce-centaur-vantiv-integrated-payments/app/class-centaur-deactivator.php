<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur
 * @subpackage Centaur/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Centaur
 * @subpackage Centaur/includes
 * @author     Centaur Marketing <dev@centaurmarketing.co>
 */
class Centaur_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
