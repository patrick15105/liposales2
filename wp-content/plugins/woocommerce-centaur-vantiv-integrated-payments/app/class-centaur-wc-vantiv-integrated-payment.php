<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur
 * @subpackage Centaur/app
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Centaur
 * @subpackage Centaur/app
 * @author     Centaur Marketing <dev@centaurmarketing.co>
 */
class Centaur_WC_Vantiv_Integrated_Payment {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Centaur_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'WOOCOMMERCE_CENTAUR_VANTIV_INTEGRATED_PAYMENTS_VERSION' ) ) {
			$this->version = WOOCOMMERCE_CENTAUR_VANTIV_INTEGRATED_PAYMENTS_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'woocommerce-centaur-vantiv-integrated-payments';

		$this->load_dependencies();
		$this->set_locale();
		$this->plugin_init();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Centaur_Loader. Orchestrates the hooks of the plugin.
	 * - Centaur_i18n. Defines internationalization functionality.
	 * - Centaur_Admin. Defines all hooks for the admin area.
	 * - Centaur_Public. Defines all hooks for the public side of the site.
	 * - Centaur_Template. Processes all templates that will be used in the plugin.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * Setup WP autoloader function
		 */
		spl_autoload_register( array($this, 'autoloader') );

		// /**
		//  * Load WC_Payment_Gateway
		//  */
		// require_once plugins_url() . '/woocommerce/includes/class-wc-payment-gateways.php';

		$this->loader = new Centaur_Loader();

	}

	/**
	 * Autoloads all classes
	 *
	 * @param 	string 	$class_name 	name of the class that will be loaded
	 * @return 	boolean return "true" if class was loaded. Otherwise, return "false"
	 *
	 * @since 	1.0.0
	 * @access 	private
	 */
	private function autoloader( $class_name ) {

		// Convert the class name to the file name
        $class_file = 'class-' . str_replace( '_', '-', strtolower($class_name) ) . '.php';
 
        // Set up the list of directories to look in
        $classes_dir = array();
        $include_dir = realpath( plugin_dir_path( __FILE__ ) );
        $classes_dir[] = $include_dir;

        // Add each of the possible directories to the list
        foreach( array( 'includes' ) as $option) {
            $classes_dir[] = str_replace('app', $option, $include_dir);
        }

        // Look in each directory and see if the class file exists
        foreach ($classes_dir as $class_dir) {
            $inc = $class_dir . DIRECTORY_SEPARATOR .  $class_file;

            // If it does require it
            if (file_exists($inc)) {
                require_once $inc;
                return true;
            }
        }

        return false;

	}
	
	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Centaur_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Centaur_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to payment functionality
	 * of the plugin. 
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	public function define_payment_hooks() {

		if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
			return;
		}

		require_once str_replace('app', 'includes', plugin_dir_path( __FILE__ ) . 'class-centaur-vantiv-integrated-payments.php');

//		$plugin_payment = new Centaur_Vantiv_Integrated_Payments( $this->get_plugin_name(), $this->get_version() );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Centaur_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}


	public function plugin_init() {
		$plugin_basename = plugin_basename( plugin_dir_path( __DIR__ ) . $this->plugin_name . '.php' );


		add_action( 'plugins_loaded', array( $this, 'define_payment_hooks' ), 11 );

		add_filter( 'woocommerce_payment_gateways', array( $this, 'add_to_payment_gateways' ) );
		add_filter( 'plugin_action_links_' . $plugin_basename, array( $this, 'plugin_links' ) );

	}

	/**
	 * Add the gateway to available gateways
	 *
	 * @since	1.0.0
	 * @param	array	$gateways
	 * @return	array
	 */
	public function add_to_payment_gateways( $gateways ) {
		array_unshift($gateways, 'Centaur_Vantiv_Integrated_Payments');
		return $gateways;
	}

	/**
	 * Adds plugin page links
	 *
	 * @since	1.0.0
	 * @param	array	$links
	 * @return	array
	 */
	public function plugin_links( $links ) {
		$plugin_links = array(
			'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=woocommerce_centaur_vantiv_integrated_payments' ) . '">' . __( 'Settings', $this->plugin_name ) . '</a>'
		);

		return array_merge( $plugin_links, $links );
	}

}
