<?php

/**
 * Fired during plugin activation
 *
 * @link       www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur
 * @subpackage Centaur/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Centaur
 * @subpackage Centaur/includes
 * @author     Centaur Marketing <dev@centaurmarketing.co>
 */
class Centaur_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
