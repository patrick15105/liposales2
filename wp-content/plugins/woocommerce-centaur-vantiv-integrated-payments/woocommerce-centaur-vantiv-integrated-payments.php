<?php



/**

 * The plugin bootstrap file

 *

 * This file is read by WordPress to generate the plugin information in the plugin

 * admin area. This file also includes all of the dependencies used by the plugin,

 * registers the activation and deactivation functions, and defines a function

 * that starts the plugin.

 *

 * @link              www.centaurmarketing.co

 * @since             1.0.0

 * @package           Centaur

 *

 * @wordpress-plugin

 * Plugin Name:       Woocommerce Vantiv Integrated Payments

 * Plugin URI:        www.centaurmarketing.co

 * Description:       Payment Integration with Vantiv Integrated Payments

 * Version:           1.2.0

 * Author:            Centaur Marketing

 * Author URI:        www.centaurmarketing.co

 * License:           GPL-2.0+

 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt

 * Text Domain:       centaur

 * Domain Path:       /languages

 */



// If this file is called directly, abort.

if ( ! defined( 'WPINC' ) ) {

	die;

}



/**

 * Currently plugin version.

 * Start at version 1.0.0 and use SemVer - https://semver.org

 * Rename this for your plugin and update it as you release new versions.

 */

define( 'WOOCOMMERCE_CENTAUR_VANTIV_INTEGRATED_PAYMENTS_VERSION', '1.2.0' );



/**

 * The code that runs during plugin activation.

 * This action is documented in app/class-centaur-activator.php

 */

function activate_centaur() {

	require_once plugin_dir_path( __FILE__ ) . 'app/class-centaur-activator.php';

	Centaur_Activator::activate();

}



/**

 * The code that runs during plugin deactivation.

 * This action is documented in app/class-centaur-deactivator.php

 */

function deactivate_centaur() {

	require_once plugin_dir_path( __FILE__ ) . 'app/class-centaur-deactivator.php';

	Centaur_Deactivator::deactivate();

}



register_activation_hook( __FILE__, 'activate_centaur' );



register_deactivation_hook( __FILE__, 'deactivate_centaur' );



/**

 * The core plugin class that is used to define internationalization,

 * admin-specific hooks, and public-facing site hooks.

 */

require plugin_dir_path( __FILE__ ) . 'app/class-centaur-wc-vantiv-integrated-payment.php';



/**

 * Begins execution of the plugin.

 *

 * Since everything within the plugin is registered via hooks,

 * then kicking off the plugin from this point in the file does

 * not affect the page life cycle.

 *

 * @since    1.0.0

 */

function run_centaur_vip_payment() {



	$plugin = new Centaur_WC_Vantiv_Integrated_Payment();

	$plugin->run();



	if ( is_admin() ) {

    	new Centaur_Plugin_Updater( $plugin->get_plugin_name(),  $plugin->get_version(), __FILE__ );

	}



}



run_centaur_vip_payment();