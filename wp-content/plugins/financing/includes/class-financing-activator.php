<?php

/**
 * Fired during plugin activation
 *
 * @link       fb/patrick15105
 * @since      1.0.0
 *
 * @package    Financing
 * @subpackage Financing/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Financing
 * @subpackage Financing/includes
 * @author     John Patrick Anonuevo <patrick@centaurmarketing.co>
 */
class Financing_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
