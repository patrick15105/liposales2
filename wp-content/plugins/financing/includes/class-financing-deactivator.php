<?php

/**
 * Fired during plugin deactivation
 *
 * @link       fb/patrick15105
 * @since      1.0.0
 *
 * @package    Financing
 * @subpackage Financing/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Financing
 * @subpackage Financing/includes
 * @author     John Patrick Anonuevo <patrick@centaurmarketing.co>
 */
class Financing_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
