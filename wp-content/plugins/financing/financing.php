<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              fb/patrick15105
 * @since             1.0.0
 * @package           Financing
 *
 * @wordpress-plugin
 * Plugin Name:       financing
 * Plugin URI:        centaurmarketing.co
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            John Patrick Anonuevo
 * Author URI:        fb/patrick15105
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       financing
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'FINANCING_VERSION', '1.0.0' );

define ('PLUGINS_URL' , plugin_dir_url( __FILE__ ));

define ('PLUGINS_PATH' , plugin_dir_path( __FILE__ ));

define ('PLUGIN' , plugin_basename( __FILE__ ));
	
define ('UPGRADE' , ABSPATH . 'wp-admin/includes/upgrade.php');



/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-financing-activator.php
 */
function activate_financing() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-financing-activator.php';
	Financing_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-financing-deactivator.php
 */
function deactivate_financing() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-financing-deactivator.php';
	Financing_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_financing' );
register_deactivation_hook( __FILE__, 'deactivate_financing' );



/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-financing.php';

require PLUGINS_PATH . 'public/function-public.php';


/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_financing() {

	$plugin = new Financing();
	$plugin->run();

}
run_financing();
