(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	
	$(function () {


			 // FINANCE 
    
    var reg = new RegExp('^[0-9]+$'); //number only
    var lower = 0;





    $("#change_amount").blur(function(){

        var is_valid = false;
        var value = $(this).val();

          if(value != ""){

            if(reg.test(value)){

                if(value < lower){
                    validation_finance(false,"Must be $"+lower+" above")
                }

            }else{
                validation_finance(false,"Invalid Amount")
            } 
        }      

    });
    

    $("#change_amount").keyup(function(){

        var value = $(this).val();

        var is_valid = false;

        if(reg.test(value)){

            if(value >= lower){

                validation_finance(true);

                $(".3-36-mon").html("$" +  (value * 0.0335).toFixed(2));

                $(".3-60-mon").html("$" +  (value * 0.0218).toFixed(2));

                $(".6-36-mon").text("$"  + (value * 0.03407).toFixed(2));

                $(".6-60-mon").text("$" +  (value * 0.02222).toFixed(2));
            }else{

                $(".computation").text("")

            }

        }else{

            $(".computation").text("Invalid Amount")

            $(this).addClass("error-border");

            $(this).parent().find(" .error-message").html("Invalid Amount");
        }


    })


    $("#finance").submit(function(e){

        e.preventDefault();

        var value = $("#change_amount").val();

        var is_valid = false;

        if(reg.test(value)){

            if(value >= lower){

                if(!$("[name='plan']").is(':checked')){

                    $("#error_finance").html('<div class="woocommerce-error">Please select Plan@ </div>')

                }else{

                    validation_finance(true);

                    $(".tab-finance-plan").removeClass('current');
                    $(".tab-finance-personal-info").addClass('current');

                    is_valid= true;
                }
                
            }else{

                validation_finance(false,"Must be "+lower+" above");

            }

        }else{

            validation_finance(false,"Invalid Amount")

        }

        if(!is_valid){
             
             $('html, body').animate({scrollTop : 0},500);

        }

        return false;

    })

    function validation_finance(is_success,message =""){

        if(is_success){

            $("#change_amount").removeClass("error-border");
            
            $("#change_amount").parent().find(".error-message").html("");

            $("#error_finance").html("");

        }else{

            $(".computation").text("Invalid Amount")
           
            $("#change_amount").addClass("error-border");

            $("#error_finance").html('<div class="woocommerce-error">'+message+'</div>')
            
            $("#change_amount").parent().find(".error-message").html(message);

        }
       

    }
    $("#finance").submit(function(e){

    	e.preventDefault();

    	return false;

    })

    function submit_finance(){
		
		$.ajax({

			url      : ajax.ajaxurl,

			type     : "POST",

			data     : {

				action : "finance_submit"
			
			},

			success  : function(data){

				console.log(data)

			}
		})
	
	}

    $("#frm_quick_application .prev").click(function(){

        $(".tab-finance-personal-info").removeClass('current');
       
        $(".tab-finance-plan").addClass('current');

    })
    
    
    $.ajax({

        url      : ajax.ajaxurl,

        type     : "POST",

        data     : {

            action    : "fill_form",

        },

        dataType : "JSON",

        success  : function(data){

            console.log(data)

            if(data.success){

                var field = data['message'];

                var appli = field['application'];

                for(var i=0; i <appli.length;i++){

                    $('[name="'+appli[i].name+'"]').val(appli[i].value);

                }

                $("#change_amount").val(field['plan_serialize'][1].value);

                $("[value='"+field['plan_serialize'][0].value+"']").attr("checked","checked");

                $("#finance").trigger("submit")

            }
        },

        complete : function(data){

            $(".finance-loader").removeClass("active");
        }
    });


	$("#frm_quick_application").submit(function(e){

		e.preventDefault();

        $(".finance-loader").addClass("active");

		$.ajax({

			url      : ajax.ajaxurl,

			type     : "POST",

			data     : {

				action    : "finance_submit",

				serialize : $(this).serializeArray(),

				plan      : $("#finance").serializeArray() 
			
			},

			dataType : "JSON",

			success  : function(data){

				console.log(data)


				if(data.success){

					$(".required_login:eq(0)").html("<div class='woocommerce-info'>"+data.message+"</div>")

				}else{


					$(".required_login:eq(0)").html("<p></p><div class='woocommerce-info'> "+data.message+" </div>");

				}
                
                $('html, body').animate({scrollTop : 0},500);
                

			},
            complete : function(data){

                $(".finance-loader").removeClass("active");
            }
		})

		return false;


	})







	});




})( jQuery );
