<?php

		
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once PLUGINS_PATH . 'admin/vendor/autoload.php';

add_action( 'wp_ajax_nopriv_finance_submit', 'finance_submit' );
add_action( 'wp_ajax_finance_submit', 'finance_submit' );



add_action( 'wp_ajax_nopriv_fill_form', 'fill_form' );
add_action( 'wp_ajax_fill_form', 'fill_form' );

function fill_form(){

	$callback  = array(

		'success' => false,

		'message' => array(

			"finance" 	  => "",

			"application" => ""

		)

	);

	if (!session_id()) {
  	  	session_start();
	}

	if(isset($_SESSION['centaur_finance_obj'])){

		$callback['success'] = true;

		$callback['message']['finance'] 	   = unserialize($_SESSION['centaur_finance_obj']['']);
		
		$callback['message']['application']    = unserialize($_SESSION['centaur_finance_obj']['application_serialize']);
		
		$callback['message']['plan_serialize'] = unserialize($_SESSION['centaur_finance_obj']['plan_serialize']);

	}

	echo json_encode($callback);

	wp_die();

}


function finance_submit(){


	$code  		  = GeneratedCodes();

  	$current_user = wp_get_current_user();

	$callback  = array(

		'success' => false,

		'message' => ''

	);




	$is_validate 	     = validation($_POST['plan']);
	
	$callback['message'] = $is_validate['message'];


	if(!$is_validate['success']){

		$callback['message'] = $is_validate['message'];
		// $_POST['plan'][0]['value']
	}else{

		$invalid = "";

		$input = $_POST['serialize'];
			
		for($i =0; $i < sizeof($input) ; $i++ ){

			if($input[$i]['value']==""){
				$invalid=true;
				break;
			}		
		}

		if($invalid){

			$callback['message'] = "All fields are required to be filled";

			echo json_encode( $callback  );

			wp_die();

		}

		if(!is_user_logged_in()){

			$url = get_site_url();

			if (!session_id()) {
			    session_start();
			}

			$_SESSION['centaur_finance_obj'] = array(

				"application_serialize"	 => serialize($_POST['serialize']),

				"plan_serialize"       	 => serialize($_POST['plan']),

				"is_direct"				 => true

			); 

			$callback['message'] = "<strong>Required Liposales account</strong> </br>Do you have existing  account ? " . "<a href='$url/my-account/' >LOGIN</a> or not <a href='$url/my-account/' >REGISTER</a>";


		}else{

			global $wpdb;

			$table      = $wpdb->prefix."centaur_finance_verification";

  			$res        = $wpdb->get_results("Select status,vfc from $table where user_id='".$current_user->ID."' and status < 3") or  array();

  			if(sizeof($res) >=1){

  				$status = $res[0]->status;

  				if($status == 3){ // Cancel{

					$field  = array( 

						"vfc"           		 => $code,

						"application_serialize"	 => serialize($_POST['serialize']),

						"plan_serialize"       	 => serialize($is_validate['plan']),

						"status"         		 =>  1, //Pending

						"date_apply"             =>  date("Y-m-d H:i:s"),

						"verified_at"			 => '',

					);

					$where  =  array(

						"user_id"            	 => $current_user->ID,

						"status"				 => 3 // Cancel

					);

					if( $wpdb->update($wpdb->prefix."centaur_finance_verification",$field , $where) ) {

						$callback   = finance_verification($is_validate['plan'],$code);


						if($callback['success']){

							$url  = get_site_url()."/finance?resend=".$code;
							$callback['message'] = "We sent you an email confirmation (".$current_user->user_email .") <a href='$url'>Resend</a>" ;	

						}else{

							$callback['message'] = "form is under maintenance please contact us for any concern";

						}


					}else{

						$callback['message'] = "Please Contact Administrator";

  					}

  				}else{
  					
  					if($status==1){ // email confirmation
  					
  						$url  = get_site_url()."/finance?resend=".$res[0]->vfc;
						
						$callback['message'] = "We sent you an email confirmation (".$current_user->user_email .") <a href='$url'>Resend</a>" ;	

					}else if($status==2){

						$url  = get_site_url()."/customer-service/";	
  						$callback['message'] = "You have currently pending request please check your email  <a href='$url'>contact us</a> for any concern" ;
					}

  						

  				}

  			}else{

				$data  = array( 

					"user_id"            	 => $current_user->ID,

					"vfc"           		 => $code,

					"application_serialize"	 => serialize($_POST['serialize']),

					"plan_serialize"       	 => serialize($is_validate['plan']),

					"status"         		 =>  1, //Pending

					"date_apply"             =>  date("Y-m-d H:i:s"),

					"verified_at"			 => '',

				);

				if( $wpdb->insert($wpdb->prefix."centaur_finance_verification",$data ) ) {

					$callback   = finance_verification($is_validate['plan'],$code);

					if($callback['success']){

						$url  = get_site_url()."/finance?resend=".$code;
						$callback['message'] = "We sent you an email confirmation (".$current_user->user_email .") <a href='$url'>Resend</a>" ;	

					}else{

						$callback['message'] = "form is under maintenance please contact us for any concern";

					}

				}else{

					$callback['message'] = "Please Contact Administrator";
				}


  			}
		

		}


	}

	echo json_encode( $callback  );

	wp_die();

}


function resend_confirmation(){

	if(isset($_GET['resend'])){

		global $wpdb;

  		$current_user = wp_get_current_user();

  		$tablename    = $wpdb->prefix."centaur_finance_verification";


		$res  = $wpdb->get_results("Select * from $tablename where user_id='".$current_user->ID."' and vfc='".$_GET['resend']."' and status='1'") or  array();

		if(sizeof($res) >=1){
			
			// echo json_encode($res[0]->plan_serialize);

			if (!session_id()) {
	  	  		session_start();
			}

			$url = get_site_url()."/customer-service";

			$_SESSION['counter'] = (isset($_SESSION['counter']) ? $_SESSION['counter'] : 0) + 1; 
			
			if($_SESSION['counter'] >= 2){
				
				$url = get_site_url()."/customer-service";
				
				echo "<div class='woocommerce-info'>if you cannot find our email please <a href='$url'>contact</a> us</div>";

			}else{

				$callback = finance_verification(unserialize($res[0]->plan_serialize),$res[0]->vfc);

				if($callback['success']){

					echo "<div class='woocommerce-info'>We sent you an email confirmation <strong>(".$current_user->user_email .") </strong> If you cannot find our email, kindly check your “SPAM Folder”. In some cases, the email gets sent there</div>";
				}else{

					echo "<div class='woocommerce-info'>if you cannot find our email please <a href='$url'>contact</a> us</div>";
				}

			}
		}

	}

}
add_shortcode( 'resend_confirmation', 'resend_confirmation' );

function verification_test(){

	global $wpdb;

	$is_validate = "";

	$url = get_site_url()."/customer-service";


	$callback = array(

		"success" => false,

		"message" => ""

	);

	if(isset($_GET['vfc']) && is_user_logged_in()){

  		$current_user = wp_get_current_user();
	 	
	 	$userid       = $current_user->ID;

	 	$verif_code   = $_GET['vfc'];

	 	$tablename    = $wpdb->prefix."centaur_finance_verification";

    	$res          = $wpdb->get_results("Select * from $tablename where user_id='$userid' and vfc='$verif_code'") or  array();

    
    	if(sizeof($res) >=1){


    		$status = $res[0]->status;

    		if($status == 1){
	    	
	    		$plan = unserialize($res[0]->plan_serialize);

	    		$plan_tr = '<tr><td colspan="2"><h4><center>[ FINANCE PLAN ]</center</h4></tr>';
				
				foreach ($plan as $key => $value) {
					
					$plan_tr .= "<tr>

						<td>" . strtoupper(str_replace('_',' ',$key))  . " </td> 

						<td>" . $value . "</td>

					</tr>";
				}

	    		$info = unserialize($res[0]->application_serialize);

		   		$info_tr="<tr><td colspan='2'> <h4><center>[ CLIENT INFORMATION ]</center</h4> </td> </tr>";
	    		
				foreach ($info as $key) {
					
					$info_tr .= "<tr>

						<td>".  strtoupper(str_replace('_',' ',$key['name']))  ."</td> <td>".$key['value']."</td>

					</tr>";
				}

				$body = "<p>Finance request by : <strong style='color:#1173b6;'> (".$current_user->user_email.") </strong> </p>
		            
		        <table style='border: 1px solid #dee2e6;width: 100%;margin: 20px 0 0;padding:7px 10px;font-size:13px;'>
		            
		            $plan_tr

		        </table>

		        <table style='border: 1px solid #dee2e6;width: 100%;margin: 10px 0;padding:7px 10px;font-size:13px;'>
		            
		            $info_tr

		        </table> ";

		        $data  = array(

					"to"         => 'patrick@centaurmarketing.co',

					"email"      => '',

					"password"   => '',

					"logo"       => 'https://www.liposales.com/wp-content/uploads/2018/08/logo2.png',

					"subject"    => 'Liposales finance confirmation',

				);

				$mail = new PHPMailer(true);

				try {
				   	    
				    $mail->isSMTP(); 

				    $mail->Mailer     = "smtp";              

				    $mail->Host       = 'smtp.gmail.com';  

				    $mail->SMTPAuth   = true;

					$mail->CharSet    = 'iso-8859-1';

					$mail->SMTPDebug  = 0;  
					
					$mail->From       = "noreply@gmail.com";

					$mail->FromName   = "Liposales";
					

				   	$mail->addAddress($data['to']);     

					if( $data['email'] == "" ){

						$mail->Username   = 'centaurmarketing.co@gmail.com';  

				   		$mail->Password   = 'c3NT@ur_2017@!';

					}else{

						$mail->Username   = $data['email'];  

				   		$mail->Password   = $data['password'];

					}              

				    $mail->Port       	  = 587;           

				    $mail->SMTPSecure 	  = 'tls';                               
				   		
				    $mail->isHTML(true);                          

				    $mail->Subject = $data['subject'];

				    $info_body     =   array(

				        "title"    =>  'Finance Request Alert',

				        "logo_src" => $data['logo'],

				        "message"  => $body

				      );

				    $mail->Body    =  email_templates($info_body);

				    $mail->send();


					$field  = array( 

						"status"			 => 2 //pending 
					);

					$where  =  array(

						"user_id"            => $current_user->ID,

						"vfc"				 => $verif_code 
					);

					if( $wpdb->update($wpdb->prefix."centaur_finance_verification",$field , $where)){
			    			
			    		$callback['success'] = true;

				    }else{

						$url = get_site_url()."/customer-service";

				    	$callback['message']  = "form is under maintenance please <a href='$url'>contact us</a> for any concern".$mail->ErrorInfo;

				    }

				} catch (Exception $e) {

				    $callback['message'] = $mail->ErrorInfo;

				}


			}else if($status == 2){
				
				$callback['message'] = "<a href='$url' class='buttons gradient-test text-white'>contact us</a> &nbsp Your request has been processing... 
				 ";
				
			}

			if (!session_id()) {
			    session_start();
			}

			unset($_SESSION['centaur_finance_obj']);

		
    	}else{

    		$callback['message'] = "Invalid verification code please <a href='$url'>contact us</a> for any concern";
    	}


		
	}
   

    return json_encode($callback);

}


add_shortcode( 'verified_code', 'verification_test' );

function finance_verification($plan,$code){

	$current_user = wp_get_current_user();

	// $current_user->user_email;
	
	$url  = '<p style="text-align:center;margin:30px 0;"> <a   href="'.get_site_url() .'/finance/thank-you?vfc='. $code.'" style="width: 100%;background: -webkit-linear-gradient(rgba(29,181,234,1) 0%,rgba(23,155,215,1) 35%,rgba(11,108,180,1) 100%); background: -o-linear-gradient(rgba(29,181,234,1) 0%,rgba(23,155,215,1) 35%,rgba(11,108,180,1) 100%);background: linear-gradient(rgba(29,181,234,1) 0%,rgba(23,155,215,1) 35%,rgba(11,108,180,1) 100%);margin: 30px 0;padding: 15px 25px!important;color: white !important;border-radius: 50px;text-decoration: none;text-transform: uppercase;">Confirm Finance</a> </p>';

	$data  = array(

		"to"         => $current_user->user_email,

		"email"      => '',

		"password"   => '',

		"logo"       => 'https://www.liposales.com/wp-content/uploads/2018/08/logo2.png',

		"subject"    => 'Liposales finance confirmation',

	);

	$mail = new PHPMailer(true);

	$callback  = array(

			'success' => false,

			'message' => ""

	);

	try {
	   	    
	    $mail->isSMTP(); 

	    $mail->Mailer     = "smtp";              

	    $mail->Host       = 'smtp.gmail.com';  

	    $mail->SMTPAuth   = true;

		$mail->CharSet    = 'iso-8859-1';

		$mail->SMTPDebug  = 0;  
		
		$mail->From       = "noreply@gmail.com";

		$mail->FromName       = "Liposales";
		

	   	$mail->addAddress($data['to']);     

		if( $data['email'] == "" ){

			$mail->Username   = 'centaurmarketing.co@gmail.com';  

	   		$mail->Password   = 'c3NT@ur_2017@!';

		}else{

			$mail->Username   = $data['email'];  

	   		$mail->Password   = $data['password'];

		}              

	    $mail->Port       	  = 587;           

	    $mail->SMTPSecure 	  = 'tls';                               

	   		
	    $mail->isHTML(true);                          

	    $mail->Subject = $data['subject'];

	    $tr="";

	    foreach($plan as $key => $value) {
		  
 			 $tr  .= "<tr>
						  <td> <b>".strtoupper(str_replace('_',' ',$key))."</b> : </td> 
					  	  <td>   ".$value."     </td> 
					</tr>";
		}


	    $info_body     =   array(

	        "title"    =>  'Finance',

	        "logo_src" => $data['logo'],

	        "message"  => "<h4 style='color:#056cb3;text-align:center;text-transform: uppercase;'> ".$data['subject']."  </h4>

	        <table style='border: 1px solid #dee2e6;width: 100%;margin: 20px 0;padding:20px 10px;font-size:13px;color:#222;'>
	            
	            $tr

	        </table>

			 <p style='font-size:14px;color:#222;'>Hi ".$current_user->display_name." please click button to confirm your request finance</p> $url");

	    $mail->Body    =  email_templates($info_body);

	    $mail->send();

	    $callback['success'] = true;

	} catch (Exception $e) {

	    $callback['message'] = $mail->ErrorInfo;

	}

	return  $callback;


 }

 function frm_email_settings(){

	global $wpdb;

	$table      = $wpdb->prefix."centaur_finance_verification";

 	$res        = $wpdb->get_results("Select * from $table") or  array();


 	if(sizeof($res) > 0){

		return json_encode($res[0]);
		
	}else{


	}

}


function consultation_header_scripts()
{	

	wp_localize_script( 'custom_js', 'ajax', array(
              
        'ajaxurl' => admin_url( 'admin-ajax.php' ),

    ));
       
    wp_enqueue_script( 'custom_js', '/wp-content/plugins/landing/public/js/financing-public.js', array( 'jquery'), '1.0.0', true );
       
}


// wp_centaur_finance_verification
function check_verification_code(){

	$callback  = array(

		'success' => false,

		'message' => ''

	);

	if(!is_user_logged_in()){

		$callback['message'] = 'Required Login your account';

	}else{

		$current_user = wp_get_current_user();

		$email = $current_user->user_email;

	
	}


}

function validation($data_plan){


	$data = array(

		"success" =>false ,

		"message" => "",

		"plan"    =>''
	);

	// $_POST['plan'][0]['value']  plan
	// $_POST['plan'][1]['value']  amount

	$value = $_POST['plan'][1]['value'];

	if(is_numeric($value)){

		$plan = $_POST['plan'][0]['value'];

		$_plans = "";

		switch ($plan) {
			
			//  3 @ 36 * 0.0335
			case 'SC2XTKFZWTANJN3N8C':

				$_plans = array(


					'plan'        		 => 'Quick Start',

					'defferred'	   		 => '3 Months',

					'months_to_pay' 	 => '36 Months',

					'loan_amount'	     => "$".number_format($value,2),

					'monthly_payment'    => "$".number_format(round( ($value * 0.0335 ),2),2) ,
				);
				
				$data['success'] = true;

				// $_POST['plan'][0]['value'] = "Quick Start : [36 months / 3 @ $0, then 36 ] @ $" . ($value * 0.0335 );

				break;
			
			// 3 @ 60 * 0.0218
			case 'SC25ZMD3V36ZR6QM8E':

				$data['success'] = true;

				$_plans = array(

					'plan'        		 => 'Quick Start',

					'defferred'	   		 => '3 Months',

					'months_to_pay' 	 => '60 Months',

					'loan_amount'	     => "$".number_format($value,2),

					'monthly_payment'    => "$".number_format(round( ($value * 0.0335 ),2),2),

				);

				// $_POST['plan'][0]['value'] = "Quick Start : [60 months / 3 @ $0, then 60 ] @ $" . ($value * 0.0218 );

				break;
			
			// 6 @ 36 * 0.03407
			case 'SC2REWE364ZD29242D':

				$data['success'] = true;

				$_plans = array(

					'plan'        		 => 'Practice Builder',

					'defferred'	   		 => '6 Months',

					'months_to_pay' 	 => '36 Months',

					'loan_amount'	     => "$".number_format($value,2),


					'monthly_payment'    => "$".number_format(round( ($value * 0.0335 ),2),2),

				);

				// $_POST['plan'][0]['value'] = "PRACTICE BUILDER : [36 months / 6 @ $0, then 36 ] @ $" . ($value * 0.03407 );
			
				break;

			// 6 @ 60 *0.02222
			case 'SC23GTMD469RRDA9Z3':

				$data['success'] = true;

				$_plans = array(


					'plan'        		 => 'Practice Builder',

					'defferred'	   		 => '6 Months',

					'months_to_pay' 	 => '60 Months',

					'loan_amount'	     => "$".number_format($value,2),

					'monthly_payment'    => "$".number_format(round( ($value * 0.0335 ),2),2),

				);

				// $_POST['plan'][0]['value'] = "PRACTICE BUILDER : [36 months / 6 @ $0, then 60 ] @ $" . ($value * 0.02222 );

				break;
			
			default:

				$data['message'] = 'Plan not found please contact us for any concern';
				
				break;
		}

		$data['plan'] = $_plans;

	}else{

		$data['message'] = "Amount Invalid";
	}


	return $data;


}

add_action('wp_enqueue_scripts', 'consultation_header_scripts');



function send_form_email($data){


	$mail = new PHPMailer(true);

	try {

	    $callback  = array(

			'success' => false,

			'message' => ""

		);
	    
	    $mail->isSMTP(); 

	    $mail->Mailer     = "smtp";              

	    $mail->Host       = 'smtp.gmail.com';  

	    $mail->SMTPAuth   = true;

		$mail->CharSet    = 'iso-8859-1';

		$mail->SMTPDebug  = 0;  
		
		$mail->From       = "Liposales";
		
		$mail->FromName   = $data['from'];

		// $mail->addAddress('patrick@centaurmarketing.co');    

	   	$mail->addAddress( $to );     



		if( $data['email'] == "" ){

			$mail->Username   = 'centaurmarketing.co@gmail.com';  

	   		$mail->Password   = 'c3NT@ur_2017@!';

		}else{

			$mail->Username   = $data['email'];  

	   		$mail->Password   = $data['password'];

		}              

	    $mail->Port       	  = 587;           

	    $mail->SMTPSecure 	  = 'tls';                               


	    $mail->addReplyTo($data['reply_to'], 'reply-to');

	    foreach ($data['to'] as $to) {


	    }

	    foreach ($data['bcc'] as $bcc) {

	    	$mail->addBCC($bcc);

		}

		foreach ($data['cc'] as $cc) {

	    	$mail->addCC($bcc);

		}

	    $mail->isHTML(true);                                  // Set email format to HTML

	    $mail->Subject = $data['subject'];

	    $tablerow="<tr><td colspan='2'> <h4><center>[ CLIENT INFORMATION ]</center</h4> </td> </tr>";

	    foreach($_POST['serialize'] as $field) {
		  
		  $tablerow  .= "<tr>
							  <td> <b>".strtoupper(str_replace('_',' ',$field['name']))."</b> : </td> 
						  	  <td>   ".$field['value']."     </td> 
						</tr>";
		}


	    $plan="<tr><td colspan='2'> <h4><center>[ FINANCE PLAN ]</center</h4> </td> </tr>";

	    foreach($_plans as $field) {
		  
 			 $plan  .= "<tr>
						  <td> <b>".strtoupper(str_replace('_',' ',$field['name']))."</b> : </td> 
					  	  <td>   ".$field['value']."     </td> 
					</tr>";
		}


	    $info_body     =   array(

	        "title"    =>  'Finance',

	        "logo_src" => $data['logo'],

	        "message"  => "<h3> ".strtoupper($data['subject'])."  </h3>
	            
	        <table style='border: 1px solid #dee2e6;width: 100%;margin: 20px 0;padding:20px 10px;font-size:13px;'>
	            
	            $tablerow

	        </table>

	        <table style='border: 1px solid #dee2e6;width: 100%;margin: 20px 0;padding:20px 10px;font-size:13px;'>
	            
	            $plan

	        </table>"

	    );

	    $mail->Body    =  email_templates($info_body);

	    $mail->send();

	    $callback['success'] = true;

	} catch (Exception $e) {

	    $callback['message'] = $mail->ErrorInfo;

	}


	return $callback;

}

function GeneratedCodes(){


    $code = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
     $generated = "";

    for($i = 0 ; $i < 60 ; $i++){
        $generated = $generated . $code[rand(0,strlen($code)-1)];
    }

    return  $generated;
}


function email_templates($info){


    $img = "";

    if($info['logo_src']!=""){

        $img = '<center><img alt="" border="0" class="CToWUd" src="'.$info['logo_src'].'" style="margin:0;padding:0;max-width:600px;border:none;font-size:14px;font-weight:bold;min-height:auto;line-height:100%;outline:none;text-decoration:none;text-transform:capitalize"></center>';

    }


    return '

    <table border="0" cellpadding="30" cellspacing="0" width="100%" >
    
    <tbody>
      
      <tr>
        
        <td align="center" bgcolor="#EEEEEE" class="m_6882686408251595769frame">
          
          <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" style="border-radius:6px" width="660">
           
            <tbody>
              
              <tr>
                
                <td class="m_6882686408251595769content" style="padding:30px 30px 30px 30px;border-radius:5px">
                  
                  <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" width="600">
                    
                    <tbody>
                      
                      <tr>

                        <td align="left" id="m_6882686408251595769content-5" style="font-size:14px;font-family:Helvetica,Arial,sans-serif;line-height:25px;color:#445566">
                        
                            '.$img.'

                            <div class="m_6882686408251595769text">

                            <h3>'. $info['message'] .'</h3>
                          
                          </div>

                        </td>

                      </tr>

                    </tbody>

                  </table>


                  <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" width="600">
                    
                    <tbody>
                      
                      <tr>
                        
                        <td class="m_6882686408251595769doublespace" height="30" style="">
                        </td>
                      
                      </tr>
                    
                    </tbody>
                  
                  </table>

                  
                  <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" width="600">
                  </table>
                
                </td>
              
              </tr>
            
            </tbody>
          
          </table>


          <table align="center" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" width="660">
            
            <tbody>
              
              <tr>
                
                <td align="center" id="m_6882686408251595769content-10" style="font-family:Arial,Helvetica,sans-serif;color:#666666;font-size:10px;line-height:18px">
                  <div class="m_6882686408251595769text">
                    <p style="margin-top:30px;margin-bottom:10px"></p>
                  </div>
                
                </td>
              
              </tr>
            
            </tbody>
          
          </table>
          
          <img alt="pixel" class="CToWUd" src="https://ci3.googleusercontent.com/proxy/veswKNcBhJ0j3UYdMmHPI506oyA1UHLGcgmQK7NT0XxX6N9haL3zCQ1H6_dSOnfcIEBPOXwxYWAVmMZQAoE6pQPB_Cc8GeMHWbkJux7rmw3rn2ALiRrquyHjcedNWPpbvC8=s0-d-e1-ft#https://mailer.000webhost.com/o/112387781/44f7a37fcb14f0ddaef46ce5b6ecf1c5/7565">
        
        </td>
      
      </tr>

    </tbody>
  
  </table>';

}













?>