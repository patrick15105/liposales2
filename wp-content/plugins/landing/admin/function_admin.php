<?php

/**
 * Fired during plugin activation
 *
 * @link       fb/patrick15105
 * @since      1.0.0
 *
 * @package    Landing
 * @subpackage Landing/admin
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Landing
 * @subpackage Landing/includes
 * @author     John Patrick Anonuevo <patrick@centaurmarketing.co>
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';



add_action( 'wp_ajax_action_frm', 'action_frm' );

function action_frm(){

	global $wpdb;

	$callback  = array(

		'success' => false,

		'message' => ""

	);


	if( $_POST['actions'] == "Added" ){

		$fields = array(

			'fields' 	    => $_POST['fields'],

			'class_button' 	=> $_POST['class'],

			'status' 	    => 1, // Default Active 

			'btn_text'      => $_POST['btn_text'],
	 		
	 		'created_date'  => date("Y-m-d H:i:s"),

	 		'updated_date'  => date("Y-m-d H:i:s"),

	 		'redirect_url'  => $_POST['redirect_url'],

		);

		if( $wpdb->insert($wpdb->prefix."forms",$fields ) ) {

			$callback['success'] = true;

		} else {

			$callback['message'] = $wpdb->error;

		}

	}else{

		$field = array(

			'fields' 	    => $_POST['fields'],

			'class_button' 	=> $_POST['class'],

			'btn_text'      => $_POST['btn_text'],
	 		
	 		'updated_date'  => date("Y-m-d H:i:s"),

	 		'redirect_url'  => $_POST['redirect_url'],


		);

		$where  =  array(

			"forms_id"      => $_POST['id']

		);

		if( $wpdb->update($wpdb->prefix."forms",$field , $where) ) {

			$callback['success'] = true;

		} else {

			$callback['message'] = $wpdb->show_errors();

		}

	}

	echo json_encode($callback);

	die();

}

add_action( 'wp_ajax_update_frm', 'update_frm' );

function update_frm(){

	global $wpdb;

	$callback  =  array('success'    => false ,'message' => "");

	$where	   =  array("forms_id"   => $_POST['id']);

	$field	   =  array('status'     => $_POST['stat'] ,'updated_date'  => date("Y-m-d H:i:s"));

	if( $wpdb->update($wpdb->prefix."forms",$field , $where) ) {

		$callback['success'] = true;

	} else {

		$callback['message'] = $wpdb->show_errors();

	}

	echo json_encode($callback);

	die();

}

function frm_select_data( ){

	global $wpdb;

    $tablename= $wpdb->prefix."forms";

    $res = $wpdb->get_results("Select * from $tablename ORDER BY forms_id DESC") or  array();

    return json_encode($res);

}


function form_consultation(){

	return '
	<h1>Finance</h1>
	</br>
	<p class="">Deferred Payment Options With LipoSales!</p>

	<div class="single-product">
	  <table class="woocommerce-product-attributes shop_attributes">
	    <thead style="background-color: #eeeff1;">
	      <tr>
	        <td>
	          <h3 class="gradient-text">
	            Quick Start
	          </h3>
	        </td>
	        <td>
	          <h3 class="gradient-text">
	            Practice Builder
	          </h3>
	        </td>
	      </tr>
	    </thead>

	    <tbody>
	      <tr>
	        <td colspan="2">
	          <h3 class="">
	            Amount : <span class="gradient-text change_text computation" style="font-size: 42px;">$30,000.00</span>
	          </h3>
	        </td>
	      </tr>

	      <tr style="background-color: white !important;">
	        <td>
	          <h3 for="" style="margin-bottom: 0;">
	            <input type="radio"> 36 Months
	          </h3>

	          <p style="margin-top: 0;">
	            3 @ <span class="woocommerce-Price-currencySymbol">$</span>0, then 36@ <span class="gradient-text">= <strong class="computation 3-36-mon">$1,005.00</strong></span>
	          </p>
	        </td>
	        <td>
	          <h3 for="" style="margin-bottom: 0;">
	            <input type="radio"> 36 Months
	          </h3>

	          <p style="margin-top: 0;">
	            6 @ <span class="woocommerce-Price-currencySymbol">$</span>0, then 36@ <span class="gradient-text">= <strong class="computation 6-36-mon" style="">$1,022.00</strong></span>
	          </p>
	        </td>
	      </tr>

	      <tr style="background-color: white !important;">
	        <td>
	          <h3 for="" style="margin-bottom: 0;">
	            <input type="radio"> 60 Months
	          </h3>

	          <p style="margin-top: 0;">
	            3 @ <span class="woocommerce-Price-currencySymbol">$</span>0, then 60@ <span class="gradient-text">= <strong class="computation 3-60-mon" style="">$654.00</strong></span>
	          </p>
	        </td>
	        <td>
	          <h3 for="" style="margin-bottom: 0;">
	            <input type="radio"> 60 Months
	          </h3>

	          <p style="margin-top: 0;">
	            6 @ <span class="woocommerce-Price-currencySymbol">$</span>0, then 60@ <span class="gradient-text">= <strong class="computation 6-60-mon" style="">$666.00</strong></span>
	          </p>
	        </td>
	      </tr>
	    </tbody>
	  </table>
	</div>

	<div class="form-group">
	  <input class="form-control" id="change_amount" placeholder="Enter Amount" type="text"> <span class="error-message"></span>
	</div>
	<button class="buttons gradient-test text-white">Sumbmit</button>
	<div class="centaur-full centaur-fixed centaur-nodisplay centaur-modal centaur-flex">
	  <div class="centaur-container">
	    <div class="centaur-header">
	      <h2 class="centaur-title gradient-text">
	        Quick Application:
	      </h2>
	      <span id="centaur-modal-close"><i class="fa fa-times"></i></span>
	    </div>

	    <div class="centaur-content" style="opacity: 1;">
	      <p>
	        &nbsp;
	      </p>

	      <div class="flex">
	        <div class="flex-1" style="width: 48%; margin-right: 4%;">
	          <div class="form-group">
	            <label for="">Legal Business Name</label> <input class="form-control" placeholder="Legal Business Name" type="text">
	          </div>

	          <div class="form-group">
	            <label for="">Business Address</label> <input class="form-control" placeholder="Legal Business Name" type="text">
	          </div>

	          <div class="form-group">
	            <label for="">Years in Business</label> <input class="form-control" placeholder="Legal Business Name" type="text">
	          </div>

	          <div class="form-group">
	            <label for="">Principal/Owner Name</label> <input class="form-control" placeholder="Principal/Owner Name" type="text">
	          </div>

	          <div class="form-group">
	            <label for="">Principal/Owner Home Address</label> <input class="form-control" placeholder="Principal/Owner Home Address" type="text">
	          </div>
	        </div>

	        <div class="flex-2" style="width: 48%;">
	          <div class="form-group">
	            <label for="">Primary Office Contact</label> <input class="form-control" placeholder="Primary Office Contact" type="text">
	          </div>

	          <div class="form-group">
	            <label for="">Business Phone & Business Fax</label> <input class="form-control" placeholder="Business Phone & Business Fax" type="text">
	          </div>

	          <div class="form-group">
	            <label for="">Web Address</label> <input class="form-control" placeholder="Web Address" type="text">
	          </div>

	          <div class="form-group">
	            <label for="">Principal/Owner SSN</label> <input class="form-control" placeholder="Principal/Owner SSN" type="text">
	          </div>

	          <div class="form-group">
	            <label for="">Email Address</label> <input class="form-control" placeholder="Email Address" type="email">
	          </div>
	        </div>
	      </div>
	      <button class="buttons gradient-test text-white">Sumbmit</button>
	    </div>
	  </div>
	</div>';
  	

}


add_shortcode( 'form_finance', 'form_consultation' );




add_action( 'wp_ajax_frm_select', 'frm_select' );

function frm_select(){

	global $wpdb;

	$id         =  $_POST['id'];

	$tablename  = $wpdb->prefix."forms";

 	$res        = $wpdb->get_results("Select * from $tablename where forms_id=".$id) or  array();

	echo json_encode($res);

	die();

}


/*

========================================================================================================                    					   Email Functionality                  ========================================================================================================


 */


add_action( 'wp_ajax_upload_logo', 'upload_logo' );

function upload_logo(){

	if($_FILES["file"]["name"] != ''){

	$test = explode('.', $_FILES["file"] ["name"] );

	$ext = end($test);

	$letters  = "abcdefghijklmnopqrstuvwxyz";

	for($i = 0 ; $i < 10 ; $i++){
		$rand .= $letters[rand(0,strlen($letters) ) - 1];
	}

	 $name     = $rand .".". $ext;

	 $location = "../wp-content/plugins/landing/upload/". $name;  

	 move_uploaded_file($_FILES["file"]["tmp_name"], $location);

	 echo  json_encode(array( "name_file" => $name , "location" => $location ));

	}

	die();

}

add_action( 'wp_ajax_settings_frm', 'settings_frm' );

function settings_frm(){

	global $wpdb;

	$table     = $wpdb->prefix."forms_email_settings";

	$callback  = array(

		'success' => false,

		'message' => ""

	);

	$field = array(

		'to' 	        => $_POST['to'],       // Default Active 

		'reply_to'      => $_POST['reply-to'],
	 		
	 	'cc'            => $_POST['cc'],

		'logo' 	        => $_POST['logo'],

		'from' 	        => $_POST['from'],

		'bcc'           => $_POST['bcc'],

		'email'         => $_POST['email'],

		'password'      => $_POST['password'],

		'subject' 		=> $_POST['subject'],

	 	'updated_date'  => date("Y-m-d H:i:s")

	);

	$where  =  array(

		"forms_email_settings_id"  => 1

	);




	if( $wpdb->update( $table , $field , $where) ) {

		$callback['success'] = true;

	} else {

		$callback['message'] = $wpdb->show_errors();



	}



	// INSERT INTO `wp_forms_email_settings` (`forms_email_settings_id`, `logo`, `updated_date`, `to`, `reply_to`, `cc`, `subject`) VALUES (NULL, '0', '2019-07-15 00:00:00', '', '', '', '');

	echo json_encode($callback);

	die();

}


add_action( 'wp_ajax_test_email', 'test_email' );

function test_email(){

	global $wpdb;

	$table      = $wpdb->prefix."forms_email_settings";

 	$res        = $wpdb->get_results("Select * from $table where forms_email_settings_id=1") or  array();

 	$callback  = array(

		'success' => false,

		'message' => ""

	);


 	if(sizeof($res) > 0){

 		$res = $res[0];

	    $data = array(

	    	"to"         => array($_POST['to']),

	    	"email"      => $res->email,

	    	"password"   => $res->password,

	    	"logo"       => $res->logo,

	    	"from"       => $res->from,

	    	"subject"    => $res->subject,

	    	"bcc"        => explode(",", $res->bcc),

	    	"cc"         => explode(",", $res->cc),

	    	"reply_to"	 => $res->reply_to,

	    );

	    $email  = email_send($data);


	    
	    if( $email['success'] ){


	    	$callback['success']  = true;



	    }else{

	    	$callback['message']  =  $email['message']; 
	    
	    }


 	}else{

 		$callback['message'] = "Check Form Email Information Specially Required Fields" ;

 	}


 	return json_encode($callback);

	die();



}



add_shortcode( 'email', 'test_emails' );

function test_emails(){

	global $wpdb;

	$table      = $wpdb->prefix."forms_email_settings";

 	$res        = $wpdb->get_results("Select * from $table where forms_email_settings_id=1") or  array();

 	$callback  = array(

		'success' => false,

		'message' => ""

	);

 	if(sizeof($res) > 0){

 		$res = $res[0];

	    $data = array(

	    	"to"         => explode(",",$res->to),

	    	"email"      => $res->email,

	    	"password"   => $res->password,

	    	"logo"       => $res->logo,

	    	"from"       => $res->from,

	    	"subject"    => $res->subject,

	    	"bcc"        => explode(",", $res->bcc),

	    	"cc"         => explode(",", $res->cc),

	    	"reply_to"	 => $res->reply_to,

	    );
	    

	    $email  = email_send($data);
	    
	    if( $email['success'] ){

	    	$callback['success']  = true;

	    }else{

	    	$callback['message']  =  $email['message']; 
	    }


 	}else{

 		$callback['message'] = "Check Form Email Information Specially Required Fields" ;

 	}


 	return json_encode($callback);

	die();

}

add_shortcode( 'select_form_email_settings', 'frm_email_settings_select' );



function frm_email_settings_select(){

	global $wpdb;

	$table      = $wpdb->prefix."forms_email_settings";

 	$res        = $wpdb->get_results("Select * from $table") or  array();


 	if(sizeof($res) > 0){

		return json_encode($res[0]);

 	}else{

 		$field = array(

			'to' 	        => "",       // Default Active 

			'reply_to'      => "",
		 		
		 	'cc'            => "",

		 	'bcc'           => "",

		 	'email'         => "",

		 	'password'      => "",

			'logo' 	        => 0,

			'subject' 		=> "",

		);

 		return json_encode($field);

 	}


}



function email_send($data){


	$mail = new PHPMailer(true);

	try {

	    $callback  = array(

			'success' => false,

			'message' => ""

		);
	    
	    $mail->isSMTP(); 

	    $mail->Mailer     = "smtp";              

	    $mail->Host       = 'smtp.gmail.com';  

	    $mail->SMTPAuth   = true;

		$mail->CharSet    = 'iso-8859-1';

		$mail->SMTPDebug  = 0;  
		
		$mail->From       = "test@centaurmarketing.co";
		
		$mail->FromName   = "testing email";    


		if( $data['email'] == "" ){

			$mail->Username   = 'centaurmarketing.co@gmail.com';  

	   		$mail->Password   = 'c3NT@ur_2017@!';

		}else{

			$mail->Username   = $data['email'];  

	   		$mail->Password   = $data['password'];

		}              

	    $mail->Port       	  = 587;           

	    $mail->SMTPSecure 	  = 'tls';                               


	    $mail->addReplyTo($data['reply_to'], 'reply-to');


	    foreach ($data['to'] as $to) {

	   		$mail->addAddress( $to );     

	    }

	    foreach ($data['bcc'] as $bcc) {

	    	$mail->addBCC($bcc);

		}

		foreach ($data['cc'] as $cc) {

	    	$mail->addCC($bcc);

		}

	    $mail->isHTML(true);                                  // Set email format to HTML

	    $mail->Subject = $data['subject'];

	    $info_body     =   array(

	        "title"    =>  $data['subject'],

	        "logo_src" => "https://www.liposales.com/wp-content/uploads/2018/08/logo2.png",

	        "message"  => "<h4> title mode  </h4>
	            
	        <table style='border: 1px solid #dee2e6;width: 100%;margin: 20px 0;'>
	            
	            <tr>
	                
	                <td style='padding: .3rem;'>CM-FORM Send Successfully</td>
	              
	                
	            </tr>

	        </table>"

	    );

	    $mail->Body    =  sending_format($info_body);

	    $mail->send();

	    $callback['success'] = true;

	} catch (Exception $e) {

	    $callback['message'] = $mail->ErrorInfo;

	    global $wp_session;

	    $wp_session['cm_error'] = $mail->ErrorInfo;

	}


	return $callback;

}



function sending_format($info){


    $img = "";

    if($info['logo_src']!=""){

        $img = '<img alt="" border="0" class="CToWUd" src="http://staging.maschouston.com/wp-content/themes/masc/img/logo.png" style="margin:0;padding:0;max-width:600px;border:none;font-size:14px;font-weight:bold;min-height:auto;line-height:100%;outline:none;text-decoration:none;text-transform:capitalize">';
    }


    return '

    <table border="0" cellpadding="30" cellspacing="0" width="100%">
    
    <tbody>
      
      <tr>
        
        <td align="center" bgcolor="#EEEEEE" class="m_6882686408251595769frame">
          
          <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" style="border-radius:6px" width="660">
           
            <tbody>
              
              <tr>
                
                <td class="m_6882686408251595769content" style="padding:30px 30px 30px 30px;border-radius:5px">
                  
                  <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" width="600">
                    
                    <tbody>
                      
                      <tr>

                        <td align="left" id="m_6882686408251595769content-5" style="font-size:14px;font-family:Helvetica,Arial,sans-serif;line-height:25px;color:#445566">
                        
                            '.$img.'

                            <div class="m_6882686408251595769text">
                            
                            <h3>'.$info['title'].'</h3>'. 

                            "<p>". $info['message'] .'</p>
                          
                          </div>

                        </td>

                      </tr>

                    </tbody>

                  </table>


                  <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" width="600">
                    
                    <tbody>
                      
                      <tr>
                        
                        <td class="m_6882686408251595769doublespace" height="30" style="border-top:1px solid #dddddd">
                        </td>
                      
                      </tr>
                    
                    </tbody>
                  
                  </table>

                  
                  <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" width="600">
                  </table>
                
                </td>
              
              </tr>
            
            </tbody>
          
          </table>


          <table align="center" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" width="660">
            
            <tbody>
              
              <tr>
                
                <td align="center" id="m_6882686408251595769content-10" style="font-family:Arial,Helvetica,sans-serif;color:#666666;font-size:10px;line-height:18px">
                  <div class="m_6882686408251595769text">
                    <p style="margin-top:30px;margin-bottom:10px"></p>
                  </div>
                
                </td>
              
              </tr>
            
            </tbody>
          
          </table>
          
          <img alt="pixel" class="CToWUd" src="https://ci3.googleusercontent.com/proxy/veswKNcBhJ0j3UYdMmHPI506oyA1UHLGcgmQK7NT0XxX6N9haL3zCQ1H6_dSOnfcIEBPOXwxYWAVmMZQAoE6pQPB_Cc8GeMHWbkJux7rmw3rn2ALiRrquyHjcedNWPpbvC8=s0-d-e1-ft#https://mailer.000webhost.com/o/112387781/44f7a37fcb14f0ddaef46ce5b6ecf1c5/7565">
        
        </td>
      
      </tr>

    </tbody>
  
  </table>';

}


add_shortcode( 'form_table', 'frm_select_data_table' );

function frm_select_data_table(){

	global $wpdb;


	if(isset($_GET['filter_data_range'])){

		$daterange = $_GET['filter_data_range'];

		$arr       =  explode("-",$daterange);
				
		$datestart = dateConvert(explode("/",$arr[0]));

		$dateend   = dateConvert(explode("/",$arr[1]));

		$date = new DateTime($dateend);

		$date->modify('+1 day');

		$dateend = strtotime($dateend . ' +1 day');

		$between   = "date_send BETWEEN '".$datestart."' AND '".$date->format('Y-m-d')."'";

	}else{

		$between = "date_send  > '0000-00-00'";
	}

	$email = "";

    if(isset($_GET['filter_email'])){


    	if($_GET['filter_email']!="all"){

    		$email = "and is_email_send='".$_GET['filter_email']."'";

    	}

    }


    $sort = "desc";


    if(isset($_GET['filter_sort'])){

    	$sort= $_GET['filter_sort'];

    }


	// $id         =  $_POST['id'];

	// $search     =  (isset($_POST['search'])) ? $_POST['search'] : '';

	// $date_end   =  (isset($_POST['date_end'])) ? $_POST['date_end'] : '';

	// $date_start =  (isset($_POST['date_start'])) ? $_POST['date_start'] : '';

	// $status     =  (isset($_POST['status'])) ? $_POST['status'] : '';

	// $sort       =  (isset($_POST['sort'])) ? $_POST['sort'] : '';

	$tablename  = $wpdb->prefix."forms_data_serialize";


 	$res        = $wpdb->get_results("Select * from $tablename where $between $email ORDER BY  date_send " . $sort ) or  array();
 	

	// $output = array(  
 //                "draw"                    =>     2,  
 //                "recordsTotal"            =>     5,  
 //                "recordsFiltered"         =>     4,  
 //                "data"                    =>     $res  
 //           );  
 //           
    
 	echo "<h1 style='text-align:center'>Total Show : <b>" . sizeof($res) . "</b></h1>";
    return json_encode($res);


	// echo json_encode($res);


}


function dateConvert($arr){

	return trim($arr[2])."-".trim($arr[0])."-".trim($arr[1]);

}

			


// // Load Composer's autoloader

// // Instantiation and passing `true` enables exceptions
// $mail = new PHPMailer(true);

// try {
//     //Server settings
//     $mail->SMTPDebug = 2;                                       // Enable verbose debug output
//     $mail->isSMTP();                                            // Set mailer to use SMTP
//     $mail->Host       = 'smtp.sendgrid.com';  // Specify main and backup SMTP servers
//     $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
//     $mail->Username   = "patricktestmode@gmail.com";                     // SMTP username
//     $mail->Password   = "password123@!";                               // SMTP password
//     $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
//     $mail->Port       = 587;                                    // TCP port to connect to

//     //Recipients
//     $mail->setFrom('from@example.com', 'Mailer');
//     $mail->addAddress('patrick@centaurmarketing.co');     // Add a recipient
//     // $mail->addAddress('ellen@example.com');               // Name is optional
//     $mail->addReplyTo('info@example.com', 'Information');
//     $mail->addCC('cc@example.com');
//     $mail->addBCC('bcc@example.com');

//     // Attachments
//     // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//     // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

//     // Content
//     $mail->isHTML(true);                                  // Set email format to HTML
//     $mail->Subject = 'Here is the subject';
//     $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
//     $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

//     $mail->send();
//     echo 'Message has been sent';
// } catch (Exception $e) {
//     echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
// }


// }

// echo "DA";
// die();



?>