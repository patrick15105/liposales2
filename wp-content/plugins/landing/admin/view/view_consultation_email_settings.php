<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       fb/patrick15105
 * @since      1.0.0
 *
 * @package    Landing
 * @subpackage Landing/admin/view/
 */
?>
  
  <?php

    $settings  = json_decode(do_shortcode( '[select_form_email_settings]'));



  ?>

  <div class="wrap">
      <p>&nbsp</p>
      <h4>Email Information</h4>

      <?php
      
      global $wp_session;

      if(isset($wp_session['cm_error'])){ ?>
          <div class="alert alert-error">
              <strong>Error: </strong> <?php echo $wp_session['cm_error']; ?>
          </div>
  
      <?php } ?>

      

      <div class="alert alert-info">
        <p class="small">
         # once you encountered email sending issue please check your <b>(firewall , anti virus , php ini , server issue )</b> </p>
  
      

      </div>


  <div class="row">


      <div class="col-md-12">
        <form action="" id="frm_email_settings">
          <table class="table table-sm small no-border table-small">
              <thead class="alert alert-dark">
                  <tr>
                    <th colspan="2">
                      <span class="dashicons dashicons-admin-generic"></span> Email Information
                    </th>
                  </tr>
              </thead>
              <tbody>
                <tr>
                  <td colspan="2">&nbsp</td>
                </tr>

                <tr>

                  <td>Upload logo </td>

                  <td>

                    <?php if($settings->logo==""){ ?>
                    <div class="box alert-ligth active">
                    <span>EMAIL LOGO </span>
                     
                    </div>

                    <?php }else{ ?>

                       <img src="../wp-content/plugins/landing/upload/<?php  echo $settings->logo; ?>" alt="LOGO" id="image_upload" name="<?php  echo $settings->logo; ?>">

                    <?php } ?>


                    <p></p>
                    <input type="file" id="file" name="file" class="alert-dark">
                  </td>
                </tr>

                <tr>

                  <td width="15%"><label for=""><span class="text-danger"><b>*</b></span> To : </label>
                  </td>
                  <td>
                    <div class="form-group">
                      <input class="form-control input-sm" value="<?php echo $settings->to; ?>" name="to" id="to" placeholder="To : " type="text" required>
                      <span class="text-muted small">Use commas to separate email Sample (doe@gmail.com , john@gmail.com)</span>
                    </div>
                  </td>
                </tr>

                 <tr>
                  <td><label for=""><span class="text-danger"><b>*</b></span> Reply-To : </label>
                  </td>

                  <td>
                    <div class="form-group">
                      <input class="form-control" name="reply-to" id="reply-to" placeholder="From : " type="text" value="<?php echo $settings->reply_to; ?>" required>
                    </div>
                   
                  </td>
                </tr>

                <tr>
                  <td><label for=""><span class="text-danger"><b>*</b></span> From Name: </label>
                  </td>

                  <td>
                    

                     <div class="form-group">
                      <input class="form-control" name="from" id="from" placeholder="From Name: " type="text" value="<?php echo isset($settings->from) ? $settings->from : ""; ?>" required>
                    </div>
                  </td>
                </tr>


                <tr>
                  <td><label for="">CC :</label>
                    
                  </td>

                  <td>
                    <div class="form-group">
                      <input class="form-control" name="cc" id="cc" value="<?php echo $settings->cc; ?>" placeholder="CC :" type="text">  <span class="text-muted small">Use commas to separate email Sample (doe@gmail.com , john@gmail.com)</span>
                    </div>
                  </td>
                </tr>

                <tr>
                  <td><label for="">BCC :</label>
                    
                  </td>

                  <td>
                    <div class="form-group">
                      <input class="form-control" name="bcc" id="bcc" value="<?php echo $settings->bcc; ?>" placeholder="BCC :" type="text">  <span class="text-muted small">Use commas to separate email Sample (doe@gmail.com , john@gmail.com)</span>
                    </div>
                  </td>
                </tr>


                <tr>
                  <td><label for="">Subject :</label>
                  </td>

                  <td>
                    <div class="form-group">
                      <textarea class="form-control" id="subject" name="subject"  cols="30"  rows="2" required value="<?php echo $settings->subject; ?>"><?php echo $settings->subject; ?></textarea>
                    </div>
                  </td>
                </tr>


                <tr class="alert-dark">
                    <td>Email Username</td>
                    <td>
                      <p class="small">the default email is centaurmarketing just leave it blank once you dont need to change</p>
                      <input type="text" name="email" id="email" value="<?php echo $settings->email; ?>"  placeholder="centaurmarketing.co@gmail.com" class="form-control">
                    </td>
                </tr>
                <tr class="alert-dark">


                  <td><label for="password">Password</label></td>
                  <td><input type="password" name="password" id="password" value="<?php echo $settings->password; ?>" placeholder="*********" class="form-control">

                    <p>&nbsp</p></td>
                </tr>

                <tr>
                  <td></td>
                  <td colspan="1">
                    <button class="btn btn-info" type="submit">Save Settings</button> 
                    <button class="btn btn-dark" type="button" id="test_email">Test Email Sending</button></td>
                </tr>

              </tbody>
          </table>
        </form>
        
          
      </div>
      
    

  </div>


     <div class="modal" id="form_modal">
     <div class="modal-dialog">
       <div class="modal-content">

         <!-- Modal Header -->
         <div class="modal-header">
           <h6 class="modal-title">Testing Email</h6>
           <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>

         <!-- Modal body -->
         <div class="modal-body small">

            <div class="">
                
                <table class="table small table-striped table-sm table-right">


                  <tr>
                    <th>LOGO :</th>
                    <td><img src="../wp-content/plugins/landing/upload/<?php  echo $settings->logo; ?>" alt="LOGO" id="" width="100px"></td>
                  </tr>

                  <tr>
                    <th>TO : </th>
                    <td><?php  echo $settings->to; ?></td>
                  </tr>

                   <tr>
                    <th>Reply To :</th>
                    <td><?php  echo $settings->reply_to; ?></td>
                  </tr>

                  <tr>
                    <th>From :</th>
                    <td><?php  echo $settings->from; ?></td>
                  </tr>

                  <tr>
                    <th>CC :</th>
                    <td><?php  echo $settings->cc; ?></td>
                  </tr>

                  <tr>
                    <th>BCC :</th>
                    <td><?php  echo $settings->bcc; ?></td>
                  </tr>

                  <tr>
                    <th>SUBJECT :</th>
                    <td><?php  echo $settings->subject; ?></td>
                  </tr>

                </table>


            </div>


            <form action="" id="frm_email_test">


              <div class="form-group">
                <div id="alert-message">
                </div>
              </div>
              
              <div class="form-group">

        

                <label for="">Form Email Testing</label>
                <input type="email" name="test_to" id="test_to" class="form-control" placeholder="Email Address" required>

              </div>

              <button class="btn btn-info btn-block" type="submit">Test!</button>
            </form>

         </div>

       

       </div>
     </div>

