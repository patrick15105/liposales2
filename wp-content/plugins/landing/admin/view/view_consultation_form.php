<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       fb/patrick15105
 * @since      1.0.0
 *
 * @package    Landing
 * @subpackage Landing/admin/view/
 */
?>


<div class="wrap">
   
   <h4>Form Setup</h4>
	

	<div class="alert alert-info small">
		
		&lt;form&gt;	
			
			the field form shorcode you created 
		
		&lt;/form&gt;

	</div>
    


   <p class="small"></p>
   <table class="table table-striped table-sm small">
   		
   		<thead class="alert-dark">
   			<tr>
   				<th width="30%"> &lt;/&gt; HTML form field</th>
   				<th>ShortCode</th>
               <th>Button Class</th>
               <th>Status</th>
               <th>Last Updated / Created Date</th>
   				<th>Action</th>
   			</tr>
   		</thead>

   		<tbody>

            <?php

               $res = do_shortcode( '[data_frm]' );

               $res = json_decode($res);

               if(sizeof($res)<=0){


            ?>
   			
   			  <tr>
             
               <td colspan="6" class="alert-danger">
                  <center>

                     No Field
                     
                  </center>



               </td>      
            </tr>


            <?php


               }else{

                  function limit_text($text, $limit) {
                    
                     if (str_word_count($text, 0) > $limit) {
                         
                         $words = str_word_count($text, 2);
                        
                         $pos = array_keys($words);
                        
                         $text = substr($text, 0, $pos[$limit]) . '...';
                     }
                     
                     return $text;
                  }

                  
                  foreach ($res as $val) {
                     
                     $fields = str_replace("<","&lt;",$val->fields);
                    
                     $fields = preg_replace('/\\\\/', '', $val->fields);

                     echo "<tr>" .

                              "<td class='small'><span class='code' >" .   limit_text(str_replace("<","&lt;",$fields), 50). "</span></td>".

                              "<td>" . "[consultation_form id='".$val->forms_id."']" . "</td>".

                              "<td>" . $val->class_button . "</td>".

                              "<td> ". ( ($val->status) ?  "<span class='badge badge-dark'>Active</span> " : " <span class='badge badge-secodary'>De-Active</span>" ). "</td>".

                              "<td>" . $val->updated_date." / " . $val->created_date . "</td>".

                              "<td><button class='btn small btn-dark btn-sm small edit' name='" . $val->forms_id."'>Edit </button> | " 

                                 . ( ($val->status) ?  "<button class='btn btn-light btn-sm delete active' name='" . $val->forms_id."'><span class='dashicons dashicons-trash'></span></button> " : " <button class='btn btn-light btn-sm small  text-muted delete' name='" . $val->forms_id."'><span class='dashicons dashicons-yes'></span>Active ?</button>" ).
                                 
                              "</td>".

                           "</tr>";

                  }

               }


            ?>


   		</tbody>




   </table>
   <button class="btn btn-info btn-sm" id="btn_add" data-toggle="modal" data-target="#myModal"><span class="dashicons dashicons-welcome-add-page"> </span>Add Fields</button>




   <div class="modal" id="form_modal">
     <div class="modal-dialog modal-lg">
       <div class="modal-content">

         <!-- Modal Header -->
         <div class="modal-header">
           <h6 class="modal-title">Field Form </h6>
           <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>

         <!-- Modal body -->
         <div class="modal-body small">

            <form action="" class="" id="frm_setup">




               <div class="form-group">
                  
                  <label for="">Form Field :
                  
                        </br>
                      
                        <ul class="small text-muted" >
                           <li>- Every field required name attribute sample &lt;input type="text" name="field_name" &gt;  &lt;/input&gt;</li>
                           <li>- if fields required add attributes required</li>
                           <li>- button in field not include it can be complicated</li>
                        </ul>

                  </label>
                  </br>


                  <code class="text-muted small"> &lt;form id="frm_consultation" class="form" &gt;</code>
                  
                  <textarea  style="font-size: 85%;color:#bcbfc1!important;background:#333;    white-space: pre;" name="fields" id="fields"  class="form-control small text-muted" id="" rows="10">
                  </textarea>


               </div>

               <div class="form-group">

                  <label for="">Class Name : 

                        </br>
                      
                        <ul class="small text-muted" >
                           <li>- Add Class Name</li>
                           <li>- For Multiple Class Add Space</li>
                        </ul>

                  </label>

                  </br>
                  <code class="text-muted small">&lt;button type="submit" class="</code>
                  <input type="text" name="class" id="class" placeholder="Class_1 Class_2" class="form-control"> <code class="text-muted small">"> </code>
                  </br>
                  <label for="">Button Text :</label>
                  <input type="text" id="btn_text" name="btn_text" class="form-control" placeholder="Button Text" class="form-control" required>
                  <code class="text-muted small">

                     &lt;/button&gt;
                  </br>
                     &lt;/form&gt;
                  </code>
               </div>

               <div class="form-group">
                  <label for="ty-url">Redirect After Submit Form : </label>

                        </br>
                      
                        <ul class="small text-muted" >
                           <li>- if you want to stay on the page just leave it blank</li>
                           <li>- please complete the url in order to avoid 404</li>
                        </ul>

                  </label>

                  <input type="text" class="form-control" id="redirect_url" name="redirect_url" placeholder="Thank You Page Redirect : https://samplepage.cpm">

               </div>


               <div class="form-group text-right">
                  <button type="submit" class="btn btn-dark">Save Fields</button>
               </div>

            </form>
            
            <!-- <h6>History</h6> -->
            <!--  <div class="form-group">
               <table class="small table table-striped table-bordered">
                     <tr>
                        <td>Updated : 2021/12/12 21:12:00</td>
                     </tr>
                     <tr>
                        <td>Updated : 2021/12/12 21:12:00</td>
                     </tr>
                     <tr>
                        <td>Updated : 2021/12/12 21:12:00</td>
                     </tr>
                     <tr>
                        <td>Updated : 2021/12/12 21:12:00</td>
                     </tr>

               </table>
            </div> -->

         </div>

       

       </div>
     </div>
</div>
   

</div>	

<!-- 
    <?php 
       echo do_shortcode('[consultation_form id="1"]');
    ?> -->