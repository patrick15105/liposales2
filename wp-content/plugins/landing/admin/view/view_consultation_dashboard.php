<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       fb/patrick15105
 * @since      1.0.0
 *
 * @package    Landing
 * @subpackage Landing/admin/view/
 */
?>



<div class="wrap">
	

	<?php



		$data  = json_decode(do_shortcode("[form_table]"));

	?>
	
	<h4>CM FORM</h4>
	
<!-- 
	<div class="row">

		
		<div class="col-md-6">

			<h6><center>Daily Consultation</center></h6>
			<canvas id="myGraph"></canvas>
		</div>

		<div class="col-md-6">
			<h6><center>Status Sending Email</center></h6>
			<canvas id="myChart"></canvas>
		</div>
	
	</div>
	
	<h1>&nbsp</h1> -->

	<div class="row">
		<div class="col-md-12">
			

			<div class="form-inline">

				<button class="btn btn-info">Export as PDF</button>

			</div>

			<p></p>
			
			<div class="alert alert-dark" role="alert">
			  	
			  	<form action="">
			  		<div class="row">


			  		<input type="hidden" name="page" value="Landing_Consultation">
			  		
			  		<div class="col-lg-3">
			  			
			  			<?php

			  			?>
			  			<input type="text" name="filter_search"  class="form-control input-sm" placeholder="Search..." value="<?php echo (isset($_GET['filter_search']) ? $_GET['filter_search'] : '' ); ?>">

			  		</div>

			  		<div class="col-lg-3">
			  			
			  			<?php 
			  				global $charset_collate;
			  				
			  				$datestart = $dateend = "";

			  				if(isset($_GET['filter_data_range'])){

				  				$daterange = $_GET['filter_data_range'];

								$arr       =  explode("-",$daterange);
										
								$datestart = trim($arr[0]," ");

								$dateend   = trim($arr[1]," ");
							
							}
														

			  			?>
						<input type="text" class="form-control" id="filter_data_range" name="filter_data_range" datestart="<?php echo $datestart; ?>" dateend="<?php echo $dateend; ?>">

			  		</div>

			  		<div class="col-lg-2">
			  			
			  			<select name="filter_email" id="filter_email" class="form-control prio" title="Email Status">
							
							<option value="all">All Email Status</option>


							<?php 

								$selected_send   = "";

								$selected_failed = "";
						
							    if(isset($_GET['filter_email'])){

							    	if($_GET['filter_email']!="all"){

								    	if($_GET['filter_email']==1)
								    		$selected_send= "selected";

								    	if($_GET['filter_email']==0)
								    		$selected_failed= "selected";
							    	}

							    }



							    echo '<option value="1" '. $selected_send.' >Success</option>';

							    echo '<option value="0" '.$selected_failed.'>Failed</option>';


							 ?>
							
			  			</select>


			  		</div>

			  		<div class="col-lg-2">
			  			
			  			<select name="filter_sort" id="filter_sort" class="form-control prio">
							

							<option value="desc">Desc - Asc  (Date Send)</option>
							
							<?php 
						
							    $check = "";
							    if(isset($_GET['filter_sort'])){

							    	if($_GET['filter_sort']=="asc")
							    		$check = "selected";

							    }

							 ?>
							<option value="asc" <?php echo $check; ?> >Asc &nbsp  - Desc (Date Send)</option>
							
			  			</select>


			  		</div>

			  		<div class="col-lg-2">
			  			<button class="btn btn-dark">Filter</button>
			  			<a href="admin.php?page=Landing_Consultation" title="reset filter" class="btn btn-light"><span class="dashicons dashicons-update"></span></a>
			  		</div>

			  	</div>
			  	</form>

			</div>

			<table id="tb_serialize" class="table  small table-bordered table-sm">

				<thead class="alert alert-dark">
					<tr>
					   <th>Header Email</th>
					   <th>Form Field</th>
					   <th>Action</th>
					   <!-- <th>Action</th> -->
					</tr>
				</thead>
	
				<tbody>
					

					
					<?php 

	
					foreach ($data as $val) { 


				    $response =  (($val->is_email_send) ? '<span class="badge-info badge">Success</span>' : '<span class="badge-danger badge">failed</span>');

					echo '	<tr>

								<td>
									<ul class="small">
										<li><strong>CC : </strong> ' . $val->cc. ' </li>
										<li><strong>To : </strong> ' . $val->to. '</li>
										<li><strong>Source : </strong> ' . $val->source . ' </li>
										<li><strong>Date Send : </strong> '.$val->date_send.' </li>
										<li><strong>Email Status : </strong> '.$response .'</li>
										<li><strong>Email Responce : </strong> '.$val->email_error .'</li>
									</ul>
								</td>

								<td>
								
									<ul class="small">

									';

									$form_data = unserialize($val->serialize_data);

									foreach ($form_data as $val) {
										
										echo "<li><strong style='text-transform:text-transform: uppercase;'>".$val['name']." : </strong> ".$val['value']."</li>";

									}

					echo 			'</ul>
									
								</td>

								<td>
									<button class="btn btn-light btn-sm" name=""><span class="dashicons dashicons-trash"></span></button> 
								</td>

							</tr>';

						}

		



				?> 
					
				</tbody>
			</table>


		</div>
		
	</div>



</div>




