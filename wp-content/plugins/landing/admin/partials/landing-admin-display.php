<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       fb/patrick15105
 * @since      1.0.0
 *
 * @package    Landing
 * @subpackage Landing/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
