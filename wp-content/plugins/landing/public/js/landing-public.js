(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */



	$( window ).load(function() {

		var counter = 0 , // counter for error
		    
		    $form   = $(".frm_cm_consultation.cm_plug"); // form consultation

		 

		function SerializeArray(){

		}


$.ajax({

	url      : ajax.ajaxurl,
	type     : "POST",
	data     : {

		action : "finance_submit"
	}
	success  : function(data){

		console.log(data)

	}
})
		

		$(".frm_cm_consultation.cm_plug").on("submit",function(e){

		    e.preventDefault();

		    var $this_form = $(this);

		    if ( $form.data('requestRunning') ) { // to avoid running twice
		        
		        return;
		    
		    }

		    $form.data('requestRunning', true); 

		    console.log($(this).serializeArray())
			$(".consultation-overlay").addClass("active");

			$.ajax({

				url      : ajax.ajaxurl,

				data     :  { 

					action    : "consultation_submit_form", 

					serialize : $(this).serializeArray(),

					id        : $(this).attr("forms_id")

				},

				type     : "POST",

				dataType : "JSON",

				complete : function(data){

					console.log(data);

					$form.data('requestRunning', false);
					
					$(".consultation-overlay").removeClass("active");

				},

				success  : function(data){

					$(this).data('requestRunning', false);

					$(this).find(':submit').removeAttr('disabled');

					if(data.success){

						var url     = $this_form.attr("url");

						var message = $(this).find(':submit').text();

						console.log($(this).html())

						if(url.trim() != ""){

							window.location = url;

						}else{

							alert("Submit Successfully");

							location.reload();

						}

					}else{

						counter +=1;

						if(counter==0){

							alert("Please Try Again");

						}else{

							alert("form is undermaintenance please try again later");

							console.log(data);

						}

						$(this).submitting = false;

					}

				},

				error : function(data){

					console.log(data)

					$(".consultation-overlay").removeClass("active");

					alert("form is undermaintenance please try again later");
					
				}

			});


			return false;

		});


	});




})( jQuery );

