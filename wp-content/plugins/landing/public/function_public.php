<?php

		
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require_once PLUGINS_PATH . 'admin/vendor/autoload.php';


add_action( 'wp_ajax_nopriv_consultation_submit_form', 'consultation_submit_form' );
add_action( 'wp_ajax_consultation_submit_form', 'consultation_submit_form' );

function consultation_submit_form(){

	global $wpdb;



	$callback  = array(

		'success' => false,

		'message' => ""

	);

	$email = json_decode(do_shortcode('[select_form_email_settings]'));


	$data_email  = array(

	    "to"         => explode(",",$email->to),

	    "email"      => $email->email,

	    "password"   => $email->password,

	    "logo"       => $email->logo,

	    "from"       => $email->from,

	    "subject"    => $email->subject,

	    "bcc"        => explode(",", $email->bcc),

	    "cc"         => explode(",", $email->cc),

	    "reply_to"	 => $email->reply_to,

	);


	$send_email = send_form_email($data_email);

	$data  = array( 

		"form_id"             => $_POST['id'],

		"date_send"           => date("Y-m-d H:i:s"),

		"serialize_data"	  => serialize($_POST['serialize']),

		"is_email_send"       => ( ($send_email['success']) ? "1" : "0"),

		"email_error"         => ( ($send_email['success']) ? "Send successfully" : $send_email['message'] ),

		"source"              =>  "",

		"cc"				  => $email->cc,

		"to"				  => $email->to

	);




	if( $wpdb->insert($wpdb->prefix."forms_data_serialize",$data ) ) {

		// $email ="";
		// $is_on = false;

		// foreach($_POST['serialize'] as $field) {
		 
		// 	if($field['name'] =="email"){

		// 		$email = $field['value'];

		// 	}

		// 	if($field['name'] == "get_newsletter"){
		// 		 $is_on = true;
		// 	}

 
		// }


		// if($is_on){

		// 	$data_subscribe =  array(
			
		// 		'form_id' 			  => 26,

		// 		"date_send"           => date("Y-m-d H:i:s"),

		// 		"serialize_data"	  => serialize([array('email' => $email , 'date_send' => date("Y-m-d H:i:s") )])

		// 	);

		// 	$wpdb->insert($wpdb->prefix."forms_data_serialize",$data_subscribe ); 

		// 	$callback['message'] = $wpdb->error;

		// }

		$callback['success'] = true;


	} else {

		$callback['message'] = $wpdb->error;

	}

	echo json_encode( $callback  );
	wp_die();

}


function consultation_header_scripts()
{	

	 	wp_localize_script( 'custom_js', 'ajax', array(
              
              'ajaxurl' => admin_url( 'admin-ajax.php' ),

        ));
       
        wp_enqueue_script( 'custom_js', '/wp-content/plugins/landing/public/js/landing-public.js', array( 'jquery'), '1.0.0', true );
       
       
      
}

add_action('wp_enqueue_scripts', 'consultation_header_scripts');



function send_form_email($data){


	$mail = new PHPMailer(true);

	try {

	    $callback  = array(

			'success' => false,

			'message' => ""

		);
	    
	    $mail->isSMTP(); 

	    $mail->Mailer     = "smtp";              

	    $mail->Host       = 'smtp.gmail.com';  

	    $mail->SMTPAuth   = true;

		$mail->CharSet    = 'iso-8859-1';

		$mail->SMTPDebug  = 0;  
		
		$mail->From       = "centaurmarketing.co@gmail.com";
		
		$mail->FromName   = $data['from'];    


		if( $data['email'] == "" ){

			$mail->Username   = 'centaurmarketing.co@gmail.com';  

	   		$mail->Password   = 'c3NT@ur_2017@!';

		}else{

			$mail->Username   = $data['email'];  

	   		$mail->Password   = $data['password'];

		}              

	    $mail->Port       	  = 587;           

	    $mail->SMTPSecure 	  = 'tls';                               


	    $mail->addReplyTo($data['reply_to'], 'reply-to');

	    foreach ($data['to'] as $to) {

	   		$mail->addAddress( $to );     

	    }

	    foreach ($data['bcc'] as $bcc) {

	    	$mail->addBCC($bcc);

		}

		foreach ($data['cc'] as $cc) {

	    	$mail->addCC($bcc);

		}

	    $mail->isHTML(true);                                  // Set email format to HTML

	    $mail->Subject = $data['subject'];

	    $tablerow="";

	    foreach($_POST['serialize'] as $field) {
		  $tablerow  .= "<tr>
							  <td> <b>".strtoupper($field['name'])."</b> </td> 
						  	  <td>   ".$field['value']."     </td> 
						</tr>";
		}

	    $info_body     =   array(

	        "title"    =>  'CONTACT PAGE',

	        "logo_src" => get_site_url() . "/wp-content/plugins/landing/upload/".$data['logo'],

	        "message"  => "<h3> ".$data['subject']."  </h3>
	            
	        <table style='border: 1px solid #dee2e6;width: 100%;margin: 20px 0;padding:20px 10px;'>
	            
	            $tablerow

	        </table>"

	    );

	    $mail->Body    =  email_templates($info_body);

	    $mail->send();

	    $callback['success'] = true;

	} catch (Exception $e) {

	    $callback['message'] = $mail->ErrorInfo;

	}


	return $callback;

}


function email_templates($info){


    $img = "";

    if($info['logo_src']!=""){

        $img = '<img alt="" border="0" class="CToWUd" src="'.$info['logo_src'].'" style="margin:0;padding:0;max-width:600px;border:none;font-size:14px;font-weight:bold;min-height:auto;line-height:100%;outline:none;text-decoration:none;text-transform:capitalize">';
    }


    return '

    <table border="0" cellpadding="30" cellspacing="0" width="100%">
    
    <tbody>
      
      <tr>
        
        <td align="center" bgcolor="#EEEEEE" class="m_6882686408251595769frame">
          
          <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" style="border-radius:6px" width="660">
           
            <tbody>
              
              <tr>
                
                <td class="m_6882686408251595769content" style="padding:30px 30px 30px 30px;border-radius:5px">
                  
                  <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" width="600">
                    
                    <tbody>
                      
                      <tr>

                        <td align="left" id="m_6882686408251595769content-5" style="font-size:14px;font-family:Helvetica,Arial,sans-serif;line-height:25px;color:#445566">
                        
                            '.$img.'

                            <div class="m_6882686408251595769text">

                            <h3>'. $info['message'] .'</h3>
                          
                          </div>

                        </td>

                      </tr>

                    </tbody>

                  </table>


                  <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" width="600">
                    
                    <tbody>
                      
                      <tr>
                        
                        <td class="m_6882686408251595769doublespace" height="30" style="border-top:1px solid #dddddd">
                        </td>
                      
                      </tr>
                    
                    </tbody>
                  
                  </table>

                  
                  <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" width="600">
                  </table>
                
                </td>
              
              </tr>
            
            </tbody>
          
          </table>


          <table align="center" border="0" cellpadding="0" cellspacing="0" class="m_6882686408251595769devicewidth" width="660">
            
            <tbody>
              
              <tr>
                
                <td align="center" id="m_6882686408251595769content-10" style="font-family:Arial,Helvetica,sans-serif;color:#666666;font-size:10px;line-height:18px">
                  <div class="m_6882686408251595769text">
                    <p style="margin-top:30px;margin-bottom:10px"></p>
                  </div>
                
                </td>
              
              </tr>
            
            </tbody>
          
          </table>
          
          <img alt="pixel" class="CToWUd" src="https://ci3.googleusercontent.com/proxy/veswKNcBhJ0j3UYdMmHPI506oyA1UHLGcgmQK7NT0XxX6N9haL3zCQ1H6_dSOnfcIEBPOXwxYWAVmMZQAoE6pQPB_Cc8GeMHWbkJux7rmw3rn2ALiRrquyHjcedNWPpbvC8=s0-d-e1-ft#https://mailer.000webhost.com/o/112387781/44f7a37fcb14f0ddaef46ce5b6ecf1c5/7565">
        
        </td>
      
      </tr>

    </tbody>
  
  </table>';

}













?>