<div class="centaur-full centaur-fixed centaur-nodisplay centaur-modal centaur-flex" id="pop-up-wheel">
	
  <div class="centaur-container">
		
	<div class="centaur-header" style="position:relative;">
			
	    <h3 class="centaur-title">Title</h3>
			
		<span id="centaur-modal-close"><i class="fa fa-times"></i></span>

	</div>

       <div class="centaur-content" style="opacity: 1;display: flex;justify-content: center;">


            <img id="pop-up-wheel-image" src="../wp-content/uploads/2019/02/Canullas_1.jpg" style="
    max-height: 400px;">

       </div>
			
  </div>

</div>

<img class="size-medium wp-image-1120 aligncenter" src="http://beta.liposales.com/wp-content/uploads/2019/01/canullas_only.jpg" alt="" width="700px" height="auto" />
<div class="table-column">

<p>Click table row to view actual picture</p>
<table>
  <tbody>
    <tr>
      <td>1</td>
      <td>1-Port Wedge</td>
    </tr>
    <tr>
      <td>2</td>
      <td>2-Port Wedge</td>
    </tr>
    <tr>
      <td>3</td>
      <td>5-Port Wedge</td>
    </tr>
    <tr>
      <td>4</td>
      <td>8-Hole Harvest</td>
    </tr>
    <tr>
      <td>5</td>
      <td>12-Track</td>
    </tr>
    <tr>
      <td>6</td>
      <td>BAN Spiral</td>
    </tr>
    <tr>
      <td>7</td>
      <td>BD-4</td>
    </tr>
    <tr>
      <td>8</td>
      <td>Basket</td>
    </tr>
    <tr>
      <td>9</td>
      <td>Basket Multi-Hole</td>
    </tr>
  </tbody>
</table>
<table>
  <tbody>
    <tr>
      <td>10</td>
      <td>Bucket</td>
    </tr>
    <tr>
      <td>11</td>
      <td>Candy Cane</td>
    </tr>
    <tr>
      <td>12</td>
      <td>Diamond</td>
    </tr>
     <tr>
      <td>13</td>
      <td>Double Basket</td>
    </tr>
    <tr>
      <td>13</td>
      <td>Double Mercedes</td>
    </tr>
    <tr>
      <td>14</td>
      <td>Double Port Spatula</td>
    </tr>
    <tr>
      <td>15</td>
      <td>Double Port Spatula</td>
    </tr>
    <tr>
      <td>16</td>
      <td>Finesse Fiber Guided</td>
    </tr>
    <tr>
      <td>17</td>
      <td>Grater</td>
    </tr>
    <tr>
      <td>18</td>
      <td>Harvest-2</td>
    </tr>
  </tbody>
</table>
<table>
  <tbody>
    <tr>
      <td>19</td>
      <td>Harvest 8-Hole</td>
    </tr>
    <tr>
      <td>20</td>
      <td>Keel Cobra</td>
    </tr>
    <tr>
      <td>21</td>
      <td>Khouri 16-Hole</td>
    </tr>
    <tr>
      <td>22</td>
      <td>Khouri 24-Hole</td>
    </tr>
    <tr>
      <td>23</td>
      <td>Merceded</td>
    </tr>
    <tr>
      <td>24</td>
      <td>Tolero</td>
    </tr>
    <tr>
      <td>25</td>
      <td>Triple Basket</td>
    </tr>
    <tr>
      <td>26</td>
      <td>Triport</td>
    </tr>
    <tr>
      <td>27</td>
      <td>Custom (Your Name)</td>
    </tr>
  </tbody>
</table>

</div>