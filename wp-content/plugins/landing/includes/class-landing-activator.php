<?php

/**
 * Fired during plugin activation
 *
 * @link       fb/patrick15105
 * @since      1.0.0
 *
 * @package    Landing
 * @subpackage Landing/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Landing
 * @subpackage Landing/includes
 * @author     John Patrick Anonuevo <patrick@centaurmarketing.co>
 */

class Landing_Activator {


	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	global $wpdb,$charset_collate;
	$version = "0.0.1";
	$installed_version = get_option('version');

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');


	$sql = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."forms_data_serialize`(
			 `data_serialize_id` int(11) NOT NULL AUTO_INCREMENT,
			 `form_id` int(11) NOT NULL,
			 `date_send` datetime NOT NULL,
			 `source` varchar(50) NOT NULL,
			 `cc` varchar(250) NOT NULL,
			 `to` varchar(250) NOT NULL,
			 `is_email_send` int(11) NOT NULL,
			 `serialize_data` text NOT NULL,
			 `email_error` text NOT NULL,
			 PRIMARY KEY (`data_serialize_id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";

	dbDelta($sql);


	$sql = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."forms_email_settings` (
			 `forms_email_settings_id` int(11) NOT NULL AUTO_INCREMENT,
			 `logo` varchar(250) NOT NULL,
			 `updated_date` datetime NOT NULL,
			 `to` varchar(250) NOT NULL,
			 `reply_to` varchar(250) NOT NULL,
			 `cc` varchar(250) NOT NULL,
			 `subject` text NOT NULL,
			 `email` varchar(250) NOT NULL,
			 `password` varchar(250) NOT NULL,
			 `bcc` text NOT NULL,
			 `from` varchar(250) NOT NULL,
			 PRIMARY KEY (`forms_email_settings_id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";

	dbDelta($sql);

	$wpdb->insert( 
		$wpdb->prefix."forms_email_settings", 
			array( 
				'forms_email_settings_id' => 1, 
			) 
		);

	


	$sql = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."forms` (
			 `forms_id` int(11) NOT NULL AUTO_INCREMENT,
			 `fields` text NOT NULL,
			 `status` int(11) NOT NULL,
			 `created_date` datetime NOT NULL,
			 `updated_date` datetime NOT NULL,
			 `class_button` varchar(250) NOT NULL,
			 `btn_text` varchar(100) NOT NULL,
			 `redirect_url` text NOT NULL,
			 PRIMARY KEY (`forms_id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
	dbDelta($sql);




	
	}

	

}


?>