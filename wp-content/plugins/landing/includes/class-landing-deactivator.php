<?php

/**
 * Fired during plugin deactivation
 *
 * @link       fb/patrick15105
 * @since      1.0.0
 *
 * @package    Landing
 * @subpackage Landing/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Landing
 * @subpackage Landing/includes
 * @author     John Patrick Anonuevo <patrick@centaurmarketing.co>
 */
class Landing_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
