<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Woocommerce_Centaur_Addon
 * @subpackage Woocommerce_Centaur_Addon/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Woocommerce_Centaur_Addon
 * @subpackage Woocommerce_Centaur_Addon/includes
 * @author     Centaur Digital Marketing <dev@centaurmarketing.co>
 */
class Woocommerce_Centaur_Addon {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Woocommerce_Centaur_Addon_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'WOOCOMMERCE_CENTAUR_ADDON_VERSION' ) ) {
			$this->version = WOOCOMMERCE_CENTAUR_ADDON_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'woocommerce-centaur-addon';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Woocommerce_Add_To_Cart_Popup_Loader. Orchestrates the hooks of the plugin.
	 * - Woocommerce_Add_To_Cart_Popup_i18n. Defines internationalization functionality.
	 * - Woocommerce_Add_To_Cart_Popup_Admin. Defines all hooks for the admin area.
	 * - Woocommerce_Add_To_Cart_Popup_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {


		/**
		 * Setup WP autoloader function
		 */
		spl_autoload_register( array($this, 'autoloader') );

		/**
		 * Start the plugin main loader
		 */
		$this->loader = new Woocommerce_Centaur_Addon_Loader();
	}

	/**
	 * Autoloads all classes
	 *
	 * @param 	string 	$class_name 	name of the class that will be loaded
	 * @return 	boolean return "true" if class was loaded. Otherwise, return "false"
	 *
	 * @since 	1.0.0
	 * @access 	private
	 */
	private function autoloader( $class_name ) {

		// Convert the class name to the file name
        $class_file = 'class-' . str_replace( '_', '-', strtolower($class_name) ) . '.php';
 
        // Set up the list of directories to look in
        $classes_dir = array();
        $include_dir = realpath( plugin_dir_path( __FILE__ ) );
        $classes_dir[] = $include_dir;

        // Add each of the possible directories to the list
        foreach( array( 'includes' ) as $option) {
            $classes_dir[] = str_replace('app', $option, $include_dir);
        }

        // Look in each directory and see if the class file exists
        foreach ($classes_dir as $class_dir) {
            $inc = $class_dir . DIRECTORY_SEPARATOR .  $class_file;
            // If it does require it
            if (file_exists($inc)) {
                require_once $inc;
                return true;
            }
        }

        return false;

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Woocommerce_Centaur_Addon_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Woocommerce_Centaur_Addon_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Woocommerce_Centaur_Addon_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_filter( 'woocommerce_product_settings', $plugin_admin, 'plugin_settings' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Woocommerce_Centaur_Addon_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		/**
		 * Ajax Actions
		 * 
		 * @since	1.0.0
		 */
		$this->loader->add_action( 'wp_ajax_centaur_add_to_cart', $plugin_public, 'add_to_cart' );
		$this->loader->add_action( 'wp_ajax_nopriv_centaur_add_to_cart', $plugin_public, 'add_to_cart' );
		$this->loader->add_action( 'wp_ajax_centaur_remove_from_cart', $plugin_public, 'remove_from_cart' );
		$this->loader->add_action( 'wp_ajax_nopriv_centaur_remove_from_cart', $plugin_public, 'remove_from_cart' );

		/**
		 * Functions
		 * 
		 * @since	1.0.0
		 */
		// Add to Cart
		$this->loader->add_action( 'woocommerce_add_to_cart', $plugin_public, 'set_product', 10, 6 );
		$this->loader->add_filter( 'woocommerce_add_to_cart_fragments', $plugin_public, 'set_fragments', 10, 1 );
		$this->loader->add_filter( 'pre_option_woocommerce_cart_redirect_after_add', $plugin_public, 'prevent_cart_redirection', 10, 1 );

		// Products Per Page
		$this->loader->add_action( 'woocommerce_before_shop_loop', $plugin_public, 'products_per_page_init', 25 );
		$this->loader->add_filter( 'loop_shop_columns', $plugin_public, 'loop_shop_columns', 100 );
		$this->loader->add_filter( 'loop_shop_per_page', $plugin_public, 'loop_shop_per_page', 100, 1 );
		$this->loader->add_action( 'init', $plugin_public, 'products_per_page_action' );

		/**
		 * Hooks for templating
		 * 
		 * @since	1.0.0
		 */
		$this->loader->add_action( 'wp_footer', $plugin_public, 'get_modal_window' );
		$this->loader->add_action( 'cm_woo_get_key', $plugin_public, 'get_product_key' );
		$this->loader->add_action( 'cm_woo_get_thumbnail', $plugin_public, 'get_product_thumbnail' );
		$this->loader->add_action( 'cm_woo_get_name', $plugin_public, 'get_product_name' );
		$this->loader->add_action( 'cm_woo_get_total_price', $plugin_public, 'get_product_total_price' );
		$this->loader->add_action( 'cm_woo_get_qty', $plugin_public, 'get_product_quantity' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Woocommerce_Centaur_Addon_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
