<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.centaurmarketing.co
 * @since             1.0.0
 * @package           Woocommerce_Centaur_Addon
 *
 * @wordpress-plugin
 * Plugin Name:       Woocommerce Centaur Addon
 * Plugin URI:        https://www.centaurmarketing.co
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Centaur Digital Marketing
 * Author URI:        https://www.centaurmarketing.co
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woocommerce-centaur-addon
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WOOCOMMERCE_CENTAUR_ADDON_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in app/class-woocommerce-centaur-addon-activator.php
 */
function activate_woocommerce_centaur_addon() {
	require_once plugin_dir_path( __FILE__ ) . 'app/class-woocommerce-centaur-addon-activator.php';
	Woocommerce_Centaur_Addon_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in app/class-woocommerce-centaur-addon-deactivator.php
 */
function deactivate_woocommerce_centaur_addon() {
	require_once plugin_dir_path( __FILE__ ) . 'app/class-woocommerce-centaur-addon-deactivator.php';
	Woocommerce_Centaur_Addon_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_woocommerce_centaur_addon' );
register_deactivation_hook( __FILE__, 'deactivate_woocommerce_centaur_addon' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'app/class-woocommerce-centaur-addon.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_woocommerce_centaur_addon() {

	$plugin = new Woocommerce_Centaur_Addon();
	$plugin->run();

	if ( is_admin() ) {
    	new Woocommerce_Centaur_Addon_Plugin_Updater( $plugin->get_plugin_name(),  $plugin->get_version(), __FILE__ );
	}

}

run_woocommerce_centaur_addon();
