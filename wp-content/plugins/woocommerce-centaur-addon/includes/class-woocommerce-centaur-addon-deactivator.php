<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Woocommerce_Centaur_Addon
 * @subpackage Woocommerce_Centaur_Addon/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Woocommerce_Centaur_Addon
 * @subpackage Woocommerce_Centaur_Addon/includes
 * @author     Centaur Digital Marketing <dev@centaurmarketing.co>
 */
class Woocommerce_Centaur_Addon_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
