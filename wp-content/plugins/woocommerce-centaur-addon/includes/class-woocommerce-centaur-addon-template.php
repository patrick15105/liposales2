<?php

/**
 * Processes the plugin templates
 *
 * @link       www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/includes
 */

/**
 * Processes the plugin templates
 *
 * This is used for processing the templates that will be used
 * throughout the plugin. 
 *
 * @since      1.0.0
 * @package    Centaur_Form
 * @subpackage Centaur_Form/includes
 * @author     Centaur Marketing <dev@centaurmarketing.co>
 */
class Woocommerce_Centaur_Addon_Template {


	/**
	 * Retrieve the general template
	 *
	 * @since	1.0.0
	 * @param	string	$slug	Template slug
	 * @param	string	$name	Template name (default: '')
	 * @param 	boolean	$load	If template will be loaded (default: true)
	 */
	public static function get_template( $slug, $name = '', $load = true) {

		$template_name = ( $name == '') ? "{$slug}.php" : "{$slug}-{$name}.php" ;

		Woocommerce_Centaur_Addon_Template::process_template( $template_name, '', $load );
		
	}

	/**
	 * Retrieve the template part
	 *
	 * @since	1.0.0
	 * @param	string	$slug	Template slug
	 * @param	string	$name	Template name (default: '')
	 * @param 	boolean	$load	If template will be loaded (default: true)
	 */
	public static function get_template_part( $slug, $name = '', $load = true) {

		$template_name = ( $name == '') ? "{$slug}.php" : "{$slug}-{$name}.php" ;

		Woocommerce_Centaur_Addon_Template::process_template( $template_name, 'parts', $load );

	}

	/**
	 * Process Templates
	 *
	 * @since	1.0.0
	 * @param	string	$$template_name	Template name (default: '')
	 * @param	string	$subdir			Target Subdirectory (default: '')
	 * @param 	boolean	$load			If template will be loaded (default: true)
	 */
	private static function process_template( $template_name, $subdir = '', $load = true ) {

		$template 		= '';
		$template_name 	= ( $subdir != '' ) ? 
							"{$subdir}/{$template_name}" :
							"{$template_name}" ;

		// Get template in Child Theme
		if ( file_exists( trailingslashit( get_stylesheet_directory( ) ) . "centaur/{$template_name}" ) ) {

			$template = trailingslashit( get_stylesheet_directory( ) ) . "centaur/{$template_name}";

		// Get template in Parent Theme
		} elseif ( file_exists( trailingslashit( get_template_directory( ) ) . "centaur/{$template_name}" ) ) {

			$template = trailingslashit( get_template_directory( ) ) . "centaur/{$template_name}";

		// Get template from Plugin folder
		} elseif ( file_exists( plugin_dir_path( dirname( __FILE__ ) ) . "templates/{$template_name}" ) ) {

			$template = plugin_dir_path( dirname( __FILE__ ) ) . "templates/{$template_name}";

		}

		// Verify if template will be included
		if ( $template != '' && !$load ) {

			include $template;

		} else {

			// Allow 3rd party plugins to filter template file from their plugin.
			$template = apply_filters( 'get_template_parts', $template, $slug, $name );

			if ( $template ) {

				load_template( $template, false );

			}

		}

	}

}