<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Woocommerce_Centaur_Addon
 * @subpackage Woocommerce_Centaur_Addon/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Woocommerce_Centaur_Addon
 * @subpackage Woocommerce_Centaur_Addon/admin
 * @author     Centaur Digital Marketing <dev@centaurmarketing.co>
 */
class Woocommerce_Centaur_Addon_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Centaur_Addon_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Centaur_Addon_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugins_url() . '/' . $this->plugin_name .'/assets/css/styles.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Centaur_Addon_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Centaur_Addon_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugins_url() . '/' . $this->plugin_name .'/assets/admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Plugin Settings 
	 *
	 * @since    1.0.0
	 */
	public function plugin_settings( $settings ) {


		// Set default options to (3|6|9|All) rows
		$dropdown_options_default =
			( apply_filters( 'loop_shop_columns', 4 ) * 3 ) . ' ' .
			( apply_filters( 'loop_shop_columns', 4 ) * 6 ) . ' ' .
			( apply_filters( 'loop_shop_columns', 4 ) * 9 ) . ' ' .
			'-1';

		$new_settings = array(

			array(
				'type' 	=> 'sectionend',
				'id' 	=> 'centaur_start'
			),

			array(
				'title' => 'Products Per Page',
				'type' 	=> 'title',
				'desc' 	=> '',
				'id' 	=> 'wppp_title'
			),

			array(
				'title'			=> __( 'List of products options', $this->plugin_name ),
				'desc'			=> __( 'Seperated by spaces <em>(-1 for all products)</em>',  $this->plugin_name ),
				'id'			=> 'centaur_dropdown_options',
				'default'		=> $dropdown_options_default,
				'type'			=> 'text',
			),

			array(
				'title'			=> __( 'Default products per page', $this->plugin_name ),
				'desc'			=> __( '-1 for all products',  $this->plugin_name ),
				'id'			=> 'centaur_default_ppp',
				'default'		=> apply_filters( 'loop_shop_per_page', get_option( 'posts_per_page' ) ),
				'css'			=> 'width:50px;',
				'type'			=> 'number',
			),

			array(
				'title'			=> __( 'Number of columns', $this->plugin_name ),
				'desc'			=> __( '',  $this->plugin_name ),
				'id'			=> 'centaur_shop_columns',
				'default'		=> apply_filters( 'loop_shop_columns', 4 ),
				'css'			=> 'width:50px;',
				'custom_attributes' => array(
					'min'	=> 0,
					'step' => 1,
				),
				'type'			=> 'number',
			),

			array(
				'type' 	=> 'sectionend',
				'id' 	=> 'centaur_options'
			),

		);

		return array_merge( $settings, $new_settings );

	}

}
