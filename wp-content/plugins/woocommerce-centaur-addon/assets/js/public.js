jQuery(document).ready(function($){
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	/**
	 * Add to Cart
	 *
	 * @since  1.0.0
	 */
	$(document).on( 'submit', '.woocommerce form.cart', function(e){

		e.preventDefault();
		var btn			= $(this).find('button[type="submit"]'),
			frm_data	= $(this).serializeArray();

		btn.find('.fa').remove();
		btn.append('<i class="fa fa-spinner"><i>');

        if( btn.attr('name') && btn.attr('name') == 'add-to-cart' && btn.attr('value') ) {
            frm_data.push( { name: 'add-to-cart', 
            				 value: btn.attr('value') } );
        }

		frm_data.push( { name: 'action',
					 value: 'centaur_add_to_cart'} );

		$( document.body ).trigger( 'adding_to_cart', [ btn, frm_data ] );

		$.ajax({
			url: centaur.ajax_url,
			type: 'POST',
			data: $.param(frm_data),
			error: function( response ) {
				console.log( response );
			},
			success: function(response) {

				btn.find('.fa').remove();

				if(response.fragments) {
					// Trigger event so themes can refresh other areas.
					$( document.body ).trigger( 'added_to_cart', [ response.fragments, response.cart_hash, btn ] );
					btn.append('<i class="fa fa-check"></i>');
				} else if(response.error){
					show_notice('error',response.error)
				} else{
					console.log(response);
				}
			}
		});

	});


	/**
	 * Remove Product
	 *
	 * @since  1.0.0
	 */
	$(document).on( 'click', '#centaur-remove-item', function(e) {
		e.preventDefault();

		var item_key = $(this).parents('tr').data('product_key'),
			quantity = $(this).parents('tr').data('qty');

		$.ajax({
			url: centaur.ajax_url,
			type: 'POST',
			data: { action: 'centaur_remove_from_cart',
					cart_key: item_key,
					qty: quantity
			},
			success: function(response) {

				if (response.fragments) {

					var fragments = response.fragments,
						cart_hash =  response.cart_hash;

					//Set fragments
			   		$.each( response.fragments, function( key, value ) {
						$( key ).replaceWith( value );
						$( key ).stop( true ).css( 'opacity', '1' ).unblock();
					});

			   		if(wc_cart_fragments_params){
				   		var cart_hash_key = wc_cart_fragments_params.ajax_url.toString() + '-wc_cart_hash';
						//Set cart hash
						sessionStorage.setItem( wc_cart_fragments_params.fragment_name, JSON.stringify( fragments ) );
						localStorage.setItem( cart_hash_key, cart_hash );
						sessionStorage.setItem( cart_hash_key, cart_hash );
					}

					$(document.body).trigger('wc_fragments_loaded');

					$('.centaur-content').html('Item removed from cart');
					$('form.cart button[type="submit"]').find('.fa').remove();

				} else {
					console.log(response);
				}

			}

		});

	});


	 /**
	  * Close Modal Window
	  * 
	  * @since  1.0.0
	  */
	 $('.centaur-container #centaur-modal-close').each( function(){

	 	$(this).on( 'click', function() {

		 	$('.centaur-modal').css("display", "none");
	 		$('.centaur-content').html('');

	 	});

	 });


	 /**
	  * Open Modal Window
	  * 
	  * @since  1.0.0
	  */
	$(document.body).on('added_to_cart',function(){
	 	$('.centaur-modal').css("display", "flex");
		$('form.cart .added_to_cart').remove();
	});

/*
	$('.centaur-component .centaur-select').each( function() {

		var height = $(this).find('.centaur-list').outerHeight() + 30;

		$(this).on('click', function() {

			if ( $(this).outerHeight() == 30 ) {
				$(this).css('height', height);
			} else {
				$(this).css('height', 30);
			}

		});

	});

	$('.centaur-select .centaur-item').each( function() {

		var value = $(this).text();

		$(this).on('click', function() {

			$('.centaur-select .centaur-item').removeClass('selected');
			$(this).addClass('selected');

			$('.centaur-select .centaur-label').text(value);

			window.location.href = window.location.pathname+"?"+$.param({'count': value})

		});

	});*/


	
	function fixHeight(){

		if($("body").hasClass('woocommerce-page')){

			var filterNavHeight = $("#secondary").height() + 50;
			
			$(".site-main").css({"min-height":filterNavHeight+"px"});



		}


	}


	fixHeight();

	/**
	 * Filter Dropdown 
	 * 
	 * @since  1.0.0
	 */
// 	$('.centaur-select').each( function() {

// 		var parent			= $(this),
// 		 	header_height	= parent.find('.centaur-header').outerHeight(),
// 			list_height		= parent.find('.centaur-list').outerHeight(),
// 			total_height	= header_height + list_height;

// 		parent.on( 'click', function(event) {
// 			event.preventDefault();
// 			parent.css( 'height', ( ( parent.outerHeight() == header_height ) ? total_height : header_height ) );
// 		});

// 		parent.find('.centaur-item').each( function() { 

// 			var value 	= $(this).attr("data-value"),
// 				vars 	= {},
// 				hash,
// 				hashes 	= window.location.href.slice( window.location.href.indexOf('?') + 1).split('&'),
// 				qryVar 	= '?';

// 		    for(var i = 0; i < hashes.length; i++)
//     		{
// 		        hash = hashes[i].split('=');
// 		        if ( hash.length > 1 ) {

// 		        	var index = hash[0],
// 		        		val = hash[1];

// 			        vars[index] = val;

// 		        }

//     		}

// 			$(this).on( 'click', function(event) {

// 				event.preventDefault();

// 				if ( vars['count'] != '' ) {
// 					vars['count'] = value;
// 				}

// 				window.location.href = window.location.pathname+"?"+$.param( vars );

// 			});			

// 		});

// 	});
    $('.centaur-select').on("mouseleave",function(){

	 	$(this).removeClass("active");
	 	$(this).css("height",$(this).find('.centaur-header').outerHeight())
	 })

	$('.centaur-select').each( function() {

	
		var parent			= $(this),
		 	header_height	= parent.find('.centaur-header').outerHeight(),
			list_height		= parent.find('.centaur-list').outerHeight(),
			total_height	= header_height + list_height;

		parent.on( 'click', function(event) {
			
			event.preventDefault();
			
			if(parent.outerHeight() == header_height){
				parent.css('height',total_height).addClass('active')
			}else{
				parent.css('height',header_height).removeClass('active')

			}

		});


        if($(this).hasClass("orderbyfilter") != true){

			parent.find('.centaur-item').each( function() {

				var value 	= $(this).attr("data-value"),
					vars 	= {},
					hash,
					hashes 	= window.location.href.slice( window.location.href.indexOf('?') + 1).split('&'),
					qryVar 	= '?';

			    for(var i = 0; i < hashes.length; i++)
	    		{
			        hash = hashes[i].split('=');
			        if ( hash.length > 1 ) {

			        	var index = hash[0],
			        		val = hash[1];

				        vars[index] = val;

			        }

	    		}

				$(this).on( 'click', function(event) {

					event.preventDefault();
                    value 	= $(this).attr("data-value");
					if ( vars['count'] != '' ) {
						vars['count'] = value;
					}


					window.location.href = window.location.pathname+"?"+$.param( vars );

				});			

			});

		}else{


			   parent.find('.centaur-item').each( function() {

				   $(this).click(function(e){

						e.preventDefault();

						window.location.href = window.location.pathname+"?"+"filters=order-"+$(this).attr("id");
						// parent.find(".centaur-label").html("Sort by " +$(this).attr("id"))
						// $(".orderby option[value='"+$(this).attr("id")+"']").prop("selected","selected")
					 //    $(".orderby").val($(this).attr("id")).trigger("change");


					})


				});

		}

	});

});