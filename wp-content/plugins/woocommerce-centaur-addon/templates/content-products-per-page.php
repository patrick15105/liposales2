<?php

/**

 *

 * This template part is for Single Procedure display

 *

 * @link       www.centuarmarketing.co

 * @since      1.0.0

 *

 * @package    Woocommerce_Add_To_Cart_Popup

 * @subpackage Woocommerce_Add_To_Cart_Popup/template

 */

global $centaur_product_count;

defined( 'ABSPATH' ) || exit;



?>

<div class="centaur-component centaur-inline" id="products-per-page" data-show-products="3">

	<div class="centaur-header">

		<div class="centaur-label">Show:</div>

	</div>

	<div class="centaur-select">

		<div class="centaur-header">

			<div class="centaur-label"><?php _e( ( ( isset($_REQUEST['count']) && $_REQUEST['count'] != "" ) ? ( ( $_REQUEST['count'] == "-1" ) ? "View All" : $_REQUEST['count']) : $centaur_product_count[0] ) , 'woocommerce-centaur-addon' ); ?></div>

			<i class="fa fa-chevron-down"></i>

		</div>

		<ul class="centaur-list">

			<?php 

			foreach ( $centaur_product_count as $product_option ) {

				if ( $product_option == -1 || ( $product_option == -1 && isset($_REQUEST['count']) && $_REQUEST['count'] == "-1" ) ) {
					echo '<li class="centaur-item" ' . ( ( isset($_REQUEST['count']) && $_REQUEST['count'] == "-1" ) ? 'selected' : '' ) . ' data-value="-1">View All</li>';
				} else {
					echo '<li class="centaur-item" ' . ( ( isset($_REQUEST['count']) && $_REQUEST['count'] == $product_option ) ? 'selected' : '' ) . ' data-value="' . $product_option . '">' . $product_option . '</li>';
				}  

			}

			?>

		</ul>

	</div>

</div>