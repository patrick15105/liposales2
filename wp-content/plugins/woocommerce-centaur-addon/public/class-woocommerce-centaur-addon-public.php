<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Woocommerce_Centaur_Addon
 * @subpackage Woocommerce_Centaur_Addon/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Woocommerce_Centaur_Addon
 * @subpackage Woocommerce_Centaur_Addon/public
 * @author     Centaur Digital Marketing <dev@centaurmarketing.co>
 */
class Woocommerce_Centaur_Addon_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $templater;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name 	= $plugin_name;
		$this->version 		= $version;
		$this->templater 	= new Woocommerce_Centaur_Addon_Template();

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Centaur_Addon_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Centaur_Addon_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugins_url() . '/' . $this->plugin_name .'/assets/css/styles.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Centaur_Addon_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Centaur_Addon_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( 
			$this->plugin_name, 
			plugins_url() . '/' . $this->plugin_name .'/assets/js/public.js', 
			array( 'jquery' ), 
			$this->version, 
			false 
		);

		wp_localize_script( 
			$this->plugin_name,
			'centaur',
			array( 
				'ajax_url' => admin_url('admin-ajax.php') 
			) 
		);

	}


	/**
	 * Getter Functions
	 *
	 * @since	1.0.0
	 */

	/**
	 * Get the Modal Window Template
	 *
	 * @since	1.0.0
	 */
	public function get_modal_window() {

		if ( is_cart() || is_checkout() ) {
			return;
		}

		$this->templater->get_template( 'content', 'modal' );

	}
	
	/**
	 * Get the Modal Content
	 *
	 * @since	1.0.0
	 */
	public function get_content() {

		ob_start();

		$this->templater->get_template_parts( 'modal/content', 'body');

		return ob_get_clean();

	}

	/**
	 * Get Product Key
	 *
	 * @since	1.0.0
	 * @param 	boolean	$echo	Display item. (Default: true)
	 */
	public function get_product_key( $echo = true ) {

		global $cm_wc_product;

		echo $cm_wc_product['item_key'];

	}

	/**
	 * Get Product Thumbnail
	 *
	 * @since	1.0.0
	 * @param 	boolean	$echo	Display item. (Default: true)
	 */
	public function get_product_thumbnail( $echo = true ) {

		global $cm_wc_product;

		$product = apply_filters( 'woocommerce_cart_item_product', $cm_wc_product['data'] );

		echo apply_filters( 'woocommerce_cart_item_thumbnail', $product->get_image() );

	}

	/**
	 * Get Product Name
	 *
	 * @since	1.0.0
	 * @param 	boolean	$echo	Display item. (Default: true)
	 */
	public function get_product_name( $echo = true ) {

		global $cm_wc_product;

		$product = apply_filters( 'woocommerce_cart_item_product', $cm_wc_product['data'] );

		echo apply_filters( 'woocommerce_cart_item_name', $product->get_title() );

	}

	/**
	 * Get Product Total Price
	 *
	 * @since	1.0.0
	 * @param 	boolean	$echo	Display item. (Default: true)
	 */
	public function get_product_total_price( $echo = true ) {

		global $cm_wc_product;

		$product = apply_filters( 'woocommerce_cart_item_product', $cm_wc_product['data'] );

		echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $product, $cm_wc_product['cart_item_qty'] ) );

	}	

	/**
	 * Get Product Quantity
	 *
	 * @since	1.0.0
	 * @param 	boolean	$echo	Display item. (Default: true)
	 */
	public function get_product_quantity( $echo = true ) {

		global $cm_wc_product;

		echo $cm_wc_product['cart_item_qty'];

	}


	/**
	 * Setter Functions
	 *
	 * @since	1.0.0
	 */

	/**
	 * Prevent Redirection to Shopping Cart
	 *
	 * @since	1.0.0
	 */
	public function prevent_cart_redirection( $value ) {

		if ( !is_admin() ) {
			return 'no';
		}

		return $value;
	}
	
	/**
	 * Add to Cart
	 *
	 * @since	1.0.0
	 */
	public function add_to_cart() {

		global $woocommerce;

		if ( !isset($_POST['action']) || $_POST['action'] != 'centaur_add_to_cart' ) {
			die();
		}

		$error 	= wc_get_notices( 'error' );
		$html 	= '';

		if( $error ) {

			// print notice
			ob_start();
			foreach( $error as $value ) {
				wc_print_notice( $value, 'error' );
			}

			$js_data =  array( 'error' => ob_get_clean() );

			wc_clear_notices(); // clear other notice
			wp_send_json($js_data);

		} else {

			do_action( 'woocommerce_ajax_added_to_cart', intval( $_POST['add-to-cart'] ) );

			wc_clear_notices(); // clear other notice
			WC_AJAX::get_refreshed_fragments();	
		}

		die();		

	}
	
	/**
	 * Remove from Cart
	 *
	 * @since	1.0.0
	 */
	public function remove_from_cart() {

		$key = sanitize_text_field($_POST['cart_key']);
		$cart = WC()->cart->get_cart();
		$_product	= $cart[$key];		
		$new_qty	= $_product['quantity'] - intval( $_POST['qty'] );

		if ( $new_qty < 0 || !$key ) {
			wp_send_json( array( 'error' => __( 'Something went wrong', 'side-cart-woocommerce' ) ) );
		}

		$is_success = ( $new_qty == 0 ) ? 
						WC()->cart->remove_cart_item( $key ) : 
						WC()->cart->set_quantity( $key, $new_qty ) ;

		if ( $is_success ) {

			$this->set_product( $key, '', '', '', '','' );
			WC_AJAX::get_refreshed_fragments();

		} elseif ( wc_notice_count( 'error' ) > 0 ) {

	    	echo wc_print_notices();

		}

		die();

	}

	/**
	 * Set Fragments
	 *
	 * @since	1.0.0
	 */
	public function set_fragments( $fragments ) {

		//Cart content
		$fragments['div.centaur-content'] = '<div class="centaur-content">' . $this->get_content() . '</div>';

		return $fragments;
	}

	/**
	 * Set Product
	 *
	 * @since	1.0.0
	 */
	public function set_product($cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data){

		global $cm_wc_product;

		$cart = WC()->cart->get_cart();
		$cm_wc_product = $cart[$cart_item_key];
		$cm_wc_product['item_key'] = $cart_item_key;
		$cm_wc_product['cart_item_qty'] = $_POST['quantity'];


	}


	/**
	 * Products Per Page Functions
	 *
	 * Frontend functions used for displaying a number of products per page
	 * 
	 * @since	1.0.0
	 */
	
	/**
	 * Trigger Products Per Page
	 *
	 * Show options for users to choose the number of products that
	 * will be displayed on each page.
	 *
	 * @since	1.0.0
	 */
	public function products_per_page_init() {

		// Display only in Categories
		if ( !woocommerce_products_will_display() ) {
			return;
		}

		$this->templater->get_template( 'content', 'products-per-page' );

	}

	/**
	 * Shop columns.
	 *
	 * Set number of columns (products per row).
	 *
	 * @since 1.2.0
	 *
	 * @param int $columns Current number of shop columns.
	 * @return int Number of columns.
	 */
	public function loop_shop_columns( $columns ) {

		return 3;

	}


	/**
	 * Per page hook.
	 *
	 * Return the number of products per page to the hook
	 *
	 * @since 1.2.0
	 *
	 * @return int Products per page.
	 */
	public function loop_shop_per_page( $per_page ) {

		if ( isset( $_REQUEST['count'] ) ) :
			$per_page = intval( $_REQUEST['count'] );
		elseif ( isset( $_COOKIE['woocommerce_products_per_page'] ) ) :
			$per_page = $_COOKIE['woocommerce_products_per_page'];
		else :
			$per_page = intval( get_option( 'wppp_default_ppp', '12' ) );
		endif;

		return $per_page;

	}


	/**
	 * Posts per page.
	 *
	 * Set the number of posts per page on a hard way, build in fix for many themes who override the offical loop_shop_per_page filter.
	 *
	 * @since 1.2.0
	 *
	 * @param	object 	$q		Existing query object.
	 * @param	object	$class	Class object.
	 * @return 	object 			Modified query object.
	 */
	public function woocommerce_product_query( $q, $class ) {

		if ( function_exists( 'woocommerce_products_will_display' ) && woocommerce_products_will_display() && $q->is_main_query() ) :
			$q->set( 'posts_per_page', $this->loop_shop_per_page() );
		endif;

	}


	/**
	 * PPP action.
	 *
	 * Set the number of products per page when the customer
	 * changes the amount in the drop down.
	 *
	 * @since 1.2.0
	 */
	public function products_per_page_action() {

		if( isset($_REQUEST['count']) ) {
			wc_setcookie( 'woocommerce_products_per_page', intval( $_REQUEST['count'] ), time() + DAY_IN_SECONDS * 2, apply_filters( 'wc_session_use_secure_cookie', false ) );
		}

	}
}