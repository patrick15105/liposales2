<?php

/**
 * 
 * 
 * @link       www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/admin
 */

/**
 * 
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/admin
 * @author     Centaur Marketing <dev@centaurmarketing.co>
 */

class Centaur_Form_Admin_Entry {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	public $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Database object for DB
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $dbObject    Database Object
	 */
	private $dbObject;

	/**
	 * Database table for Forms
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $dbTable    Database Table
	 */
	private $dbTable;

	/**
	 * Object Item
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $item
	 */
	private $item;

	/**
	 * Object Type
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      string    $object    
	 */
	public $object;

	/**
	 * Constructor
	 *
	 * @since	1.0.0
	 */
	public function __construct( $plugin_name, $version, $entry_id = "" ) {

		global $wpdb;

		$this->plugin_name 	= $plugin_name;
		$this->version 		= $version;
		$this->dbObject		= $wpdb;
		$this->dbTable		= "{$wpdb->prefix}centform_entry";

		if ( $entry_id != "" && is_numeric( $entry_id ) ) {

			// Get Technical Details of the Entry
			$this->get_technical_info( $entry_id );

			// Retrieve User System Information
			$this->item[0]['title'] 	= $this->get_form_title( $this->item[0]['form_id'] );
			$this->item[0]['os'] 		= $this->get_system_details( $this->item[0]['user_agent'], "os" );
			$this->item[0]['device'] 	= $this->get_system_details( $this->item[0]['user_agent'], "device" );
			$this->item[0]['browser'] 	= $this->get_system_details( $this->item[0]['user_agent'], "browser" );

			// Get Field Details of the Entry
			$this->get_meta_info( $entry_id );

			$this->item = (object) $this->item[0];

		}

	}

	/**
	 * Retrieve Form data
	 *
	 * @since	1.0.0
	 * @param	int		$per_page		Records per page
	 * @param	int		$page_number	Current page number
	 *
	 * @return	mixed 
	 */
	private function get_record( $conditions, $return_type = "OBJECT", $table = "", $subtable = "" ) {

		$sql 	= "";
		$count 	= 1; 

		foreach ( $conditions as $key => $value ) {

			$sql .= $key . '="' . $value . '"';

			if ( ( $count + 1 ) < count( $conditions ) ) {
				$sql .= ' AND ';
			}

			$count++;

		}

		return $this->dbObject->get_results( "SELECT * FROM " . ( ( $table != "" ) ? $this->dbObject->prefix . $table : $this->dbTable ) . $subtable . " WHERE " . $sql, $return_type );

	}

	/**
	 * Retrieve Technical Information
	 *
	 * @since	1.0.0
	 * @param	int		$entry_id		Entry ID
	 */
	public function get_technical_info( $entry_id ) {

		$this->item = $this->get_record( array( "id" => $entry_id ), "ARRAY_A" );

	}

	/**
	 * Retrieve Technical Information
	 *
	 * @since	1.0.0
	 * @param	int		$entry_id		Entry ID
	 */
	public function get_meta_info( $entry_id ) {

		$details = $this->get_record( array( "entry_id" => $entry_id ), "OBJECT", "", "_meta" );

		foreach ( $details as $detail ) {
			$this->item[0]['fields'][$detail->meta_key] = $detail->meta_value;
		}

	}

	/**
	 * Retrieve Item
	 *
	 * @since	1.0.0
	 * @return	object
	 */
	public function get_item() {

		return $this->item;

	}

	/**
	 * Retrieve Form Title
	 *
	 * @since	1.0.0
	 * @param	int		$form_id		Form ID
	 * @return	string
	 */
	public function get_form_title( $form_id ) {

		$details = $this->get_record( array( "id" => $form_id ), "ARRAY_A", "centform_form", "" ); 

		return $details[0]['title'];

	}

	/**
	 * Retrieve System Info
	 *
	 * @since	1.0.0
	 * @param	string	$user_agent		User Agent
	 * @param	string	$type			Default: os. System Type.
	 * @return	string
	 */
	private function get_system_details( $user_agent, $type = "os" )
	{

		$systems     = array('/windows phone/i'    	=>  'Windows Phone 8',
	                         '/windows nt 10.0/i'    =>  'Windows 10',
	                         '/windows nt 6.3/i'     =>  'Windows 8.1',
	                         '/windows nt 6.2/i'     =>  'Windows 8',
	                         '/windows nt 6.1/i'     =>  'Windows 7',
	                         '/windows nt 6.0/i'     =>  'Windows Vista',
	                         '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
	                         '/windows nt 5.1/i'     =>  'Windows XP',
	                         '/windows xp/i'         =>  'Windows XP',
	                         '/windows nt 5.0/i'     =>  'Windows 2000',
	                         '/windows me/i'         =>  'Windows me',
	                         '/win98/i'              =>  'Windows 98',
 	                         '/win95/i'              =>  'Windows 95',
	                         '/win16/i'              =>  'Windows 3.11',
                             '/macintosh|mac os x/i' =>  'Mac OS X',
	                         '/mac_powerpc/i'        =>  'Mac OS 9',
    	                     '/linux/i'              =>  'Linux',
        	                 '/ubuntu/i'             =>  'Ubuntu',
            	             '/iphone/i'             =>  'iPhone',
                	         '/ipod/i'               =>  'iPod',
                    	     '/ipad/i'               =>  'iPad',
                        	 '/android/i'            =>  'Android',
                             '/blackberry/i'         =>  'BlackBerry',
	                         '/webos/i'              =>  'Mobile');

		$browsers    = array('/msie/i'       		 =>  'Internet Explorer',
    	                     '/firefox/i'    		 =>  'Firefox',
            	             '/chrome/i'     		 =>  'Chrome',
        	                 '/safari/i'     		 =>  'Safari',
                	         '/opera/i'      		 =>  'Opera',
                    	     '/netscape/i'   		 =>  'Netscape',
                        	 '/maxthon/i'    		 =>  'Maxthon',
                             '/konqueror/i'  		 =>  'Konqueror',
	                         '/mobile/i'     		 =>  'Handheld Browser');

		$items = ( $type == "browser" ) ? $browsers : $systems ;

	    foreach ($items as $regex => $value) {

        	if (preg_match($regex, $user_agent)) {

		    	if ( $type == 'device' ) {
		    		return ( !preg_match( '/(windows|mac|linux|ubuntu|phone)/i', $value ) )
                      ? 'Mobile' : 'Desktop' ;
	    		} else {
		    		return $value;
	    		}

    		}

    	}

    	return "Unknown";

 	}

}