<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/admin
 * @author     Centaur Marketing <dev@centaurmarketing.co>
 */
class Centaur_Form_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Centaur_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Centaur_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_style( 
			'font_awesome', 
			plugins_url() . '/' . $this->plugin_name .'/assets/css/font-awesome/css/font-awesome.min.css', 
			array(), 
			$this->version, 
			'all' );

		wp_enqueue_style( 
			$this->plugin_name, 
			plugins_url() . '/' . $this->plugin_name .'/assets/css/styles.css', 
			array(), 
			$this->version, 
			'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Centaur_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Centaur_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		
		wp_enqueue_media();
		
		wp_enqueue_script( 
			$this->plugin_name, 
			plugins_url() . '/' . $this->plugin_name .'/assets/js/form-handler.js', 
			array( 'jquery' ), 
			$this->version, 
			false 
		);

		wp_localize_script( 
			$this->plugin_name,
			'centaur_builder',
			array( 
				'ajax_url' 	=> admin_url('admin-ajax.php'),
				'nonce'		=> wp_create_nonce( $this->plugin_name . "-builder" ),
				'save_exit'	=> admin_url( 'admin.php?page=centaur-form' ),
				'save_next'	=> admin_url( 'admin.php?page=centaur-form-build' )
			) 
		);

	}

	/**
	 * Adds a menu on the WP Admin Dashboard
	 *
	 * @since	1.0.0
	 */
	public function add_admin_menu() {


		// Main
		$hook = add_menu_page(
					'All Entries', 
					'Centaur Forms',
					'manage_options',
					$this->plugin_name,
					array($this, 'display_form_entries')
				);

		// View All Submenu
		add_submenu_page(
			$this->plugin_name,
			'Inbox',
			'Inbox',
			'manage_options',
			$this->plugin_name,
			array($this, 'display_form_entries')
		);

		// Entries Submenu
		$hook2 = add_submenu_page(
					$this->plugin_name,
					'Forms',
					'View Forms',
					'manage_options',
					$this->plugin_name . '-forms',
					array($this, 'display_forms')
				);

		// Create Form Submenu
		add_submenu_page(
			$this->plugin_name,
			'Centaur Form',
			'Create Form',
			'manage_options',
			$this->plugin_name . '-build',
			array($this, 'display_form_builder')
		);

		// Settings Submenu
		add_submenu_page(
			$this->plugin_name,
			'Settings',
			'Settings',
			'manage_options',
			$this->plugin_name . '-settings',
			array($this, 'display_form_settings')
		);

		// Statistics Submenu
		add_submenu_page(
			$this->plugin_name,
			'Statistics',
			'Statistics',
			'manage_options',
			$this->plugin_name . '-statistics',
			array($this, 'display_form_stat')
		);

		if( !isset( $_REQUEST['entry_id'] ) && empty( $_REQUEST['entry_id'] ) ) {
			add_action( "load-$hook", array( $this, 'screen_option_entries' ) );
		}

		add_action( "load-$hook2", array( $this, 'screen_option_forms' ) );

	}

	/**
	 * Displays the forms
	 *
	 * @since	1.0.0
	 */
	public function display_forms() {

		Centaur_Form_Template::get_template("admin/archive", "forms", false);

	}

	/**
	 * Screen options for Form List
	 *
	 * @since	1.0.0
	 */
	public function screen_option_forms() {

		global $cf_forms;

		$option = 'per_page';
		$args   = [
			'label'   => 'Forms',
			'default' => 5,
			'option'  => 'forms_per_page'
		];

		add_screen_option( $option, $args );

		$cf_forms = new Centaur_Form_Admin_List( $this->plugin_name, $this->version );

	}

	/**
	 * Set Form options
	 *
	 * @since	1.0.0
	 */
	public function set_option($status, $option, $value) {
 
	    if ( 'forms_per_page' == $option || 'entries_per_page' == $option) {
			return $value;
	    }
 
    	return $status;
 
	}

	/**
	 * Display the build form
	 *
	 * @since	1.0.0
	 */
	public function display_form_builder() {

		global $cent_form;

		$cent_form = new Centaur_Form_Admin_Builder( $this->plugin_name, $this->version );

		if ( isset( $_GET['action'] ) && $_GET['action'] == 'edit' && !empty($_GET['form_id']) ) {
			$cent_form->set_item( $_GET['form_id'] );
		}

		Centaur_Form_Template::get_template("admin/builder", "form", false);

	}

	/**
	 * Displays the form entries
	 *
	 * @since	1.0.0
	 */
	public function display_form_entries() {

		if( !isset( $_REQUEST['entry_id'] ) && empty( $_REQUEST['entry_id'] ) ) {
			Centaur_Form_Template::get_template("admin/archive", "entries", false);
		} else {

			global $cent_entry;

			$cent_entry = new Centaur_Form_Admin_Entry( $this->plugin_name, $this->version, $_REQUEST['entry_id'] );

			Centaur_Form_Template::get_template("admin/single", "entry", false);
		}

	}

	/**
	 * Screen options for Entries List
	 *
	 * @since	1.0.0
	 */
	public function screen_option_entries() {

		global $cf_entries;

		$option = 'per_page';
		$args   = [
			'label'   => 'Entries',
			'default' => 5,
			'option'  => 'entries_per_page'
		];

		add_screen_option( $option, $args );

		$cf_entries = new Centaur_Form_Admin_List( $this->plugin_name, $this->version, "entry" );

	}

	/**
	 * Register the form settings
	 * 
	 * @since	1.0.0
	 * @since   1.1.0 	Removed autoresponse. Added from name and from email.
	 */
	public function register_plugin_settings() {

		// Register Settings
		register_setting( 'centaur-form-settings', 'preloader-path' );
		register_setting( 'centaur-form-settings', 'header-path' );
		register_setting( 'centaur-form-settings', 'cform-from-name' );
		register_setting( 'centaur-form-settings', 'cform-from-email' );

		// PMS Integration Settings
		register_setting( 'centaur-form-settings', 'cform-integrate_pms' );
		register_setting( 'centaur-form-settings', 'pms-samedb' );
		register_setting( 'centaur-form-settings', 'pms-name' );

		// Postmark Integration Settings
		register_setting( 'centaur-form-settings', 'cform-integrate_postmark' );
		register_setting( 'centaur-form-settings', 'cform-integrate_stopforumspam' );

	}

	/**
	 * Displays the form settings
	 * 
	 * @since	1.0.0
	 */
	public function display_form_settings() {

		Centaur_Form_Template::get_template("admin/content", "settings", false);

	}

	/**
	 * Displays the Addon settings
	 * 
	 * @since	1.0.0
	 */
	public function display_form_addon() {

		Centaur_Form_Template::get_template("admin/content", "settings", false);

	}

	/**
	 * Displays the form statistics
	 * 
	 * @since	1.0.0
	 */
	public function display_form_stat() {

		Centaur_Form_Template::get_template("admin/content", "statistics", false);

	}

	/**
	 * Displays the form statistics
	 * 
	 * @since	1.0.0
	 */
	public function get_form_field() {

		// Security Check
		check_ajax_referer( 'centaur-form-builder', 'nonce' );

		$cent_form = new Centaur_Form_Admin_Builder( $this->plugin_name, $this->version );

		$cent_form->add_form_field();

		die();

	}

	/**
	 * Handle Form Saving
	 *
	 * @since	1.0.0 
	 */
	public function save_form(){

		$cform_object = new Centaur_Form_Admin_Builder( $this->plugin_name, $this->version );

		// Check if there are data input
		if ( !isset( $_POST['data'] ) && empty( $_POST['data'] ) ) {
			die( __( 'No data provided', $this->plugin_name ) );
		}

		// Security Check
		check_ajax_referer( 'centaur-form-builder', 'nonce' );

		$cform_object->handle_form_actions( $_POST['data'], $_POST['id'], $_POST['status'], true );

		die();

	}

 	/**
	 * Add settings action link to the plugins page.
	 *
	 * @since    1.0.0
	 */
	public function add_action_links( $links ) {
    	/*
	     *  Documentation : https://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)
	     */
		$settings_link = array('<a href="' . admin_url( 'admin.php?page=' . $this->plugin_name ) . '-settings">' . __('Settings', $this->plugin_name) . '</a>',);
		return array_merge(  $settings_link, $links );
	}
}