<?php

/**
 * This class is for processing Form Entries 
 * 
 * @link       www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/public
 * @author     Centaur Marketing <dev@centaurmarketing.co>
 */

class Centaur_Form_Entry {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	public $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Database object for DB
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $dbObject    Database Object
	 */
	private $dbObject;

	/**
	 * Database table for Forms
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $dbTable    Database Table
	 */
	private $dbTable;

	/**
	 * General Form Settings
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $settings
	 */
	private $settings;

	/**
	 * Constructor
	 *
	 * @since	1.0.0
	 */
	public function __construct( $plugin_name, $version ) {

		global $wpdb;

		$this->plugin_name 	= $plugin_name;
		$this->version 		= $version;
		$this->dbObject		= $wpdb;
		$this->dbTable		= "{$wpdb->prefix}centform_entry";
		$this->settings 	= get_option("centaur-form-settings");

		error_log( print_r($this->settings, true) );

	}

	/**
	 * Handles Entry Actions
	 *
	 * @since	1.0.0
	 * @param	array	$data
	 * @param	int		$form_id
	 * @param 	boolean $echo	
	 */
	public function handle_entry_actions( $data, $form_id, $echo = false ) {

		// Get Form Settings
		if ( isset( $form_id ) && is_numeric( $form_id ) ) {

			// Retrieve Form Details
			$form 		= $this->get_form( $form_id );
			$settings 	= json_decode( $form->settings );
			$is_spam 	= false;

			// Retrieve Form Data Entry
			$entries	= json_decode( stripslashes( $data ) );

			// Process and Save General Entry Information to DB
			$entry_id 	= $this->add( $form_id );

			// Process and Save Entry Meta Information to DB
			foreach ( $entries as $entry ) {
				$this->add_meta( $form_id, $entry_id, $entry );

				if ( strtolower($entry->name) == 'email' ) {

					// Check if email is spam
					foreach ( $settings as $key => $value ) {

						if ( strpos( $key, "integrate" ) !== false ) {
							$is_spam = $this->form_integration_handler( $key, array( 'email' => $entry->value ) );	
						}
					}

					if ( $is_spam ) {
						$this->update_status( $entry_id, "spam" );
					}
				}

			}

			if ( !$is_spam ) {

				$args = array(
					"recipients" 	=> $settings->notify_email,
					"bcc" 			=> $settings->notify_bcc,
					"subject" 		=> $settings->notify_name,
					"notification" 	=> $settings->notify_note,
					"entry"			=> $data
				);

				// Send Email to Form Receiver
				$response = $this->send_notification( ( (object) $args ) );

				if ( ! $response ) {
					global $ts_mail_errors;
					global $phpmailer;
					if ( !isset($ts_mail_errors) ) {
						$ts_mail_errors = array();

						if ( isset($phpmailer) ) {
							echo  '{"action":"message","value":"Failure to send email: ' . $phpmailer->ErrorInfo . '"}';
							die();
						}
					}
				}

			}

			// Check and Process other 3rd Party Integrations
			foreach ( $settings as $key => $value ) {
				if ( strpos( $key, "integrate" ) !== false ) {

					error_log("3rd party Integration");
					$this->form_integration_handler( $key, $data, $form->title, "third-party" );
				}
			}

			echo  '{"action":"' . $settings->confirm_action . '","value":"' . ( ($settings->confirm_action == 'redirect' ) ? trim($settings->confirm_value, " ") : $settings->confirm_value) . '"}';

		}

	}

	/**
	 * Add New Form Entry
	 *
	 * @since	1.0.0
	 * @param 	int		$form_id
	 *
	 * @return  int 
	 */
	private function add( $form_id, $source = "" ) {

		// Check if there is spam integration

		$this->dbObject->insert(
			$this->dbTable,
			array( 'form_id'			=> $form_id,
				   'ip_address' 		=> $this->get_ip_address(),
				   'source_url'			=> $this->get_source(),
				   'remote_ip_address' 	=> filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP ),
				   'user_agent' 		=> $_POST['user']
			),
			array( '%d', '%s', '%s', '%s', '%s' )				
		);

		return $this->dbObject->insert_id;		

	}

	/**
	 * Add New Entry Meta
	 *
	 * @since	1.0.0
	 * @param 	string 	$data
	 * @param 	array 	$data
	 * @param 	int 	$position
	 *
	 * @return  int 
	 */
	private function add_meta( $form_id = '', $entry_id = '', $data ) {

		$this->dbObject->insert(
			$this->dbTable . "_meta",
			array( 'form_id' 	=> $form_id,
				   'entry_id' 	=> $entry_id,
				   'meta_key' 	=> $data->name,
				   'meta_value' => $data->value
			),
			array( '%d', '%d', '%s', '%s' )			
		);

	}

	/**
	 * Update Existing Form
	 *
	 * @since	1.0.0
	 * @param 	int 	$form_id
	 * @param 	array 	$data
	 *
	 * @return  int 
	 */
	private function update_status( $entry_id, $status ) {
	
		$this->dbObject->update(
			$this->dbTable,
			array( 'status' => $status ),
			array( 'id' => $entry_id ),
			array( '%s' ),
			array( '%d' )			
		);

	}

	/**
	 * Retrieve Form data
	 *
	 * @since	1.0.0
	 * @param	int		$id		Record ID
	 *
	 * @return	mixed 
	 */
	private function get_form( $id ) {

		$formDBTable = $this->dbObject->prefix . "centform_form";

		return $this->dbObject->get_row( "SELECT * FROM $formDBTable WHERE id = " . $id );

	}

	/** 
	 * Email Sending
	 *
	 * @since	1.0.0
	 */
	
	/**
	 * Send Email Notification 
	 *
	 * @since	1.0.0
	 * @since   1.1.0 	Added details such as bcc, organization and other sender info to $headers
	 * @param	array	$args
	 *
	 * @return	boolean
	 */
	private function send_notification( $args ) {

		remove_filter( "wp_mail_content_type", array( $this, "set_html_content_type") );
		add_filter( "wp_mail_content_type", array( $this, "set_html_content_type") );

		$recipients = explode( ' ', $args->recipients );
		$subject 	= "[" . get_bloginfo( 'name' ) . "] " . $args->subject;
		$message 	= ( get_option('header-path') != "" ) ? 
			'
			<html>
				<body>
					<img src="' . get_option('header-path') . '"/>
					<br/><br/><br/>
			'
			: 
			"
			<html>
				<body>
			"
			; 

		// Set Initial Message
		$message 	.= $args->notification . "<br/><br/>";

		// Set Sender Details
		$message 	.= "<table>";

		$user_data 	= json_decode( stripslashes( $args->entry ) );

		foreach( $user_data as $data ) {

			$message 	.= "<tr>
								<td>" . $data->name . ": </td>
								<td>" . $data->value . "</td>
							</tr>";

			$subject 	.= ( ( strtolower($data->name) ) == "name" ) ? " - " . $data->value : "" ;

		}

		$message 	.= "<tr>
							<td>Source: </td>
							<td>" . $this->get_source() . "</td>
						</tr>";

		$message 	.= "</table>";
		$message 	.= "</body></html>";

		// Set Header
		$headers[] = "From: " . ( ( get_option('cform-from-name') != "" ) ? get_option('cform-from-name') : "" ) .  " <" . strip_tags( get_option('cform-from-email') ) . ">\r\n";

		if ( $args->bcc != "" ) {
			$bcc_recipients = explode( ' ', $args->bcc );

			foreach( $bcc_recipients as $recipient ) {
				$headers[] = "Bcc: " . strip_tags( $recipient ) . "\r\n";
			}
		}
		
		$headers[] = "Organization: " . get_bloginfo() . "\r\n";
		$headers[] = "MIME-Version: 1.0\r\n";
		$headers[] = "Content-type: text/plain; charset=iso-8859-1\r\n";
		$headers[] = "X-Priority: 3\r\n";
		$headers[] = "X-Mailer: PHP" . phpversion();

		// Send Email
		$result = wp_mail( $recipients, $subject, $message, $headers );

		return $result;

	}
	
	/**
	 * Set HTML Content type for mail 
	 *
	 * @since	1.0.0
	 * @return	string
	 */
	public function set_html_content_type() {
		return "text/html";
	}

	/**
	 * Process Client IP Address
	 *
	 * @since	1.0.0
	 */
	
	/**
	 * Retrieve Client IP Address
	 *
	 * @since	1.0.0
	 * 
	 * @return	string
	 */
	private function get_ip_address() {

		return ( $this->validate_ip_address( $_POST['ip'] ) ) ? $_POST['ip'] : "" ;

		$ip_keys 	= array('HTTP_CLIENT_IP', 
							'HTTP_X_FORWARDED_FOR', 
							'HTTP_X_FORWARDED', 
							'HTTP_X_CLUSTER_CLIENT_IP', 
							'HTTP_FORWARDED_FOR', 
							'HTTP_FORWARDED');

		foreach ( $ip_keys as $key ) {

			if ( array_key_exists( $key, $_SERVER ) === true && !empty( $_SERVER[$key] ) ) {
	            foreach ( explode( ',', $_SERVER[$key] ) as $ip_address ) {
	                // trim for safety measures
    	            $ip_address = trim($ip_address);
        	        // attempt to validate IP
            	    if ( $this->validate_ip_address( $ip_address ) ) {
                	    return $ip_address;
                	}
                }
			}

		}

		return "";

	}

	/**
	 * Validate IP Addresss
	 *
	 * @since	1.0.0
	 * @param	string 	$ip_address
	 *
	 * @return	boolean
	 */
	private function validate_ip_address( $ip_address ) {

		if ( strtolower( $ip_address ) === 'unknown' ) {
			return false;
		}

		// generate ipv4 network address
		$ip_address = ip2long( $ip_address );

		// if the ip is set and not equivalent to 255.255.255.255
		if ( $ip_address !== false && $ip_address !== -1 ) {
			// make sure to get unsigned long representation of ip
			// due to discrepancies between 32 and 64 bit OSes and
			// signed numbers (ints default to signed in PHP)
			$ip_address = sprintf('%u', $ip_address);

			// do private network range checking
			if ($ip_address >= 0 && $ip_address <= 50331647) return false;
			if ($ip_address >= 167772160 && $ip_address <= 184549375) return false;
			if ($ip_address >= 2130706432 && $ip_address <= 2147483647) return false;
			if ($ip_address >= 2851995648 && $ip_address <= 2852061183) return false;
			if ($ip_address >= 2886729728 && $ip_address <= 2887778303) return false;
			if ($ip_address >= 3221225984 && $ip_address <= 3221226239) return false;
			if ($ip_address >= 3232235520 && $ip_address <= 3232301055) return false;
			if ($ip_address >= 4294967040) return false;
		}

		return true;

	}

	/**
	 * Get Source or Referrer
	 *
	 * @since	1.0.0
	 * @param	string 	$ip_address
	 *
	 * @return	boolean
	 */
	private function get_source( $force_ssl = false ) {

		if ( isset($_POST['source']) && !empty($_POST['source']) ) {
			$url = $_POST['source'];
		} else {
			$url = 'http' . ( ( $_SERVER['HTTPS'] == 'on' || $force_ssl ) ? 's' : '' ) . '://';
			$url .= $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
		}

		// Check if there is utm parameters in url
		if ( strpos( $url, "utm_source" ) != false && strpos( $url, "utm_medium" ) != false && strpos( $url, "utm_campaign" ) != false ) {

			$utm_list 	= array();
			$get_query 	= explode( "?", $url );

			parse_str( $get_query[1], $utm_list );

			return "Google Ad: " . $utm_list['utm_campaign'] . " ( " . $utm_list['utm_source'] . " | " . $utm_list['utm_medium'] . " )";

		// Otherwise, get referral
		} elseif ( isset($_POST['referral']) && !empty($_POST['referral']) ) {

			$referrer = $_POST['referral'];

			if ( strpos( $referrer, "plus.google.com" ) != false || strpos( $referrer, "facebook.com" ) != false || strpos( $referrer, "twitter.com" ) != false || strpos( $referrer, "instagram.com" ) != false || strpos( $referrer, "pinterest.com" ) != false ) {
				return "Social"; 
			} elseif ( strpos( $referrer, "google.com" ) != false || strpos( $referrer, "bing.com" ) != false || strpos( $referrer, "yahoo.com" ) != false  ) {
				return "Organic"; 
			}

			return $url;

		}

		return "Direct";
	}

	/**
	 * 3rd Party Integrations
	 *
	 * @since	1.0.0
	 */

	/**
	 * Handle Integrations to other systems
	 *
	 * @since	1.0.0
	 * @param	string 	$ip_address
	 * @param	array 	$data
	 * @param	string 	$title
	 *
	 * @return 	mixed
	 */
	private function form_integration_handler( $integration, $data, $title = "", $type = "spam-check" ) {

		if ( get_option("cform-" . $integration) == 'on' ) {

			if ( $type == "spam-check" ) {

				switch ($integration) {
					case "integrate_postmark": 

						$object = new Centaur_Form_Postmark_Integration();
						return $object->integrate( $data['email'] );

					break;
					case "integrate_stopforumspam": 

						$object = new Centaur_Form_Stopforumspam_Integration();
						return $object->integrate( $data['email'] );

					break;
				}

			} elseif ( $type == "third-party" )  {

				switch ($integration) {
					case "integrate_pms": 

						$object = new Centaur_Form_PMS_Integration();
						return $object->integrate( $data, $title );

					break;
				}

			}

		}

	}

}