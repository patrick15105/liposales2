<?php

/**
 *  
 * 
 * @link       www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/admin
 */

/**
 * 
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/admin
 * @author     Centaur Marketing <dev@centaurmarketing.co>
 */

if ( ! class_exists('WP_List_Table') ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}	

class Centaur_Form_Admin_Builder extends WP_List_Table {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	public $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Database object for DB
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $dbObject    Database Object
	 */
	private $dbObject;

	/**
	 * Database table for Forms
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $dbTable    Database Table
	 */
	private $dbTable;

	/**
	 * Step for form creation
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      int    $form_step    
	 */
	public $form_step = 1;

	/**
	 * Check if new form
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      boolean    
	 */
	public $is_new;

	/**
	 * Database Item
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      object    
	 */
	public $item;

	/**
	 * Database Item
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      int    
	 */
	private $item_field_count;

	/**
	 * Constructor
	 *
	 * @since	1.0.0
	 */
	public function __construct( $plugin_name, $version ) {

		global $wpdb;

		$this->plugin_name 	= $plugin_name;
		$this->version 		= $version;
		$this->dbObject		= $wpdb;
		$this->dbTable		= "{$wpdb->prefix}centform_form";

		parent::__construct( [
			'singular' 	=> __ ( 'Form', $this->plugin_name ), 
			'plural' 	=> __ ( 'Forms', $this->plugin_name ),
			'ajax'		=> false
		] );

	}

	/**
	 * Handles Form Actions
	 *
	 * @since	1.0.0
	 */
	public function handle_form_actions( $data, $form_id, $status, $echo = false ) {

		$form_data 		= json_decode( stripslashes($data) );
		$title 			= "";
		$form 			= array();
		$is_start 		= true;

		if( ! is_null( $form_data ) ) {

			foreach( $form_data as $data ) {

				$name = explode( '_', $data->name );

				if( $name[0] == 'cform' ) {
					$form['settings'][str_replace( "-", "_", $name[1] )] = $data->value;
				} elseif( $name[0] == 'cformfield' ) {

					if ( $name[2] == "id" ) {

						$field_count 	= ( $is_start ) ? 0 : $field_count + 1 ;
						$is_start 		= false;

					}

					$form['fields'][$field_count][str_replace( "-", "_", $name[2] )] = $data->value;
				}

			}

			$form['settings']['status'] = ( $status == "draft" || $status == "save" ) ? "draft" : "published" ;

			$form_id = ( $form_id == "" ) ? $this->add( $form['settings'] ) : $this->update( $form_id, $form ) ;

		}
		
		if ( $echo ) {
			echo $form_id;
		}

	}

	/**
	 * Add New Form
	 *
	 * @since	1.0.0
	 * @param array $data
	 *
	 * @return  int 
	 */
	private function add( $data = array() ) {

		$this->dbObject->insert(
			$this->dbTable,
			array( 'title' 		=> $data['title'],
				   'settings' 	=> json_encode( $this->process_data( $data ) ),
				   'status'		=> $data['status']
			),
			array( '%s', '%s', '%s' )				
		);

		return $this->dbObject->insert_id;		

	}

	/**
	 * Add New Field
	 *
	 * @since	1.0.0
	 * @param 	string 	$data
	 * @param 	array 	$data
	 * @param 	int 	$position
	 *
	 * @return  int 
	 */
	private function add_field( $form_id = '', $data = array(), $position = 0 ) {

		$this->dbObject->insert(
			$this->dbTable . "_meta",
			array( 'form_id' 	=> $form_id,
				   'title' 		=> $data['label'],
				   'settings' 	=> json_encode( $this->process_data( $data ) ),
				   'position'	=> $position
			),
			array( '%d', '%s', '%s', '%d' )			
		);

	}

	/**
	 * Update Existing Form
	 *
	 * @since	1.0.0
	 * @param 	int 	$form_id
	 * @param 	array 	$data
	 *
	 * @return  int 
	 */
	private function update( $form_id = "", $data = array() ) {
	
		$form_details 	= $data['settings'];
		$form_fields 	= $data['fields'];

		print_r($form_details);

		// Update Form Details
		if ( $form_details['title'] != "" ) {
			$this->dbObject->update(
				$this->dbTable,
				array( 'title' 			=> $form_details['title'],
					   'settings' 		=> json_encode( $this->process_data( $form_details ) ),
					   'status' 		=> $form_details['status'],
					   'date_updated'	=> current_time('mysql', 1)
				),
				array( 'id' => $form_id ),
				array( '%s', '%s', '%s', '%s' ),
				array( '%d' )			
			);
		} 

		// Process Fields
		if ( count( $form_fields ) > 0 ) {

			// Set Position to 0
			$field_position = 0;

			// Delete All Fields
			$this->dbObject->delete( $this->dbTable . "_meta", array( 'form_id' => $form_id ) );

			// Add Fields
			foreach( $form_fields as $form_field ) {

				$this->add_field( $form_id, $form_field, $field_position );

				$field_position++;

			}

		}

		return $form_id;

	}

	/**
	 * Process Data Array
	 *
	 * @since	1.0.0
	 * @param 	array 	$data
	 *
	 * @return  array
	 */
	private function process_data( $data ) {

		$settings = array();

		foreach( $data as $key => $value ) {

			if( $key != 'title' && $key != 'status' && $key != 'label' ) {
				$settings[$key] = $value;
			}

		}

		return $settings;

	}

	/**
	 * Display Settings Form
	 *
	 * @since 	1.0.0
	 */
	public function display_config_fields() {

		Centaur_Form_Template::get_template("admin/parts/form", "settingsx", false);
	}

	public function display_form_settings() {

		Centaur_Form_Template::get_template("admin/parts/form", "settings", false);
	}


	/**
	 * Display Fields Form
	 *
	 * @since 	1.0.0
	 */
	public function display_field_settings() {

		Centaur_Form_Template::get_template("admin/parts/form", "fields", false);
	}

	/**
	 * Display Fields Panels
	 *
	 * @since 	1.0.0
	 */
	public function display_form_fields() {

		if ( isset( $_GET['form_id'] ) && is_numeric( $_GET['form_id'] ) ) {

			$fields = $this->get_form_fields( $_GET['form_id'] );

			foreach( $fields as $field ) {

				global $cform_meta;

				$cform_meta 		 = json_decode( $field->settings, true );
				$cform_meta['label'] = $field->title; 

				Centaur_Form_Template::get_template("admin/parts/fields/field", $cform_meta['type'], false);

			}

		}


	}

	/**
	 * Displays the form sidebar
	 * 
	 * @since	1.0.0
	 */
	public function display_form_sidebar() {

		Centaur_Form_Template::get_template("admin/parts/form", "sidebar", false);

	}

	/**
	 * Retrieve Form data
	 *
	 * @since	1.0.0
	 * @param	int		$id		Record ID
	 *
	 * @return	mixed 
	 */
	private function get_record( $id ) {

		$row 	= $this->dbObject->get_row( "SELECT * FROM $this->dbTable WHERE id = " . $id );
		$record = json_decode( $row->settings, true );

		$record['id'] 		= $row->id;
		$record['title'] 	= $row->title;


		return $record;

	}

	/**
	 * Retrieve Data
	 *
	 * @since	1.0.0
	 * @param	int		$id		Record ID
	 */
	public function set_item( $id ) {

		$this->item = (object) $this->get_record( $id );

	}


	/* FORM FIELDS FUNCTIONS */

	/**
	 * Get Field Records
	 *
	 * @since	1.0.0
	 * @param	int		$id		Record ID
	 */ 
	private function get_form_fields( $id ) {

		$results = $this->dbObject->get_results( "SELECT * FROM {$this->dbTable}_meta WHERE form_id = " . $id . " ORDER BY position ASC" );

		$this->item_field_count = count($results);

		return $results;

	}

	/**
	 * Retrieve Form data
	 *
	 * @since	1.0.0
	 * @param	int		$id		Record ID
	 *
	 * @return	mixed 
	 */
	public function add_form_field() {

	    if ( ! isset( $_POST['id'] ) && !is_numeric( $_POST['id'] ) ) {
			die( 'Go get a life script kiddies' );
    	} else {

    		global $cform_meta;

    		$field_list = $this->get_form_fields( $_POST['id'] );

			$cform_meta['id'] 	= "new";
			$cform_meta['type'] = $_POST['type'];

			Centaur_Form_Template::get_template("admin/parts/fields/field", $cform_meta['type'], false);

    	}

	}

}