<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur
 * @subpackage Centaur/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Centaur
 * @subpackage Centaur/public
 * @author     Centaur Marketing <dev@centaurmarketing.co>
 */
class Centaur_Form_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 * @since    1.1.1		Added local jquery-ui css
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Centaur_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Centaur_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class. 
		 */

		wp_enqueue_style( 
			$this->plugin_name, 
			plugins_url() . '/' . $this->plugin_name .'/assets/css/styles.css', 
			array(), 
			$this->version, 
			'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 * @since    1.1.1		Added local jquery-ui
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Centaur_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Centaur_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
        if(!is_front_page()){
            
		wp_enqueue_script( 
			$this->plugin_name, 
			plugins_url() . '/' . $this->plugin_name .'/assets/js/form-sender.js', 
			array( 'jquery' ), 
			$this->version, 
			false 
		);

		wp_enqueue_script( 
			'jquery-ui', 
			plugins_url() . '/' . $this->plugin_name .'/assets/js/jquery-ui.min.js', 
			array( 'jquery' ), 
			$this->version, 
			false 
		);

		wp_localize_script( 
			$this->plugin_name,
			'centaur_form', 
			array( 
				'ajax_url' => admin_url('admin-ajax.php'),
				'nonce'		=> wp_create_nonce( $this->plugin_name . "-entry-save" )
			) 
		);
		
        }

	}

	/**
	 * Shortcode function for displaying Form
	 *
	 * @since	1.0.0
	 * @since   1.1.0 	Added columns function.
	 * @since   1.1.0 	Added placeholder and required codes.
	 * @since   1.1.0 	Added 'name' and 'procedures' to switch option type.
	 */
	public function display_form( $attr ) {

		global $wpdb;
		global $centaur_form;

		$dbTable 	= "{$wpdb->prefix}centform_form";
	    $attribute  = shortcode_atts( array( 'id' => '1' ), $attr );

	    // Get Form Details
	    $sql  = $wpdb->prepare( "SELECT * FROM $dbTable WHERE id=%d", $attribute['id'] );
	    $form = $wpdb->get_row( $sql, ARRAY_A );

	    if ( $form['status'] == 'published' ) {

	    	$settings 		= json_decode( $form['settings'] );
	    	$field_count 	= 0;
	    	$count 			= 0;

		    // Get Form Fields
		    $sql 	= $wpdb->prepare( "SELECT * FROM {$dbTable}_meta WHERE form_id=%d ORDER BY position ASC", $attribute['id'] );
	    	$fields = $wpdb->get_results( $sql );
		    $field_string = "";

		    // Display Form Title
		    $form['title'] = ( isset($settings->show_title) && $settings->show_title == 'on' ) ? "<h2 class=\"centaur-title centaur-block\">" . $form['title'] . "</h2>" : "" ;

		    // Process Column Count		    
		    $fields_per_column = ceil( count($fields) / $settings->column_count );
		    $form['columns'] = $settings->column_count;

		    foreach( $fields as $field ) {

		    	if ( $count == 0 ) {
		    		$field_string = $field_string . "
						<div class=\"centaur-field-column\">
		    		";
		    	}

	    		$options = json_decode( $field->settings );

		    	$required 		= ( isset( $options->required ) && $options->required == "on" ) ? true : false;
		    	$display_label 	= ( isset( $options->displaylabel ) && $options->displaylabel == "on" ) ? true : false;
		    	$placeholder 	= ( isset( $options->placeholder ) && $options->placeholder != "" ) ?  "placeholder=\"{$options->placeholder} " . 
						( ( $required ) ? "*" : "" ) . "\" ": "";

	    		$field_string 	= $field_string . "<div class=\"centaur-field-group\">";

	    		// Field Label 
	    		$field_string 	.= ( ( $display_label ) ? "<label for=\"{$field->title}\" class=\"centaur-label centaur-block\">{$field->title}" . 
						( ( $required ) ? "<span id=\"required\">*</span>" : "" ) . "</label>" : "" );


		    	switch( $options->type ) {

		    		case 'name': 
						
						if ( $options->format == "first-last" ) {
					    	$field_string = $field_string . "
					    		<div class=\"centaur-inline\">
						    		<input type=\"text\" name=\"first_name\" class=\"centaur-block centaur-input\"  data-required=\"{$required}\"  data-type=\"{$options->type}\" placeholder=\"First Name " . 
						( ( $required ) ? "*" : "" ) . "\" />
					    		</div>
					    		<div class=\"centaur-inline\">
						    		<input type=\"text\" name=\"last_name\" class=\"centaur-block centaur-input\"  data-required=\"{$required}\"  data-type=\"{$options->type}\" placeholder=\"Last Name " . 
						( ( $required ) ? "*" : "" ) . "\"/>
					    		</div>";
						} else {
					    	$field_string = $field_string . "
					    		<input type=\"text\" name=\"{$field->title}\" class=\"centaur-block centaur-input\"  data-required=\"{$required}\"  data-type=\"{$options->type}\" {$placeholder}/>";
						}

		    		break;
	    			case 'procedures': 
	
		    			$pms = new Centaur_Contact_Form_PMS_Integration();

				    	$field_string = $field_string . "
			    			<select name=\"{$field->title}\" class=\"centaur-block centaur-input\" data-required=\"{$required}\" data-type=\"procedures\">";

			    		$field_options = $pms->get_procedures();

			    		if ( count($field_options) > 0 ) {
				    		foreach ( $field_options as $field_option ) {
				    			$field_value = $field_option->name;
				    			$field_string = $field_string . "<option value=\"{$field_value}\">{$field_value}</option>";
				    		}
			    		}

				    	$field_string = $field_string . "
			    			</select>";
		    		break;
	    			case 'paragraph-text': 
				    	$field_string = $field_string . "
			    			<textarea name=\"{$field->title}\" class=\"centaur-block centaur-input\" data-required=\"{$required}\" data-type=\"paragraph-text\"  {$placeholder}></textarea>";
		    		break;
	    			case 'calendar': 

	    				if ( isset( $options->displaywidget ) && $options->displaywidget == 'on' )  {

					    	$field_string = $field_string . "
				    			<script>
				    			jQuery(document).ready(function($) {
									$(function() {
									    $( \".centaur-calendar\" ).datepicker({
											altField: \"#cform-field-calendar\"
									    });

									    $( \".centaur-calendar\" ).datepicker( \"option\", \"altField\", \"#cform-field-calendar\" );
									});
								});		
			    				</script>";

					    	$field_string = $field_string . "
				    			<div class=\"centaur-calendar\"></div>
				    			<input type=\"hidden\" name=\"{$field->title}\" id=\"cform-field-{$options->type}\" class=\"centaur-block centaur-input\" data-required=\"{$required}\" data-type=\"{$options->type}\" {$placeholder} />
				    			";
	    				} else {
					    	$field_string = $field_string . "
				    			<script>
				    			jQuery(document).ready(function($) {
									$(function() {
									    $( \".centaur-calendar\" ).datepicker();
									} );
								});		
			    				</script>";


					    	$field_string = $field_string . "
				    			<input type=\"text\" name=\"{$field->title}\" id=\"cform-field-{$options->type}\" class=\"centaur-block centaur-input centaur-calendar\" data-required=\"{$required}\" data-type=\"{$options->type}\" {$placeholder} />";
				    	}

		    		break;
		    		default:
				    	$field_string = $field_string . "
				    		<input type=\"text\" name=\"{$field->title}\" id=\"cform-field-{$options->type}\" class=\"centaur-block centaur-input\" data-required=\"{$required}\" data-type=\"{$options->type}\" {$placeholder} />
			    			";
		    		break;

		    	}
	    		$field_string = $field_string . "
						<p class=\"centaur-message\"></p>
						<p class=\"centaur-caption\">{$options->description}</p>
					</div>
		    		";

		    	$count++;
		    	$field_count++;

		    	if ( $count >= $fields_per_column || $field_count == count($fields) ) {
		    		$count 			= 0;
		    		$field_string 	= $field_string . "</div>";
		    	}

		    }

		    $form['field_string'] 	= $field_string;
		    $form['preloader']		= get_option("preloader-path");

		    $centaur_form = (object) $form;

			Centaur_Form_Template::get_template("forms/single", "form", false);

	    } else {
	    	echo '<div class="centaur-block message">Sorry! This is not available for viewing.</div>';
	    }

	}

	/**
	 * Save Form Entry
	 *  
	 * @since	1.0.0
	 */
	public function save_entry() {

		$entry = new Centaur_Form_Entry( $this->plugin_name, $this->version );

		// Check if there are data input
		if ( !isset( $_POST['data'] ) && empty( $_POST['data'] ) ) {
			die( __( 'No data provided', $this->plugin_name ) );
		}

		// Security Check
		check_ajax_referer( $this->plugin_name . "-entry-save", 'nonce' );

		$entry->handle_entry_actions( $_POST['data'], $_POST['id'], true );

		die();

	}
	
}
