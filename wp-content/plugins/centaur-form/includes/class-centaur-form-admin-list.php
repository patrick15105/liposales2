<?php

/**
 * 
 * 
 * @link       www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/admin
 */

/**
 * 
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/admin
 * @author     Centaur Marketing <dev@centaurmarketing.co>
 */

if ( ! class_exists('WP_List_Table') ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}	

class Centaur_Form_Admin_List extends WP_List_Table {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	public $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Database object for DB
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $dbObject    Database Object
	 */
	private $dbObject;

	/**
	 * Database table for Forms
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $dbTable    Database Table
	 */
	private $dbTable;

	/**
	 * Step for form creation
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      int    $form_step    
	 */
	public $form_step = 1;

	/**
	 * Object Type
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      string    $object    
	 */
	public $object;

	/**
	 * Additional Entry Columns to be added
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      array    $entry_columns    
	 */
	public $entry_columns;

	/**
	 * Constructor
	 *
	 * @since	1.0.0
	 */
	public function __construct( $plugin_name, $version, $table = "form"  ) {

		global $wpdb;

		$this->plugin_name 	= $plugin_name;
		$this->object 		= $table;
		$this->version 		= $version;
		$this->dbObject		= $wpdb;
		$this->dbTable		= "{$wpdb->prefix}centform_" . $table;

		parent::__construct( [
			'singular' 	=> __ ( ucwords($table), $this->plugin_name ), 
			'plural' 	=> __ ( ucwords($table) . 's', $this->plugin_name ),
			'ajax'		=> false
		] );

	}

	/**
	 * Handles data query and filter, sorting, and pagination
	 *
	 * @since	1.0.0
	 */
	public function prepare_items() {

		if ( ( $this->object == 'entry'  && isset($_GET['form_id']) && !empty($_GET['form_id']) ) || $this->object == 'form' ) {

			$this->_column_header = $this->get_column_info();

			$search_key = isset( $_REQUEST['s'] ) ? wp_unslash( trim( $_REQUEST['s'] ) ) : '';

			$this->process_bulk_actions();

		  	$per_page     	= $this->get_items_per_page( ( ( $this->object == 'entry' ) ? "entrie" : $this->object) . 's_per_page', 5 );

			$current_page 	= $this->get_pagenum();
			$total_items  	= $this->record_count();

			$data 			= $this->get_records( $per_page, $current_page );
			$this->items 	= ( $search_key ) ? $this->filter_records( $data, $search_key ) : $data;

			$this->set_pagination_args( [
				'total_items' => $total_items, //WE have to calculate the total number of items
				'per_page'    => $per_page //WE have to determine how many items to show on a page
			] );

		}
  	
	}

	/**
	 * Retrieve Form data
	 *
	 * @since	1.0.0
	 * @param	int		$per_page		Records per page
	 * @param	int		$page_number	Current page number
	 *
	 * @return	mixed 
	 */
	public function get_records( $per_page = 5, $page_number = 1 ) {

		$sql = "SELECT * FROM " . $this->dbTable . " " . ( ( $this->object == 'entry' ) ? "WHERE form_id=" . $_GET['form_id'] : "" );

		if ( ! empty( $_REQUEST['orderby'] ) ) {
			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
		}

		$sql .= " LIMIT $per_page";

		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

		$result = $this->dbObject->get_results( $sql, 'ARRAY_A' );

		return $result;

	}

	/**
	 * Filter the table based on the search key
	 *
	 * @since	1.0.0
	 * @param	array	$data		Database records
	 * @param	string	$key		Search Key
	 *
	 * @return	array
	 */
	public function filter_records( $data, $key ) {

		$filtered_data = array_values( 
							array_filter( $data, function( $row ) use( $key ) {
								foreach( $row as $row_val ) {
									if ( stripos( $row_val, $key ) !== false ) { 
										return true; 
									}				
								}			
							}) 
						);

		return $filtered_data;		

	}

	/**
	 * Delete a form record
	 *
	 * @since	1.0.0
	 * @param	int		$id		Form ID
	 */
	public function delete_record( $id ) {

		$this->dbObject->delete(
			$this->dbTable,
			['id' => $id],
			['%d']
		);

	}

	/**
	 * Returns the count of form records in the database
	 *
	 * @since	1.0.0
	 * @return	null|string	
	 */
	public function record_count() {

		$sql = "SELECT COUNT(*) FROM " . $this->dbTable;

		return $this->dbObject->get_var( $sql );

	}

	/**
	 * Text displayed when no data is available
	 *
	 * @since	1.0.0
	 */
	public function no_items() {
		_e( 'No forms avaliable.', $this->plugin_name );
	}

	/**
	 * Method for title column
	 *
	 * @since	1.0.0
	 * @param	array	$item	an array of DB data
	 */
	public function column_title( $item ) {

		// Create a none
		$delete_nonce	= wp_create_nonce( 'cf_delete_form' );

		$title			= '<strong>' . $item['title'] . '</strong>';

		$actions		= [ 'settings' => sprintf( '<a href="?page=%s&view=%s&action=%s&form_id=%s">Settings</a>', 
											esc_attr( "centaur-form-build" ), 
											'settings', 
											'edit', 
											absint( $item['id'] ) ),
							'edit' => sprintf( '<a href="?page=%s&view=%s&action=%s&form_id=%s">Edit</a>', 
											esc_attr( "centaur-form-build" ), 
											'edit', 
											'edit', 
											absint( $item['id'] ) ),		
							'delete' => sprintf( '<a href="?page=%s&action=%s&form_id=%s&_wpnonce=%s">Delete</a>', 
											esc_attr( $_REQUEST['page'] ), 
											'delete', 
											absint( $item['id'] ), 
										$delete_nonce ) 
							];

		return $title . $this->row_actions( $actions );

	}

	/**
	 * Method for name column
	 *
	 * @since	1.0.0
	 * @param	array	$item	an array of DB data
	 */
	public function column_name( $item ) {

		$name 		= "";

		$sql 		= 'SELECT * FROM ' . $this->dbTable . '_meta 
						WHERE meta_key like "%name%" AND entry_id=' . $item['id'];

		$results 	= $this->dbObject->get_results( $sql );

		foreach ( $results as $result ) {
			if( $result->meta_key == 'name' ) {
				$name  = $result->meta_value;
				break;
			} else {
				$name .= $result->meta_value . " ";
			}
		}

		return "<strong>{$name}</strong>";

	}

	/**
	 * Method for actions column
	 *
	 * @since	1.0.0
	 * @param	array	$item	an array of DB data
	 */
	public function column_action( $item ) {

		return "<a href=\"?page=" . esc_attr( $_REQUEST['page'] ) . "&form_id=" . absint( $_REQUEST['form_id'] ) . "&entry_id=" . absint( $item['id'] ) . "\">View</a>";

	}

	/**
	 * Method for email column
	 *
	 * @since	1.0.0
	 * @param	array	$item	an array of DB data
	 */
	public function column_email( $item ) {

		$sql 		= 'SELECT * FROM ' . $this->dbTable . '_meta 
						WHERE meta_key="Email" AND entry_id=' . $item['id'];

		$results 	= $this->dbObject->get_results( $sql );

		return $results[0]->meta_value;

	}

	/**
	 * Method for message column
	 *
	 * @since	1.0.0
	 * @param	array	$item	an array of DB data
	 */
	public function column_message( $item ) {

		$sql 		= 'SELECT * FROM ' . $this->dbTable . '_meta 
						WHERE ( meta_key="Message" OR meta_key="Description" ) AND entry_id=' . $item['id'];

		$results 	= $this->dbObject->get_results( $sql );

		return ( ( count($results) > 0 ) ? $results[0]->meta_value : "" );

	}
	/**
	 * Render a column when no column specific method exists.
	 *
	 * @since	1.0.0
	 * @param	string	$item	
	 * @param	string	$column_name	
	 *
	 * @return	mixed
	 */
	public function column_default( $item, $column_name ) {

		switch ( $column_name ) {
			case 'source':
				return $item['source_url'];
			case 'system':
				return $this->systemInfo( $item['user_agent'], 'system' );
			case 'shortcode':
				return '[centaur_form id="' . $item['id'] . '"]';
			case 'status':
			case 'date_created':
				return $item[ $column_name ];
			default:
				return print_r( $item, true );
		}

	}

	/**
	 * Detect System Info
	 *
	 * @since	1.0.0
	 */
	private function systemInfo( $data, $type = "os" )
	{

		$os_array       = array('/windows phone/i'    	=>  'Windows Phone 8',
            	                '/windows nt/i'    		=>  'Windows',
    	                        '/windows xp/i'         =>  'Windows XP',
            	                '/windows me/i'         =>  'Windows me',
                	            '/win98/i'              =>  'Windows 98',
                    	        '/win95/i'              =>  'Windows 95',
                        	    '/win16/i'              =>  'Windows 3.11',
                            	'/macintosh|mac os x/i' =>  'Mac OS X',
	                            '/mac_powerpc/i'        =>  'Mac OS 9',
    	                        '/linux/i'              =>  'Linux',
        	                    '/ubuntu/i'             =>  'Ubuntu',
            	                '/iphone/i'             =>  'iPhone',
                	            '/ipod/i'               =>  'iPod',
                    	        '/ipad/i'               =>  'iPad',
                        	    '/android/i'            =>  'Android',
                            	'/blackberry/i'         =>  'BlackBerry',
	                            '/webos/i'              =>  'Mobile');

	    foreach ($os_array as $regex => $value) {

        	if (preg_match($regex, $data)) {

		    	if ( $type == 'system' ) {
		    		return $value;
	    		} elseif (  $type == 'os' ) {
		    		return $value;
	    		}

    		}

    	}

    	return "Unknown Platform";

 	}

	/**
	 * Render the bulk edit checkbox
	 *
	 * @since	1.0.0
	 * @param	string	$item	
	 *
	 * @return	string
	 */
	public function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['id']
		);
	}

	/**
	 * Associative array of columns
	 *
	 * @since	1.0.0
	 * 
	 * @return	array
	 */
	public function get_columns() {

		if ( $this->object == 'entry' ) {
			$columns = $this->entry_columns;
		} else {
			$columns = [
				'cb'      		=> '<input type="checkbox" />',
				'title'    		=> __( 'Title', $this->plugin_name ),
				'shortcode'		=> __( 'Shortcode', $this->plugin_name ),
				'status' 		=> __( 'Status', $this->plugin_name ),
				'date_created'  => __( 'Date Created', $this->plugin_name )
			];
		}

		return $columns;

	}

	/**
	 * Sortable Columns
	 *
	 * @since	1.0.0
	 * 
	 * @return	array
	 */
	public function get_sortable_columns() {
		$sortable_columns = array(
			'title' 	=> array( 'Title', true ),
			'name' 		=> array( 'Name', true ),
			'email' 	=> array( 'Email', true ),
			'device' 	=> array( 'Device', true ),
			'status' 	=> array( 'Status', true )
		);

		return $sortable_columns;
	}

	/**
	 * Returns an array containg bulk actions
	 *
	 * @since	1.0.0
	 * 
	 * @return	array
	 */
	public function get_bulk_actions() {
		$actions  = [
			'bulk-delete' => 'Delete'
		];

		return $actions ;		
	}

public function process_bulk_actions() {

  //Detect when a bulk action is being triggered...
  if ( 'delete' === $this->current_action() ) {

    // In our file that handles the request, verify the nonce.
    $nonce = esc_attr( $_REQUEST['_wpnonce'] );

    if ( ! wp_verify_nonce( $nonce, 'cf_delete_form' ) ) {
      die( 'Go get a life script kiddies' );
    }
    else {
     $this->delete_record( absint( $_GET['form_id'] ) );

      wp_redirect( esc_url( add_query_arg() ) );
      exit;
    }

  }

  // If the delete bulk action is triggered
  if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
       || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
  ) {

    $delete_ids = esc_sql( $_POST['bulk-delete'] );

    // loop over the array of record IDs and delete them
    foreach ( $delete_ids as $id ) {
      $this->delete_record( $id );

    }

    wp_redirect( esc_url( add_query_arg() ) );
    exit;
  }
}

	/**
	 * Display List of Forms
	 *
	 * @since	1.0.0
	 */
	public function display_form_list() {

//		$sql = "SELECT id, title FROM {$this->dbObject->prefix}centform_form WHERE status=\"publish\" ";
		$sql 	 = "SELECT id, title FROM {$this->dbObject->prefix}centform_form";
		$results = $this->dbObject->get_results( $sql );

		echo "<select id=\"select-cform-list\">";
		echo "<option>- Select - </option>";

		foreach ( $results as $result ) {
			echo "<option value=\"{$result->id}\" " . ( ( $result->id == $_GET['form_id'] ) ? "selected" : "" ) . ">{$result->title}</option>";
		}

		echo "</select>";

		if ( isset($_GET['form_id']) && is_numeric($_GET['form_id']) ) {

			$sql 	 = "SELECT title, type FROM {$this->dbObject->prefix}centform_form_meta WHERE form_id = " . $_GET['form_id'];
			$results = $this->dbObject->get_results( $sql );

			$this->entry_columns['cb'] = '<input type="checkbox" />';

			foreach ( $results as $result ) {

				if ( $result->type == 'name' || $result->type == 'single-line-text' ) {
					$this->entry_columns['name'] 	= __( 'Name', $this->plugin_name );
				} elseif ( $result->type == 'email' ) {
					$this->entry_columns['email'] 	= __( 'Email', $this->plugin_name );
				} elseif ( $result->type == 'paragraph-text' ) {
					$this->entry_columns['message'] = __( 'Message', $this->plugin_name );
				}
			}

			$this->entry_columns['source'] 			= __( 'Source', $this->plugin_name );
			$this->entry_columns['status'] 			= __( 'Status', $this->plugin_name );
			$this->entry_columns['date_created'] 	= __( 'Date Created', $this->plugin_name );
			$this->entry_columns['action'] 			= __( 'Actions', $this->plugin_name );

		}

	}


}