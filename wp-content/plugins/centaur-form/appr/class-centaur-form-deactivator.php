<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Centaur_Form
 * @subpackage Centaur_Form/includes
 * @author     Centaur Marketing <dev@centaurmarketing.co>
 */
class Centaur_Form_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

		global $wpdb;

		require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );

		foreach ( Centaur_Form_Deactivator::get_db_schema() as $table_name => $sql ) {

			dbDelta($sql);

		}

	}

	/**
	 * Database Schema
	 *
	 * @since 	1.0.0
	 * @return	array	list of database tables
	 */
	public static function get_db_schema() {

		global $wpdb;

		$table 				= array(); // Set table array	
		$table_pre 			= $wpdb->prefix . 'centform_'; // Set Database Table Prefix

		// Form Table
		$table[$table_pre . 'form'] = 
			"DROP TABLE " . $table_pre . "form";

		// Form Meta Table
		$table[$table_pre . 'form_meta'] = 
			"DROP TABLE " . $table_pre . "form_meta";

		// Entry Table
		$table[$table_pre . 'entry'] = 
			"DROP TABLE " . $table_pre . "entry";

		// Entry Meta Table
		$table[$table_pre . 'entry_meta'] = 
			"DROP TABLE " . $table_pre . "entry_meta";

		// Settings Table
		$table[$table_pre . 'settings'] = 
			"DROP TABLE " . $table_pre . "settings";

		return $table;

	}

}