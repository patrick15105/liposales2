<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Centaur_Form
 * @subpackage Centaur_Form/includes
 * @author     Centaur Marketing <dev@centaurmarketing.co>
 */
class Centaur_Form {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Centaur_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'CENTAUR_FORM_VERSION' ) ) {
			$this->version = CENTAUR_FORM_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'centaur-form';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Centaur_Loader. Orchestrates the hooks of the plugin.
	 * - Centaur_i18n. Defines internationalization functionality.
	 * - Centaur_Admin. Defines all hooks for the admin area.
	 * - Centaur_Public. Defines all hooks for the public side of the site.
	 * - Centaur_Template. Processes all templates that will be used in the plugin.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * Setup WP autoloader function
		 */
		spl_autoload_register( array($this, 'autoloader') );

		$this->loader = new Centaur_Form_Loader();

	}

	/**
	 * Autoloads all classes
	 *
	 * @param 	string 	$class_name 	name of the class that will be loaded
	 * @return 	boolean return "true" if class was loaded. Otherwise, return "false"
	 *
	 * @since 	1.0.0
	 * @since   1.1.0 Replace "app" with "appr"
	 * @access 	private
	 */
	private function autoloader( $class_name ) {

		// Convert the class name to the file name
        $class_file = 'class-' . str_replace( '_', '-', strtolower($class_name) ) . '.php';
 
        // Set up the list of directories to look in
        $classes_dir 		= array();
        $include_dir 		= realpath( plugin_dir_path( __FILE__ ) );
        $classes_dir[] 		= $include_dir;

        // Add each of the possible directories to the list
        foreach( array( 'includes', 'integrations' ) as $option) {
            $classes_dir[] = str_replace( 'appr', $option, $include_dir );
        }

        // Look in each directory and see if the class file exists
        foreach ($classes_dir as $class_dir) {
            $inc = $class_dir . DIRECTORY_SEPARATOR .  $class_file;
            // If it does require it
            if (file_exists($inc)) {
                require_once $inc;
                return true;
            }
        }

        return false;

	}
	
	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Centaur_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Centaur_Form_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Centaur_Form_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_filter( 'set-screen-option', $plugin_admin, 'set_option', 10, 3 );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_admin_menu' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_plugin_settings' );

		$plugin_basename = plugin_basename( plugin_dir_path( __DIR__ ) . $this->plugin_name . '.php' );
		$this->loader->add_filter( 'plugin_action_links_' . $plugin_basename, $plugin_admin, 'add_action_links' );

		/**
		 * 
		 * Ajax Actions
		 * 
		 * @since	1.0.0
		 */
		$this->loader->add_action( 'wp_ajax_cform_save_form', $plugin_admin, 'save_form' );
		$this->loader->add_action( 'wp_ajax_nopriv_cform_save_form', $plugin_admin, 'save_form' );

		$this->loader->add_action( 'wp_ajax_cform_add_form_field', $plugin_admin, 'get_form_field' );
		$this->loader->add_action( 'wp_ajax_nopriv_cform_add_form_field', $plugin_admin, 'get_form_field' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Centaur_Form_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		/**
		 * Setup Shortcode
		 *
		 * @since	1.0.0
		 */
		$this->loader->add_shortcode( 'centaur_form', $plugin_public, 'display_form' );
		
		/**
		 * Ajax Actions
		 * 
		 * @since	1.0.0
		 */
		$this->loader->add_action( 'wp_ajax_cform_save_entry', $plugin_public, 'save_entry' );
		$this->loader->add_action( 'wp_ajax_nopriv_cform_save_entry', $plugin_public, 'save_entry' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Centaur_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
