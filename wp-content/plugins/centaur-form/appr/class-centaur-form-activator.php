<?php

/**
 * Fired during plugin activation
 *
 * @link       www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Centaur_Form
 * @subpackage Centaur_Form/includes
 * @author     Centaur Marketing <dev@centaurmarketing.co>
 */
class Centaur_Form_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		global $wpdb;

		require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );

		foreach ( Centaur_Form_Activator::get_db_schema() as $table_name => $sql ) {

			dbDelta($sql);

		}

	}

	/**
	 * Database Schema
	 *
	 * @since 	1.0.0
	 * @return	array	list of database tables
	 */
	public static function get_db_schema() {

		global $wpdb;

		$table 				= array(); // Set table array	
		$table_pre 			= $wpdb->prefix . 'centform_'; // Set Database Table Prefix
		$charset_collate 	= $wpdb->get_charset_collate();
		$max_index_length 	= 191; 

		// Form Table
		$table[$table_pre . 'form'] = 
			"CREATE TABLE " . $table_pre . "form (
				id mediumint(8) unsigned not null auto_increment,
				title varchar(150) not null,
				settings longtext,
				status varchar(20) not null default 'draft',
				date_created datetime not null default NOW(),
				date_updated datetime,
				PRIMARY KEY  (id)
			) $charset_collate";

		// Form Meta Table
		$table[$table_pre . 'form_meta'] = 
			"CREATE TABLE " . $table_pre . "form_meta (
				id mediumint(8) unsigned not null auto_increment,
				form_id mediumint(8) not null,
				title varchar(150) not null,
				settings longtext not null, 
				position int(2) not null default 0,
				PRIMARY KEY  (id)
			) $charset_collate";

		// Entry Table
		$table[$table_pre . 'entry'] = 
			"CREATE TABLE " . $table_pre . "entry (
				id int(10) unsigned not null auto_increment,
				form_id mediumint(8) not null,
				ip varchar(50) not null,
				user_agent varchar(250) not null default '',
				is_read int(1) not null default 0,
				author bigint(20) unsigned,
				status varchar(20) not null default 'active',
				date_created datetime not null default NOW(),
				date_updated datetime,
				PRIMARY KEY  (id)
			) $charset_collate";

		// Entry Meta Table
		$table[$table_pre . 'entry_meta'] = 
			"CREATE TABLE " . $table_pre . "entry_meta (
				id bigint(20) unsigned not null auto_increment,
				form_id mediumint(8) not null,
				entry_id int(10) not null,
				meta_key varchar(255),
				meta_value longtext,
				PRIMARY KEY  (id),
				KEY meta_key (meta_key($max_index_length)),
				KEY entry_id (entry_id),
				KEY meta_value (meta_value($max_index_length))				
			) $charset_collate";

		// Views Table
		$table[$table_pre . 'views'] = 
			"CREATE TABLE " . $table_pre . "views (
				id mediumint(8) unsigned not null auto_increment,
				form_id mediumint(8) not null,
				ip varchar(50) not null,
				user_agent varchar(250) not null default '',
				platform varchar(250) not null default '',
				referral varchar(250) not null default '',
				first_date_inquiry datetime not null default NOW(),
				last_date_inquiry datetime,
				PRIMARY KEY  (id)
			) $charset_collate";

		// Settings Table
		$table[$table_pre . 'settings'] = 
			"CREATE TABLE " . $table_pre . "settings (
				id mediumint(8) unsigned not null auto_increment,
				settings longtext,
				date_created datetime not null default NOW(),
				date_updated datetime,
				PRIMARY KEY  (id)
			) $charset_collate";

		return $table;

	}

}
