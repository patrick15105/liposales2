<?php

/**
 * Template for viewing all Forms
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/templates/admin
 */

global $cf_forms;

?>
<div class="centaur-section">

	<?php 
	if ( isset($_REQUEST['result']) && $_REQUEST['result'] == "success" ) {
		echo '<div class="centaur-notification success">
				<div class="message">Success! <span class="centaur-close"><i class="fa fa-times"></i></span></div>
			  </div>';
	}
	?>

	<div class="centaur-heading">
		<h1 class="centaur-title"><?php _e('View All Forms', 'centaur-forms');?></h1>
	</div>
	<div class="centaur-content" id="poststuff">
		<div class="metabox-holder columns-2" id="post-body">
			<div id="post-body-content">
				<div class="meta-box-sortables ui-sortable">
					<form method="POST">
						<?php
							$cf_forms->prepare_items();
							$cf_forms->search_box(  __( 'Find', $cf_forms->plugin_name ), $cf_forms->plugin_name );
							$cf_forms->display();							
						?>
					</form>
				</div>
			</div>
		</div>
		<br class="clear"/>
	</div>
</div>