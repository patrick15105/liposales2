<?php

/**
 * Template for viewing Plugin Settings
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/templates/admin
 */

?>
<div class="centaur-section">
	<div class="centaur-heading">
		<h1 class="centaur-title"><?php _e('Settings', 'centaur-forms');?></h1>
	</div>
	<div class="centaur-content wrap">

		<form action="options.php" method="POST" name="cform-settings" enctype="multipart/form-data">

			<?php settings_fields( 'centaur-form-settings' ); ?>
			<?php do_settings_sections( 'centaur-form-settings' ); ?>

			<!-- General Settings Panel -->
			<div class="centaur-inline" id="panel-general">
				<div class="centaur-header">
					<h3 class="centaur-title">General</h3>
					<p class="centaur-caption">Manage form configurations</p>
				</div> 
				<div class="centaur-section">
					<div class="centaur-wrap">
						<div class="centaur-header">
							<h4 class="centaur-title">Email</h4>
						</div>
						<table class="form-table">
							<tbody>
								<tr>
									<th scope="row">Preloader</th>
									<td>
										<div id="cform-preview">
											<img data-src="" src="<?php echo esc_attr( get_option('preloader-path') ); ?>" class="centaur-thumbnail" id="cform-thumbnail-preloader"/>
										</div>
										<input type="hidden" name="preloader-path" value="<?php echo esc_attr( get_option('preloader-path') ); ?>" />
										<input type="button" value="Upload Image" class="button-primary" id="upload-image" />
										<p class="centaur-caption">Use GIF images only</p>
									</td>
								</tr>
								<tr>
									<th scope="row">Header</th>
									<td>
										<div id="cform-preview">
											<img data-src="" src="<?php echo esc_attr( get_option('header-path') ); ?>" class="centaur-thumbnail" id="cform-thumbnail-header"/>
										</div>
										<input type="hidden" name="header-path" value="<?php echo esc_attr( get_option('header-path') ); ?>" />
										<input type="button" value="Upload Image" class="button-primary" id="upload-image" />
									</td>
								</tr>
								<tr>
									<th scope="row">From Name</th>
									<td>
										<input type="text" name="cform-from-name" id="cform-from-name" class="centaur-input" value="<?php echo ( get_option('cform-from-name') != '' ) ? get_option('cform-from-name') : "" ; ?>" />
										<p class="centaur-caption">Enter the name you would like the notification email sent from.</p>
									</td>
								</tr>
								<tr>
									<th scope="row">From Email</th>
									<td>
										<input type="text" name="cform-from-email" id="cform-from-email" class="centaur-input" value="<?php echo ( get_option('cform-from-email') != '' ) ? get_option('cform-from-email') : get_bloginfo('admin_email') ; ?>" />
										<p class="centaur-caption">Set email address where email notification will be sent from.</p>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<!-- Integrations Panel -->
			<div class="centaur-inline" id="panel-integration">
				<div class="centaur-header">
				<h3 class="centaur-title">Integrations</h3>
					<p class="centaur-caption">Manage Form Integrations with other systems and plugins</p>
				</div>
				<table class="form-table">
					<tbody>
						<tr>
							<th scope="row">Patient Management System</th>
							<td>
								<div class="centaur-header">
									<input type="checkbox" name="cform-integrate_pms" id="trigger-subpanel-integration" <?php echo ( get_option('cform-integrate_pms') == "on" ) ? "checked" : "" ; ?> />
									<label for="cform-integrate_pms">Enable</label>
								</div>
								<div class="centaur-wrap" id="subpanel-integration">
									<div class="centaur-block">
										<span class="centaur-caption">Use same wordpress database? </span>
										<input type="checkbox" name="pms-samedb" <?php echo ( get_option('pms-samedb') == "on" ) ? "checked" : "" ; ?> >
									</div>
									<p class="centaur-caption centaur-block">If no, please provide the following database details for PMS Access:</p>
									<div class="centaur-inline">
										<label for="pms-name" class="centaur-label centaur-block">Database Name</label>
										<input type="text" name="pms-name" class="centaur-input centaur-block" value="<?php echo esc_attr( get_option('pms-name') ); ?>">
									</div>
								</div>
							</td>
						</tr>
						<!--tr>
							<th scope="row">Spamcheck by Postmark</th>
							<td>
								<div class="centaur-header">
									<input type="checkbox" name="cform-integrate_postmark" id="trigger-subpanel-integration" <?php echo ( get_option('cform-integrate_postmark') == "on" ) ? "checked" : "" ; ?>  />
									<label for="cform-integrate_postmark">Enable</label>
								</div>
							</td>
						</tr-->
						<tr>
							<th scope="row">Stop Forum Spam</th>
							<td>
								<div class="centaur-header">
									<input type="checkbox" name="cform-integrate_stopforumspam" id="trigger-subpanel-integration" <?php echo ( get_option('cform-integrate_stopforumspam') == "on" ) ? "checked" : "" ; ?>  />
									<label for="cform-integrate_stopforumspam">Enable</label>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<?php submit_button('Save Changes', 'primary', 'submit', TRUE); ?>
			
		</form>

		<div class="centaur-section"></div>
	</div>

</div>