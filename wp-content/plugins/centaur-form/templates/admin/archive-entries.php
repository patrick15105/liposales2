<?php

/**
 * Template for viewing all Form Entries
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/templates/admin
 */

global $cf_entries;

?>
<div class="centaur-section">
	<div class="centaur-heading">
		<h1 class="centaur-title"><?php _e('Entries', $cf_entries->plugin_name );?></h1>
	</div>
	<div class="centaur-contet">
		<div class="centaur-header">
			<span class="centaur-label">Select Form</span>
			<?php 
				$cf_entries->display_form_list();
			?>
		</div>
	</div>
	<div class="centaur-content" id="poststuff">
		<div class="metabox-holder columns-2" id="post-body">
			<div id="post-body-content">
				<div class="meta-box-sortables ui-sortable">

					<?php 

					if ( isset($_GET['form_id']) && !empty( $_GET['form_id'] ) && is_numeric( $_GET['form_id'] ) ) {

					?>
					<form method="POST">
						<?php
							$cf_entries->prepare_items();
							$cf_entries->search_box(  __( 'Find', $cf_entries->plugin_name ), $cf_entries->plugin_name );
							$cf_entries->display();							
						?>
					</form>
					<?php

					} else {
					?>
					<div class="centaur-block centaur-message">Please select a from to view entries.</div>
					<?php
					}

					?>

				</div>
			</div>
		</div>
		<br class="clear"/>
	</div>
</div>