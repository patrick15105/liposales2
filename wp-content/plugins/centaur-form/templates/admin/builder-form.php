<?php

/**
 * Template for viewing all Forms
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/templates/admin
 */

global $cent_form;

$form_data = $cent_form->item;

?>

<div class="centaur-component" id="cform-builder" data-id="<?php echo ( isset( $_GET['form_id'] ) )? $_GET['form_id'] : ""; ?>" >

	<?php 
	if ( isset($_REQUEST['result']) && $_REQUEST['result'] == "success" ) {
		echo '<div class="centaur-notification success">
				<div class="message">Success! <span class="centaur-close"><i class="fa fa-times"></i></span></div>
			  </div>';
	}
	?>

	<div class="centaur-header">
		<h1 class="centaur-title">
			<?php _e( ( isset($form_data->title) ) ? $form_data->title: "Create New Form", 'centaur-forms');?>
			<small><?php echo ( isset( $_GET['form_id'] ) )? "[ ID: " . $_GET['form_id'] ." ]" : ""; ?></small>				
		</h1>

		<div class="centaur-subheader">
			<a href="?page=<?php echo esc_attr( $_REQUEST['page'] ); ?>&view=edit<?php echo ( ( isset($_REQUEST['form_id']) ) ? "&action=edit&form_id=" . absint( $_REQUEST['form_id'] ): "" ); ?>" class="centaur-link <?php echo ( ( isset($_GET['view']) && $_GET['view'] == 'edit' ) ? 'active' : '' ); ?>">Edit Fields</a>
			&nbsp;|&nbsp;
			<a href="?page=<?php echo esc_attr( $_REQUEST['page'] ); ?>&view=settings<?php echo ( ( isset($_REQUEST['form_id']) ) ? "&action=edit&form_id=" . absint( $_REQUEST['form_id'] ): "" ); ?>" class="centaur-link <?php echo ( ( ( !isset( $_GET['action'] ) && !isset( $_GET['form_id'] ) ) || ( isset($_GET['view']) && $_GET['view'] == 'settings' ) ) ? 'active': '' ); ?>">Form Settings</a>
		</div>

	</div>

	<form action="#" method="POST" id="cform-builder-form" class="centaur-content" enctype="multipart/form-data" >

		<?php 

		// Settings View
		if ( ( !isset( $_GET['action'] ) && !isset( $_GET['form_id'] ) ) || ( isset($_GET['view']) && $_GET['view'] == 'settings' ) ) {
			$cent_form->display_form_settings(); 
		}

		// Edit Fields View
		if ( isset($_GET['view']) && $_GET['view'] == 'edit' ) {
			$cent_form->display_field_settings(); 
		}

		?>

	</form>

</div>