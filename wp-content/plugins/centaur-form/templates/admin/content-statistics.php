<?php

/**
 * Template for viewing Plugin Statistics
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/templates/admin
 */

?>
<div class="centaur-section">
	<div class="centaur-heading">
		<h1 class="centaur-title"><?php _e('View Statistics', 'centaur-forms');?></h1>
	</div>
	<div class="centaur-content"></div>
</div>