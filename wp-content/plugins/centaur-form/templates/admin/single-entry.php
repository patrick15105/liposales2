<?php

/**
 * Template for viewing all Form Entries
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/templates/admin
 */

global $cent_entry;

$entry = $cent_entry->get_item();

?>
<div class="centaur-section">
	<div class="centaur-heading">
		<span class="centaur-caption centaur-block">
			<a href="?page=centaur-form&form_id=<?php echo $entry->form_id; ?>" class="centaur-link"><i class="fa fa-arrow-left"></i>Back to Entries</a>
		</span>
		<h1 class="centaur-title"><?php _e( ucwords($entry->title) . " Form: Entry #" . $entry->id, $cent_entry->plugin_name ); ?></h1>
	</div>

	<!-- GENERAL INFORMATION -->
	<div class="centaur-inline">
		<table class="form-table">
			<tbody>
			<?php 
				foreach ( $entry->fields as $meta_key => $meta_value ) {

					if ( $meta_key == "first_name" ) {
						echo "
							<tr>
							<th scope=\"row\">Name</th>
							<td>{$meta_value}";
					} elseif ( $meta_key == "last_name" ) {
						echo " {$meta_value}</td>
							</tr>";
					} else {
						echo "
						<tr>
							<th scope=\"row\">{$meta_key}</th>
							<td>{$meta_value}</td>
						</tr>";
					} 

				}
			?>	
			</tbody>
		</table>		
	</div>
	<!-- TECHNICAL DETAILS -->
	<div class="centaur-inline">
		<div class="centaur-header">
			<h4 class="centaur-title">Technical Information</h4>
		</div>
		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row">Status</th>
					<td><?php echo $entry->status; ?></td>
				</tr>
				<tr>
					<th scope="row">Source URL</th>
					<td><?php echo $entry->source_url; ?></td>
				</tr>
				<tr>
					<th scope="row">IP Address</th>
					<td><?php echo ( $entry->ip_address != "" ) ? $entry->ip_address : $entry->remote_ip_address; ?></td>
				</tr>
				<tr>
					<th scope="row">Device</th>
					<td><?php echo $entry->device; ?></td>
				</tr>
				<tr>
					<th scope="row">Operating System</th>
					<td><?php echo $entry->os; ?></td>
				</tr>
				<tr>
					<th scope="row">Browser</th>
					<td><?php echo $entry->browser; ?></td>
				</tr>
			</tbody>
		</table>		
	</div>
</div>