<?php

/**
 * Template for viewing all Forms
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/templates/admin
 */

global $cf_forms;

?>
<div class="centaur-component" id="create-cfform">
	<div class="centaur-header">
		<h1 class="centaur-title"><?php _e('Create Form', 'centaur-forms');?></h1>
		<p class="centaur-caption no-space">Provide the basic settings of the form</p>
	</div>
	<div class="centaur-content">

		<div class="postbox-container" id="primary-cfcontainer">

			<!-- STEP 1 -->
			<div class="postbox" id="cfform-settings">
				<div class="centaur-container">
					<label class="centaur-label centaur-block">Title</label>
					<input type="text" name="cfform[title]" class="centaur-block"/>
				</div>
				<div class="centaur-container">
					<h2 class="centaur-title">Notifications</h2>
					<p class="centaur-caption">To send email notifications, provide the following details</p>
					<table class="form-table">
						<tbody>
							<tr>
								<th>Email</th>
								<td><input type="text" name="cfform[notif_email]" class="centaur-block"/></td>
							</tr>
							<tr>
								<th>Subject</th>
								<td><input type="text" name="cfform[notif_subject]" class="centaur-block" /></td>
							</tr>
							<tr>
								<th>Message</th>
								<td><textarea name="cfform[notif_message]" class="centaur-block"></textarea></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="centaur-container">
					<h2 class="centaur-title">Confirmation</h2>
					<p class="centaur-caption">To send email notifications, provide the following details</p>
					<table class="form-table">
						<tbody>
							<tr>
								<th>Action</th>
								<td><select name="cfform[confirm_action]">
										<option value="message">Message</option>	
										<option value="redirect">Redirect to URL</option>	
									</select>
								</td>
							</tr>
							<tr>
								<th>Message</th>
								<td><textarea name="cfform[confirm_message]" class="centaur-block"></textarea></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<!-- STEP 2 -->
			<div class="postbox" id="cfform-fields">
				<div class="centaur-container">
				</div>
			</div>


		</div>
		
		<div class="postbox-container" id="secondary-cfcontainer">
			<div class="postbox submitbox">
				<div class="centaur-container active">
					<h2 class="centaur-title"><span class="centaur-step">1</span>Settings</h2>
				</div>
				<div class="centaur-container">
					<h2 class="centaur-title"><span class="centaur-step">2</span>Fields</h2>
				</div>
				<div class="centaur-container centaur-actions">
					<div class="centaur-inline centaur-item">
						<input type="submit" name="save" id="save-draft" value="Save Draft" class="button">
					</div>
					<div class="centaur-inline centaur-item">
						<input type="hidden" name="original_publish" id="original_publish" value="Publish">
						<input type="submit" name="publish" id="publish" value="Save & Next" class="button button-primary">
					</div>
				</div>
			</div>
		</div>

	</div>
</div>