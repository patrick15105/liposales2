<?php

/**
 * Template for form settings
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/templates/admin
 */

global $cform_meta;

?>
<div class="centaur-item">

	<!-- Preview of the Field -->
	<div class="centaur-container" id="cform-preview">
		<?php 

		if ( ! isset($cform_meta['displaylabel']) ) {
			
			echo '<label for="labelPrev" id="_cform-label-view" class="centaur-label centaur-inline">Phone</label>';

		} 

		?>
		<div class="centaur-inline" id="cform-field-actions">
			<i class="fa fa-gear" id="settings"></i>
			<i class="fa fa-times" id="remove"></i>
		</div>
		
		<input type="text" name="_labelPrev" value="" class="centaur-block" placeholder="<?php echo ( isset($cform_meta['placeholder']) && $cform_meta['placeholder'] != "" )? $cform_meta['placeholder'] : "Phone Number" ; ?>" disabled/>
		<p id="_cform-description-view"></p>
	</div>

	<!-- Field Settings -->
	<div class="centaur-container" id="cform-field-settings">
		<input type="hidden" name="cformfield_<?php echo $cform_meta['id']; ?>_id" value="<?php echo ( isset($cform_meta['id']) ) ? $cform_meta['id'] : "new"; ?>" />
		<input type="hidden" name="cformfield_<?php echo $cform_meta['id']; ?>_type" value="phone" />
		<div class="centaur-field-group">
			<label for="_label" class="centaur-label centaur-block">Label</label>
			<input type="text" name="cformfield_<?php echo $cform_meta['id']; ?>_label" id="label" value="<?php echo ( isset($cform_meta['label']) ) ? $cform_meta['label'] : "Phone" ; ?>" class="centaur-block" placeholder="Phone" />
		</div>
		
		<div class="centaur-field-group">
			<input type="checkbox" name="cformfield_<?php echo $cform_meta['id']; ?>_displaylabel" <?php echo ( isset($cform_meta['displaylabel']) ) ? "checked" : ""; ?> >
			<label for="label" class="centaur-label">Display Label?</label>
		</div>

		<div class="centaur-field-group">
			<label for="_label" class="centaur-label centaur-block">Placeholder</label>
			<input type="text" name="cformfield_<?php echo $cform_meta['id']; ?>_placeholder" id="placeholder" value="<?php echo ( isset($cform_meta['placeholder']) && $cform_meta['placeholder'] != "" ) ? $cform_meta['placeholder'] : "" ; ?>" class="centaur-block" />
		</div>

		<div class="centaur-field-group">
			<label for="_description" class="centaur-label centaur-block">Description</label>
			<textarea name="cformfield_<?php echo $cform_meta['id']; ?>_description" id="description" class="centaur-block" ><?php echo ( isset($cform_meta['description']) ) ? $cform_meta['description'] : "" ; ?></textarea>
		</div>

		<div class="centaur-field-group">
			<input type="checkbox" name="cformfield_<?php echo $cform_meta['id']; ?>_required" <?php echo ( isset($cform_meta['required']) ) ? "checked" : ""; ?> >
			<label for="label" class="centaur-label">Is Required?</label>
		</div>

	</div>

</div>