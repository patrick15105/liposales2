<?php

/**
 * Template for form settings
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/templates/admin
 */

global $cent_form;

?>
<div class="postbox submitbox">
	<div class="centaur-actions" id="cform-primary-actions">
		<div class="centaur-inline centaur-item">
			<a href="#" class="button" data-action="draft">Save Draft</a>
		</div>
		<div class="centaur-inline centaur-item">
		<?php
			if ( !isset( $_GET['form_id'] ) ) {
		?>
			<a href="#" class="button button-primary" data-action="save">Save & Next</a>
		<?php
			} else {
		?>
			<a href="#" class="button button-primary" data-action="publish">Publish</a>
		<?php
			}
		?>
		</div>
	</div>	
</div>

<div class="postbox submitbox">

	<div class="centaur-container" id="add-cform-fields">
		<h2 class="centaur-title">Available Fields</h2>
			
		<ul class="centaur-list">
			<li class="centaur-item">
				<a href="" class="centaur-link centaur-block" id="single-line-text">Single Line Text<i class="fa fa-plus"></i></a>
			</li>
			<li class="centaur-item">
				<a href="" class="centaur-link centaur-block" id="paragraph-text">Paragraph Text<i class="fa fa-plus"></i></a>
			</li>
			<li class="centaur-item">
				<a href="" class="centaur-link centaur-block" id="name">Name<i class="fa fa-plus"></i></a>
			</li>
			<li class="centaur-item">
				<a href="" class="centaur-link centaur-block" id="email">Email<i class="fa fa-plus"></i></a>
			</li>
			<li class="centaur-item">
				<a href="" class="centaur-link centaur-block" id="phone">Phone<i class="fa fa-plus"></i></a>
			</li>
			<li class="centaur-item">
				<a href="" class="centaur-link centaur-block" id="calendar">Calendar<i class="fa fa-plus"></i></a>
			</li>
		</ul>
					
	</div>

</div>