<?php

/**
 * Template for form settings
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/templates/admin
 */

global $cent_form;

$form_data = $cent_form->item;

?>

<div class="centaur-content">
	<div class="centaur-container">
		<h2 class="centaur-title">General</h2>
		<table class="form-table">
			<tbody>
				<tr>
					<th>Title</th>
					<td>
						<input type="text" name="cform_title" class="centaur-block" id="cform-form-title" value="<?php echo ( isset($form_data->title) ) ? $form_data->title : ""; ?>"/>
						<p class="error message"></p>
					</td>
				</tr>
				<tr>
					<th>Display Title?</th>
					<td>
						<input type="checkbox" name="cform_show-title" <?php echo ( isset($form_data->show_title) ) ? "checked" : ""; ?> >
						<p class="error message"></p>
					</td>
				</tr>
				<tr>
					<th>Number of Columns</th>
					<td>
						<select name="cform_column-count" id="cform-form-columns" class="centaur-block">
							<option value="1" <?php echo ( isset($form_data->column_count) && $form_data->column_count == 1 ) ? "selected" : ""; ?> >1 Column</option>
							<option value="2" <?php echo ( isset($form_data->column_count) && $form_data->column_count == 2 ) ? "selected" : ""; ?> >2 Columns</option>
							<option value="3" <?php echo ( isset($form_data->column_count) && $form_data->column_count == 3 ) ? "selected" : ""; ?> >3 Columns</option>
						</select>
						<p class="error message"></p>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="centaur-container">
		<h2 class="centaur-title">Notifications</h2>
		<table class="form-table">
			<tbody>
				<tr>
					<th>Send To</th>
					<td>
						<input type="text" name="cform_notify-email" class="centaur-block" id="cform-form-notify-email" value="<?php echo ( isset($form_data->notify_email) ) ? $form_data->notify_email : "" ; ?>" />
						<p class="centaur-caption">Separate each email address with a comma (,) or space.</p>
						<p class="error message"></p>
					</td>
				</tr>
				<tr>
					<th>BCC</th>
					<td>
						<input type="text" name="cform_notify-bcc" class="centaur-block" id="cform-form-notify-bcc" value="<?php echo ( isset($form_data->notify_bcc) ) ? $form_data->notify_bcc : "" ; ?>" />
						<p class="centaur-caption">Separate each email address with a comma (,) or space.</p>
						<p class="error message"></p>
					</td>
				</tr>
				<tr>
					<th>Subject</th>
					<td>
						<input type="text" name="cform_notify-name" class="centaur-block" id="cform-form-notify-name" value="<?php echo ( isset($form_data->notify_name) ) ? $form_data->notify_name : "" ; ?>" />
						<p class="error message"></p>
					</td>
				</tr>
				<tr>
					<th>Message</th>
					<td><textarea name="cform_notify-note" class="centaur-block" id="cform-form-notify-message"><?php echo ( isset($form_data->notify_note) ) ? $form_data->notify_note : "" ; ?></textarea></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="centaur-container">
		<h2 class="centaur-title">Confirmation</h2>
		<p class="centaur-caption">To send email notifications, provide the following details</p>
		<table class="form-table">
			<tbody>
				<tr>
					<th>Action</th>
					<td><select name="cform_confirm-action">
							<option value="message" <?php echo ( isset($form_data->confirm_action) && $form_data->confirm_action == "message" ) ? "selected" : "" ; ?> >Message</option>	
							<option value="redirect" <?php echo ( isset($form_data->confirm_action) && $form_data->confirm_action == "redirect" ) ? "selected" : "" ; ?> >Redirect to URL</option>	
						</select>
					</td>
				</tr>
				<tr>
					<th>Message</th>
					<td>
						<textarea name="cform_confirm-value" class="centaur-block" id="message">
							<?php echo ( isset($form_data->confirm_value) ) ? $form_data->confirm_value : "" ; ?>
						</textarea>
						<input type="text" name="cform_confirm-value" class="centaur-block" id="redirect" value="<?php echo ( isset($form_data->confirm_value) ) ? $form_data->confirm_value : "" ; ?>" style="display: none;"/>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="centaur-container">
		<h2 class="centaur-title">Integrations</h2>
		<p class="centaur-caption">Integrate to other Centaur Systems</p>
		<table class="form-table">
			<tbody>
				<tr>
					<th>Patient Management System</th>
					<td><input type="checkbox" name="cform_integrate-pms" <?php echo ( isset($form_data->integrate_pms) ) ? "checked" : ""; ?> >
						<p class="centaur-caption"><a href="<?php echo admin_url( 'admin.php?page=centaur-form-settings' ); ?>">Click here</a> for additional settings.</p>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>