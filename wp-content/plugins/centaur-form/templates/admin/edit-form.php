<?php

/**
 * Template for viewing all Forms
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/templates/admin
 */

global $cf_forms;

?>
<div class="centaur-component" id="create-cfform">
	<div class="centaur-header">
		<h1 class="centaur-title"><?php _e('Create Form', 'centaur-forms');?></h1>
		<p class="centaur-caption no-space">Provide the basic settings of the form</p>
	</div>
	<div class="centaur-content">

		<div class="postbox-container" id="primary-cfcontainer">

			<div class="postbox" id="cfform-fields">
				<div class="centaur-container centaur-header">
					<h2 class="centaur-title">Form Title | <small>ID #000</small></h2>
				</div>
				<div class="centaur-container">
					
				</div>
			</div>

		</div>
		
		<div class="postbox-container" id="secondary-cfcontainer">
			<div class="postbox submitbox">
				<div class="centaur-container active">
					<h2 class="centaur-title">Settings<i class="fa fa-chevron-down"></i></h2>
					<div class="centaur-content">
						<label class="centaur-label centaur-block">Title</label>
						<input type="text" name="cfform[title]" class="centaur-block" />
						<hr/>

						<h2 class="centaur-title">Notifications</h2>
						<p class="centaur-caption">To send email notifications, provide the following details</p>
						<label class="centaur-label centaur-block">Email</label>
						<input type="text" name="cfform[notif_email]" class="centaur-block"/>
						<label class="centaur-label centaur-block">Subject</label>
						<input type="text" name="cfform[notif_subject]" class="centaur-block" />
						<label class="centaur-label centaur-block">Message</label>
						<textarea name="cfform[notif_message]" class="centaur-block"></textarea>
						<hr/>

						<h2 class="centaur-title">Confirmation</h2>
						<p class="centaur-caption">To send email notifications, provide the following details</p>
						<label class="centaur-label centaur-block">Action</label>
						<select name="cfform[confirm_action]">
							<option value="message">Message</option>	
							<option value="redirect">Redirect to URL</option>
						</select>
						<label class="centaur-label centaur-block">Message</label>
						<textarea name="cfform[confirm_message]" class="centaur-block"></textarea>						
					</div>
				</div>
				<div class="centaur-container">
					<h2 class="centaur-title">Fields <i class="fa fa-chevron-down"></i></h2>
				</div>
				<div class="centaur-container centaur-actions">
					<div class="centaur-inline centaur-item">
						<input type="submit" name="save" id="save-draft" value="Save as Draft" class="button">
					</div>
					<div class="centaur-inline centaur-item">
						<input type="hidden" name="original_publish" id="original_publish" value="Publish">
						<input type="submit" name="publish" id="publish" value="Publish" class="button button-primary">
					</div>
				</div>
			</div>
		</div>

	</div>
</div>