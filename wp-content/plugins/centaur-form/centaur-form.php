<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              www.centaurmarketing.co
 * @since             1.0.0
 * @since   		  1.1.0 	Replace "app" with "appr"
 * @package           Centaur
 *
 * @wordpress-plugin
 * Plugin Name:       Centaur Form
 * Plugin URI:        www.centaurmarketing.co
 * Description:       Easily manage web forms and form entries inside the Wordpress Admin.
 * Version:           1.1.1
 * Author:            Centaur Marketing
 * Author URI:        www.centaurmarketing.co
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       centaur
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CENTAUR_FORM_VERSION', '1.1.1' );

/**
 * The code that runs during plugin activation.
 * This action is documented in appr/class-centaur-activator.php
 */
function activate_centaur_form() {
	require_once plugin_dir_path( __FILE__ ) . 'appr/class-centaur-form-activator.php';
	Centaur_Form_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in appr/class-centaur-deactivator.php
 */
function deactivate_centaur_form() {
	require_once plugin_dir_path( __FILE__ ) . 'appr/class-centaur-form-deactivator.php';
	Centaur_Form_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_centaur_form' );
register_deactivation_hook( __FILE__, 'deactivate_centaur_form' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'appr/class-centaur-form.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_centaur_form() {

	$plugin = new Centaur_Form();
	$plugin->run();

	if ( is_admin() ) {
    	new Centaur_Form_Plugin_Updater( $plugin->get_plugin_name(),  $plugin->get_version(), __FILE__ );
	}

}

run_centaur_form();