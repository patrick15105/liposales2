jQuery(document).ready(function($) {

	'use strict';



	var CFormHandler = {



			formID: "",



			ipAddress: "",



			/**

			 * Initiate Form Handler

			 *

			 * @since  1.0.0

			 */

			init: function() {



				$(document).ready( CFormHandler.ready );



			},



			/**

			 * Document Ready Function

			 *

			 * @since  1.0.0

			 */

			ready: function() {



				// Retrieve User IP Address

				$.getJSON( "https://api.ipify.org?format=jsonp&callback=?", function(data) { 

					CFormHandler.ipAddress = data.ip; 

				});



				$(".page #cform-handler").each( function() {

					CFormHandler.formActions($(this));

				});



			},



			/**

			 * Input Actions

			 *

			 * @since 1.0.0

			 */

			formActions: function( CForm ) {



                CForm.on( "click", ".centaur-actions .fa-times", function() {

                	CForm.find(".centaur-modal").removeClass("active");

                	CForm.find(".centaur-input").val("");

                });



				// Submit Button

				CForm.on( "click", "input[type=submit]#submit, button[type=submit]#submit", function(event) {

					event.preventDefault();

					CFormHandler.validateForm( CForm );

				});



			},



			/**

			 * Validate Fields

			 *

			 * @since  1.0.0

			 * @return string

			 */

			validateField: function( required = false, value = "", type = "" ) {



				var message = '';



				// Validate if field is required and it has value.

				if ( required && value == "" ) {

					message = 'Sorry but this field is required.';

				} else if ( value != "" ) {

					if ( type == "email" ) {

						var emailReg = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

						if(!emailReg.test( value ) ) {

							message = 'Please enter a valid email address.';

						}					

					}

				}



				return message;



			},



			/**

			 * Validate Form

			 *

			 * @since  1.0.0

			 */

			validateForm: function( CForm ) {



				var valid = true;



				CForm.find(".centaur-input").each( function() {



					var message = CFormHandler.validateField( $(this).data("required"), $(this).val(), $(this).data("type") );



					if ( message != '' ) {

						$(this).addClass("error");

						$(this).parent().find(".centaur-message").addClass("error").text(message);

						valid = false;

					} else {

						$(this).removeClass("error");

						$(this).parent().find(".centaur-message").removeClass("error").text("");

					}



				});



				if ( valid ) {



					CForm.find(".centaur-modal").addClass("active");

					CFormHandler.processForm( CForm );



				}



			},



			/**

			 * Process Form

			 *

			 * @since  1.0.0

			 */

			processForm: function( CForm ) {



				var data = {

						action: 'cform_save_entry',

						data: JSON.stringify( CForm.find("#cform-entry-form").serializeArray() ),

						source: window.location.href,

						referral: document.referrer,

						user: navigator.userAgent,

						ip: CFormHandler.ipAddress,

						id: CForm.data('id'),

						nonce: centaur_form.nonce

					};



				$.post(centaur_form.ajax_url, data, function(res) {



					CForm.find(".centaur-modal").removeClass("active").empty();



					var response = JSON.parse(res);



					// If action is redirection, redirect to url 

					if ( response.action == "redirect" ) {

						window.location.href = response.value;

					// Otherwise, show message	

					} else if ( response.action == "message" ) {

						var message = '<div class="centaur-container"><div class="centaur-actions"><i class="fa fa-times" id="centaur-modal-close"></i></div>' + response.value + '</div>';

						CForm.find(".centaur-modal").addClass("active").append(message);

					}



				}).fail( function( xhr, textStatus, e ) {

					console.log( 'Error: ' + xhr.responseText + " " + e);

				});



			},



		};



	CFormHandler.init();



});	