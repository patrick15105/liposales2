jQuery(document).ready(function($) {
	'use strict';

	var CForm,
		CFConfig,
		CFBuilder = {

		/**
		 * Form ID
		 *
		 * @since  1.0.0
		 */
		formID: "",

		/**
		 * Form ID
		 *
		 * @since  1.0.0
		 */
		withErrors: false,

		/**
		 * Initiate
		 *
		 * @since  1.0.0
		 */
		init: function() {

			$(document).ready(CFBuilder.ready);

		},

		/**
		 * Document Ready
		 *
		 * @since  1.0.0
		 */
		ready: function() {

			CForm = $("#cform-builder");

			CFBuilder.formID = CForm.data('id');

			// Setup Panel
			CFBuilder.setPanels();
			
			// Bind Actions
			CFBuilder.bindActions();
			
			// Bind Actions
			CFBuilder.sortFields();

		},

		/**
		 * Set Panel Displays
		 *
		 * @since  1.0.0
		 */
		setPanels: function() {

			var sidebar = $("#centaur-secondary .postbox");

			$('#centaur-primary .postbox').addClass("active");
			sidebar.find(".centaur-container").addClass("active");

			if ( CFBuilder.formID == "" ) {
				$("#cform-fields").removeClass("active");
				sidebar.find("#add-cform-fields").removeClass("active");
			} else {
				$("#cform-settings").removeClass("active");
				sidebar.find("#cform-settings").removeClass("active");
			}

		},

		/**
		 * Binding Actions
		 *
		 * @since  1.0.0
		 */
		bindActions: function() {

			CFBuilder.buttonActions();

			CFBuilder.inputActions();

		},

		/**
		 * Binding Panel Actions
		 *
		 * @since  1.0.0
		 */
		buttonActions: function() {

			// Submit Button
			CForm.on( "click", "#cform-primary-actions .button", function(event) {
				event.preventDefault();
				CFBuilder.formHandler( $(this).data("action") );
				$(this).append('<i class="fa fa-spinner"><i>');
			});

			// Toggle Sidebar Buttons
			CForm.on( "click", "#centaur-secondary .centaur-title", function(event) {
				$(this).parent().toggleClass('active');
				$(this).parent().siblings().toggleClass('active');
			});

			// Add Fields
			CForm.on( "click", "#add-cform-fields .centaur-link", function(event) {
				event.preventDefault();
				console.log('test');
				CFBuilder.addField( $(this).attr("id") );
			});

			// Toggle Settings
			CForm.on( "click", "#cform-preview #settings", function(event) {
				event.preventDefault();
				$(this).parents(".centaur-item").toggleClass("active");
			});

			// Remove Element
			CForm.on( "click", "#cform-preview #remove", function(event) {
				event.preventDefault();
				$(this).parents(".centaur-item").remove();
			});

		},

		/**
		 * Bind Validations
		 *
		 * @since  1.0.0
		 */
		inputActions: function() {

			// Form Title
			CForm.on( "focusout", "#cform-form-title", function(event) {
				CFBuilder.withErrors = CFBuilder.validateInput( "cform-form-title", "input", true );
			});

			// Form Notify Email
			CForm.on( "focusout", "#cform-form-notify-email", function(event) {
				CFBuilder.withErrors = CFBuilder.validateInput( "cform-form-notify-email", "email", true );
			});

			// Form Notify Subject
			CForm.on( "focusout", "#cform-form-notify-name", function(event) {
				CFBuilder.withErrors = CFBuilder.validateInput( "cform-form-notify-name", "input", true );
			});

			// Label Change
			CForm.on( "input", "#label", function(event) {
				event.preventDefault();
				$(this).parents(".centaur-item").find("#_cform-label-view").text( $(this).val() );
			});

			// Description Change
			CForm.on( "input", "#description", function(event) {
				event.preventDefault();
				$(this).parents(".centaur-item").find("#_cform-description-view").text( $(this).val() );
			});			

			// Description Change
			CForm.on( "input", "#format", function(event) {
				event.preventDefault();
				$(this).parents(".centaur-item").find(".centaur-field-group").removeClass("active");
				$(this).parents(".centaur-item").find("#name-" + $(this).val() ).addClass("active");
			});	

			// Description Change
			CForm.on( "change", "#confirmation", function(event) {
				event.preventDefault();
				$(this).parents(".centaur-item").find(".centaur-field-group").removeClass("active");
				if ( $(this).is(":checked") ) {
					$(this).parents(".centaur-item").find("#email-confirm").addClass("active");
				} else {
					$(this).parents(".centaur-item").find("#email-simple").addClass("active");
				}
			});

			// Add Location Field
			CForm.on( "change", "#cform-form-location", function(event) {
				event.preventDefault();
				if ( $("#cfform-field-list #location").length ) {
					$("#cfform-field-list #location").remove();
				} else {
					CFBuilder.addField("location");
				}
			});

			// Add Procedures Field
			CForm.on( "change", "#cform-form-procedures", function(event) {
				event.preventDefault();
				if ( $("#cfform-field-list #procedures").length ) {
					$("#cfform-field-list #procedures").remove();
				} else {
					CFBuilder.addField("procedures");
				}
			});

			// Remove Procedures Field if PMS Integration is unclicked.
			CForm.on( "change", "#trigger-subpanel-pms", function(event) {
				event.preventDefault();
				if ( !$(this).is(":checked") && $("#cfform-field-list #procedures").length ) {
					$("#cfform-field-list #procedures").remove();
				}
				$("#cform-form-procedures").prop('checked', false);
			});

		},

		/**
		 * Input Validations
		 *
		 * @since  1.0.0	
		 */
		validateInput: function( name, type, required = false ) {

			var parent 	= $( "#" + name ),
				value 	= parent.val(),
				message = "",
				filter 	= /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

			if ( required && value == "" ) {
				message = "Oh no! This is a required field. Kindly add a value here. Thanks!";
			} else if( type == "email" ) {

				var email_list = value.split(" ");
				for ( var i = 0; i < email_list.length; i++) {

					if ( !filter.test( email_list[i] ) ) {
						message = "Sorry but you entered an invalid email.";
					}
				}

			}

			parent.parent().find(".error").text( message );
			
			return ( message != "" ) ? true : false;

		},

		/**
		 * Form Validation
		 *
		 * @since  1.0.0
		 */
		validateForm: function() {

			var valid = true;

			if ( CFBuilder.formID == "" ) {

				if ( CFBuilder.validateInput( "cform-form-title", "input", true ) ) {
					valid = false;
				}

				if ( CFBuilder.validateInput( "cform-form-notify-email", "email", true ) ) {
					valid = false;
				}

				if ( CFBuilder.validateInput( "cform-form-notify-name", "input", true ) ) {
					valid = false;
				}

			}

			return valid;

		},

		/**
		 * Handle Form Submissions
		 *
		 * @since  1.0.0	
		 */
		formHandler: function( action ) {

			if ( CFBuilder.withErrors = CFBuilder.validateForm() ) {

				console.log( JSON.stringify( $('#cform-builder-form').serializeArray() ) );

				var data = {
					action: 'cform_save_form',
					data: JSON.stringify($('#cform-builder-form').serializeArray()),
					id: CFBuilder.formID,
					status: action,
					nonce: centaur_builder.nonce
				};

				$.post(centaur_builder.ajax_url, data, function(res) {

					if ( CFBuilder.formID == "" ) {
						CForm.attr( "data-id", res );
						CFBuilder.formID = res;
					}

					if ( action == "draft") {
						window.location.href = centaur_builder.save_exit + "&result=success";
					} else {
						window.location.href = centaur_builder.save_next + "&view=settings&action=edit&form_id=" + CFBuilder.formID + "&result=success";
					}

				}).fail( function( xhr, textStatus, e ) {
					console.log( 'Error: ' + xhr.responseText );
				});

			}

		},

		/**
		 * Add New Field
		 *
		 * @since  1.0.0	
		 */
		addField: function( type ) {

			var data = {
				action: 'cform_add_form_field',
				id: CFBuilder.formID,
				type: type,
				nonce: centaur_builder.nonce
			};

			$.post(centaur_builder.ajax_url, data, function(res) {

				$('#cfform-field-list').append(res);

			}).fail( function( xhr, textStatus, e ) {
				console.log( 'Error: ' + xhr.responseText );
			});

		},

		/**
		 * Sort Fields
		 *
		 * @since  1.0.0
		 */
		sortFields: function() {

			var fields = $('#cfform-field-list');

			fields.sortable({
				items: '> .centaur-item',
				axis: 'y',
				delay: 100,
				opacity: 0.75
			});

		},

	}

	CFBuilder.init();

	$(".form-table #upload-image").each( function(event) {

		var button 	= $(this),
			preview = button.parent().find("#cform-preview .centaur-thumbnail");

		button.on( "click", function(event) {
			var send_attachment_bkp = wp.media.editor.send.attachment;
			wp.media.editor.send.attachment = function(props, attachment) {
				preview.attr('src', attachment.url);
				preview.val(attachment.id);
				button.prev().val(attachment.url);
				wp.media.editor.send.attachment = send_attachment_bkp;
    		}
			wp.media.editor.open(button);
			return false;
		});

	});


	$("#trigger-subpanel-integration, #trigger-subpanel-pms").each( function() {
		var button	= $(this),
			panel	= button.parents("td").find("#subpanel-integration, #subpanel-pms");

		if ( button.is(':checked') ) {
			panel.addClass("active");
		}

		button.on( "click", function(event) {
			if ( !$(this).is(':checked') ) {
				panel.removeClass("active");
			} else {
				panel.addClass("active");
			}
		});
	});

	/**
	 * Redirect to Entries list
	 * 
	 * @since  1.0.0
	 */
	$("#select-cform-list").on( "change", function(event) {
		event.preventDefault();

		var form_id = $(this).val();

		window.location.href = window.location.href + "&form_id=" + form_id;

	});


	$(document).on( "click", ".centaur-notification .centaur-close", function(event) {
		$(".centaur-notification").hide();
	});

});	