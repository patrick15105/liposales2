<?php 

/**
 * File for integration PMS to Centaur Form
 *
 * A class definition that includes attributes and functions that 
 * will be used for integrating PMS to Centaur Form.
 *
 * @link       www.centaurmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/integrations
 * @author     Centaur Marketing <dev@centaurmarketing.co>
 */

class Centaur_Form_Stopforumspam_Integration {

	/**
	 * Handles Integration Actions
	 *
	 * @since	1.0.0
	 * @param	string	$email_address
	 * @param	string	$options
	 *
	 * @return	boolean
	 */	
	public function integrate( $email_address ) {

		$results 	= $this->verify( $email_address );
		$is_spam 	= false;
		$email_info = $results->email;

		error_log( "Integration stopforumspam: " . $email_address);

		if ( $email_info->appears == "1" ) {
			$is_spam = true;
		}

		error_log( "Integration stopforumspam results: " . ( (string) $is_spam ) );

		return $is_spam;

	}

	/**info@ranksindia.net
	 * Email Verification
	 *
	 * @since	1.0.0
	 * @param	string	$email_address
	 * @param	string	$options
	 *
	 * @return	object
	 */
	private function verify( $email_address ) {

		if ( empty($email_address) ) {
			return false;
		}


		$url = 'http://api.stopforumspam.org/api?email=' . $email_address . '&json&confidence';
		$results = wp_remote_get( $url );

		return json_decode( $results['body'] );
	}

}