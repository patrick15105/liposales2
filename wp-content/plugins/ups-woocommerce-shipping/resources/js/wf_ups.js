jQuery(document).ready(function(){

	// Toggle Estimated delivery related data
	ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_estimated_delivery', '.ph_ups_est_delivery' );
	jQuery('#woocommerce_wf_shipping_ups_enable_estimated_delivery').click(function(){
		ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_estimated_delivery', '.ph_ups_est_delivery' );
	});

	// Toggle pickup options
	wf_ups_load_pickup_options();
	jQuery('#woocommerce_wf_shipping_ups_pickup_enabled').click(function(){
		wf_ups_load_pickup_options();
	});
	
	// Toggle declaration Statement for NAFTA Certificate
	ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_commercial_invoice', '#woocommerce_wf_shipping_ups_nafta_co_form');
	jQuery('#woocommerce_wf_shipping_ups_commercial_invoice').click(function(){
		ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_commercial_invoice', '#woocommerce_wf_shipping_ups_nafta_co_form');
	});

	// Toggle declaration Statement for Producer Option & Blanket Period
	ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_nafta_co_form', '#woocommerce_wf_shipping_ups_nafta_producer_option');
	ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_nafta_co_form', '#woocommerce_wf_shipping_ups_blanket_begin_period');
	ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_nafta_co_form', '#woocommerce_wf_shipping_ups_blanket_end_period');
	jQuery('#woocommerce_wf_shipping_ups_nafta_co_form').click(function(){
		ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_nafta_co_form', '#woocommerce_wf_shipping_ups_nafta_producer_option');
		ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_nafta_co_form', '#woocommerce_wf_shipping_ups_blanket_begin_period');
		ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_nafta_co_form', '#woocommerce_wf_shipping_ups_blanket_end_period');
	});

	jQuery('#woocommerce_wf_shipping_ups_commercial_invoice').click(function(){
		ph_ups_toggle_nafta_certificate_options();
	});
	jQuery('#woocommerce_wf_shipping_ups_nafta_co_form').click(function(){
		ph_ups_toggle_nafta_certificate_options();
	});
	ph_ups_toggle_nafta_certificate_options();
	function ph_ups_toggle_nafta_certificate_options()
	{
		if( ! jQuery('#woocommerce_wf_shipping_ups_commercial_invoice').is(':checked') ) {
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_commercial_invoice', '#woocommerce_wf_shipping_ups_nafta_producer_option' );
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_commercial_invoice', '#woocommerce_wf_shipping_ups_blanket_begin_period' );
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_commercial_invoice', '#woocommerce_wf_shipping_ups_blanket_end_period' );
		}
		else if( ! jQuery('#woocommerce_wf_shipping_ups_nafta_co_form').is(':checked') ){
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_nafta_co_form', '#woocommerce_wf_shipping_ups_nafta_producer_option' );
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_nafta_co_form', '#woocommerce_wf_shipping_ups_blanket_begin_period' );
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_nafta_co_form', '#woocommerce_wf_shipping_ups_blanket_end_period' );
		}
		else{
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_nafta_co_form', '#woocommerce_wf_shipping_ups_nafta_producer_option' );
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_nafta_co_form', '#woocommerce_wf_shipping_ups_blanket_begin_period' );
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_nafta_co_form', '#woocommerce_wf_shipping_ups_blanket_end_period' );
		}
	}
	
	// Toggle declaration Statement for Commercial Invoice
	ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_commercial_invoice', '#woocommerce_wf_shipping_ups_declaration_statement');
	jQuery('#woocommerce_wf_shipping_ups_commercial_invoice').click(function(){
		ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_commercial_invoice', '#woocommerce_wf_shipping_ups_declaration_statement');
	});

	jQuery('#woocommerce_wf_shipping_ups_pickup_date').change(function(){
		wf_ups_load_working_days();
	});

	// Toggle Minimum Insurance amount
	ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_insuredvalue', '#woocommerce_wf_shipping_ups_min_order_amount_for_insurance' );
	jQuery('#woocommerce_wf_shipping_ups_insuredvalue').click(function(){
		ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_insuredvalue', '#woocommerce_wf_shipping_ups_min_order_amount_for_insurance' );
	});

	// Toggle Minimum Insurance amount
	ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_ship_from_address_different_from_shipper', '.ph_ups_different_ship_from_address' );
	jQuery('#woocommerce_wf_shipping_ups_ship_from_address_different_from_shipper').click(function(){
		ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_ship_from_address_different_from_shipper', '.ph_ups_different_ship_from_address' );
	});

	// Toggle Label Size
	ph_toggle_ups_label_size();
	jQuery('#woocommerce_wf_shipping_ups_show_label_in_browser').click(function(){
		ph_toggle_ups_label_size();
	});

	// Toggle UPS Freight Class Settings
	ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_freight', '#woocommerce_wf_shipping_ups_freight_class' );
	jQuery('#woocommerce_wf_shipping_ups_enable_freight').click(function(){
		ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_freight', '#woocommerce_wf_shipping_ups_freight_class' );
	});

	// Toggle UPS Density Based Ratig
	ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_freight', '#woocommerce_wf_shipping_ups_enable_density_based_rating' );
	jQuery('#woocommerce_wf_shipping_ups_enable_freight').click(function(){
		ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_freight', '#woocommerce_wf_shipping_ups_enable_density_based_rating' );
	});

	ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_density_based_rating', '#woocommerce_wf_shipping_ups_density_length' );
	jQuery('#woocommerce_wf_shipping_ups_enable_density_based_rating').click(function(){
		ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_density_based_rating', '#woocommerce_wf_shipping_ups_density_length' );
	});
	ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_density_based_rating', '#woocommerce_wf_shipping_ups_density_width' );
	jQuery('#woocommerce_wf_shipping_ups_enable_density_based_rating').click(function(){
		ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_density_based_rating', '#woocommerce_wf_shipping_ups_density_width' );
	});
	ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_density_based_rating', '#woocommerce_wf_shipping_ups_density_height' );
	jQuery('#woocommerce_wf_shipping_ups_enable_density_based_rating').click(function(){
		ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_density_based_rating', '#woocommerce_wf_shipping_ups_density_height' );
	});
	

	ph_ups_toggle_density_description();
	function ph_ups_toggle_density_description()
	{
		if( ! jQuery('#woocommerce_wf_shipping_ups_enable_density_based_rating').is(':checked') || ! jQuery('#woocommerce_wf_shipping_ups_enable_freight').is(':checked') ) {
			jQuery('#woocommerce_wf_shipping_ups_density_description').next('p').hide();
		}
		else{
			jQuery('#woocommerce_wf_shipping_ups_density_description').next('p').show();
		}
	}
	jQuery('#woocommerce_wf_shipping_ups_enable_freight').click(function(){
		if( ! jQuery('#woocommerce_wf_shipping_ups_enable_density_based_rating').is(':checked') || ! jQuery(this).is(':checked') ) {
			jQuery('#woocommerce_wf_shipping_ups_density_description').next('p').hide();
		}
		else{
			jQuery('#woocommerce_wf_shipping_ups_density_description').next('p').show();
		}
	});
	jQuery('#woocommerce_wf_shipping_ups_enable_density_based_rating').click(function(){
		if( ! jQuery(this).is(':checked') ) {
			jQuery('#woocommerce_wf_shipping_ups_density_description').next('p').hide();
		}
		else{
			jQuery('#woocommerce_wf_shipping_ups_density_description').next('p').show();
		}
	});
	jQuery('#woocommerce_wf_shipping_ups_enable_freight').click(function(){
		ph_ups_toggle_density_dimensions();
	});
	jQuery('#woocommerce_wf_shipping_ups_enable_density_based_rating').click(function(){
		ph_ups_toggle_density_dimensions();
	});
	ph_ups_toggle_density_dimensions();
	function ph_ups_toggle_density_dimensions()
	{
		if( ! jQuery('#woocommerce_wf_shipping_ups_enable_freight').is(':checked') ) {
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_freight', '#woocommerce_wf_shipping_ups_density_length' );
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_freight', '#woocommerce_wf_shipping_ups_density_width' );
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_freight', '#woocommerce_wf_shipping_ups_density_height' );
		}
		else if( ! jQuery('#woocommerce_wf_shipping_ups_enable_density_based_rating').is(':checked') ){
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_density_based_rating', '#woocommerce_wf_shipping_ups_density_length' );
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_density_based_rating', '#woocommerce_wf_shipping_ups_density_width' );
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_density_based_rating', '#woocommerce_wf_shipping_ups_density_height' );
		}
		else{
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_density_based_rating', '#woocommerce_wf_shipping_ups_density_length' );
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_density_based_rating', '#woocommerce_wf_shipping_ups_density_width' );
			ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_enable_density_based_rating', '#woocommerce_wf_shipping_ups_density_height' );
		}
	}

	// Toggle Label Format based on Print Label Type option and Display Label in Browser.
	ph_ups_toggle_label_format();
	jQuery('#woocommerce_wf_shipping_ups_print_label_type').change(function(){
		ph_ups_toggle_label_format();
		ph_toggle_ups_label_size();
	});
	// End of Toggle Label Format

	//Toggle Email Settings
	ph_ups_toggle_label_email_settings();
	jQuery('#woocommerce_wf_shipping_ups_auto_email_label').change(function(){
		ph_ups_toggle_label_email_settings();
	});
	// End of Toggle Email Settings

	// UPS Shipping Details Toggle
	jQuery('.ph_ups_other_details').next('.ph_ups_hide_show_product_fields').hide();
	jQuery('.ph_ups_other_details').click(function(event){
		event.stopImmediatePropagation();
		jQuery('.ups_toggle_symbol').toggleClass('ups_toggle_symbol_click');
		jQuery(this).next('.ph_ups_hide_show_product_fields').toggle();
	});

	// UPS Shipping Details Toggle - Variation Level
	jQuery(document).on('click','.ph_ups_var_other_details',function(){
		event.stopImmediatePropagation();
		jQuery(this).find('.ups_var_toggle_symbol').toggleClass('ups_var_toggle_symbol_click');
		jQuery(this).next('.ph_ups_hide_show_var_product_fields').toggle();
	});

	// Toggle Hazardous Materials
	ph_ups_toggle_hazardous_materials();
	jQuery('#_ph_ups_hazardous_materials').change(function(){
		ph_ups_toggle_hazardous_materials();
	});

	// Toggle Hazardous Materials - Variation Level - By Default
	jQuery(document).on('click','.woocommerce_variation',function(){
		ph_ups_toggle_var_hazardous_materials_on_load(this);
	});

	// Toggle Hazardous Materials - Variation Level - On Click
	jQuery(document).on('change','input.ph_ups_variation_hazmat_product',function(){
		ph_ups_toggle_var_hazardous_materials(this);
	});

	// End of Toggle Hazardous Materials

	jQuery("#generate_return_label").click(function(){
		service=jQuery("#return_label_service").val();
		if(service == '' ){
			alert("Select service");
			return false;
		}
		else{
			href=jQuery(this).attr('href');
			href=href+"&return_label_service="+service;
			jQuery(this).attr('href',href);
		}
		return true;
	});

	jQuery('.ph_label_custom_description').attr({'maxlength':50, 'rows':2});

	//Custom Description For Label
	ph_ups_custom_description_for_label();
	jQuery('#woocommerce_wf_shipping_ups_label_description').change(function(){
		ph_ups_custom_description_for_label();
	});
});

/**
 * Toggle Label Size option.
 */
function ph_toggle_ups_label_size(){
	if( jQuery("#woocommerce_wf_shipping_ups_print_label_type").val() == 'gif' || jQuery("#woocommerce_wf_shipping_ups_print_label_type").val() == 'png' ) {
		ph_ups_toggle_based_on_checkbox_status( '#woocommerce_wf_shipping_ups_show_label_in_browser', '#woocommerce_wf_shipping_ups_resize_label' );
	}
	else{
		jQuery("#woocommerce_wf_shipping_ups_resize_label").closest('tr').hide();
	}
}

// Toggle based on checkbox status
function ph_ups_toggle_based_on_checkbox_status( tocheck, to_toggle ){
	if( ! jQuery(tocheck).is(':checked') ) {
		jQuery(to_toggle).closest('tr').hide();
	}
	else{
		jQuery(to_toggle).closest('tr').show();
	}
}

function wf_ups_load_pickup_options(){
	var checked	=	jQuery('#woocommerce_wf_shipping_ups_pickup_enabled').is(":checked");
	if(checked){
		jQuery('.wf_ups_pickup_grp').closest('tr').show();
	}else{
		jQuery('.wf_ups_pickup_grp').closest('tr').hide();
	}
	wf_ups_load_working_days();
}

function wf_ups_load_working_days(){
	var pickup_date = jQuery('#woocommerce_wf_shipping_ups_pickup_date').val();
	if( pickup_date != 'specific' ){
		jQuery('.pickup_working_days').closest('tr').hide();
	}else{
		jQuery('.pickup_working_days').closest('tr').show();
	}
}

/**
 * Toggle Label Format based on Print Label Type option and Display Label in Browser.
 */
function ph_ups_toggle_label_format() {
	if( jQuery("#woocommerce_wf_shipping_ups_print_label_type").val() == 'gif' ) {
		jQuery("#woocommerce_wf_shipping_ups_label_format").closest('tr').show();
	}
	else{
		jQuery("#woocommerce_wf_shipping_ups_label_format").closest('tr').hide();
	}
}

/**
 * Toggle UPS Label Email Settings.
 */
function ph_ups_toggle_label_email_settings() {
	if( jQuery("#woocommerce_wf_shipping_ups_auto_email_label").val() == null ) {
		jQuery(".ph_ups_email_label_settings").closest('tr').hide();
	}
	else{
		jQuery(".ph_ups_email_label_settings").closest('tr').show();
	}
}

/**
 * Toggle Hazardous Materials Settings
**/
function ph_ups_toggle_hazardous_materials(){
	if( jQuery("#_ph_ups_hazardous_materials").is(":checked") ){
		jQuery(".ph_ups_hazardous_materials").show();
	}
	else{
		jQuery(".ph_ups_hazardous_materials").hide();
	}
}

/**
 * Toggle Hazardous Materials Settings - Variation Level - Onload
**/
 function ph_ups_toggle_var_hazardous_materials_on_load(e){
 	if( jQuery(e).find(".ph_ups_variation_hazmat_product").is(':checked') ){
 		jQuery(e).find(".ph_ups_var_hazardous_materials").show();
 	}else{
 		jQuery(e).find(".ph_ups_var_hazardous_materials").hide();
 	}
 }

/**
 * Toggle Hazardous Materials Settings - Variation Level - Onclick
**/
 function ph_ups_toggle_var_hazardous_materials(e){
 	if( jQuery(e).is(':checked') ){
 		jQuery(e).closest( '.woocommerce_variation' ).find(".ph_ups_var_hazardous_materials").show();
 	}else{
 		jQuery(e).closest( '.woocommerce_variation' ).find(".ph_ups_var_hazardous_materials").hide();
 	}
 }

/**
 * Custom Description For Label
**/
function ph_ups_custom_description_for_label() {
	if( jQuery("#woocommerce_wf_shipping_ups_label_description").val() == 'custom_description' ) {
		jQuery("#woocommerce_wf_shipping_ups_label_custom_description").closest('tr').show();
	}
	else{
		jQuery("#woocommerce_wf_shipping_ups_label_custom_description").closest('tr').hide();
	}
}