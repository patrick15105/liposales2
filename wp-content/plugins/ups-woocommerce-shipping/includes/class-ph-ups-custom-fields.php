<?php

if(!class_exists('ph_ups_custom_checkout_fields')){
	
	class ph_ups_custom_checkout_fields{
		
		public function __construct()
		{
			$this->settings 			= get_option( 'woocommerce_'.WF_UPS_ID.'_settings', null );
			$this->debug	  			= isset( $this->settings['debug'] ) && $this->settings['debug'] == 'yes' ? true : false;
			$this->recipients_tin 	= ( isset($this->settings['recipients_tin']) && ! empty($this->settings['recipients_tin']) && $this->settings['recipients_tin'] == 'yes' ) ? true : false;

			$this->init();	
		}
		
		private function init(){

			add_action( 'woocommerce_before_shipping_calculator', array($this, 'reset_custom_data_before_shipping_calculator'), 10, 0 ); 

			// Add  custom field in checkout page
			add_filter( 'woocommerce_checkout_fields' , array( $this, 'ph_ups_add_custom_checkout_fields') );

			add_filter( 'woocommerce_order_formatted_billing_address', array($this,'ph_ups_order_formatted_billing_address'),10,3 );
			add_filter( 'woocommerce_order_formatted_shipping_address', array($this,'ph_ups_order_formatted_shipping_address'),10,3 );
			
			add_filter( 'woocommerce_formatted_address_replacements',  array($this,'ph_ups_formatted_address_replacements'),10,3  );
			add_filter('woocommerce_localisation_address_formats', array($this,'ph_ups_address_formats'));
			
			//Display Custom Data in my-account/address
			add_filter( 'woocommerce_my_account_my_address_formatted_address',array($this,'ph_ups_my_account_formated_address'),12,3 );

			// Updating Custom Field value
			add_action( 'woocommerce_checkout_update_order_review', array($this,'ph_ups_update_custom_fields'), 1, 1 );

			// Add Custom Field Data in Woocommerce cart shipping packages.
			add_filter( 'woocommerce_cart_shipping_packages', array( $this, 'ph_ups_update_custom_fields_details_in_package') );
			
			// Save Access Point Details in Meta Key for Order
			add_action('woocommerce_checkout_update_order_meta', array( $this, 'ph_add_custom_field_meta_data'), 12, 2);

		}

		public function reset_custom_data_before_shipping_calculator()
		{
			$this->ph_update_custom_field_datas();
		}

		private function ph_update_custom_field_datas( $value='' )
		{
			WC()->session->set('ph_ups_tax_id_num', $value);
		}

		public function ph_ups_add_custom_checkout_fields( $fields )
		{
			
			if( $this->recipients_tin )
			{
				$fields['billing']['shipping_tax_id_num'] = array(
					'label' 		=> __('Tax Identification Number', 'ups-woocommerce-shipping'),
					'placeholder'	=> _x('', 'placeholder', 'ups-woocommerce-shipping'),
					'required'		=> false,
					'clear'			=> false,
					'type'			=> 'text',
					'priority'		=> '115',
				);
			}
			return apply_filters('ph_checkout_fields', $fields, $this->settings);
		}
		
		public function ph_ups_order_formatted_billing_address( $array, $address_fields )
		{ 
			
			if( $this->recipients_tin )
			{
				$array['tax_id_num'] = '';
			}

			return $array; 
		}

		public function ph_ups_order_formatted_shipping_address( $array,$address_fields )
		{

			if( $this->recipients_tin )
			{
				$recipients_tin	=	$this->ph_get_custom_field_datas($address_fields);

				$array['tax_id_num'] = $recipients_tin;
			}

			return $array; 
		}

		private function ph_get_custom_field_datas( $order_details='' )
		{
			if( !empty( $order_details ) )
			{
				if( WC()->version < '2.7.0' )
				{
					return ( isset($order_details->shipping_tax_id_num) ) ? $order_details->shipping_tax_id_num : '';
				}else{
					$address_field = $order_details->get_meta('_shipping_tax_id_num');
					return $address_field;
				}
			}else{
				return WC()->session->get('ph_ups_tax_id_num');
			}
		}

		public function ph_ups_formatted_address_replacements( $array, $address_data )
		{
			if( $this->recipients_tin )
			{
				$recipients_tin = ! empty($address_data['tax_id_num']) ? __( 'TIN:  ', 'ups-woocommerce-shipping' ).$address_data['tax_id_num'] :'';

				$array['{tax_id_num}'] = $recipients_tin;
			}

			return $array; 
		}

		public function ph_ups_address_formats( $formats )
		{
			if( $this->recipients_tin )
			{
				foreach ($formats as $key => $format) {
					$formats[$key] = $format."\n{tax_id_num}";
				}	
			}	

			return $formats;
		}

		public function ph_ups_my_account_formated_address( $array, $customer_id, $name  )
		{
			if( $this->recipients_tin )
			{
				$getting_recipients_tin = get_user_meta( $customer_id, $name . '_tax_id_num', true );
				$recipients_tin			=	( isset($getting_recipients_tin) ) ? $getting_recipients_tin : '';
				$array['tax_id_num'] 	= ($name . '_tax_id_num' == 'shipping_tax_id_num') ? $recipients_tin :'';
			}
			
			return $array; 
		}

		public function ph_ups_update_custom_fields($updated_data)
		{
			$this->ph_update_custom_field_datas();

			$updated_fields = explode("&",$updated_data);

			if(is_array($updated_fields)){
				foreach($updated_fields as $updated_field){
					$updated_field_values = explode('=',$updated_field);
					if(is_array($updated_field_values)){
						if(in_array('shipping_tax_id_num',$updated_field_values)){
							$this->ph_update_custom_field_datas( urldecode($updated_field_values[1] ) );
						}
					}
				}
			}
			WC()->cart->calculate_shipping();
		}

		public function ph_ups_update_custom_fields_details_in_package( $packages )
		{
			if( $this->recipients_tin )
			{
				foreach( $packages as &$package ) {
					if( ! empty($package['contents']) ) {
						$ups_recipients_tin = WC()->session->get('ph_ups_tax_id_num');
						if( ! empty($ups_recipients_tin) )
							$package['ph_ups_tax_id_num'] = $ups_recipients_tin;
					}
				}
			}
			return $packages;
		}

		
		public function ph_add_custom_field_meta_data($order_id, $post){

			if( $this->recipients_tin )
			{
				$recipients_tin 	= ( is_array($post) && isset($post['shipping_tax_id_num']) ) ? $post['shipping_tax_id_num'] : ' ';

				update_post_meta( $order_id, 'ph_ups_shipping_tax_id_num', $recipients_tin );
			}

		}
		
	}
	
	new ph_ups_custom_checkout_fields();
}