<?php 
/* Template Name:  Customer Service Page - Custom */ ?>

<?php get_header(); ?>

<div class="columns landing-page">	
	<div class="col-1">
		<h1 class="gradient-text">CUSTOMER SERVICE</h1>
		<p class="desc">Got a question or concern? Please feel free to reach out by submitting the form below and our customer service team will reach you shortly</p>

		<?php echo do_shortcode('[centaur_form id="1"]'); ?>		

	
		
	</div>

	<div class="col-1">
		
		<div class="row">
			<div class="columns-1-3">
				<h2 class="gradient-text">CALL US</h2>
				<p><a href="telno:1-866-336-1333">1-866-336-1333</a></p>				
			</div>
			<div class="columns-1-3">
				<h2 class="gradient-text">VISIT US</h2>
				<p>2003 Edwards St. Houston, TX 77007</p>

			</div>
			<div class="columns-1-3">
				<h2 class="gradient-text">EMAIL US</h2>
				<p><a href="mailto:info@liposales.com">info@liposales.com</a></p>
			</div>
		</div>

		<div class="row">
			<div class="map">
			<div class="mapouter"><div class="gmap_canvas"><iframe width="800" height="600" id="gmap_canvas" src="https://maps.google.com/maps?q=%202003%20Edwards%20St.%20Houston%2C%20TX%2077007&t=&z=11&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.pureblack.de/google-maps/"></a></div><style>.mapouter{text-align:right;height:586px;width:783px;}.gmap_canvas {overflow:hidden;background:none!important;height:586px;width:783px;}</style></div>
			</div>

		</div>
		

	</div>

</div>


<?php get_footer(); ?>



