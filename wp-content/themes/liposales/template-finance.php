<?php

/**

 * The template for displaying the homepage.

 *

 * This page template will display any functions hooked into the `homepage` action.

 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components

 * use the Homepage Control plugin.

 * https://wordpress.org/plugins/homepage-control/

 *

 * Template name: Liposales Finance

 *

 * @package storefront

 */

get_header(); ?>




<h1>Finance</h1>
<br>


<?php  do_shortcode('[resend_confirmation]'); ?>


<?php if(!is_user_logged_in()){ ?>
  
  <div class="required_login">

      <div class="woocommerce-info required_login" id="required_login">
        Do you have existing  account ? <a href="<?php echo get_site_url() .'/my-account/?redirect=finance'; ?>">LOGIN</a> or not <a href="<?php echo get_site_url() . '/my-account/?action=register'; ?>" target="_blank">REGISTER</a>
      </div>
  </div>
<?php } ?>


<?php

if (!session_id()) {
    session_start();
}
$is_active = "";

if(isset($_SESSION['centaur_finance_obj'])){

    $is_active = "active";
    if($_SESSION['centaur_finance_obj']!=""){
      $_SESSION['centaur_finance_obj']['is_direct'] = false;
    }
}

?>

<div class="finance-loader <?php echo $is_active;?>">
  <div class="lds-spinner">
    <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
  </div>
</div>
<span id="error_finance" class="required_login"></span>
<p>&nbsp</p>


<div class="wpmc-tabs-wrapper">
    <ul class="wpmc-tabs-list wpmc-2-tabs">
        <li class="wpmc-tab-item current tab-finance-plan">
            <div class="wpmc-tab-number">1</div>
            <div class="wpmc-tab-text">Finance Plan</div>
        </li>
        <li class="wpmc-tab-item wpmc-review tab-finance-personal-info">
            <div class="wpmc-tab-number">2</div>
            <div class="wpmc-tab-text">Personal Information</div>
        </li>

   
      </ul>
</div>

<div class="tab-finance current tab-finance-plan" id="tab-finance-plan">
    
    <form action="" id="finance" name="finance">
      
      <p class=""> Deferred Payment Options With LipoSales! </p>
      
      <div class="single-product">

        <table class="table">

          <thead>

            <tr>

              <td>

                <h3 class="gradient-text">

                  Quick Start

                </h3>

              </td>

              <td>

                <h3 class="gradient-text">

                  Practice Builder

                </h3>

              </td>

            </tr>

          </thead>

          <tbody>
          
            <tr style="background-color: white !important;">

              <td>

                <h3 for="" style="margin-bottom: 0;">

                  <input name="plan" type="radio" value="SC2XTKFZWTANJN3N8C"> 36 Months

                </h3>

                <p style="margin-top: 0;">

                  3 @ <span class="woocommerce-Price-currencySymbol">$</span>0, then 36@ <span class="gradient-text"><strong class="computation 3-36-mon"></strong></span>

                </p>

              </td>

              <td>

                <h3 for="" style="margin-bottom: 0;">

                  <input name="plan" type="radio" value="SC2REWE364ZD29242D"> 36 Months

                </h3>



                <p style="margin-top: 0;">

                  6 @ <span class="woocommerce-Price-currencySymbol">$</span>0, then 36@ <span class="gradient-text"><strong class="computation 6-36-mon" style=""></strong></span>

                </p>

              </td>

            </tr>



            <tr style="background-color: white !important;">

              <td>

                <h3 for="" style="margin-bottom: 0;">

                  <input name="plan" type="radio" value="SC25ZMD3V36ZR6QM8E"> 60 Months

                </h3>



                <p style="margin-top: 0;">

                  3 @ <span class="woocommerce-Price-currencySymbol">$</span>0, then 60@ <span class="gradient-text"><strong class="computation 3-60-mon" style=""></strong></span>

                </p>

              </td>

              <td>

                <h3 for="" style="margin-bottom: 0;">

                  <input name="plan" type="radio" value="SC23GTMD469RRDA9Z3"> 60 Months

                </h3>



                <p style="margin-top: 0;">

                  6 @ <span class="woocommerce-Price-currencySymbol">$</span>0, then 60@ <span class="gradient-text"><strong class="computation 6-60-mon" style=""></strong></span>

                </p>

              </td>

            </tr>

          </tbody>

        </table>

      </div>



      <div class="form-group">

        <input class="form-control" id="change_amount" name="amount" placeholder="Enter Amount" type="text"> <span class="error-message"></span>

      </div>

      <p style="text-align: right;">
        <button class="buttons gradient-test text-white" type="submit">Next</button>
      </p>

  </form>
</div>

<div class="tab-finance tab-finance-personal-info" id="tab-finance-personal-info" >

      
    <p>Quick Application</p>

    <form id="frm_quick_application" name="frm_quick_application">

        <div class="flex" style="align-items: baseline;">

          <div class="flex-1" style="width: 48%; margin-right: 4%;">

            <div class="form-group">

               <input class="form-control" name="full_name" placeholder="*Full Name" type="text">

            </div>

            <div class="form-group">

               <input class="form-control" name="legal_business_name" placeholder="*Legal Business Name" type="text">

            </div>

            <div class="form-group">

              <input class="form-control" name="business_address" placeholder="*Business Address" type="text">

            </div>


            <div class="form-group">

               <input class="form-control" name="business_year" placeholder="*Years in Business" type="text">

            </div>



            <div class="form-group">

              <input class="form-control" name="principal_owner_name" placeholder="*Principal/Owner Name" type="text">

            </div>

            
          </div>


          <div class="flex-2" style="width: 48%;">

            <div class="form-group">

               <input class="form-control" name="principal_owner_address" placeholder="*Principal/Owner Home Address" type="text">

            </div>

            <div class="form-group">

               <input class="form-control" name="primary_office_contact" placeholder="*Primary Office Contact" type="text">

            </div>



            <div class="form-group">

               <input class="form-control" name="business_phone_fax" placeholder="*Business Phone & Business Fax" type="text">

            </div>



            <div class="form-group">

              <input class="form-control" name="web_address" placeholder="*Web Address" type="text">

            </div>


            <div class="form-group">

              <input class="form-control" name="principal_owner_ssn" placeholder="*Principal/Owner SSN" type="text">

            </div>

            

          </div>



        </div>

        <div class='form-group'>
              
              <button class="prev buttons gradient-test text-white" type="button">Back</button>
              
              <button class="buttons gradient-test text-white" type="submit" style="float: right;" >Submit</button>

              <div style="clear: both;"></div>
              
         </div>


      </form>

</div>







<?php

get_footer();





