<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: finance
 *
 * @package storefront
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			
			<!-- SLIDER -->
			<div class="col-full hero-contain">
                
                
                <div class="banner-slider">
                    
                    <!-- <div class="banner-slider-container banner-slider-slick">
                      	
                		<div class="slider-flex">
                			
                			<div class="flex-1">
                				
                				<h1 class="section-header gradient-text">Shop For <br>Canisters</h1>
                
                				<p>High quality, surgical-grade <br>canisters </p>
                				
                				<a href="/canisters/" class="buttons gradient-test text-white">Find Out More</a>
                
                			</div> 
                
                			<div class="flex-2">
                
                				<img src="/wp-content/uploads/2018/11/25-off-canisters.png" alt="Canister">
                
                			</div> 
                
                		</div>
                
                    </div> -->

                    <!-- Nouvag Power-Assisted Liposuction
Advanced liposuction equipment. The most trusted brand by Doctors in the US and EU. -->


        
                    <div class="banner-slider-container banner-slider-slick">
                      	
                		<div class="slider-flex">
                			
                			<div class="flex-1">
                				
                				<h2 class="section-header gradient-text">Nouvag LipoSurg<br>Power-Assisted Liposuction</h2>
                				<p>The most trusted brand by Doctors in the US and EU</p>
                				
                				<a href="./equipment/power-assisted-liposuction/package" class="buttons gradient-test text-white">Find Out More</a>
                
                			</div> <!--  flex-1 -->
                
                			<div class="flex-2">
                
                				<img src="./wp-content/uploads/2019/05/powerlipo.png" alt="New Power Assisted Lipo">
                
                			</div> <!--  flex-2 -->
                
                		</div> <!--  slider-flex-->
                
                    </div> <!--  banner-slider-container -->
                    
                
                </div> <!--  banner-slider -->
			
			
			</div>
			<!-- /SLIDER -->
			
			
			<div style="text-align: center; padding : 25px 0 1px; background: white; /* border-bottom: 2px solid #dee2e5; */">
                
                <div class="col-full" style="max-width:950px;">
                   
                    <h2 class="section-header gradient-text" style="text-align: center;">Leading Liposuction Equipment Manufacturer</h2>
                    
                    <p style="font-size: 15px;">For more than a decade, LipoSales has offered high-quality medical equipment to physicians globally. As the provider of a wide variety of liposuction machines and products such as <a href='/cannulas/'>cannulas</a>, <a href="/canisters/">canisters</a>, and <a href="/tubings/">tubing</a>. we have established ourselves as <b>one of the trusted providers in the field of plastic surgery in the United States.</b></p>
                   
                    <p style="font-size: 15px;">Our surgical devices are <b>safe and effective</b> , and most of all, <b>reliable</b> to ensure doctors deliver the most consistent and satisfying surgical procedure results to their patients.</p>
               
                </div>
           
            </div>

			<div class="why-us-contain">
				<div class="col-full">
					<div class="three-item why-slick-contain">
						<div class="slick-item">
							<div>
								<i class="la la-plane gradient-test gradient-icon mar-b-10"></i>
								<p>SHIPPING WORLDWIDE</p>
							</div>
						</div>
						<div class="slick-item">
							<div>
								<i class="la la-unlock gradient-test gradient-icon mar-b-10"></i>
								<p>Safe &amp; secure payment</p>
							</div>
						</div>
						<div class="slick-item">
							<div>
								<i class="la la-refresh gradient-test gradient-icon mar-b-10"></i>
								<p>MONEY BACK GUARANTEE!</p>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- /why-us -->


			<!-- Main Content -->
			<div class="main-content">
				<div class="col-full">
					<?php
					/**
					 * Functions hooked in to homepage action
					 *
					 * @hooked storefront_homepage_content      - 10
					 * @hooked storefront_product_categories    - 20
					 * @hooked storefront_recent_products       - 30
					 * @hooked storefront_featured_products     - 40
					 * @hooked storefront_popular_products      - 50
					 * @hooked storefront_on_sale_products      - 60
					 * @hooked storefront_best_selling_products - 70
					 */
					do_action( 'homepage' ); ?>
	
					<?php // echo do_shortcode('[woo_featured]'); ?>

				</div>

				<div class="col-full">
					<div class="storefront-product-section testimonial-contain tac">
						<h2 class="section-header gradient-text">Testimonials</h2>
				
						<div class="testimonial-slider-contain">
							<i class="la la-comment-o gradient-test gradient-icon"></i>
							<div class="single-item">
							    <div class="slick-item">
									<p>"After switching to Liposales and working with their products, nothing else could compare. I could not ask for a better, more dependable line of products to use at our practice!"</p>
									<p class="author">Dr. Patrick Hsu, Memorial Plastic Surgery</p>
								<!-- <span class="position">Dolor Solor sit amet</span> -->
								</div>
								<div class="slick-item">
									<p>"Our practice has used their products for several years now. The products are durable, safe, and incredibly reliable. We wouldn't know where we would be without Liposales. Their customer service is second-to-none. Would recommend!"</p>
									<p class="author">Dr. Ergun Kocak, Midwest Breast & Aesthetics</p>
								<!-- <span class="position">Dolor Solor sit amet</span> -->
								</div>
							
								<div class="slick-item">
									<p>"We make it a point to always keep their products in constant stock in our practice. After having switched to Liposales, no other range of products could compare. The cannulas are easy to use and have given us great results every time."</p>
									<p class="author">Dr. Cara Downey, Downey Plastic Surgery</p>
								<!-- <span class="position">Dr. T. Davidson</span> -->
								</div>
							</div> <!-- single-item -->
						</div> <!-- /testimonial-slider-contain -->
					</div> <!-- /testimonial-contain -->
				</div>

			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
