<?php /* Template Name:  Liposales UPS track Page */

get_header();

?>

<h1>Track Order</h1>

<p>You can track your orders by simply placing your order number on the form below. 
The order number is available on your invoice e-mail. If you experience any trouble accessing your e-mail account, feel free to contact us <a href='../customer-service/'>here</a></p>
<form id="ups_track">
		

	<p class="form-row form-row-wide error-border"> 
	  <input type="text" class="input-text " name="text_ups_track" id="text_ups_track" placeholder="Order Number" value="">

	</p>
	<a href="https://www.ups.com/track?loc=en_US&tracknum"><span id='direct-page'></span></a>
	<button type="submit" class="woocommerce-Button buttons gradient-test text-white" name="submit" value="submit">Submit Order Number</button>

</form>

<?php get_footer(); ?>