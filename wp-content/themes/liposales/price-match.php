<?php 
/* Template Name:  Price Match Page - Custom */ ?>

<?php get_header(); ?>

<div class="landing-page price-match">	
	<h1 class="gradient gradient-text text-title">LIPOSALES PRICE MATCHING</h1>
	<h4><strong>We provide clients with two methods upon request</strong></h4>
	<p>1. Call us at +1-866-336-1333. We are open from (insert operating hours). Please provide us the web address (URL) of the product to allow us to confirm product details and provide you with a lower price.</p>
	<p>2. Complete the form below to request a price match. Please include the URL of the item along with your contact information. A representative will respond to your request within one (1) business day.	</p>
	

	<?php echo do_shortcode('[centaur_form id="2"]'); ?>
</div>


<?php get_footer(); ?>



