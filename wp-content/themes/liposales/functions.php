<?php
 /**
 * Enable ACF 5 early access
 * Requires at least version ACF 4.4.12 to work
 */
define('ACF_EARLY_ACCESS', '5');

/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
// add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
    wp_dequeue_style( 'storefront-style' );
    wp_dequeue_style( 'storefront-woocommerce-style' );
}

/**
 * Note: DO NOT! alter or remove the code above this text and only add your custom PHP functions below this text.
 */

add_filter('upload_size_limit', 'increase_upload');
function increase_upload($bytes) {
    return 262144000;
}


function cm_display_custom_logo() {
?>
    <div class="site-branding">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="site-logo-link" rel="home">
              <img alt="Medical Equipment Manufacturer" src="<?php echo get_site_url(); ?>/wp-content/uploads/2018/08/logo2.png" class="" alt="Medical Equipment Manufacturer"  />
              <!--<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2019/01/logo_liposales-e1548753585952.jpg" class="mobile_logo" alt="<?php echo get_bloginfo( 'name' ); ?>" />-->
            
               <!-- <img alt="Medical Equipment Manufacturer" src="https://res.cloudinary.com/liposales/image/upload/v1553049794/logo_liposales-mobile.jpg" class="mobile_logo" alt="Medical Equipment Manufacturer" /> -->
               <!--alt="<?php echo get_bloginfo( 'name' ); ?>-->
              <!-- <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/liposales-logo.png" alt="<?php echo get_bloginfo( 'name' ); ?>" /> -->

        </a>
    </div>
<?php
}

add_action( 'init' , 'header_add_and_remove' , 15 );
function header_add_and_remove() {
        remove_action( 'storefront_header', 'storefront_social_icons', 10 );
        remove_action( 'storefront_header', 'storefront_product_search', 40 );
        remove_action( 'storefront_header', 'storefront_header_cart', 60 );
        remove_action( 'storefront_page', 'storefront_page_header', 10 );
        remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );

        add_action( 'storefront_header', 'storefront_product_search', 9 );
        add_action( 'storefront_header', 'storefront_header_cart', 35 );
        add_action( 'storefront_page', 'storefront_post_new_header', 10 );
        add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
}

add_action( 'init', 'storefront_custom_logo' );
function storefront_custom_logo() {
    remove_action( 'storefront_header', 'storefront_site_branding', 20 );
    add_action( 'storefront_header', 'cm_display_custom_logo', 20 );
}

/**
 * Custom Product Summary Layout
 * 
 * @since  1.0.0  
 */
add_action( 'init' , 'product_custom_summary');
function product_custom_summary() {

    remove_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt','20');
    remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta','40');
    remove_action('woocommerce_single_product_summary','woocommerce_template_single_sharing','50');

}


if ( ! function_exists( 'storefront_content_container' ) ) {
    /**
     * The content container
     */
    function storefront_content_container() {
        echo '<div class="content-container">';
    }
}

if ( ! function_exists( 'storefront_content_container_close' ) ) {
    /**
     * The content container close
     */
    function storefront_content_container_close() {
        echo '</div>';
    }
}

add_action( 'storefront_header', 'storefront_content_container', 25 );
add_action( 'storefront_header', 'storefront_content_container_close', 38 );

// Load styles
function centaur_styles()
{
    // wp_register_style('normalize', get_stylesheet_directory_uri() . '/css/normalize.min.css', array(), '1.0', 'all');
    // wp_enqueue_style('normalize');
    wp_register_style('base', get_stylesheet_directory_uri() . '/base.css', array(), '1.6', 'all');
    wp_enqueue_style('base');

    wp_register_style('line-awesome', get_stylesheet_directory_uri() . '/css/line-awesome.min.css', array(), '1.0', 'all');
    wp_enqueue_style('line-awesome');

    wp_register_style('slick', get_stylesheet_directory_uri() . '/css/slick.css', array(), '1.0', 'all');
    wp_enqueue_style('slick');

    wp_register_style('slick-theme', get_stylesheet_directory_uri() . '/css/slick-theme.css', array(), '1.0', 'all');
    wp_enqueue_style('slick-theme');

 
    $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
    if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0; rv:11.0') !== false)) {
        
          wp_register_style('ie-style', get_stylesheet_directory_uri() . '/ie-style.css', array(), '1.2', 'all');
          wp_enqueue_style('ie-style');
        
    }

    if (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !==  FALSE){
         wp_register_style('ie-browser-style', get_stylesheet_directory_uri() . '/css/ie-style.css', array(), '1.0', 'all');
         wp_enqueue_style('ie-browser-style');
    }
    
   
}
add_action('wp_enqueue_scripts', 'centaur_styles');

// Load scripts (header.php)
function centaur_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
        
        
        // if (is_front_page() ) {
        //     wp_register_script('functions-script', get_stylesheet_directory_uri() . '/js/functions.js', array(), '', true);
        // }else{
        // }
        // 
        wp_register_script('functions-script', get_stylesheet_directory_uri() . '/js/functions_2.js', array(), '', true);
        
        wp_enqueue_script('functions-script');

  



        wp_register_script('slick-script', get_stylesheet_directory_uri() . '/js/slick.js', array(), '', true);
        wp_enqueue_script('slick-script');
        
        
       wp_enqueue_script( 'custom_js', get_stylesheet_directory_uri().'/js/ajax-javascript.js', array( 'jquery'), '1.0.0', true );
       
            wp_localize_script( 'custom_js', 'ajax_posts', array(
                    'ajaxurl' => admin_url( 'admin-ajax.php' ),
        ));
      
    }
    
    
    
    
    
}

add_action('init', 'centaur_header_scripts');

// ADD DIV AT MENU NAV
class CM_Walker extends Walker_Nav_Menu {
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class='sub-menu'>\n";
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul><div class='open-menu-link'><i class='la la-angle-down'></i></div>\n";
    }
}

function storefront_primary_navigation() {
        ?>
    <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'storefront' ); ?>">
        <button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false"><span><?php echo esc_attr( apply_filters( 'storefront_menu_toggle_text', __( 'Site Navigation', 'storefront' ) ) ); ?></span></button>
            <?php
            wp_nav_menu(
                array(
                    'theme_location'    => 'primary',
                    'container_class'   => 'primary-navigation',
                    'walker' => new CM_Walker
                    )
            );
            ?>
        </nav><!-- #site-navigation -->
    <?php
}

if ( ! function_exists( 'storefront_cart_link' ) ) {
    function storefront_cart_link() {
        ?>
            <!--a class="cart-contents" href="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" title="<?php _e( 'View your shopping cart', 'storefront' ); ?>">
                <span class="count"><?php echo WC()->cart->get_cart_contents_count();?></span> My Cart
            </a-->
            <a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php _e( 'View your shopping cart', 'storefront' ); ?>">
                <span class="count"><?php echo WC()->cart->get_cart_contents_count();?></span> My Cart
            </a>
        <?php
    }
}

if ( ! function_exists( 'storefront_handheld_footer_bar' ) ) {
    function storefront_handheld_footer_bar() {
        $links = array(
            'my-account' => array(
                'priority' => 10,
                'callback' => 'storefront_handheld_footer_bar_account_link',
            ),
            'help'     => array(
                'priority' => 20,
                'callback' => 'storefront_handheld_footer_bar_help_link',
            ),
            'cart'       => array(
                'priority' => 30,
                'callback' => 'storefront_handheld_footer_bar_cart_link',
            ),
        );

        if ( wc_get_page_id( 'myaccount' ) === -1 ) {
            unset( $links['my-account'] );
        }

        if ( wc_get_page_id( 'cart' ) === -1 ) {
            unset( $links['cart'] );
        }

        $links = apply_filters( 'storefront_handheld_footer_bar_links', $links );
        ?>
        <div class="storefront-handheld-footer-bar">
            <ul class="columns-<?php echo count( $links ); ?>">
                <?php foreach ( $links as $key => $link ) : ?>
                    <li class="<?php echo esc_attr( $key ); ?>">
                        <?php
                        if ( $link['callback'] ) {
                            call_user_func( $link['callback'], $key, $link );
                        }
                        ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php
    }
}

if ( ! function_exists( 'storefront_handheld_footer_bar_help_link' ) ) {
    function storefront_handheld_footer_bar_help_link() {
        ?>
        <a class="help" href="/help/">Help</a>
        <?php
        wp_nav_menu( array(
            'theme_location' => 'help-menu',
            'menu_class'     => 'help-menu',
            'depth'          => 1
        ) );
        ?>
        <?php
    }
}

if (!function_exists( 'cm_setup' ) ) {
     function cm_setup() {
        /* MENU */
        register_nav_menu('help-menu', __('Help Menu', 'cm'));
        register_nav_menu('footer-primary', __('Footer Primary Menu', 'cm'));
        register_nav_menu('footer-secondary', __('Footer Secondary Menu', 'cm'));
    }
}
add_action('after_setup_theme', 'cm_setup');

// REMOVED HOMEPAGE CONTENT
function cm_remove_actions_parent_theme() {
   remove_action('homepage', 'storefront_homepage_content',      10);
   remove_action('homepage', 'storefront_recent_products',       30);
   // remove_action('homepage', 'storefront_featured_products',     40);
   remove_action('homepage', 'storefront_popular_products',      50);
   remove_action('homepage', 'storefront_on_sale_products',      60);
   remove_action('homepage', 'storefront_best_selling_products', 70);

};
add_action( 'init', 'cm_remove_actions_parent_theme', 1);

// PRODUCT CATEGORIES
if ( ! function_exists( 'storefront_product_categories' ) ) {
    function storefront_product_categories( $args ) {

        if ( storefront_is_woocommerce_activated() ) {
            
            
            // $count = (wp_is_mobile()) ? 4 : 3 ;
           

            $args = apply_filters( 'storefront_product_categories_args', array(
                'limit'             => 4,
                'columns'           => 4,
                'child_categories'  => 0,
                'orderby'           => 'name',
                'title'             => __( 'Product Categories', 'storefront' ),
            ) );

        $ids = '';
        $terms = get_terms('product_cat', array('exclude' => array( 15 ) ) );
        foreach ($terms as $term) { //create the comma list of ids
            $ids .= $term->term_id;
            $ids .= ',';
        }
        $ids = rtrim($ids, ","); //trim last comma

            $shortcode_content = storefront_do_shortcode( 'product_categories', apply_filters( 'storefront_product_categories_shortcode_args', array(
                'number'  => intval( $args['limit'] ),
                'columns' => intval( $args['columns'] ),
                'orderby' => esc_attr( $args['orderby'] ),
                'parent'  => esc_attr( $args['child_categories'] ),
                'ids'       => esc_attr( $ids ),
            ) ) );

            /**
             * Only display the section if the shortcode returns product categories
             */
            if ( false !== strpos( $shortcode_content, 'product-category' ) ) {

                echo '<section class="storefront-product-section storefront-product-categories tac" aria-label="' . esc_attr__( 'Product Categories', 'storefront' ) . '">';

                do_action( 'storefront_homepage_before_product_categories' );

                echo '<h2 class="section-header gradient-text">' . wp_kses_post( $args['title'] ) . '</h2>';

                do_action( 'storefront_homepage_after_product_categories_title' );

                echo $shortcode_content;

                do_action( 'storefront_homepage_after_product_categories' );

                echo '</section>';

            }
        }
    }
}

// FEATURED PRODUCTS
if ( ! function_exists( 'storefront_featured_products' ) ) {
    function storefront_featured_products( $args ) {

        if ( storefront_is_woocommerce_activated() ) {
            
            
            //  $column_count = ((wp_is_mobile()) ? 2 : 3); 
             
            //  $page = ((wp_is_mobile()) ? 2 : 6); 
             
            // $count = (wp_is_mobile()) ? 4 : 3 ;
             
            $args = apply_filters( 'storefront_featured_products_args', array(
                'limit'      =>  4,
                'columns'    =>  4,
                // 'orderby' => 'date',.
                'per_page'   =>  4,
                'orderby'    => 'rating',
                'order'      => 'asc',
                'title'      => __( 'Featured Products', 'storefront' ),
            ) );

            $shortcode_content = storefront_do_shortcode( 'featured_products', apply_filters( 'storefront_featured_products_shortcode_args', array(
                // 'per_page' => intval( $args['limit'] ),
                'columns'     => intval( $args['columns'] ),
                'orderby'     => esc_attr( $args['orderby'] ),
                'order'       => esc_attr( $args['order'] ),
                'per_page'    => 4,
            ) ) );

            /**
             * Only display the section if the shortcode returns products
             */
            if ( false !== strpos( $shortcode_content, 'product' ) ) {

                echo '<section class="storefront-product-section storefront-featured-products tac" aria-label="' . esc_attr__( 'Featured Products', 'storefront' ) . '">';

                do_action( 'storefront_homepage_before_featured_products' );

                echo '<h2 class="section-header gradient-text">' . wp_kses_post( $args['title'] ) . '</h2>';

                do_action( 'storefront_homepage_after_featured_products_title' );

                echo $shortcode_content;

                do_action( 'storefront_homepage_after_featured_products' );

                echo '
                    <a href="'.get_site_url().'/shop" class="buttons gradient-test text-white">Find Out More</a>
                </section>';

            }
        }
    }
}

function woocommerce_after_shop_loop_remove_hook(){
    remove_action( 'woocommerce_after_shop_loop',       'woocommerce_catalog_ordering',             10 );
    remove_action( 'woocommerce_after_shop_loop',       'woocommerce_result_count',                 20 );
}
add_action( 'woocommerce_after_shop_loop', 'woocommerce_after_shop_loop_remove_hook', 1 );

function woocommerce_before_shop_loop_remove_hook(){
    remove_action( 'woocommerce_before_shop_loop',       'woocommerce_result_count',                 20 );
    remove_action( 'woocommerce_before_shop_loop',       'storefront_woocommerce_pagination',        30 );
    remove_action( 'woocommerce_before_shop_loop',       'storefront_sorting_wrapper',               9 );
}
add_action( 'woocommerce_before_shop_loop', 'woocommerce_before_shop_loop_remove_hook', 1 );

// remove the short description field in single product
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);

function remove_short_description() {
    remove_meta_box( 'postexcerpt', 'product', 'normal');
}
add_action('add_meta_boxes', 'remove_short_description', 999);

// CHANGE NUMBER OF RELATED PRODUCTS
add_filter( 'woocommerce_output_related_products_args', 'change_number_related_products', 20 );
function change_number_related_products( $args ) {
    $args['posts_per_page'] = 4; // # of related products
    $args['columns'] = 4; // # of columns per row
    return $args;
}

/**
 * Rename product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {
    $tabs['additional_information']['title'] = __( 'Specifications' );    // Rename the additional information tab
    return $tabs;
}

/**
 * Customize product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_product_videos_tab' );
function woo_product_videos_tab( $tabs ) {

    if( !empty(get_field('media')) ) {

        $tabs['videos'] = array(
            'title'     => __( 'Videos (' . count(get_field('media')) . ')', 'woocommerce' ),
            'priority'  => 50,
            'callback'  => 'woo_product_videos_tab_content'
        );

    }

    return $tabs;

}

function woo_product_videos_tab_content() {

    wc_get_template( 'single-product/tabs/videos.php' );

}

/* PRODUCT GALLERY */
add_action( 'init' , 'product_gallery_hooks');
function product_gallery_hooks() {

    remove_action( 'woocommerce_after_main_content', 'storefront_after_content', 10);
    add_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );    
    add_action( 'woocommerce_sort_wrapper', 'storefront_sorting_wrapper', 9 );

}






// Costumizing 
add_filter('woocommerce_default_address_fields', 'override_default_address_checkout_fields', 20, 1);


function override_default_address_checkout_fields( $address_fields ) {
    $address_fields['first_name']['placeholder'] = 'First name';
    $address_fields['last_name']['placeholder'] = 'Last name';
    $address_fields['company']['placeholder'] = 'Company Name';
    $address_fields['state']['placeholder'] = 'State';
    $address_fields['postcode']['placeholder'] = 'Postcode / ZIP';
    $address_fields['city']['placeholder'] = 'Town / City';


    return $address_fields;
}


// Add extra register fields to My account Register 

function wooc_extra_register_fields(){?>
    


    <div id="register_form">
        <div class="u-columns col2-set">

            <div class="u-column1 col-1">

                <p class="form-row form-row-first woocommerce-form-row validate-required" id="billing_first_name_field" data-priority="10">
                    <span class="woocommerce-input-wrapper">
                        <input type="text" class="input-text " name="billing_first_name" id="reg_billing_first_name" data-placeholder="First name" placeholder="*First name" value="<?php echo ( ! empty( $_POST['billing_first_name'] ) ) ? esc_attr( wp_unslash( $_POST['billing_first_name'] ) ) : ''; ?>"  autocomplete="given-name">
                    </span>
                </p>

                <p class="form-row form-row-last validate-required woocommerce-form-row" id="billing_last_name_field" data-priority="20">
                    <span class="woocommerce-input-wrapper">
                            <input type="text" class="input-text " data-placeholder="Last name"  name="billing_last_name" id="reg_billing_last_name" placeholder="*Last name" value="<?php echo ( ! empty( $_POST['billing_last_name'] ) ) ? esc_attr( wp_unslash( $_POST['billing_last_name'] ) ) : ''; ?>" autocomplete="family-name">
                    </span>
                </p>
                <div class="clear"></div>

                <?php
              

                     $field = [
                            'id'   => 'billing_country',
                            'type' => 'country',
                            'required' => 1,
                            'class' => ['address-field'],
                            'placeholder' => 'Select Country',
                            'default'     => 'US',
                    ];
                    woocommerce_form_field( 'billing_country', $field, '' );
                    
                    $field = [
                        'id'   => 'billing_state',
                        'type' => 'state',
                        'required' => 1,
                        'class' => ['address-field'],
                        'placeholder' => 'State',
                        'validate' => ['state']
                    ];
                    woocommerce_form_field( 'billing_state', $field, '' );
                       wp_enqueue_script( 'wc-country-select' );
                ?>

               


                <p class="form-row form-row-wide address-field validate-required" id="billing_address_1_field" data-priority="50">
                    <span class="woocommerce-input-wrapper">
                        <input type="text" class="input-text " name="billing_address_1" id="reg_billing_address_1" placeholder="Address 1" value="<?php echo ( ! empty( $_POST['billing_address_1'] ) ) ? esc_attr( wp_unslash( $_POST['billing_address_1'] ) ) : ''; ?>" autocomplete="address-line1">
                    </span>
                </p>

                 <p class="form-row form-row-wide address-field validate-required" id="billing_address_1_field" data-priority="50">
                    <span class="woocommerce-input-wrapper">
                        <input type="text" class="input-text " name="billing_address_2" id="reg_billing_address_2" placeholder="Address 2" value="<?php echo ( ! empty( $_POST['billing_address_2'] ) ) ? esc_attr( wp_unslash( $_POST['billing_address_2'] ) ) : ''; ?>" autocomplete="address-line2">
                    </span>
                </p>
            </div>

            <div class="u-column1 col-1">
                
                <p class="form-row form-row-wide address-field validate-required" id="billing_city_field" data-priority="70" data-o_class="form-row form-row-wide address-field validate-required">
                    <span class="woocommerce-input-wrapper">
                        <input type="text" class="input-text " value="<?php echo ( ! empty( $_POST['billing_city'] ) ) ? esc_attr( wp_unslash( $_POST['billing_city'] ) ) : ''; ?>" name="billing_city" id="reg_billing_city" placeholder="Town / City" autocomplete="Town / City">
                    </span>
                </p>
                
                 <p class="form-row form-row-wide" id="billing_company_field" data-priority="30">
                    <span class="woocommerce-input-wrapper">
                        <input type="text" class="input-text " name="billing_company" id="billing_company" placeholder="Company Name" value="<?php echo ( ! empty( $_POST['billing_company'] ) ) ? esc_attr( wp_unslash( $_POST['billing_company'] ) ) : ''; ?>" autocomplete="organization">
                    </span>
                </p>

                <p class="form-row form-row-wide address-field validate-required validate-postcode" id="billing_postcode_field" data-priority="90" data-o_class="form-row form-row-wide address-field validate-required validate-postcode">
                    <span class="woocommerce-input-wrapper">
                        <input type="text" class="input-text " name="billing_postcode" id="reg_billing_postcode" value="<?php echo ( ! empty( $_POST['billing_postcode'] ) ) ? esc_attr( wp_unslash( $_POST['billing_postcode'] ) ) : ''; ?>" placeholder="Postcode / ZIP" value="" autocomplete="postal-code">
                    </span>
                </p>

                <p style="display:inline-block"; class="form-row form-row-wide validate-required validate-phone" id="billing_phone_field" data-priority="100">
                    <span class="woocommerce-input-wrapper">
                        <input type="tel" class="input-text"  value="<?php echo ( ! empty( $_POST['billing_phone'] ) ) ? esc_attr( wp_unslash( $_POST['billing_phone'] ) ) : ''; ?>"  name="billing_phone" id="reg_billing_phone" placeholder="Phone" value="" autocomplete="tel">
                    </span>
                </p>

               
                <p class="woocommerce-form-row woocommerce-form-row--wide validate-email form-row form-row-wide">
                    <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" placeholder="*Email address" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>"  data-placeholder="Email address" />
                </p>
                

                <div class="flex baseline">
                    <p class="woocommerce-form-row form-row-first woocommerce-form-row--wide form-row form-row-wide form-field-password" style="margin-bottom: 0;">
                        <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" value="" data-placeholder="Password"  placeholder="*Password" autocomplete="new-password" />

                    </p>
                    <p class="woocommerce-form-row form-row-last woocommerce-form-row--wide form-row form-row-wide form-field-password">
                        <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="reg_retype_password" id="reg_retype_password" value=""  placeholder="*Retype-Password" autocomplete="new-password" />

                    </p>
               </div>
            </div>

        </div>

        <div class="u-columns col2-set">
            
            <div class="u-column1 col-1 term">
                 <div class="pop-up error"> Check Terms and Conditions </div>
                 <p> <input type="checkbox" name="term_condition" id="term_condition"> </input> I Agree to the  <a href="/terms-and-conditions/" class="is-link" target="_blank" >Terms and Conditions  ?</a>  </p>    
                 
            </div>
            
            <div class="u-column1 col-1">
                <p class="woocommerce-FormRow form-row frm-password ">
                    <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                    <button type="submit"  class="woocommerce-Button buttons gradient-test text-white" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register Account', 'woocommerce' ); ?></button>
                 

                 </p>
            </div>
       
        </div>
    </div>
<?php
}


add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );


//  Adding Validation Firstname and Lastname (REQUIRED FIELDS) to Myaccount 
function wooc_validate_extra_register_fields( $username, $email, $error ) {
    
    if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'])
    ){
        $error->add( 'billing_first_name_error', __('First name is Required', 'woocommerce' ));
    }
    if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'])
    ){
        $error->add( 'billing_last_name_error', __('Last name is Required', 'woocommerce' ));
    }
}

add_action( 'woocommerce_register_post','wooc_validate_extra_register_fields', 10, 3);


function wooc_save_extra_register_fields( $customer_id ) {
   

    if ( isset( $_POST['billing_first_name'] ) ) {
        // WordPress default first name field.
        update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );

        // WooCommerce billing first name.
        update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );

    }


    if ( isset( $_POST['billing_last_name'] ) ) {

        // WordPress default last name field.
        update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );

        // WooCommerce billing last name.
        update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
    }


    if ( isset( $_POST['billing_company'] ) ) {
        

        update_user_meta( $customer_id, 'billing_company', sanitize_text_field( $_POST['billing_company'] ) );
    }

    if ( isset( $_POST['billing_country'] ) ) {
       

        update_user_meta( $customer_id, 'billing_country', sanitize_text_field( $_POST['billing_country'] ) );
    }


    if ( isset( $_POST['billing_address_1'] ) ) {
       

        update_user_meta( $customer_id, 'billing_address_1', sanitize_text_field( $_POST['billing_address_1'] ) );
    }

     if ( isset( $_POST['billing_address_2'] ) ) {
       

        update_user_meta( $customer_id, 'billing_address_2', sanitize_text_field( $_POST['billing_address_2'] ) );
    }


    if ( isset( $_POST['billing_city'] ) ) {
        update_user_meta( $customer_id, 'billing_city', sanitize_text_field( $_POST['billing_city'] ) );
    }

    if ( isset( $_POST['billing_state'] ) ) {
        // WooCommerce billing address
        update_user_meta( $customer_id, 'billing_state', sanitize_text_field( $_POST['billing_address_1'] ) );
    }


    if ( isset( $_POST['billing_phone'] ) ) {
        // WooCommerce billing address
        update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
    }


    if ( isset( $_POST['billing_postcode'] ) ) {
        // WooCommerce billing postcode
        update_user_meta( $customer_id, 'billing_postcode', sanitize_text_field( $_POST['billing_postcode'] ) );
    }

    if ( isset( $_POST['billing_city'] ) ) {
        // WooCommerce billing city
        update_user_meta( $customer_id, 'billing_city', sanitize_text_field( $_POST['billing_city'] ) );
    }

     if ( isset( $_POST['billing_email'] ) ) {
        // WooCommerce billing city
        update_user_meta( $customer_id, 'billing_email', sanitize_text_field( $_POST['email'] ) );
    }

    if ( isset( $_POST['billing_state'] ) ) {
        // WooCommerce billing state
        update_user_meta( $customer_id, 'billing_state', sanitize_text_field( $_POST['billing_state'] ) );
    }

}

add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );

function my_woocommerce_add_error( $error ) {
    if( '<strong>ERROR</strong>: The username or password you entered is incorrect. <a href="'.get_site_url().'/my-account/lost-password/" title="Password Lost and Found">Lost your password</a>?' == $error ) {
        $error = '<strong>ERROR</strong>: The Email or password you entered is incorrect. <a href="'.get_site_url().'/my-account/lost-password/" title="Password Lost and Found">Lost your password</a>?';
    }
    return $error;
}
add_filter( 'woocommerce_add_error', 'my_woocommerce_add_error' );

function my_woocommerce_replacing_error( $error ) {
    

    $placeholder="";
    switch ($error) {

        case '<strong>First name</strong> is a required field.':
        case 'First name is a required field.':
        case '<strong>Error:</strong> First name is Required':
                $placeholder='First name';
            break;


        case '<strong>Error:</strong> Last name is Required':
        case '<strong>Last name</strong> is a required field.':
        case 'Last name is a required field.':
                $placeholder='Last name';
            break;


        case 'Street address is a required field.':
                 $placeholder='House number and street name';
            break;


        case '<strong>Error:</strong> Username is required.':    
        case '<strong>Error:</strong> Username is required.':
                 $placeholder='Email address';
            break;


        case 'Enter a username or email address.':
                $placeholder="Username or email";
            break;

        case '<strong>Error:</strong> An account is already registered with your email address. Please log in.':
        case '<strong>Error:</strong> Please provide a valid email address.':
        case 'Email address is a required field.':
        case '<strong>Email address</strong> is a required field.':
                $placeholder='Email address';
            break;
        
        case 'Your current password is incorrect.':
                $placeholder="Current password (leave blank to leave unchanged)";
            break;

        case 'New passwords do not match.':
                 $placeholder="Confirm new password";
            break;


        case 'New passwords do not match.':
                 $placeholder="Confirm new password";
            break;

        case 'ZIP is a required field.':
        case 'Please enter a valid postcode / ZIP.':
        case 'Postcode / ZIP is a required field.':
                $placeholder="Postcode / ZIP";
            break;


        case 'Phone is a required field.':
        case '<strong>Phone</strong> is not a valid phone number.':
                $placeholder="Phone";
            break;

        case 'Town / City is a required field.':
                  $placeholder="Town / City";
            break;    


       case 'State / County is a required field.':
                  $placeholder="Country";
            break;


        case '<strong>ERROR</strong>: Invalid username. <a class="is-link" href="'.get_site_url().'/my-account/lost-password/">Lost your password?</a>':
                $placeholder="Username";
            break;
            
        case '<strong>ERROR</strong>: The Email or password you entered is incorrect. <a href="'.get_site_url().'/my-account/lost-password/" title="Password Lost and Found">Lost your password</a>?':
        case '<strong>Error:</strong> Please enter an account password.':
        case '<strong>ERROR</strong>: The password field is empty.':
        case '<strong>ERROR</strong>: Invalid email address. <a class="is-link" href="'.get_site_url().'/my-account/lost-password/">Lost your password?</a>':
        case '<strong>ERROR</strong>: The password you entered for the email address <strong>'.(isset($_POST['username'])?$_POST['username']:"").'</strong> is incorrect. <a class="is-link" href="'.get_site_url().'/my-account/lost-password/">Lost your password?</a>':
                $placeholder="Password";
            break;

        case '<strong>Billing First name</strong> is a required field.':
                $placeholder="billing_first_name";
            break;


        case '<strong>Billing Town / City</strong> is a required field.':
                 $placeholder="billing_city";
            break;

        case '<strong>Billing Phone</strong> is a required field.':
                $placeholder="billing_phone";
            break;


        case '<strong>Billing Last name</strong> is a required field.':
                $placeholder="billing_last_name";
            break;

        case '<strong>Billing Street address</strong> is a required field.':
                 $placeholder="billing_address_1";
            break;

        case '<strong>Billing State</strong> is a required field.':
        case '<strong>Billing State / County</strong> is a required field.':
                $placeholder="billing_state";
            break;

        case '<strong>Billing ZIP</strong> is a required field.':
        case '<strong>Billing Postcode / ZIP</strong> is a required field.':
        case '<strong>Billing ZIP</strong> is not a valid postcode / ZIP.':
                $placeholder="billing_postcode";
            break;

        case '<strong>Billing Email address</strong> is a required field.':
                $placeholder="billing_email";
            break;
       
    
        case '<strong>Create account password</strong> is a required field.':
                $placeholder="account_password";
                break;

        case '<strong>Shipping Street address</strong> is a required field.':
                 $placeholder="shipping_address_1";
                break;

        case '<strong>Shipping First name</strong> is a required field.':
                $placeholder="shipping_first_name";
                break;
        case '<strong>Shipping Last name</strong> is a required field.':
                $placeholder="shipping_last_name";
                break;

        case '<strong>Shipping State / County</strong> is a required field.':
                $placeholder="shipping_state";
                break;
        case '<strong>Shipping Town / City</strong> is a required field.':
                $placeholder="shipping_city";
                break;

        case '<strong>Shipping Postcode / ZIP</strong> is a required field.':
                $placeholder="shipping_postcode";
                break;
        default:
            break;
    }

   $hide=($placeholder!='')?'display-none':'';
   $error="<span id='error-".$placeholder."'  class=".$hide.">".$error."</span>";

    return $error;
}


add_filter( 'woocommerce_add_error', 'my_woocommerce_replacing_error' );

/* UPDATE CART HEADER BUTTONS */
add_action('init', 'cart_header_buttons_custom');
function cart_header_buttons_custom() {

    remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10 );
    remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20 );
    
    add_action( 'woocommerce_widget_shopping_cart_buttons', 'woo_widget_shopping_cart_button_view_cart', 10 );
    add_action( 'woocommerce_widget_shopping_cart_buttons', 'woo_widget_shopping_cart_proceed_to_checkout', 20 );

}

function woo_widget_shopping_cart_button_view_cart() {

    echo '<a href="' . esc_url( wc_get_cart_url() ) . '" class="buttons button-outline outline-default button-light">' . esc_html__( 'Go to cart', 'woocommerce' ) . '</a>';

}

function woo_widget_shopping_cart_proceed_to_checkout() {

    echo '<a href="' . esc_url( wc_get_checkout_url() ) . '" class="buttons gradient-test text-white">' . esc_html__( 'Checkout', 'woocommerce' ) . '</a>';
}

/**
 * Remove Add to cart button
 *
 * @since  1.0.0
 */
add_action( 'init', 'remove_add_to_cart_button' );
function remove_add_to_cart_button() {
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart');
}

function add_login_logout_register_menu( $items, $args ) {

    if ( $args->theme_location != 'secondary' ) {
        return $items;
    }

    if ( is_user_logged_in() ) {

        global $current_user; 
        wp_get_current_user();

        $base_url = get_site_url();
        $myaccount = '<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                        <a href="#">' . __( 'Welcome! ' . wp_trim_words($current_user->display_name,1,'...') ) . '</a>
                        <ul class="sub-menu">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page">
                                <a href="'.$base_url.'/my-account/edit-account/">' . __( 'My Profile' ) . '</a>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page">
                                <a href="'.$base_url.'/my-account/edit-address/">' . __( 'My Addresses' ) . '</a>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page">
                                <a href="'.$base_url.'/my-account/orders/">' . __( 'My Orders' ) . '</a>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page">
                                <a href="' . esc_url( wc_logout_url( wc_get_page_permalink( 'myaccount' ) ) ) . '">' . __( 'Log Out' ) . '</a>
                            </li>
                        </ul>
                      </li>';

    } else {
        //  ' . woocommerce_new_login_form( $args ) . '
        $args = array(
            'hidden' => true,
            'redirect' => ( is_ssl() ? 'https://' : 'http://' ) . wc_get_page_permalink( 'myaccount' )
        );
        $myaccount='<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                        <a href="' . wp_login_url() . '">' . __( 'My Account' ) . '</a></li>';
        if(!is_checkout() && !is_account_page() ){
            
             $myaccount = '<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                        <a href="' . wp_login_url() . '">' . __( 'My Account' ) . '</a>
                        <div class="sub-menu">
                            <div class="form-login">
                                <div class="form-header"></div>
                                <div class="pad-10">
                                    
                                    <form id="login" action="login" method="post">
                                        <p class="status display-none"></p>
                                        <input id="email_login" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="email_login"  placeholder="Email address">
                                  
                                        <input id="password_login" class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password_login" placeholder="Password">
                                        <label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
                                            
                                            <input class="woocommerce-form__input woocommerce-form__input-checkbox  display-none" name="rememberme" type="checkbox" id="rememberme" value="forever"> 
                                            <span class="toggle-check"></span>
                                            <span class="rememberme_text">Remember me</span>
                            
                                         </label>
                                        <a class="lost" href="' . wp_lostpassword_url() . '">Lost your password?</a>
                                        
                                        <button class="submit_button buttons gradient-test text-white" type="submit" value="Login" name="submit">Login</button>'.
                                        wp_nonce_field( 'ajax-login-nonce', 'security' )
                                    .'
                                  
                                     
                                    
                                    </form>
                                    <p class="register">
                                        Don\'t have an account?<br/><a href="/my-account/?action=register">Click Here</a> to register!  
                                    </p>
                                </div>
                            </div>
                        </div>
                      </li>';
        }
    }
 
    return $myaccount . $items;

}

function ajax_login_init(){

    wp_register_script('ajax-login-script', get_stylesheet_directory_uri()  . '/js/ajax-login-script.js'); 
    wp_enqueue_script('ajax-login-script');

    wp_localize_script( 'ajax-login-script', 'ajax_login_object', array( 
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'redirecturl' => get_site_url() . '/my-account/',
        'loadingmessage' => __('Sending user info, please wait...')
    ));

    // Enable the user with no privileges to run ajax_login() in AJAX
    add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
}

function ajax_login(){

    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-login-nonce', 'security' );

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = $_POST['remember'];

    $user_signon = wp_signon( $info, false );
    if ( is_wp_error($user_signon) ){
        echo json_encode(array('loggedin'=>false, 'message'=>__(' <div class="alert-error">Email or Password you entered is incorrect</div>')));
    } else {
        echo json_encode(array('loggedin'=>true, 'message'=>__('Login successful, redirecting...')));
    }

    die();
}


// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
    add_action('init', 'ajax_login_init');
}



function woocommerce_new_login_form( $args = array() ) {

                    $defaults = array(
                            'message'  => '',
                            'redirect' => '',
                            'hidden'   => false,
                    );
    
                    $args = wp_parse_args( $args, $defaults );
    

   ob_start();
    wc_get_template( 'global/form-login.php', $args );
   $content = ob_get_contents();
   ob_end_clean();

   return $content;
}


add_filter( 'wp_nav_menu_items', 'add_login_logout_register_menu', 0, 2 );

function storefront_post_new_header() {
        ?>
        <header class="entry-header">
            <?php
            storefront_post_thumbnail( 'full' );
            the_title( '<h1 class="gradient-text">', '</h1>' );
            ?>
        </header><!-- .entry-header -->
        <?php
}


/**
 * Show the product title in the product loop. By default this is an H2.
 */
function woocommerce_template_loop_product_title() {
    echo '<p class="woocommerce-loop-product__title">' . wp_trim_words( get_the_title(), 10, '...' ) . '</p>';
}

function your_prefix_setup() {
    remove_theme_support( 'wc-product-gallery-lightbox' );
}

add_action( 'wp', 'your_prefix_setup' );


// add_filter( 'woocommerce_checkout_fields' , 'add_confirm_password_checkout_field', 10, 1 );
// function add_confirm_password_checkout_field( $fields ) {
//     if ( get_option( 'woocommerce_registration_generate_password' ) != 'no' )
//         return $fields;

//     $fields['account']['account_password']['class'] = array('form-row-first');
//     $fields['account']['account_password-2'] = array(
//         'type' => 'password',
//         'label' => __( 'Password confirmation', 'woocommerce' ),
//         'required'          => true,
//         'placeholder' => _x('Confirmation', 'placeholder', 'woocommerce'),
//         'class' => array('form-row-last'),
//         'label_class' => array('hidden')
//     );
//     return $fields;
// }

// add_action( 'woocommerce_checkout_process', 'confirm_password_checkout_validation' );
// function confirm_password_checkout_validation() {
//     if ( ! is_user_logged_in() && ( WC()->checkout->must_create_account || ! empty( $_POST['createaccount'] ) ) ) {
//         if ( strcmp( $_POST['account_password'], $_POST['account_password-2'] ) !== 0 )
//             wc_add_notice( __( "Passwords doesn’t match.", "woocommerce" ), 'error' );
//     }
// }
// 
// ----- validate password match on the registration page
function registration_errors_validation($reg_errors, $sanitized_user_login, $user_email) {
    global $woocommerce;
    extract( $_POST );
    if ( strcmp( $password, $reg_retype_password ) !== 0 ) {
        return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'woocommerce' ) );
    }
    return $reg_errors;
}
add_filter('woocommerce_registration_errors', 'registration_errors_validation', 10,3);

// ----- add a confirm password fields match on the registration page
function wc_register_form_password_repeat() {
    ?>
    <p class="form-row form-row-wide">
        <label for="reg_password2"><?php _e( 'Password Repeat', 'woocommerce' ); ?> <span class="required">*</span></label>
        <input type="password" class="input-text" name="password2" id="reg_password2" value="<?php if ( ! empty( $_POST['password2'] ) ) echo esc_attr( $_POST['password2'] ); ?>" />
    </p>
    <?php
}
add_action( 'woocommerce_register_form', 'wc_register_form_password_repeat' );

// ----- Validate confirm password field match to the checkout page
function lit_woocommerce_confirm_password_validation( $posted ) {
    $checkout = WC()->checkout;
    if ( ! is_user_logged_in() && ( $checkout->must_create_account || ! empty( $posted['createaccount'] ) ) ) {
        if ( strcmp( $posted['account_password'], $posted['account_confirm_password'] ) !== 0 ) {
            wc_add_notice( __( 'Passwords do not match.', 'woocommerce' ), 'error' ); 
        }
    }
}
add_action( 'woocommerce_after_checkout_validation', 'lit_woocommerce_confirm_password_validation', 10, 2 );

// ----- Add a confirm password field to the checkout page
function lit_woocommerce_confirm_password_checkout( $checkout ) {
    if ( get_option( 'woocommerce_registration_generate_password' ) == 'no' ) {

        $fields = $checkout->get_checkout_fields();

        $fields['account']['account_confirm_password'] = array(
            'type'              => 'password',
            'label'             => __( 'Confirm password', 'woocommerce' ),
            'required'          => true,
            'placeholder'       => _x( 'Confirm Password', 'placeholder', 'woocommerce' )
        );

        $checkout->__set( 'checkout_fields', $fields );
    }
}
add_action( 'woocommerce_checkout_init', 'lit_woocommerce_confirm_password_checkout', 10, 1 );

function disable_shipping_calc_on_cart( $show_shipping ) {
    if( is_cart() ) {
        return false;
    }
    return $show_shipping;
}
add_filter( 'woocommerce_cart_ready_to_calc_shipping', 'disable_shipping_calc_on_cart', 99 );

function get_ups_tracking( $order_id ) {

    $ups_tracking   = new WF_Shipping_UPS_Tracking();
    $order_notice   = "";

    $shipment_id_cs = get_post_meta( $order_id, "ups_shipment_ids", true );
    $ups_track_url  = "http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=";

    if( $shipment_id_cs != '' ) {

        $shipment_ids = explode(",", $shipment_id_cs);
        
        if( empty( $shipment_ids ) ) {
            return;
        }
        
        foreach ( $shipment_ids as $shipment_id ) {
            $order_notice   .= '<a href="' . $ups_track_url . $shipment_id . '" target="_blank">Track Shipping Here</a>';
        }

        echo '<p>'.$order_notice.'</p>';

    }

}
add_action('woocommerce_ups_track_shipping_display', 'get_ups_tracking');

// add_filter( 'default_checkout_billing_country', 'change_default_checkout_country' );

// function change_default_checkout_country() {
//   return 'US'; // country code
// }

add_action( 'woocommerce_single_product_summary', 'dev_designs_show_sku', 5 );

function dev_designs_show_sku(){
    
    global $product;
    
    if($product->get_sku()!=""){
        echo '<p class="sku">SKU: ' . $product->get_sku() . "</p>";
    }
    
    // $products = array_filter( array_map( 'wc_get_product', $product->get_children() ) );        
    // foreach ( $products as $_product ) {
    //     echo $_product->get_sku();
    // }
}


function GetTrackingNumber(){
   
    	$order = wc_get_order($_POST['order_id']);
    	
    	
    	$data = array(

    	    success     => false,
    	    tracknumber => ""
    	    
    	);
    	
        if(json_encode($order)!="false"){
            
            $data['success'] = true;
            
            $data['tracknumber'] = key($order->get_data()['meta_data'][27]->value);
            // echo  key($order->get_data()['meta_data'][27]->value);
            
        }
        
        echo json_encode($data);
    	
    	 die();
    
    
}
add_action( 'wp_ajax_nopriv_GetTrackingNumber', 'GetTrackingNumber' );
add_action( 'wp_ajax_GetTrackingNumber', 'GetTrackingNumber' );

add_action( 'wp', 'deactivate_rocket_lazyload_on_single' );
function deactivate_rocket_lazyload_on_single() {
    if ( is_product() || is_page('canisters') || is_page('tubings')  || is_page('accessories') ) {
        add_filter( 'do_rocket_lazyload', '__return_false' );
    }
}
/** Disable Ajax Call from WooCommerce */
add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_cart_fragments', 11); 
function dequeue_woocommerce_cart_fragments() { 

    if (is_front_page() ) {
        
         wp_dequeue_script('wc-cart-fragments');
         
    }
}

add_filter( 'wc_product_has_unique_sku', '__return_false' ); 

add_filter( 'woocommerce_email_headers', 'woocommerce_completed_order_email_bcc_copy', 10, 2);
 
function woocommerce_completed_order_email_bcc_copy( $headers, $email ) {
    if ($email == 'customer_completed_order') {
        $headers .= 'BCC: Liposales <sales@liposales.com>' . "\r\n"; //just repeat this line again to insert another email address in BCC
    }
 
    return $headers;
}

add_filter( 'woocommerce_get_price_html', 'react2wp_woocommerce_hide_product_price' );


function react2wp_woocommerce_hide_product_price( $price ) {
   

    $hide_price = false;

    switch ($price) {
           
         case '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>29,500.00</span>':
                    
                $hide_price = true;

            break;

        case '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>19,500.00</span>':
                    
                $hide_price = true;

            break;

        case '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>12,000.00</span>':
                    
                $hide_price = true;

            break;
            
        default:
                # code...
            break;
    }


    if($hide_price){

        if(is_single()){

            return "";

        }else{

            return (($hide_price) ? '<span class="woocommerce-Price-amount amount">Click For Pricing</span>' : $price);

        }


    }else{

        return $price;

    }

   


}


//  remove product category base
add_filter('request', function( $vars ) {
    global $wpdb;
    if( ! empty( $vars['pagename'] ) || ! empty( $vars['category_name'] ) || ! empty( $vars['name'] ) || ! empty( $vars['attachment'] ) ) {
        $slug = ! empty( $vars['pagename'] ) ? $vars['pagename'] : ( ! empty( $vars['name'] ) ? $vars['name'] : ( !empty( $vars['category_name'] ) ? $vars['category_name'] : $vars['attachment'] ) );
        $exists = $wpdb->get_var( $wpdb->prepare( "SELECT t.term_id FROM $wpdb->terms t LEFT JOIN $wpdb->term_taxonomy tt ON tt.term_id = t.term_id WHERE tt.taxonomy = 'product_cat' AND t.slug = %s" ,array( $slug )));
        if( $exists ){
            $old_vars = $vars;
            $vars = array('product_cat' => $slug );
            if ( !empty( $old_vars['paged'] ) || !empty( $old_vars['page'] ) )
                $vars['paged'] = ! empty( $old_vars['paged'] ) ? $old_vars['paged'] : $old_vars['page'];
            if ( !empty( $old_vars['orderby'] ) )
                    $vars['orderby'] = $old_vars['orderby'];
                if ( !empty( $old_vars['order'] ) )
                    $vars['order'] = $old_vars['order'];    
        }
    }
    return $vars;
});



