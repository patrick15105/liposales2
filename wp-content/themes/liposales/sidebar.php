<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package storefront
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area" role="complementary">
	<div class="z-index">
		<span class="aside-title gradient gradient-text">Filter By:</span>
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</div>
</div>
<!-- #secondary -->
