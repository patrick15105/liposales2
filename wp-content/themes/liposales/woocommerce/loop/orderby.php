<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<form class="woocommerce-ordering" method="get">
	<!--<label for="orderby" class="label">Sort By:</label>-->
	<!--<select name="orderby" class="orderby input-text">-->
	<!--	<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>-->
	<!--		<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><span><?php echo esc_html( $name ); ?></span></option>-->
	<!--	<?php endforeach; ?>-->
	<!--</select>-->
	
	<div class="centaur-component centaur-inline" id="sortby-page" data-show-products="3">
		<div class="centaur-header">
			<div class="centaur-label" style="opacity: 0">Show:</div>
		</div>
		<div class="centaur-select orderbyfilter" style="width: 225px;right: 20px;">
        
        <?php
			 $current ="Sort by popularity";

			 foreach ( $catalog_orderby_options as $id => $name ) {
			    
			    if(trim(selected( $orderby, $id ,false )) !=''){

			    	$current =  $name ;
			    	break;

			    }


			 }


			 ?>

			<div class="centaur-header">
				<div class="centaur-label"><?php  echo $current; ?></div>
				<i class="fa fa-chevron-down"></i>
			</div>


			<ul class="centaur-list">

				<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
					
					<li id="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?> class="centaur-item "><?php echo esc_html( $name ); ?></li>

				<?php endforeach; ?>
	
			</ul>

			<select name="orderby" class="orderby" style="display:none">
				<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
					<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
				<?php endforeach; ?>
			</select>
		</div>
    </div>
	<input type="hidden" name="paged" value="1" />
	<?php wc_query_string_form_fields( null, array( 'orderby', 'submit', 'paged', 'product-page' ) ); ?>
</form>
