<?php
/**
 *
 * This template part is for Single Procedure display
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Woocommerce_Add_To_Cart_Popup
 * @subpackage Woocommerce_Add_To_Cart_Popup/template
 */

defined( 'ABSPATH' ) || exit;

?>
<table class="centaur-table clearfix">
	<tr data-product_key="<?php do_action('cm_woo_get_key'); ?>" data-qty="<?php do_action('cm_woo_get_qty'); ?>">
		<td class="centaur-thumb"><?php do_action('cm_woo_get_thumbnail'); ?></td>
		<td class="centaur-name"><?php do_action('cm_woo_get_name'); ?></td>
		<td class="centaur-subtotal"><?php do_action('cm_woo_get_total_price'); ?></td>
		<td class="centaur-action"><span id="centaur-remove-item"><i class="fa fa-times"></i></span></td>
	</tr>
</table>