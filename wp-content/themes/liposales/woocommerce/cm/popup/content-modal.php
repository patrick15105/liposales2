<?php
/**
 *
 * This template part is for Single Procedure display
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Woocommerce_Add_To_Cart_Popup
 * @subpackage Woocommerce_Add_To_Cart_Popup/template
 */

defined( 'ABSPATH' ) || exit;

?>

<div class="centaur-full centaur-fixed centaur-nodisplay centaur-modal centaur-flex">

	<div class="centaur-container">
		<div class="centaur-header">
			<h3 class="centaur-title"><i class="fa fa-check centaur-inline"></i><span class="gradient gradient-text centaur-inline">This item has been added to your carts:</span></h3>
			<span id="centaur-modal-close"><i class="fa fa-times"></i></span>
		</div>

		<div class="centaur-content"></div>

		<div class="centaur-actions centaur-flex">
			<a href="<?php echo wc_get_cart_url(); ?>" class="btn btn-outline"><?php _e('View Cart','woocommerce-add-to-cart-popup'); ?></a>
			<a href="<?php echo wc_get_checkout_url(); ?>" class="btn btn-outline"><?php _e('Checkout','woocommerce-add-to-cart-popup'); ?></a>
			<a class="btn" id="centaur-modal-close"><?php _e('Continue Shopping','woocommerce-add-to-cart-popup'); ?></a>
		</div>

	</div>
	
</div>