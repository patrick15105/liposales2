<?php
/**
 * Checkout login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

if ( is_user_logged_in() || 'no' === get_option( 'woocommerce_enable_checkout_login_reminder' ) ) {
	return;
}

?>
<div class="woocommerce-form-login-toggle">
	<?php wc_print_notice( apply_filters( 'woocommerce_checkout_login_message', __( 'Returning customer?', 'woocommerce' ) ) . ' <a href="#openmodal" class="showlogin">' . __( 'Click here to login', 'woocommerce' ) . '</a>', 'notice' ); ?>
</div>
<div id="modal-login">
	<div class="overlays"></div>
	<div class="modal">
		<div class="modal-header" style="
		    background: whitesmoke;
		    padding: 20px;
		">
		  <h1 class="gradient gradient-text" style="
		    text-transform: none;
		    font-size: 23px;
		    margin: 0;
		"> Have LipoSales Account?</h1>
		<span class="closelogin">&times</span>
		    </div>
		<div class="modal-body">
			<?php
			woocommerce_login_form(
				array(
					'message'  => __( 'If you have shopped with us before, please enter your details below. If you are a new customer, please proceed to the Billing &amp; Shipping section.', 'woocommerce' ),
					'redirect' => wc_get_page_permalink( 'checkout' ),
					'hidden'   => true,
				)
			);
			?>
			<div class="form-register">
				<h3 class="gradient-text gradient not-register-text">Not registered at LipoSales?</h3>
				<p class="not-register-text--text">Once you've done checking out as guest, you will have the option to save your billing details, and have it as your LipoSales account.</p>
				<a href="<?php echo get_site_url(); ?>/my-account/?action=register?>" class="btn">Register now</a>
			</div>
		</div>	
	</div>
</div>
