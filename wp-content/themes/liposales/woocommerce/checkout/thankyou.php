<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="woocommerce-order">

	<?php if ( $order ) : ?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : $order_tag= wc_get_order( $order->get_id() );

            $items =array();
            
            $count = 0;
            
            foreach ($order_tag->get_items() as $item_id => $item_data) {

                $count += 1;

                array_push($items,[
                          
                          "id"              => $item_data->get_product_id(),
                          
                          "name"            => $item_data->get_name(),
                          
                          "list_name"       => $item_data->get_name(),
                          
                          "brand"           => "LipoSales",
                          
                          "category"        => "",
                          
                          "variant"         => $item_data->get_variation_id(),
                          
                          "list_position"   => $count,
                          
                          "quantity"        => $item_data->get_quantity(),
                          
                          "price"           => $item_data->get_total(),
                    
                ]);



            }

            $order_data = $order_tag->get_data();

            $gtag = array(

                "transaction_id" => $order->get_order_number(),

                "affiliation"    => "LipoSales",

                "value"          => $order->get_total(),

                "currency"       => "USD",

                "tax"            => $order_data['total_tax'],

                "shipping"       =>  $order_data['shipping_total'],

                "item"           =>  $items

            );

            // echo json_encode($gtag);
           
            ?>

            <script>
                
                gtag('event', 'purchase', <?php echo json_encode($gtag); ?>);
                console.log(<?php echo json_encode($gtag); ?>);

            </script>

            <!-- Event snippet for PAL Conversions conversion page -->
            <script>
              gtag('event', 'conversion', {'send_to': 'AW-712531574/xLfCCLHQ7qkBEPa84dMC'});
            </script>

			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); ?></p>
            
            
            <div class="columns landing-page thank-you">    
       
                <div class="col-1">
                    <h1 class="gradient-text gradient">THANK YOU </br>FOR THE ORDER!</h1>
                    <h3>Your order has been confirmed and you will receive an order confirmation email shortly</h3>
                </div> 
                
                 <div class="col-1">
                    <img src="<?php echo get_site_url().'/wp-content/themes/liposales/img/thankyoupageicon.png' ; ?>" alt="">
                </div>
   
             </div>
                
            <table class="woocommerce-table table-plain woocommerce-thankyou-order-details order_details">
                
                <tbody>
                    <tr>
                        <td>
                            <h4 class="small"><?php _e( 'Order number:', 'woocommerce' ); ?></h4>
                            <h4><strong><?php echo $order->get_order_number(); ?></strong></h4>
                        </td>
                        <td>
                             <h4 class="small"><?php _e( 'Total:', 'woocommerce' ); ?></h4>
                             <h4><strong><?php echo $order->get_formatted_order_total(); ?></strong></h4>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <h4 class="small"><?php _e( 'Date:', 'woocommerce' ); ?></p>
                             <h4><strong><?php echo wc_format_datetime( $order->get_date_created() ); ?></strong></h4>
                        </td>
                        <td>
                            <h4 class="small"><?php _e( 'Payment method:', 'woocommerce' ); ?></h4>
                            <h4><strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong></h4>
                        </td>
                        
                    </tr>
                </tbody>
            </table> 




		    
		<?php endif; ?>

		<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

	<?php endif; ?>

</div>
