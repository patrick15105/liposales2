
<?php
	$class_active='additional_information_tab active';
	// adding class to active tab
?>
<h1 class="gradient-text gradient myaccount-text">My Account</h1>
<div class="single-product">
	<div class="woocommerce-tabs wc-tabs-wrapper">
		<ul class="tabs wc-tabs" role="tablist">
			
			<li aria-controls="tab-additional_information" class="<?php if($myaccountactive=='Account details'){ ?> <?php echo $class_active; } ?>" id="tab-title-additional_information" role="tab">
					<a href="<?php echo get_site_url(); ?>/my-account/edit-account/">Account details</a>
			</li>
			
			<li aria-controls="tab-reviews" class="reviews_tab <?php if  ($myaccountactive=='Address'){ ?> <?php echo $class_active; } ?>" id="tab-title-reviews" role="tab">
					<a href="<?php echo get_site_url(); ?>/my-account/edit-address/">Address</a>
			</li>

			<li aria-controls="tab-reviews" class="reviews_tab  <?php if ($myaccountactive=='Orders history') {echo $class_active; } ?>"  role="tab">
					<a href="<?php echo get_site_url(); ?>/my-account/orders/">Orders</a>
			</li>
			
		</ul>
		<div class="woocommerce-Tabs-panel panel entry-content wc-tab" id="tab-description" role="tabpanel" aria-labelledby="tab-title-description" >
	
	