<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>



<?php

	// this function for FINANCE
	if (!session_id()) {
		session_start();
	}
				
	if(isset($_SESSION['centaur_finance_obj'])){

		if($_SESSION['centaur_finance_obj']['is_direct']){
			
			wp_redirect(get_site_url(). "/finance");
			exit;

		}

	} // </ function for FINANCE >

	printf(
		__( '<a href="%1$s" class="logout">Log out <i class="la la-sign-out"></i></a>', 'woocommerce' ),
		esc_url( wc_logout_url( wc_get_page_permalink( 'myaccount' ) ) )
	);
 ?>
<h2 class="gradient-text gradient" style="text-transform: capitalize;">Welcome, <?php echo  esc_html( $current_user->display_name );  ?> </h2>
<p>From your account dashboard you can view  and manage your</p>


<div class="box">
	<?php
	printf(
		__('<a class="img-box" href="%1$s"> <h3>Recent Order</h3></a>
			<a class="img-box" href="%2$s"> <h3>Shipping and Billing Addresses</h3></a>
			<a class="img-box" href="%3$s"> <h3>Edit your password and account details</h3></a>', 'woocommerce' ),
		esc_url( wc_get_endpoint_url( 'orders' ) ),
		esc_url( wc_get_endpoint_url( 'edit-address' ) ),
		esc_url( wc_get_endpoint_url( 'edit-account' ) )

	);
	?>


</div>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
