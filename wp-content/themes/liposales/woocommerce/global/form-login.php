<?php
/**
 * Login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( is_user_logged_in() ) {
	return;
}

?>
<?php $hidden=false; ?>

<form class="woocommerce-form woocommerce-form-login login" method="post">

	<?php do_action( 'woocommerce_login_form_start' ); ?>
		
	<p class="form-row form-row-firsst woocommerce-form-row woocommerce-form-row--wide form-row-wide">
		<label for="username"><?php esc_html_e( 'Username or email', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="text" class="input-text" placeholder="Email address" name="username" id="username" autocomplete="username" />
	</p>
	<p class="form-row form-row-last form-row-wide woocommerce-form-row woocommerce-form-row--wide">
		<label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input class="input-text" placeholder="Password" type="password" name="password" id="password" autocomplete="current-password" />
	</p>
	<div class="clear"></div>

	<?php do_action( 'woocommerce_login_form' ); ?>
	
	<p class="lost_password">
		<label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
			<input class="woocommerce-form__input woocommerce-form__input-checkbox display-none" name="rememberme" type="checkbox" id="rememberme" value="forever" />
			<span class="toggle-check"><span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span></span>
		</label>
		<span class="pull-right">Not Registered at Liposales? <a href="<?php echo get_site_url()."/my-account/?action=register"; ?>" >&nbsp Register</a></span>
	</p>
	<div class="form-actions">
		<div class="column">
 			<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
			<button type="submit" class="buttons gradient-test text-white full-width" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>"><?php esc_html_e( 'Sign In', 'woocommerce' ); ?></button>
			<input type="hidden" name="redirect" value="<?php echo esc_url( $redirect ) ?>" />

			<p class="lost_password text-center pad-10">
				<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a>
			</p>
		</div>
		<div class="column">
		    
			<button data-href="<?php echo wc_get_cart_url(); ?>" id="wpmc-back-to-cart" class="buttons gradient-test text-white alt" type="button">Back to Cart</button>
			<button id="wpmc-skip-login" class="buttons gradient-test text-white button-active current alt" type="button">Guest Checkout</button>
		</div>

	</div>

	<div class="clear"></div>
	<span class="text-message">
	<?php echo ( $message ) ? wpautop( wptexturize( $message ) ) : ''; // @codingStandardsIgnoreLine ?>
		</span>
	<?php do_action( 'woocommerce_login_form_end' ); ?>

</form>
