<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author     WooThemes
 * @package    WooCommerce/Templates
 * @version    1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

//the_title( '<h1 class="product_title entry-title gradient-test gradient-text">', '</h1>' );
// the_title( '<h1 class="gradient-text">', '</h1>' );


global $post;

$terms = get_the_terms( $post->ID, 'product_cat' );


$img = "";


foreach ($terms as $term) {
    
    if($term->term_id== "38"){

		$img = "<img width='50%' src='/wp-content/uploads/2019/07/CPM-BW-LOGO-ENG.jpg' ></img>";

    	break;
    } 
   
}



//the_title( '<h1 class="product_title entry-title gradient-test gradient-text">', '</h1>' );
the_title( '<h1 class="gradient-text"  category="" >', '</h1> '. $img );

