<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<header class="woocommerce-products-header tac">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title gradient gradient-text"><?php woocommerce_page_title(); ?></h1>
	<?php endif; ?>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>

<div class="mobile-filter flex">

	<div class="mobile-filter-contain">
		<div class="btn-filter accordion">
			Filter <i class="la la-filter"></i>
		</div>
		<div class="mobile-filter-option-contain">

			<div class="mobile-filter-products">
				<div class="mobile-filter-btn btn-products accordion">
					Products <i class="la la-angle-down"></i>
				</div>
				<div class="mobile-filter-products-option-contain option-contain">
					<div class="widget woocommerce widget_product_categories">
					    <ul class="product-categories">
					        <li class="cat-item cat-item-35"><a href="http://localhost/liposales/product-category/aspirators/">Aspirators</a></li>
					        <li class="cat-item cat-item-32 current-cat"><a href="http://localhost/liposales/product-category/canisters/">Canisters</a></li>
					        <li class="cat-item cat-item-37"><a href="http://localhost/liposales/product-category/cannulas/">Cannulas</a></li>
					        <li class="cat-item cat-item-38"><a href="http://localhost/liposales/product-category/garments/">Garments</a></li>
					        <li class="cat-item cat-item-36"><a href="http://localhost/liposales/product-category/infiltrations/">Infiltrations</a></li>
					        <li class="cat-item cat-item-23"><a href="http://localhost/liposales/product-category/surgical-drains/">Surgical Drains</a></li>
					        <li class="cat-item cat-item-30"><a href="http://localhost/liposales/product-category/tubing/">Tubing</a></li>
					        <li class="cat-item cat-item-15"><a href="http://localhost/liposales/product-category/uncategorized/">Uncategorized</a></li>
					    </ul>
					</div>
				</div>
			</div>
			<!-- /products -->

			<div class="mobile-filter-accessories">
				<div class="mobile-filter-btn btn-accessories accordion">
					Accessories <i class="la la-angle-down"></i>
				</div>
				<div class="mobile-filter-accessories-option-contain option-contain">
					<div class="widget woocommerce widget_product_categories">
					    <ul class="product-categories">
					        <li class="cat-item cat-item-35"><a href="#">Accessories 1</a></li>
					        <li class="cat-item cat-item-32 current-cat"><a href="#">Accessories 2</a></li>
					        <li class="cat-item cat-item-37"><a href="#">Accessories 3</a></li>
					    </ul>
					</div>
				</div>
			</div>
			<!-- /accessories -->

			<div class="mobile-filter-brand">
				<div class="mobile-filter-btn btn-brand accordion">
					Brand <i class="la la-angle-down"></i>
				</div>
				<div class="mobile-filter-accessories-option-contain option-contain">
					<div class="widget woocommerce widget_product_categories">
					    <ul class="product-categories">
					        <li class="cat-item cat-item-35"><a href="#">Brand 1</a></li>
					        <li class="cat-item cat-item-32 current-cat"><a href="#">Brand 2</a></li>
					        <li class="cat-item cat-item-37"><a href="#">Brand 3</a></li>
					    </ul>
					</div>
				</div>
			</div>
			<!-- /brand -->

		</div> <!-- /mobile-filter-option-contain -->
	</div> <!-- /mobile-filter-contain -->

	<div class="mobile-sort-contain">
		<div class="btn-filter accordion">
			Sort <i class="la la-sort"></i>
		</div>
		<div class="mobile-sort-option-contain">
			
		</div>
	</div> <!-- /mobile-sort-contain -->

	<div class="mobile-show-contain">
		<div class="btn-filter accordion">
			Show <i class="la la-eye"></i>
		</div>
		<div class="mobile-show-option-contain">
			
		</div>
	</div>

</div>

<div class="overlay"></div>

<?php
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked wc_print_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 *
			 * @hooked WC_Structured_Data::generate_product_data() - 10
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	?>
	
	<div class="tar">
		
		<div class="back-to-top tac">
			<a href="#" class="btn-return">RETURN TO TOP <i class="la la-arrow-up"></i></a>
		</div>

		<?php

		/**
		 * Hook: woocommerce_after_shop_loop.
		 *
		 * @hooked woocommerce_pagination - 10
		 */
		do_action( 'woocommerce_after_shop_loop' );

		?>

	</div>
	
	<?php

} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );