<?php 
/* Template Name:  Liposales Thank You Page */ ?>

<?php get_header(); ?>

<div class="columns landing-page thank-you">	
	<div class="col-1 gradient-text gradient">
		<img src="<?php echo get_site_url().'/wp-content/themes/liposales/img/thankyoupageicon.png' ; ?>" alt="">
	</div>
	<div class="col-1">
		<h1 class="gradient-text gradient title">Thank You!</h1>
		<p class="desc">
			Your email has been successfully submitted and we will get in touch with you shortly. For inquiries, contact <a href="telno:1-866-336-1333">1-866-336-1333</a>
		</p>
		<p class="desc button-desc">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="buttons gradient-test text-white">Go To Home</a>
		</p>
	</div>
</div>


<?php get_footer(); ?>