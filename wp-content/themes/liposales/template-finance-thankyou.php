<?php 
/* Template Name:  Finance Thank you page*/ ?>

<?php get_header(); ?>

<?php
	
	$verif_code="";

	$data = array(

		'success' => false,

		'message' => ''
	);

	if( is_user_logged_in()){

		if(!isset($_GET['vfc'])){

			global $wp_query;
			
			$wp_query->set_404();
			
			status_header( 404 );
			
			get_template_part( 404 ); 

			exit();

		}else{

			$finance_data  = do_shortcode('[verified_code]');
				
			$verif_code    = json_decode($finance_data,true);

			if($verif_code['success']){

				$data['success'] = true;

			}else{ 

				$data['message'] = $verif_code['message'];

			}


		}

	} ?>


	<?php

		if(!$data['success']){

			?>

				<div class="columns landing-page" style="padding: 10%">	
			
					<div class="woocommerce-info" style="width: 100%">
						
						<p><center><?php echo $data['message']; ?></center></p>

					</div>
			
				</div>
	
			<?php

		}else{ ?>
			
			
			<div class="columns landing-page" style="padding: 10%">	
				

				<div class="col-1 gradient-text gradient">
					<img src="<?php echo get_site_url().'/wp-content/themes/liposales/img/thankyoupageicon.png' ; ?>" alt="">
				</div>


				<div class="col-1">
					
					<h2 class="gradient-text gradient title">Thank you for requesting</h2>
					
					<p class="desc">
						
						Wait for 3-7 days for admin approval  

					</p>

					<p class="desc button-desc">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="buttons gradient-test text-white">Go To Home</a>
					</p>
				</div>

			</div>
		

		<?php } ?>







<?php get_footer(); ?>