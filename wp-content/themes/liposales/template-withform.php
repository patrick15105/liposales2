<?php
/**
 * Template Name: Post with Form
 * The template for displaying post with form at the bottom.
 * 
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php if ( get_field("location") == "top" ): ?>

			<div class="form-container"><?php echo do_shortcode(get_field("shortcode")); ?></div>

			<?php endif; ?>


			<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php
				/**
				 * Functions hooked in to storefront_page add_action
				 *
				 * @hooked storefront_page_header          - 10
				 * @hooked storefront_page_content         - 20
				 */
				do_action( 'storefront_page' );
			?>
			</article>

			<?php endwhile; // End of the loop. ?>

			<?php if ( get_field("location") == "bottom" ): ?>

			<div class="form-container"><?php echo do_shortcode(get_field("shortcode")); ?></div>

			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
do_action( 'storefront_sidebar' );
get_footer();
