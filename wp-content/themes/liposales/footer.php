<?php

/**

 * The template for displaying the footer.

 *

 * Contains the closing of the #content div and all content after

 *

 * @package storefront

 */



?>



		<?php if( !is_front_page() ) { ?>

			</div><!-- .col-full -->
	    
	    </div><!-- .container-full-width -->

		<?php } ?>

	</div><!-- #content -->



	<?php do_action( 'storefront_before_footer' ); ?>



	<footer id="colophon" class="site-footer" role="contentinfo">



		<?php

		/**

		 * Functions hooked in to storefront_footer action

		 *

		 * @hooked storefront_footer_widgets - 10

		 * @hooked storefront_credit         - 20

		 */

		do_action( 'storefront_footer' ); ?>


<div class="newsletter-contain">
		<div class="col-full">

			<div class="subscribe-contain">

				<div>

					<h3 class="section-header gradient-text">subscribe to our newsletter</h3>

					<p>State-of-the-art medical equipment at affordable prices.</p>

				</div>

				<form action="" class="subscribe-submit">

					<input type="text" placeholder="Enter your email address">

					<input type="submit" value="Submit" class="send">

				</form>

			</div>

		</div>
	
</div>



		<div class="footer-primary-contain">

			<div class="col-full">

				<?php

		        wp_nav_menu( array(

		            'theme_location' => 'footer-primary',

		            'menu_class'     => 'footer-primary',

		            'depth'          => 1

		        ) );

		        ?>

		    </div>

		</div>

	<div class="copyright-contain caption">
		
		<div class="col-full">

			<div class="footer-secondary-contain">

				<div class="social-contain">

					<ul>

						<li><a  href="https://www.facebook.com/liposales" class="facebook" target="_blank"  rel="nofollow noopener"><i class="la la-facebook-official"></i></a></li>

						<li><a href="https://twitter.com/Liposales_com" class="twitter" target="_blank" rel="nofollow noopener"><i class="la la-twitter"></i></a></li>

						<li><a href="https://www.instagram.com/liposales/"  target="_blank" rel="nofollow noopener"><i class="la la-instagram"></i></a></li>

						<li><a href="https://www.youtube.com/channel/UCGDottbQEVG8JuCUAgcEUww"  target="_blank" rel="nofollow noopener"><i class="la la-youtube-play"></i></a></li>

						<li><a  href="https://vimeo.com/liposales"  target="_blank" rel="nofollow noopener"><i class="la la-vimeo"></i></a></li>

					</ul>

				</div>

				<div class="footer-nav-contain">

					

					<?php

			        wp_nav_menu( array(

			            'theme_location' => 'footer-secondary',

			            'menu_class'     => 'footer-secondary',

			            'depth'          => 1

			        ) );

			        ?>
			        
			        <div>&copy; 2018 . All rights reserved.</div>

			    </div>

			    <div class="powered">

			    	Powered by <a href="https://www.centaurmarketing.co/" class="is-link" rel="noopener nofollow">Centaur Marketing</a>

			    </div>

			</div>



		</div><!-- .col-full -->
	</div>

	</footer><!-- #colophon -->



	<?php do_action( 'storefront_after_footer' ); ?>



</div><!-- #page -->



<?php wp_footer(); ?>


<?php 
	
	  if(is_front_page()){

		$shop_url = get_site_url();

		$shop_name = "Liposales";

		$markup  = array(
			"@context"=> "http://schema.org",
			'@type' => 'Organization',
			'name'  => $shop_name,
			'url'   => $shop_url,
			'contactPoint' => [array(
				"@type" 	   => "ContactPoint",
				"telephone"    => "+1-866-336-1333",
				"contactType"  => "Customer service"
			)],
			'logo'     =>  $shop_url . "/wp-content/uploads/2018/08/logo2.png",
			"location" => array(
		    	"@type"    => "Place",
			    "address"  => array(
				    "@type" 		  => "PostalAddress",
				    "addressLocality" => "2003 Edwards St. Houston, TX 77007",
				    "addressRegion"   => "TX",
				    "postalCode"	  => "77007"
			    ),
		    ),
			"sameAs"=> [
				"https://www.facebook.com/liposales",
			    "https://twitter.com/Liposales_com",
			    "https://www.instagram.com/liposales/",
			    "https://www.youtube.com/channel/UCGDottbQEVG8JuCUAgcEUww",
			    "https://vimeo.com/liposales"
			]
		);
?>
		<script type="application/ld+json"><?php echo json_encode($markup); ?></script>
<?php } ?>

</body>

</html>






