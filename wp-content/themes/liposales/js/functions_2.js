/*
*   Theme Functions
*/


(function ($, root, undefined) {
    
    $(function () {
        
        'use strict';
        
        $("#menu-secondary .menu-item-has-children > a").each(function(e) {

            var trigger = $(this);

            $(this).on("click", function(event) {
                event.preventDefault();
            });

            $(this).parent().hover(
                function ( event ) {
                    trigger.next().addClass("active");
                },
                function ( event ) {
                    trigger.next().removeClass("active");
                }
            );

        });
        
        $("#site-navigation ul li.menu-item").click(function(){


            var a  = $(this).find('.open-menu-link');
            if($(a).prev().hasClass('active')){
                $(a).removeClass('active');
                $(a).next().stop(true,true).removeClass('active');
                $(a).prev().removeClass('active');
                $(a).siblings().removeClass('active');
            }else{
                $(a).next().stop(true,true).slideDown();
                $(a).addClass('active');
                $('.nav-menu li > a').not($(this)).next().stop(true,true).removeClass('active');
                $(a).prev().addClass('active');
                $('.nav-menu li > a').not($(this)).removeClass('active');
                $(a).siblings().addClass('active');
            }

        })

        // MENU NAV
        // $('.open-menu-link').click(function() {
        //     if($(this).prev().hasClass('active')){
        //         $(this).removeClass('active');
        //         $(this).next().stop(true,true).removeClass('active');
        //         $(this).prev().removeClass('active');
        //         $(this).siblings().removeClass('active');
        //     }else{
        //         $(this).next().stop(true,true).slideDown();
        //         $(this).addClass('active');
        //         $('.nav-menu li > a').not($(this)).next().stop(true,true).removeClass('active');
        //         $(this).prev().addClass('active');
        //         $('.nav-menu li > a').not($(this)).removeClass('active');
        //         $(this).siblings().addClass('active');
        //     }
        // });

        $('.accordion').click(function() {
            $(this).next().stop().slideUp('fast');
            $(this).parent().addClass('active');
            $('.overlay').stop().addClass('active');
            $(this).next().stop().slideDown('fast');
        });

        $('.overlay').click(function() {
            $(this).removeClass('active');
            $('.accordion').parent().removeClass('active');
            $('.accordion').next().stop().hide();
        });
        
          $('.banner-slider').slick({
             
               autoplay: true,
               dots: true,
              infinite: true,
              speed: 300,
              fade: true,
               cssEase: 'linear',
        });
        
        

        $('.three-item').slick({
          autoplay: true,
          infinite: true,
          speed: 300,
          cssEase:  'linear',
          slidesToShow: 3,
          slidesToScroll: 3,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });

        $('.single-item').slick({
            autoplay: true,
            dots: true,
            prevArrow: false,
            nextArrow: false
        });
        
        $("#wpmc-skip-login").click(function(){
            console.log("trigger")
            $("#billing_country").trigger("change")
        })



        if($("body").hasClass("woocommerce-account") || $("body").hasClass("woocommerce-checkout")){
                
                $("#billing_email").attr("placeholder","Email address");
                $("#billing_phone").attr("placeholder","Phone");
               
                
                form_validation.woocommerce_load();
        }
        


       var ulContent="";
       
       var $ul = $('ul.woocommerce-error');
       
       if(ulContent !== $ul.html()){
            
            ulContent = $ul.html();
            $ul.trigger('contentChanged');
        
        }

        $(document.body).on('checkout_error', form_validation.woocommerce_load);


        $(".showlogin").click(showModal);
        $(".closelogin").click(closeModal);


        var $modal = $("#modal-login");

        function  showModal(){
           // Show Modal
           $("#modal-login").addClass('active');

        }

        function closeModal(){
           // Close Modal
           $("#modal-login").removeClass('active');
       }
       
       
       if($(".products li").length < 1 ){
           $(".woocommerce-sort-filter").addClass("center");
       }

        
        // Iterate over each select element
        // $('select').each(function () {
        //     // Cache the number of options
        //     var $this = $(this),
        //         numberOfOptions = $(this).children('option').length;

        //     // Hides the select element
        //     $this.addClass('s-hidden');

        //     // Wrap the select element in a div
        //     $this.wrap('<div class="select"></div>');

        //     // Insert a styled div to sit over the top of the hidden select element
        //     $this.after('<div class="styledSelect"></div>');

        //     // Cache the styled div
        //     var $styledSelect = $this.next('div.styledSelect');

        //     // Show the first select option in the styled div
        //     $styledSelect.text($this.children('option').eq(0).text());

        //     // Insert an unordered list after the styled div and also cache the list
        //     var $list = $('<ul />', {
        //         'class': 'options'
        //     }).insertAfter($styledSelect);

        //     // Insert a list item into the unordered list for each select option
        //     for (var i = 0; i < numberOfOptions; i++) {
        //         $('<li />', {
        //             text: $this.children('option').eq(i).text(),
        //             rel: $this.children('option').eq(i).val()
        //         }).appendTo($list);
        //     }

        //     // Cache the list items
        //     var $listItems = $list.children('li');

        //     // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
        //     // $styledSelect.live("click", function (e) {
        //     $(document).on("click", ".styledSelect", function (e) {
        //         e.stopPropagation();
        //         $('div.styledSelect.active').each(function () {
        //             $(this).removeClass('active').next('ul.options').hide();
        //         });
        //         $(this).toggleClass('active').next('ul.options').toggle();
        //     });

        //     // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
        //     // Updates the select element to have the value of the equivalent option
        //     $listItems.click(function (e) {
        //         e.stopPropagation();
        //         $styledSelect.text($(this).text()).removeClass('active');
        //         $this.val($(this).attr('rel'));
        //         $list.hide();
        //         /* alert($this.val()); Uncomment this for demonstration! */
        //     });

        //     // Hides the unordered list when clicking outside of it
        //     $(document).click(function () {
        //         $styledSelect.removeClass('active');
        //         $list.hide();
        //     });

        // });
        
        /**
         * For Mobile version
         */
        
        if( $(window).width() > 767 ) {

            // Sticky Header
            $(window).on( "scroll", function () {
                var fromTop = $(this).scrollTop();
                $("#page").toggleClass("sticky-header", (fromTop > 0));                
            });

        } else {

            $("#secondary .aside-title").on("click", function() {

                $("#secondary").toggleClass("active");

                if( !$("#secondary").hasClass("active") ) {
                    $("#secondary .widget").removeClass("active");                    
                }


            });


            $("#secondary .widget h3").each(function() {

                $(this).on("click", function() {
                   $(this).parent().parent().toggleClass('active');
                });

            });

        }


    });


   
})(jQuery, this);


var form_validation = (function(){
    
 
    var $ = jQuery.noConflict();

    var $error_list = $("ul.woocommerce-error > li");


    function _error_message(mes){ 

        // Removing Word Error and html tags
        // return <span> mesage </span>
        return "<span class='error-message'>"+mes.replace(/<(?:.|\n)*?>/gm, '').replace('Error: ', '')+"</span>";

    }

    function _add_error_select_field(selector,message){

        // triggering specific select with of an parameter
        var id = "select"+selector;

        // triggering parent of field select 
        // and adding error border 
        // find select2-selection--single and adding border
        
        if($(selector).length){
        $(id).parents('p').addClass("error-border")
              .find('.select2-selection--single')
              .addClass("error-border");

        // triggering parent of select and appending message
        $(id).parents('p').append(message);
        
        }else{
            $("#error-"+id).removeClass("display-none");
        }

    }


    function _add_error_input_field(selector,message,id){
        
        if($(selector).length){
            
            // adding border error field input
            $(selector).addClass("error-border");
            // tringgering parent field and append message 
            $(selector).parents("p").append(message);
            
        }else{
            $("#error-"+id).removeClass("display-none");
        }

    }

    function form_checking_validation(){
         $(".error-border").removeClass("error-border");
         $(".error-message").remove();
        $("ul.woocommerce-error > li > span ").each(function(){

            // Curent li tags <>
            var list_id = $(this).attr("id") +"";
            
            list_id = (list_id).replace("error-","");
            
            // targeting field that have placeholder same as `list_id` 
            var selector =  "[placeholder='"+list_id+"']";
            
            // Set Message 
            var message = _error_message($(this).html());
    

            // Checking current page 
            if($("body").hasClass('woocommerce-checkout')){
                
                list_id =(list_id).replace("error-","");
                // Checking if value if null
                if(list_id != ""){
                    
                    selector = "#"+(list_id).replace("error-","");
                    
                    $(selector).parents("p").find('.error-message').remove();

                    // if list_id select billing_state  and shippig_state
                    if(list_id == "billing_state" || list_id == "shipping_state" ){

                        // Adding error to select fields 
                        _add_error_select_field(selector,message);    

                    }else{

                        //  if list_id selector form field
                        if( list_id == "Email address" ||  list_id == 'Password' )  {

                            // Triggering form field id 
                           
                            selector =  "[placeholder='"+list_id+"']";
                            console.log("[placeholder='"+list_id+"']");
                            

                        }

                        // Add error input field to form customer details
                        _add_error_input_field(selector,message,list_id);

                    }
                }

            }else{
                
                $(this).removeClass("display-none");
                
              _add_error_input_field(selector,message,list_id);

              $("[data-placeholder='"+list_id+"']")
                      .parents("p").addClass("error-border")
                      .append(message);
            }
            
            console.log(selector)

        });

        _no_error_response();
    }

    function _no_error_response(){
    
        console.log("display none : " + $(".woocommerce-error > li.display-none").length + " Total Count " + $(".woocommerce-error > li").length)
    
      var total_list = $(".woocommerce-error > li ").length;

      // if the error is not form field
      // and hide woocommerce-error 
      if(total_list == $(".woocommerce-error > li span.display-none").length){

          $(".woocommerce-error").css({display: 'none'});

      }

    }

    // Expose Function
    return {
        woocommerce_load   : form_checking_validation,
    }  

})();


var quantity = (function(){

    var value = 1;

    // Cache DOM
    var $ = jQuery.noConflict();
    var $qty = $(".quantity").prepend("<div id='decrement' class='la la-minus'></div><div id='increment' class='la la-plus'></div>");
    
    var $input = "";
    
    var $icon_minus = $qty.find('.la-minus');
    
    var $icon_plus = $qty.find('.la-plus');

    // Bind Events
    $icon_plus.on('click' , addQuantity);
    $icon_minus.on('click' , minusQuantity);

    // Set Value 
    function _setValue(){

        // $input = $(".quantity input");
        
        $input.trigger("change");
        $input.val(value);
        
        

    }

    function addQuantity(){
        

        $input = $(this).parents(".quantity").find('input');

        // Checking value
        if(value < $input.attr('max') || $input.attr('max')=='')
            value = parseInt($input.val()) + 1;
        
        // Calling function _setValue
        _setValue(); 

    }

    function minusQuantity(){
        
        $input = $(this).parents(".quantity").find('input');

        //  Value must be greater than 1
        if(value > 1)
            value = parseInt($input.val()) - 1;

        // Calling function _setValue
        _setValue();

    }

    // Expose Function
    // return {
    //     addQuantity   : addQuantity,
    //     minusQuantity : minusQuantity
    // }

})();


    var $ = jQuery.noConflict();



    $(".form-field-password").append('<i class="la la-eye"></i><i class="la la-eye-slash"></i>');

        var $active = $(".form-field-password i.la-eye");
        var $deactive = $(".form-field-password i.la-eye-slash")




    $active.click(function(){

         $(this).parent(".form-field-password").addClass("active").find("input").attr("type","text");

    })

    $deactive.click(function(event) {
          
       $(this).parent(".form-field-password").removeClass("active").find("input").attr("type","password");

    });




    $("form.woocommerce-form-register").submit(function(e) {
       
        /* Act on the event */

        
        var $is_check_term = $("#term_condition"),
            $password = $("#reg_password"),
            $re_type_pass = $("#reg_retype_password"),
            $fname  = $("#reg_billing_first_name"),
            $lname =  $("#reg_billing_last_name"),
            $email =  $("#reg_email"),
            no_error = true;


        $(this).find("p.error-border").removeClass('error-border').find(".error-message").remove();
            

        if($fname.val()==""){

             $fname.parents("p").addClass("error-border")
                .append("<span class='error-message'>First Name field is a required</span>");

            no_error = false;
        }


        if($lname.val()==""){

            $lname.parents("p").addClass("error-border")
                .append("<span class='error-message'>Last Name field is a required</span>");

            no_error = false;
            
        }
        
        if($email.val()==""){

            $email.parents("p").addClass("error-border")
                .append("<span class='error-message'>Email field is a required</span>");

            no_error = false;
            
        }

        if($password.val()==""){
           
            $password.parents("p").addClass("error-border")
                .append("<span class='error-message'>Password field is a required</span>");

            no_error = false;

        }else{

            if($password.val() != $re_type_pass.val()){

                $re_type_pass.parents("p").addClass("error-border")
                      .append("<span class='error-message'>Password does not match the confirm password</span>");   
            
                 no_error = false;
            

            }else if(!$is_check_term.is(':checked')){
                
               $(".term .pop-up").addClass("active");
               no_error = false;
                
            }else{

                return true;
            }

        }


        if(!no_error){

            e.preventDefault();

            return false;
        }



        
                
        

    });
    
    
    
    // if($("#billing_country").val() == ""){
       
    //   $("#billing_country").val("US").change();
        
    // }

    
    var height      = 0 ,
        curr_height = 0;

    $("a .woocommerce-loop-product__title").each(function(index, el) {
            
        curr_height =$(this).height();

        if(curr_height > height){
            height = curr_height;
        }

    });
    

    $(".woocommerce-loop-product__title").css("height",height+"px");
    
    $(document).on("click", ".form-actions #wpmc-back-to-cart", function () {
        window.location.href = $(this).data('href');
    });
    
  
    
    $("#ups_track").submit(function(e){
        

        e.preventDefault();
        
        $order_id = $("#text_ups_track");
    
        $(".error-message").remove();
        
        $.ajax({

            url  : ajax_posts.ajaxurl+"/?action=GetTrackingNumber",
    
            type : "POST",
    
            data : {order_id : $order_id.val() },
            
            dataType : "JSON",
    
            success :function(data){
                
                console.log(data)
    
                if(data.success){
        
                    // pop('https://www.ups.com/track?loc=en_US&tracknum='+data.tracknumber+'&requester=WT/trackdetails');
                    
                    var href = 'https://www.ups.com/track?loc=en_US&tracknum='+data.tracknumber+'&requester=WT/trackdetails';
                    
                    $("#direct-page").parent("a").attr("href",href).trigger("click");
                    
                    $("#direct-page").trigger("click");
                  
                    
                    console.log(href);
    
                }else{
                    
                   $order_id.addClass("error-border").parent("p").append('<span class="error-message">Order no. is not exist!</span>');
                    
                }
            }
    
    
        })



        return false;



    })
    
    $(".table-info tbody tr").click(function(){

        var index = $(this).find("td").first().html();
        
        var default_date = "2019/07";

        var index_changes = [1,2,26];

        for(var i=0 ; i < index_changes.length ; i++){
                if(index_changes[i] == index){
                     default_date = "2019/09";
                }
        }

        if(index==5){
            default_date = "2019/09";
            index = index+"-1";
        }

        $("#pop-up-wheel .centaur-header h3").html($(this).find("td").last().html())
    
        $("#pop-up-wheel-image").prop("src","https://www.liposales.com/wp-content/uploads/"+default_date+"/C"+index+".jpg");
    
        $(".centaur-content").html("<img src='https://www.liposales.com/wp-content/uploads/"+default_date+"/C"+index+".jpg' style='height:400px;'></img>");
        
        $("#pop-up-wheel").css("display","flex");
    })

    
    $("section.custom-page li.product").click(function(){
        
       console.log($(this).find("a").attr("href")); 
       
       window.location.href = $(this).find("a").attr("href");
        
    });

   