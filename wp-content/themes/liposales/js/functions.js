/*
*   Theme Functions
*/

(function ($, root, undefined) {
    
    $(function () {
        
        'use strict';
        
        $("#menu-secondary .menu-item-has-children > a").each(function(e) {

            var trigger = $(this);

            $(this).on("click", function(event) {
                event.preventDefault();
            });

            $(this).parent().hover(
                function ( event ) {
                    trigger.next().addClass("active");
                },
                function ( event ) {
                    trigger.next().removeClass("active");
                }
            );

        });
        
       

        // MENU NAV
        // $('.open-menu-link').click(function() {
        //     if($(this).prev().hasClass('active')){
        //         $(this).removeClass('active');
        //         $(this).next().stop(true,true).removeClass('active');
        //         $(this).prev().removeClass('active');
        //         $(this).siblings().removeClass('active');
        //     }else{
        //         $(this).next().stop(true,true).slideDown();
        //         $(this).addClass('active');
        //         $('.nav-menu li > a').not($(this)).next().stop(true,true).removeClass('active');
        //         $(this).prev().addClass('active');
        //         $('.nav-menu li > a').not($(this)).removeClass('active');
        //         $(this).siblings().addClass('active');
        //     }
        // });
        
        $("#site-navigation ul li.menu-item").click(function(){


            var a  = $(this).find('.open-menu-link');
            if($(a).prev().hasClass('active')){
                $(a).removeClass('active');
                $(a).next().stop(true,true).removeClass('active');
                $(a).prev().removeClass('active');
                $(a).siblings().removeClass('active');
            }else{
                $(a).next().stop(true,true).slideDown();
                $(a).addClass('active');
                $('.nav-menu li > a').not($(this)).next().stop(true,true).removeClass('active');
                $(a).prev().addClass('active');
                $('.nav-menu li > a').not($(this)).removeClass('active');
                $(a).siblings().addClass('active');
            }

        })


        $('.accordion').click(function() {
            $(this).next().stop().slideUp('fast');
            $(this).parent().addClass('active');
            $('.overlay').stop().addClass('active');
            $(this).next().stop().slideDown('fast');
        });

        $('.overlay').click(function() {
            $(this).removeClass('active');
            $('.accordion').parent().removeClass('active');
            $('.accordion').next().stop().hide();
        });
        
        
         $('.banner-slider').slick({
             
              autoplay: true,
             dots: true,
              infinite: true,
              speed: 300,
              fade: true,
              cssEase: 'linear',
          
         });
        

        $('.three-item').slick({
          autoplay: true,
          infinite: true,
          speed: 300,
          cssEase:  'linear',
          slidesToShow: 3,
          slidesToScroll: 3,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });

        $('.single-item').slick({
            autoplay: true,
            dots: true,
            prevArrow: false,
            nextArrow: false
        });
        
       
        
        if( $(window).width() > 767 ) {

            // Sticky Header
            $(window).on( "scroll", function () {
                var fromTop = $(this).scrollTop();
                $("#page").toggleClass("sticky-header", (fromTop > 0));                
            });

        } else {

            $("#secondary .aside-title").on("click", function() {

                $("#secondary").toggleClass("active");

                if( !$("#secondary").hasClass("active") ) {
                    $("#secondary .widget").removeClass("active");                    
                }


            });


            $("#secondary .widget h3").each(function() {

                $(this).on("click", function() {
                   $(this).parent().parent().toggleClass('active');
                });

            });

        }



        alert("")













    });


   
})(jQuery, this);

