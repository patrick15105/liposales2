<?php
/**
 *
 * This template part is for Add to cart modal window
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Woocommerce_Centaur_Addon
 * @subpackage Woocommerce_Centaur_Addon/template
 */

defined( 'ABSPATH' ) || exit;

?>

<div class="centaur-full centaur-fixed centaur-nodisplay centaur-modal centaur-flex">

	<div class="centaur-container">
		<div class="centaur-header">
			<h3 class="centaur-title"><i class="fa fa-check"></i>This item has been added to your cart:</h3>
			<span id="centaur-modal-close"><i class="fa fa-times"></i></span>
		</div>

		<div class="centaur-content"></div>

		<div class="centaur-actions centaur-flex">
			<a href="<?php echo wc_get_cart_url(); ?>" class="buttons gradient-test text-white"><?php _e('View Cart','woocommerce-add-to-cart-popup'); ?></a>
			<a href="<?php echo wc_get_checkout_url(); ?>" class="buttons gradient-test text-white"><?php _e('Checkout','woocommerce-add-to-cart-popup'); ?></a>
			<a class="buttons gradient-test text-white" id="centaur-modal-close"><?php _e('Continue Shopping','woocommerce-add-to-cart-popup'); ?></a>
		</div>

	</div>
	
</div>