<?php

/**
 * Form Template for public viewing
 *
 * @link       www.centuarmarketing.co
 * @since      1.0.0
 *
 * @package    Centaur_Form
 * @subpackage Centaur_Form/templates/admin
 */

global $centaur_form;

?>

<div class="centaur-wrap" id="cform-handler" data-id="<?php echo $centaur_form->id; ?>">

	<?php echo $centaur_form->title; ?>

	<div class="centaur-section">
		<form action="#" method="POST" id="cform-entry-form" class="centaur-content centaur-field-columns-<?php echo $centaur_form->columns; ?>" enctype="multipart/form-data">
			<?php echo $centaur_form->field_string; ?>
			<div class="centaur-actions centaur-fields-group button-submit">
				<button type="submit" class="buttons gradient-test text-white" id="submit">Send Question/Concern </button>
			</div>
		</form>
	</div>
	<div class="centaur-modal">
		<div class="centaur-wrap">
			<img src="<?php echo $centaur_form->preloader; ?>" class="centaur-preloader" alt="Centaur Form Preloader" />
		</div>
	</div>
</div>