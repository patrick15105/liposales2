<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package storefront
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<main id="main" class="site-main" role="main">

			<div class="error-404 not-found">

				<div class="page-content">

					<div class="column-2">

						<h1 class="gradient gradient-text">404</h1>
						
					</div>

					<div class="column-2 last">

						<h2 class="gradient gradient-text">Page Not Found</h2>

						<p class="not-found-message">Sorry The page that you are looking for may moved, changed or deleted.</p>

						<div class="footer">	
							<a href="" class="btn">Go to Home</a>
							<a href="" class="btn btn-outline">Previous Page</a>
						</div>

					</div>
					<div class="clear-fix"></div>

				</div><!-- .page-content -->
			</div><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer();
