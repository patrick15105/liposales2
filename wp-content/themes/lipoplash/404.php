<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>
		    <div class="overlay"></div>
		    <div class="logo">
			    <a href="<?php echo home_url(); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/img/liposales-logo.png" alt="Logo" class="logo-img">
				</a>
			</div>

			<!-- article -->
			<article id="post-404">
				
				<div class="error-404 not-found">
    				<div class="page-content">
    					<div class="column-2">
    						<h1 class="gradient gradient-text">404</h1>
    					</div>
    
    					<div class="column-2 last">
    						<h2 class="gradient gradient-text">Page Not Found</h2>
    						<p class="not-found-message">Sorry The page that you are looking for may moved, changed or deleted.</p>
    
    						<div class="footer">	
    							<a href="<?php echo home_url(); ?>" class="btn">Go to Home</a>
    							<a href="<?php echo home_url(); ?>" class="btn btn-outline">Previous Page</a>
    						</div>
    
    					</div>
    				<div class="clear-fix"></div>
    			  </div><!-- .page-content -->
			   </div><!-- .error-404 -->

			<ul class="social-icons">
				<li><a href="https://www.facebook.com/liposales" class="facebook" target="_blank" rel="nofollow noopener"><span>Facebook</span></a></li>
				<li><a href="https://twitter.com/Liposales_com" class="twitter" target="_blank" rel="nofollow noopener"><span>Twitter</span></a></li>
				<li><a href="https://www.instagram.com/liposales/" class="instagram" target="_blank" rel="nofollow noopener"><span>Instagram</span></a></li>
				<li><a href="https://www.youtube.com/channel/UCGDottbQEVG8JuCUAgcEUww" class="youtube" target="_blank" rel="nofollow noopener"><span>Youtube</span></a></li>
				<li><a href="https://www.linkedin.com/company/liposales" class="linkedin" target="_blank" rel="nofollow noopener"><span>LinkedIn</span></a></li>
				<li><a href="https://vimeo.com/liposales" class="vimeo" target="_blank" rel="nofollow noopener"><span>Vimeo</span></a></li>
				<li><a href="https://plus.google.com/u/1/b/103205953918927992422/103205953918927992422" class="googleplus" target="_blank" rel="nofollow noopener"><span>GooglePlus</span></a></li>
			</ul>
			
			<ul class="link-info">
				<li>&copy; 2018 . All rights reserved. </li>
				<li>Disclaimer</li>
				<li>Sitemap</li>
				<li>Privacy Policy</li>
			</ul>

			<p class="powered-by">Powered by <strong>Centaur Marketing</strong></p>
			<!-- .social-icons -->

		</div>

	</div>

<?php get_footer(); ?>
