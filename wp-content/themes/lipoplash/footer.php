
		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<script type='text/javascript'>
			jQuery('#countdown').countdown('2018/10/15', function(event) {
			  jQuery(this).html(event.strftime(''
			  	+ '<div><span class="gradient gradient-text time">%D</span> <span>Days</span></div>'
			  	+ '<div><span class="gradient gradient-text time">%H</span> <span>Hours</span></div>'
			  	+ '<div><span class="gradient gradient-text time">%M</span> <span>Minutes</span></div>'
			  	+ '<div><span class="gradient gradient-text time">%S</span> <span>Seconds</span></div>'
			  	));
			});

			$wh = jQuery(window).height();
			$ww = jQuery(window).width();

			console.log($wh);
			console.log($ww);

			if( $wh <= 670 ) {
				jQuery('.splash-contain').css('height', 'auto');
			}

			jQuery(window).resize(function(){
				$wh = jQuery(window).height();
				$ww = jQuery(window).width();

				console.log($wh);

				if( $wh <= 670 ) {
					jQuery('.splash-contain').css('height', 'auto');
				} else {
					jQuery('.splash-contain').css('height', '100vh');
				}

			});

		</script>

	</body>
</html>
