<?php get_header(); ?>

	<div class="splash-contain">
		
		<div>

			<div>
				<div class="left">
					<!-- logo -->
					<div class="logo">
						<a href="<?php echo home_url(); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/liposales-logo.png" alt="Logo" class="logo-img">
						</a>
					</div>
					<!-- /logo -->
					<p class="coming-soon">our new online store is COMING SOON!</p>
					<div id="countdown"></div>
					<p class="has-arrow">For more information or to place an order, please contact us</p>
					<a href="tel:713-517-2454"><p class="contact-info">1-866-336-1333</p></a>
					<a href="mailto:info@liposales.com"><p class="contact-info">info@liposales.com</p></a>
				</div>
				<!-- /left -->
				
				<div class="right">
					<p class="gradient gradient-text">Contact us</p>
					<?php echo do_shortcode('[mc_contact_top form=ContactForm]'); ?>
					<!-- <form action="">
						<div class="form-group">
						    <input type="text" name="name" id="name" placeholder="Name" aria-label="Name" aria-hidden="true" />
						</div>
						<div class="form-group">
						    <input type="email" name="email" id="email" placeholder="Email" aria-label="Email" aria-hidden="true" />
						</div>
						<div class="form-group">
						    <input type="tel" name="telephone" id="telephone" placeholder="Telephone" aria-label="Telephone" aria-hidden="true" />
						</div>
						<div class="form-group">
						    <textarea id="message" name="message" placeholder="Message" cols="30" rows="5" aria-label="Message" aria-hidden="true"></textarea>
						</div>
						<div class="form-group form-half float-submit">
						    <input type="submit" value="Send Message" class="btn btn-submit">
						</div>
					</form> -->
				</div>
				<!-- /right -->
			</div>
			
			<ul class="social-icons">
				<li><a href="https://www.facebook.com/liposales" class="facebook" target="_blank" rel="nofollow noopener"><span>Facebook</span></a></li>
				<li><a href="https://twitter.com/Liposales_com" class="twitter" target="_blank" rel="nofollow noopener"><span>Twitter</span></a></li>
				<li><a href="https://www.instagram.com/liposales/" class="instagram" target="_blank" rel="nofollow noopener"><span>Instagram</span></a></li>
				<li><a href="https://www.youtube.com/channel/UCGDottbQEVG8JuCUAgcEUww" class="youtube" target="_blank" rel="nofollow noopener"><span>Youtube</span></a></li>
				<li><a href="https://www.linkedin.com/company/liposales" class="linkedin" target="_blank" rel="nofollow noopener"><span>LinkedIn</span></a></li>
				<li><a href="https://vimeo.com/liposales" class="vimeo" target="_blank" rel="nofollow noopener"><span>Vimeo</span></a></li>
				<li><a href="https://plus.google.com/u/1/b/103205953918927992422/103205953918927992422" class="googleplus" target="_blank" rel="nofollow noopener"><span>GooglePlus</span></a></li>
			</ul>
			<!-- .social-icons -->

		</div>

	</div>

<?php get_footer(); ?>