<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'liposales' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'j~ERxt?=|Ac6==Y]2: uYs<^_0^zni/p;(sHQ9INFt|YJ)c-]Vov]IHAFfE=$A(P' );
define( 'SECURE_AUTH_KEY',  ':Z[]812EpJm86b1g)$L(Y:|{J+/;P}x~&6j&KA<# c_94l4bV.5o*x#t<kA5dMB:' );
define( 'LOGGED_IN_KEY',    '<op#:tZ||uZyA8A7OeWMg.bqIv;=s%LdNT*QX;fv@@xBrM.ej{vtr0]:Xo&._=^@' );
define( 'NONCE_KEY',        '~PiWv.rAj ,Kj$U_.% W+%EL0{X0CQ*rMvFYZD_{7R(44JsjLWmW Q%8RTKO)S3$' );
define( 'AUTH_SALT',        'Z!d#iD+4GTCqN^tJ!6(Yl?|sfx#2 k*2(6V}+%19mY+GRPB{i74Ij$2mHy@OJ15D' );
define( 'SECURE_AUTH_SALT', '>$[%W@#gb=mMau&}=OF/KW{|=o|0|:@{*}7{L/e_P,QZP.[<}Jv-C!G}c3e2hlf=' );
define( 'LOGGED_IN_SALT',   'cgXtcc<Nc|h0^ tp/g*a*`b1s)j2M*8fRar>5gu;46wj7}@koFdB+ry/m|k4S+W-' );
define( 'NONCE_SALT',       'a7D=+ V9e^!9oE.U]Y-rcKRexNgHCdSgQE`8nW|V;T9f)eLXj$2|GD/S+_C>*u0=' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
